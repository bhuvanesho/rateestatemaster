import React, { Component } from 'react';
import {
    AppRegistry, StyleSheet, TextInput, View, Text, TouchableOpacity, Image, KeyboardAvoidingView, Animated,
    Linking, Keyboard, TouchableWithoutFeedback
} from 'react-native';
import { Container, Content } from 'native-base';
import { MKCheckbox } from 'react-native-material-kit';
import styles, { IMAGE_HEIGHT, IMAGE_HEIGHT_SMALL } from '../login/styles';
import { database } from '../../firebaseConfig/config';
import { AsyncStorage } from 'react-native';

import * as firebase from 'firebase';
var criteriaJSON = require('../../assets/criteria.json');
import { criteriaDto } from '../../firebaseConfig/dto';
//import { database } from '../../firebaseConfig/config';
const launchscreenLogo1 = require("../../../assets/loginScreenLogo.png");
import { connect } from 'react-redux';
import { register } from '../../actions/registerActions';
var SQLite = require('react-native-sqlite-storage')
var db = SQLite.openDatabase({ name: "HomeRatingDB.db" });
import dbMethod from '../../Db/database';
class Somepage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            username: "",
            password: "",
            confirmPassword: ""
        }
    }


    handleRegister = () => {
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        const { username, password, confirmPassword } = this.state;
        if (!username || !password || !confirmPassword) {
            alert("Fields Can't Be Blank");
            return;
        }

        if (!username.match(mailformat)) {
            alert("You have entered an invalid email address!");
            return false;
        }

        if (password !== confirmPassword) {
            alert("Password must match with Confirm Password");
            return;
        } else {
            var success = '';
            var success = this.props.register({ username, password });
            if (success != false) {
                this.props.navigation.navigate("Agreement")
            }

        }
    }

  


    render() {
        return (

            <TouchableWithoutFeedback onPress={() => { Keyboard.dismiss() }}>
                <Container style={styles.container}>
                    {/* <KeyboardAvoidingView behavior="padding" style={{ flex: 1 }}> */}

                    <View style={styles.MainContainer}>
                        <View style={{ alignItems: 'center' }}>
                            <Image source={launchscreenLogo1} style={styles.loginScreenLogo} />
                        </View>
                        <Text>{"\n"}</Text>
                        <View >
                            <Text style={styles.title}>USERNAME (E-mail)</Text>
                            <TextInput required={true}
                                autoCapitalize="none"
                                onChangeText={(text) => this.setState({ username: text })}
                                style={styles.TextInputStyleClass} />
                        </View>
                        <Text></Text>
                        <View >
                            <Text style={styles.title}>PASSWORD</Text>
                            <TextInput secureTextEntry={true}
                                autoCapitalize="none"
                                onChangeText={(password) => this.setState({ password: password })}
                                style={styles.TextInputStyleClass} />
                        </View>

                        <Text></Text>
                        <View >
                            <Text style={styles.title}>CONFIRM PASSWORD</Text>
                            <TextInput secureTextEntry={true}
                                autoCapitalize="none"
                                onChangeText={(confirmPassword) => this.setState({ confirmPassword: confirmPassword })}
                                style={styles.TextInputStyleClass} />
                        </View>

                        <Text>{'\n'}</Text>

                        <View style={{ alignItems: 'flex-end' }}>
                            <TouchableOpacity
                                style={styles.SubmitButtonStyle}

                                onPress={this.handleRegister}
                            >
                                <Text style={styles.TextStyle}> REGISTER </Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    {/* </KeyboardAvoidingView> */}
                </Container>
            </TouchableWithoutFeedback>

        );
    }
}


const mapStateToProps = state => {
    return {
        error: state.register.error,
        loading: state.register.loading,
        register: state.register.register,
    };
}
export default connect(mapStateToProps, { register })(Somepage);