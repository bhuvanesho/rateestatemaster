const resources = {
	en:{
		"Success": "Success",
		"Failure": "Fail",
		"LoginSuccess": "LoggedIn Successfully",
		"LoginFail": "Invalid Username or Password     ",
		"RegisterSuccess": "Registered Successfully",
		"RegisterFail": "Username has been  already Registered",
		"ProfileSuccess": "Profile has been updated Sucessfully",
		"ProfileFail": "username has been already exists.",
		"Invalid Input": "Invalid Input"
	}
}

export default resources;
