export var userDto={
    "accountId":"",
    "userId":"",
    "username":"",
    "password":"",
    "EmailId":"",
    "phoneNumber":"",
    "createdDate":"",
    "updatedDate":"",
    "LoggedIn":"",
    "loginCount":"",
    "loggedInflag":"",
    "status":"",
    "userType":""
};
export var accountDto={
    "accountId":"",
    "accountName":"",
    "password":"",
    "activeStatus":"",
    "EmailId":"",
    "phoneNumber":"",
    "createdDate":Date.now(),
    "createdBy":"",
    "modifiedDate":Date.now(),
    "modifiedBy":""
};
export var profileDto={
    "id":"",
    "userId":"",
    "FirstName":"",
    "LastName":"",
    "DOB":"",
    "Gender":"",
    "MaritalStatus":"",
    "RangeOfHouseholdIncome":"",
    "NumberOfPeopleinHome":"",
    "AgeRangeOfPeopleFrom":"",
    "AgeRangeOfPeopleTo":"",
    "TypeOfHomeDesired":"",
    "DoYouOwnVehicle":"",
    "SharedUser":"",
    "BudgetedPrice":""   
};
export var propertyDto={
    "Id":"",
    "userId":"",
    "PropertyName":"",
    "Address":"",
    "Description":"",
    "latitude":"",
    "longitude":"",
    "Amount":"",
    "Status":"",
    "ListingStatus":"",
    "Category":"",
    "Beds":"",
    "TotalBath":"",
    "Area":"",
    "BuildYear":"",
    "NoOfStorey":"",
    "Education":"",
    "Arts":"",
    "Accomodation":"",
    "createdDate":"",
    "RatingStatus":"",
    "OverallRating":"",
    "SortedRating":"",
    "Comments":"",
    "FavouriteProperty":"",
    "updatedDate":""
};
export var criteriaDto={
    "Id":"",
    "CriteriaName":"",
    "CriteriaType":"",
    "Weightage":"",
    "Percentage":"",
    "filterPercentage":"",
    "UserId":"",
    "createdDate":"",
    "updatedDate":"",
    "SortedStatus":"",
    "Status":""
};
export var sharedProfile={
    "Id":"",
    "ProfileId":"",
    "FirstName":"",
    "LastName":"",
    "createdDate":"",
    "updatedDate":"",
    "emailId":"",
    "UserId":""
};
export var propertyPhotoDto={
    "Id":"",
    "Header":"",
    "PropertyImage":"",
    "PropertyName":"",
    "createdDate":"",
    "updatedDate":"",
    "UserId":""
};
export var userPropertyCriteria={
    "Id":"",
    "CriteriaId":"",
    "UserId":"",
    "Rating":"",
    "createdDate":"",
    "updatedDate":""
};
export var ratings={
    "Id":"",
    "UserId":"",
    "CriteriaId":"",
    "PropertyId":"",
    "Rating":"",
    "createdDate":"",
    "updatedDate":""
};
export var queryTable={
    "Id":"",
    "UserId":"33333",
    "username":"",
    "filterNsortQuery":"",
    "Rated":"",
    "NotRated":"",
    "PriceFrom":"",
    "PriceTo":"",
    "LowToHighRating":"",
    "HighToLowRating":"",
    "LowToHighPrice":"",
    "HighToLowPrice":"",
    "CheckAll":"",
    "ParticularCriteria":"",
    "SortedCriteria":""
};

export var FilterAndSortDto = {
    "UserId" : "",
    "username": "",
    "filterNsortQuery" : "",
    "Rated" : "",
    "PriceFrom" : "",
    "PriceTo" : "",
    "LowToHighRating" : "",
    "HighToLowRating" : "",
    "LowToHighPrice" : "",
    "HighToLowPrice" : "",
    "CheckAll" : "",
    "ParticularCriteria" : "",
    

}

