import React, { Component } from 'react';
import { ScrollView, RefreshControl,Text, View, TextInput, Button, Image, TouchableOpacity, DatePickerIOS, Picker, TouchableWithoutFeedback, KeyboardAvoidingView, Keyboard } from 'react-native';
import Orientation from "react-native-orientation";
import { Container, Header, Left, Body, Icon, Right, Item, Input, Content, Footer, FooterTab, Label } from 'native-base';
import styles from './styles';
import { MKTextField } from 'react-native-material-kit';
import FloatingLabel from 'react-native-floating-labels';
import { AsyncStorage } from 'react-native';
import { personalView } from '../../actions/actionPersonalView';
import { connect } from 'react-redux';
import { database } from '../../firebaseConfig/config';
import * as firebase from "firebase";
import { Alert } from 'react-native';
import helper from '../helper'
import Login from '../../model/Login';
const launchscreenLogo1 = require("../../../assets/loginScreenLogo.png");


class Home extends Component {
    constructor(props) {
        super(props)
        this.getPrimaryUser();
        this.getInfluencers();
        this.state = {
            infulancerdata: [],
            primaryUserData:[],
            userId: '',
            firstNameBorderColor: '',
            firstNameLabelFont: 21,
            firstNameLabelFontColor: '#ededed',
            floatinglabelStyle: { fontFamily: 'AspiraXXXNar-Regular', paddingLeft: 0 },
            active: false,
            personalViewActiveCss: '',
            permanetUserId: '',
            userId: '',
            primaryUserId:'',
            refreshing: false,
        }

    }
    async componentDidMount() {
        try {

        } catch (error) {

        }

    }

    getPrimaryUser=async()=>{
        var userId = await AsyncStorage.getItem('permanetUserId');
        var primaryUser = [];
        var ref = database.ref('User/')
        ref.orderByChild('userId')
            .startAt(userId)
            .endAt(userId)
            .once('value', function (snapshot) {
                snapshot.forEach(function (childSnap) {
                    this.setState({primaryUserId:childSnap.val().accountId})
                    var ref1 = database.ref('profile/')
                    ref1.orderByChild('userId')
                        .startAt(childSnap.val().accountId)
                        .endAt(childSnap.val().accountId)
                        .on('value', function (snapshot1) {
                            snapshot1.forEach(function (childSnap1) {
                                    primaryUser.push({
                                        "primaryUserName": childSnap1.val().firstName + " " + childSnap1.val().lastName,
                                        "userId": childSnap1.val().userId,
                                    })
                            }.bind(this));
                            this.setState({ primaryUserData: primaryUser })
                        }.bind(this));
                }.bind(this));
            }.bind(this));
    }

    getInfluencers = async () => {
        var userId = await AsyncStorage.getItem('permanetUserId');
        this.setState({ permanetUserId: userId })
        //set the current user id to state
        const currentUserId = await AsyncStorage.getItem('userId');
        this.setState({ userId: currentUserId })

        var influencers = [];
        var ref = database.ref('User/')
        ref.orderByChild('accountId')
            .startAt(userId)
            .endAt(userId)
            .once('value', function (snapshot) {
                snapshot.forEach(function (childSnap) {
                    var ref1 = database.ref('profile/')
                    ref1.orderByChild('userId')
                        .startAt(childSnap.val().userId)
                        .endAt(childSnap.val().userId)
                        .on('value', function (snapshot1) {
                            snapshot1.forEach(function (childSnap1) {
                                if (childSnap.val().accountId != childSnap1.val().userId) {
                                    influencers.push({
                                        "influencerName": childSnap1.val().firstName + " " + childSnap1.val().lastName,
                                        "userId": childSnap1.val().userId,
                                        "accountId": childSnap.val().accountId
                                    })
                                }
                            }.bind(this));
                            this.setState({ infulancerdata: influencers })
                        }.bind(this));
                    //this.setState({infulancerdata : influencers })
                }.bind(this));
                //this.setState({infulancerdata : influencers })
            }.bind(this));
        //this.setState({infulancerdata : influencers })

    }

    setId = async (id, name) => {
        try {
            await AsyncStorage.setItem('userId', id);
            await AsyncStorage.setItem('influencerNameValue', name);

            this.setState({ userId: id })
            this.setState({ active: true })
            if (this.state.permanetUserId !== id) {
                Alert.alert('', 'You are entering read-only mode for the selected shared account. To exit, return to the Home tab and select Personal View.');
                setTimeout(() => {
                    this.props.navigation.navigate('WelcomeSkip');
                    
                }, 100);
            } else {
                //Alert.alert('','Switching back to the Normal mode. ');
                //page to be navigated to the showings screen here
                //this.setNavigation(id,this.state.permanetUserId);
                setTimeout(() => {
                    this.props.navigation.navigate('WelcomeSkip');
                    
                }, 100);
            }



        } catch (error) {
            console.log(error)
        }

    }

    setNavigation = async (userId, permanetUserId) => {




        //userId = await this.getParameterValue('user')
        //console.log(userId + '  value pulled from db')
        //permanetUserId = await this.getParameterValue('permanent');

        if (userId === permanetUserId) {
            this.setState({ navigation: 'ProfileUpdate' })
            //set two major flags which control the read only for floating label and dropdowns in all screens
            await AsyncStorage.setItem('editableValue', JSON.stringify(true));
            await AsyncStorage.setItem('dropdownDisabledValue', JSON.stringify(false));

            //setParameters(JSON.stringify(true),'editable');
            //setParameters(JSON.stringify(false),'dropdown');



        }
        else {
            this.setState({ navigation: 'Profile' })
            //set two major flags which control the read only for floating label and dropdowns in all screens
            await AsyncStorage.setItem('editableValue', JSON.stringify(false));
            await AsyncStorage.setItem('dropdownDisabledValue', JSON.stringify(true));

            //setParameters(JSON.stringify(false),'editable');
            //setParameters(JSON.stringify(true),'dropdown');
        }


    }



    getPersonalViewStyle = (touch, id) => {

        if (touch && this.state.permanetUserId === id) {

            return {
                backgroundColor: '#1DA4DF',
                color: '#fff',
                fontFamily: 'AspiraXXXNar-Regular',
                justifyContent: 'center',
                marginTop: 40,
                paddingTop: 15,
                borderRadius: 10,
                borderWidth: 1,
                borderColor: '#fff',
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                fontSize: 21,
                zIndex: 1,

            }

        } if (touch && this.state.permanetUserId !== id) {
            return {
                backgroundColor: '#ededed',
                color: "#727171",
                fontFamily: 'AspiraXXXNar-Regular',
                justifyContent: 'center',
                marginTop: 40,
                paddingTop: 15,
                borderRadius: 10,
                borderWidth: 1,
                borderColor: '#fff',
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                fontSize: 21,
                zIndex: 1,
            }
        }

        if (this.state.permanetUserId === id) {
            return {
                backgroundColor: '#1DA4DF',
                color: '#fff',
                fontFamily: 'AspiraXXXNar-Regular',
                justifyContent: 'center',
                marginTop: 40,
                paddingTop: 15,
                borderRadius: 20,
                borderWidth: 1,
                borderColor: '#1DA4DF',
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                fontSize: 21,
                zIndex: 1,

            }
        }

        if (this.state.permanetUserId !== id) {
            return {
                backgroundColor: '#ededed',
                color: "#727171",
                fontFamily: 'AspiraXXXNar-Regular',
                justifyContent: 'center',
                marginTop: 40,
                paddingTop: 15,
                borderRadius: 10,
                borderWidth: 1,
                borderColor: '#fff',
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                fontSize: 21,
                zIndex: 1,
            }
        }

    }
    getActiveStyle = (touched, id) => {

        if (touched && this.state.userId === id) {
            return {
                backgroundColor: "#1DA4DF",
                color: '#fff',

                fontFamily: 'AspiraXXXNar-Regular',
                fontSize: 21,
                margin: 0,
                marginTop: 0,
                // borderRadius: 10,
                paddingTop: 15,
                paddingLeft: 5,
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                zIndex: 1,
            }
        }
        if (touched && this.state.userId !== id) {
            return {
                backgroundColor: "#ededed",
                color: "#727171",

                fontFamily: 'AspiraXXXNar-Regular',
                fontSize: 21,
                margin: 0,
                marginTop: 0,
                // borderRadius: 10,
                paddingTop: 15,
                paddingLeft: 5,
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                zIndex: 1,

            }
        }
        if (this.state.userId === id) {
            return {
                backgroundColor: "#1DA4DF",
                color: '#fff',

                fontFamily: 'AspiraXXXNar-Regular',
                fontSize: 21,
                margin: 0,
                marginTop: 0,
                // borderRadius: 12,
                paddingTop: 15,
                paddingLeft: 5,
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                zIndex: 1,

            }
        }
        if (this.state.userId !== id) {
            return {
                backgroundColor: "#ededed",
                color: "#727171",
                fontFamily: 'AspiraXXXNar-Regular',
                fontSize: 21,
                margin: 0,
                marginTop: 0,
                //  borderRadius: 12,
                paddingTop: 15,
                paddingLeft: 5,
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                zIndex: 1,

            }
        }

    }

    logout = async () => {
        //await AsyncStorage.setItem('userId', '');
        //await AsyncStorage.setItem('influencerNameValue', '');
       // await AsyncStorage.setItem('agentId', '');
        AsyncStorage.clear();
        setTimeout(() => {
            this.props.navigation.navigate('Login');
        }, 1000);

    }
    _onRefresh = () => {
        this.setState({refreshing: true});
        this.getInfluencers().then(() => {
          this.setState({refreshing: false});
        });
      }
    render() {

        
        return (
            <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            } >
            <Container style={styles.container}>
                <Header style={styles.headerBg}>
                    <Left>
                    </Left>
                    <Body>
                    </Body>
                    <Right></Right>
                </Header>
                <View style={styles.propertyDetailsBG} >
                    <View style={styles.propertyFlex}>
                        <Left></Left>
                        <Text style={styles.propertyDetails}>HOME</Text>
                        <Right >
                            <TouchableOpacity onPress={() => this.logout()}>
                                <Image style={styles.filterIcon} source={require('../../../assets/logoutIcon.png')} />
                            </TouchableOpacity >
                        </Right>
                    </View>
                </View>
                <View style={styles.MainContainer}>

                    <View style={{ alignItems: 'center' }}>
                        <Image source={launchscreenLogo1} style={styles.loginScreenLogo2} />
                    </View>

                   
                        {(this.state.primaryUserId!=this.state.permanetUserId) && this.state.primaryUserData && this.state.primaryUserData.map((item, id) => {
                            return (
                            <View style={{ alignItems: 'center', marginTop:20, marginBottom:-30}} >
                                     <Text style={styles.emailTextFieldBlue}>View Your Primary User Account</Text>
                                    <TouchableOpacity onPress={() => this.setId(item.userId, item.primaryUserName)}>
                                        <Text style={this.getActiveStyle(this.state.active, item.userId)}>{item.primaryUserName} </Text>
                                        <Image source={(this.state.userId === item.userId) ? require('../../../assets/shareGrey.png') : require('../../../assets/share.png')} style={styles.shareImage2}></Image>
                                    </TouchableOpacity>
                                    <Text>{"\n"}</Text>
                                </View>
                            )
                        })} 
                

                    <View style={{ alignItems: 'center' }} >
                        <TouchableOpacity onPress={() => this.setId(this.state.permanetUserId, '')} >
                            <Text style={this.getPersonalViewStyle(this.state.active, this.state.userId)} >Personal View</Text>
                        </TouchableOpacity>
                    </View>

                   

                    <View style={styles.textInputStyle1Content}>
                        <Text style={styles.emailTextFieldBlue}>{this.state.infulancerdata.length>0 ? 'View a shared account':''}</Text>
                        {this.state.infulancerdata && this.state.infulancerdata.map((item, id) => {
                            return (
                                <View style={{ alignItems: 'center' }} >
                                    
                                    <TouchableOpacity onPress={() => this.setId(item.userId, item.influencerName)}>
                                        <Text style={this.getActiveStyle(this.state.active, item.userId)}>{item.influencerName} </Text>
                                        <Image source={(this.state.userId === item.userId) ? require('../../../assets/shareGrey.png') : require('../../../assets/share.png')} style={styles.shareImage2}></Image>
                                    </TouchableOpacity>
                                    <Text>{"\n"}</Text>
                                </View>
                            )
                        })}
                    </View>


                </View>
            </Container>
            </ScrollView>
        )
    }
}
const mapStateToProps = state => {
    return {
        state: state

    };
}

export default connect(mapStateToProps, { personalView })(Home);
