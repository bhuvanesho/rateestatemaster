import React, { Component } from 'react';
import { Text, View, TextInput,  Button,  Image, TouchableOpacity, DatePickerIOS, Picker, TouchableWithoutFeedback, KeyboardAvoidingView, Keyboard, ScrollView } from 'react-native';
import Orientation from "react-native-orientation";
import { Container, Header,  Left, Body, Icon, Right, Item, Input, Content, Footer, FooterTab, Label } from 'native-base';
import styles from './styles';
import { MKTextField } from 'react-native-material-kit';
import FloatingLabel from 'react-native-floating-labels';
import {AsyncStorage} from 'react-native';
import { personalView } from '../../actions/actionPersonalView';
import { connect } from 'react-redux';
import {database } from '../../firebaseConfig/config';
import * as firebase from "firebase";
import { setUserIds } from '../../src/model/Login';

const launchscreenLogo1 = require("../../../assets/loginScreenLogo.png");


class Home extends Component{
    constructor(props){
        super(props)
        this.state={
            infulancerdata:[],
            userId:'',
            firstNameBorderColor: '#dcdcdc',
            firstNameLabelFont: 21,
        }
    }
    async componentDidMount(){
        try {
            const userId = await AsyncStorage.getItem('userId');
           // console.log("userId", userId)
            this.getInfluencers(userId)
        }catch(error){

        }
       
    }
  getInfluencers = (userId) => {
     // console.log("getInfluncers", userId)
        var influencers=[]; 
        var accounts =[];
        var users = firebase.database().ref("User/");
        var children = firebase.database().ref("profile");
        //console.log("children",children);
        users.orderByKey().on("value", function (snapshot) {    
          var user = snapshot.val();
          for(var k in user) {
            var account = user[k];
            if(account.accountId === userId && account.accountId!==account.userId){
              accounts.push(account);
              firebase.database().ref("profile/"+account.userId)
                        .orderByKey().on("value", function(snapshot) {
                          var profile = snapshot.val();
                         // console.log("profile",profile);
                         
                            influencers.push( {
                                "influencerName": profile.firstName + " " + profile.lastName,
                                "userId":profile.userId
                            })
                        
                  this.setState({infulancerdata : influencers })
              }.bind(this))
            }
          }
         
      }.bind(this));
      //reset the variable to avoid repetition of data
      influencers=[];
    } 


    setId = async (id) =>{
      //  console.log(id)
        try{
            await AsyncStorage.setItem('userId', id);
            this.setUserIds(id,'user')
        }catch (error) {
           // console.log(error)
        }

    }

    render(){
        let arrayData = this.state.infulancerdata

        //console.log(arrayData)
        return(
            <Container style={styles.container}>
           <View style={styles.MainContainer}>
                        <View style={{ alignItems: 'center' }}>
                            
                            <Image source={launchscreenLogo1} style={styles.loginScreenLogo2} />

                        </View>
                        <View style={{ alignItems: 'center' }} >
                            <TouchableOpacity
                                style={styles.personalButtonStyle2}>
                                <Text style={styles.TextStyle}>Personal View</Text>
                            </TouchableOpacity>
                        </View>
                        
                        <View style={styles.textInputStyle1Content}>
                            <Text style={styles.emailTextFieldBlue}>View a shared account</Text>
                        </View>

                        {this.state.infulancerdata && this.state.infulancerdata.map((item, id) => {
                            return (
                                <View>
                                    <View style={{ alignItems: 'center' }} >
                                        <TouchableOpacity
                                            style={styles.buttonInputStyleClass}>
                                            <Text style={styles.TextStyle2}
                                                onPress={() => this.setId(item.userId)}>{item.influencerName}
                                            </Text>  
                                            <Image source={require('../../../assets/share.png')} style={styles.shareImage2}></Image>
                                        </TouchableOpacity>
                                    </View>
                                    <Text>{"\n"}</Text>
                                </View>
                            )
                        })}
                    </View>
        </Container>
        )
    }
}
const mapStateToProps = state => {
    return {
        state: state
        
    };
}

export default connect(mapStateToProps, { personalView  })(Home);
 
