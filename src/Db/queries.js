//--------create scripts for creating tables----------

var user = "CREATE TABLE IF NOT EXISTS User(Id INTEGER PRIMARY KEY AUTOINCREMENT , username TEXT NOT NULL UNIQUE, password TEXT NOT NULL UNIQUE, EmailId TEXT,phoneNumber INT, createdDate DATE ,updatedDate DATE,LoggedIn TEXT,loginCount INT, loggedInflag INT,status TEXT)";

var codeType = 'CREATE TABLE IF NOT EXISTS CodeType (Id INTEGER PRIMARY KEY AUTOINCREMENT , Code TEXT NOT NULL UNIQUE, createdDate   NUMERIC NOT NULL ,updatedDate   NUMERIC)';

var codeValue = 'CREATE TABLE IF NOT EXISTS CodeValue (Id  INTEGER PRIMARY KEY AUTOINCREMENT , Name TEXT NOT NULL UNIQUE,CodeTypeId BIGINT NOT NULL , createdDate   NUMERIC NOT NULL ,updatedDate   NUMERIC , FOREIGN KEY(CodeTypeId) REFERENCES CodeType(Id))';

//var profile = 'CREATE TABLE IF NOT EXISTS Profile (Id INTEGER PRIMARY KEY AUTOINCREMENT , FirstName TEXT NOT NULL UNIQUE, LastName TEXT NOT NULL UNIQUE, BirthYear   INT NOT NULL,BirthMonth INT NOT NULL ,Gender INT NOT NULL ,MaritalStatus INT NOT NULL ,RangeOfHouseholdIncome INT NOT NULL ,NumberOfPeopleinHome INT NOT NULL ,AgeRangeOfPeopleFrom INT NOT NULL , AgeRangeOfPeopleTo INT NOT NULL ,TypeOfHomeDesired INT NOT NULL ,DoYouOwnVehicle INT NOT NULL ,SharedUser INT NOT NULL)';

var profile = 'CREATE TABLE IF NOT EXISTS Profile (Id INTEGER PRIMARY KEY AUTOINCREMENT , FirstName TEXT  UNIQUE, LastName TEXT  UNIQUE, DOB DATE  ,Gender INT  ,MaritalStatus INT  ,RangeOfHouseholdIncome INT  ,NumberOfPeopleinHome INT ,AgeRangeOfPeopleFrom INT  , AgeRangeOfPeopleTo INT  ,TypeOfHomeDesired INT ,DoYouOwnVehicle INT  ,SharedUser INT, BudgetedPrice INT )';

var property = 'CREATE TABLE IF NOT EXISTS Property(Id INTEGER PRIMARY KEY AUTOINCREMENT , PropertyName TEXT NOT NULL UNIQUE, Address TEXT NOT NULL , Description TEXT NOT NULL ,latitude INT,longitude INT, Amount INT  NOT NULL ,  Status TEXT NOT NULL , ListingStatus TEXT NOT NULL ,  Category TEXT NOT NULL , Beds INT  NOT NULL , TotalBath INT  NOT NULL , Area INT NOT NULL , BuildYear INT  NOT NULL ,  NoOfStorey INT NOT NULL,Education INT, Arts INT, Accomodation INT, createdDate   NUMERIC ,RatingStatus TEXT,OverallRating INT,SortedRating INT,Comments TEXT,FavouriteProperty Text, updatedDate   DATE)';

var criteria = 'CREATE TABLE IF NOT EXISTS Criteria (Id INTEGER PRIMARY KEY AUTOINCREMENT , CriteriaName TEXT NOT NULL UNIQUE,CriteriaType TEXT NOT NULL, Weightage INT , Percentage INT ,filterPercentage INT, UserId INT , createdDate   NUMERIC NOT NULL ,updatedDate   NUMERIC,SortedStatus Text,Status TEXT)';

var sharedProfile = 'CREATE TABLE IF NOT EXISTS SharedProfile(Id INTEGER PRIMARY KEY AUTOINCREMENT, ProfileId INT NOT NULL, FirstName TEXT , LastName TEXT  , createdDate DATE,updatedDate DATE ,emailId TEXT, FOREIGN KEY(ProfileId) REFERENCES Profile(Id))';

var propertyPhotos = 'CREATE TABLE IF NOT EXISTS  PropertyPhotos(Id AUTO INCREMENT INT, Header INT, PropertyId	 INT , PropertyImage BLOB UNIQUE,PropertyName TEXT NOT NULL,createdDate DATE,updatedDate DATE ,  FOREIGN KEY(PropertyId) REFERENCES Property(Id))';

var userPropertyCriteria = 'CREATE TABLE IF NOT EXISTS  UserPropertyCriteria(Id AUTO INCREMENT INT, CriteriaId  INT NOT NULL,  UserId  INT NOT NULL, Rating INT,createdDate DATE,updatedDate DATE,FOREIGN KEY(CriteriaId) REFERENCES Criteria(Id),FOREIGN KEY(UserId) REFERENCES User(Id))';

//var ratings = 'CREATE TABLE IF NOT EXISTS Ratings (Id  INTEGER PRIMARY KEY AUTOINCREMENT ,CriteriaId INT NOT NULL, PropertyId INT , UserId BIGINT, Rating INT , createdDate   DATE  ,updatedDate   DATE)';

var ratings = 'CREATE TABLE IF NOT EXISTS Ratings (Id  INTEGER PRIMARY KEY AUTOINCREMENT ,CriteriaId INT NOT NULL, PropertyId INT NOT NULL , UserId BIGINT , Rating NUMERIC NOT NULL, createdDate   DATE NOT NULL ,updatedDate   DATE, FOREIGN KEY(PropertyId) REFERENCES Property(Id), FOREIGN KEY(CriteriaId) REFERENCES Criteria(Id))';

var queryTable = 'CREATE TABLE IF NOT EXISTS FilterAndSort(Id INTEGER PRIMARY KEY AUTOINCREMENT,username TEXT NOT NULL UNIQUE,filterNsortQuery TEXT NOT NULL,Rated INT,NotRated INT,PriceFrom INT,PriceTo INT,LowToHighRating    INT,HighToLowRating INT,LowToHighPrice INT,HighToLowPrice INT,CheckAll INT,ParticularCriteria INT, SortedCriteria TEXT)';

var context = 'CREATE TABLE IF NOT EXISTS context(userId TEXT NOT NULL,permanentUserId TEXT NOT NULL,influencerNameValue TEXT NOT NULL,editableValue TEXT NOT NULL,dropdownDisabledValue TEXT NOT NULL)';

 var propertyIndexing1 = 'CREATE INDEX IF NOT EXISTS index_Amount ON Property (Amount, OverallRating, SortedRating)';
 //var propertyIndexing1 = 'CREATE INDEX IF NOT EXISTS index_Amount ON Property (Amount, OverallRating, SortedRating)';
// var propertyIndexing12 = 'CREATE INDEX index_AmountDESC ON Property Amount DESC)';
// var propertyIndexing2 = 'CREATE INDEX index_OverallRating ON Property OverallRating ASC)';
// var propertyIndexing3 = 'CREATE INDEX index_FilteredRating ON Property SortedRating ASC)';
// var propertyIndexing22 = 'CREATE INDEX index_OverallRatingDESC ON Property OverallRating DESC)';
// var propertyIndexing32 = 'CREATE INDEX index_FilteredRatingDESC ON Property SortedRating DESC)';


 
 var createTables = [user, codeType, codeValue, profile, property, criteria, sharedProfile, propertyPhotos, userPropertyCriteria, ratings, queryTable,propertyIndexing1,context];
// var createTables = [user, codeType, codeValue, profile, property, criteria, sharedProfile, propertyPhotos, userPropertyCriteria, ratings, queryTable,propertyIndexing1,propertyIndexing12,propertyIndexing2,propertyIndexing3,propertyIndexing22,propertyIndexing32];
//-------insert for codetype  -----------------
var codetypes = ['Gender', 'MaritalStatus', 'TypeOfHomeDesired', 'QuestionYN', 'RangeOfHouseholdIncome', 'BirthMonth', 'BudgetedPrice']

var codevalues = ['Male', 'Female', 'Single', 'Married', 'Common Law', 'Condo', 'Town home', 'Duplex', 'Detached', 'Estate home', 'Undecided', 'Yes', 'No', '< $60,000', '$60,000 - $100,000', '$100,000 - $150,000', '$150,000 - $200,000', '$200,000 - $250,000', '> $250,000', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'Septemper', 'October', 'November', 'December', '<$200,000', '$200,001 - $400,000', '$400,001 - $600,000', '$600,001 - $800,000', '$800,001 - $1,000,000', '$1,000,001 - $1,200,000', '$1,200,001 - $1,400,000', ' $1,400,001 - $1,600,000', '$1,600,001 - $1,800,000', '$1,800,001 - $2,000,000', '$2,000,001 - $3,000,000', '$3,000,001 - $4,000,000', '$4,000,001 - $5,000,000', '>$5,000,000'];

var codetypevalue = [1, 1, 2, 2, 2, 3, 3, 3, 3, 3, 3, 4, 4, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7];

export { createTables, codetypes, codevalues, codetypevalue };