
import React from "react";
//import Setup from "./src/boot/setup";
import Setup from "./src/view/App";
// import Setup from "./src/view/criteria/test";
import { createStore, applyMiddleware,  compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import rootReducer from './src/reducer/index';

export default class App extends React.Component {

  render() {
    console.disableYellowBox = true;
    return (
      <Provider store={createStore(rootReducer, {}, compose(applyMiddleware(thunk)))}>
        <Setup />
      </Provider>
    
  
  );
  }
}