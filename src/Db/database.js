'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
var SQLite = require('react-native-sqlite-storage')
var db = SQLite.openDatabase({ name: "HomeRatingDB.db" });
var createQueries = require('./queries');
var homesJSON = require('../assets/homes.json');
var propertyImages = require('../assets/propertyImages.json');
var criteriaJSON = require('../assets/criteria.json');
import alertMSG from '../assets/resources';


export default class Database extends Component {

  constructor(props) {
    super(props);
    this.state = {
      record: null,
      timepassed: false,
      loggedIn: null
    }
  }

  // **************** function to create table in sqliteDB ****************

  createTable() {
    for (let i = 0; i < createQueries.createTables.length; i++) {
      db.transaction((tx) => {
        try{
        tx.executeSql(createQueries.createTables[i]);
        }
        catch(error){
        alert(error);
        }
      })
    }
  }
  // *************  function to insert  datas in CodeType table ****************
  insertcodeType() {
    for (let i = 0; i < createQueries.codetypes.length; i++) {
      db.transaction((tx) => {
        //tx.executeSql('DROP table Ratings');
        tx.executeSql('INSERT INTO CodeType (`Code`, `createdDate`)  VALUES ("' + createQueries.codetypes[i] + '", CURRENT_TIMESTAMP)');
      })
    }
  }
  // *************  function to insert  datas in CodeValue table ****************

  insertcodeValue() {
    for (let i = 0; i < createQueries.codevalues.length; i++) {
      db.transaction((tx) => {
        //tx.executeSql('DROP table CodeValue');
        tx.executeSql('INSERT INTO CodeValue (`Name`,`CodeTypeId`, `createdDate`)  VALUES ("' + createQueries.codevalues[i] + '",' + createQueries.codetypevalue[i] + ', CURRENT_TIMESTAMP)');
      })
    }
    
  }

  insertContext(){
    var Context = "CREATE TABLE IF NOT EXISTS ContextTable(Id INTEGER PRIMARY KEY AUTOINCREMENT , userId TEXT, permanentUserId TEXT , influencerNameValue TEXT,editableValue TEXT, dropdownDisabledValue TEXT)";

    db.transaction((tx) => {
      tx.executeSql(Context);
      tx.executeSql('Delete from ContextTable');
      tx.executeSql("INSERT INTO ContextTable (userId,permanentUserId,influencerNameValue,editableValue,dropdownDisabledValue)  VALUES ('a','b','c','d','e')");
      
    })
  }

  // *************  function to get  total table in sqliteDB ****************


  sqliteMaster() {
    db.transaction((tx) => {
      tx.executeSql('SELECT * FROM Ratings', [], (tx, results) => {
        //var len = results.rows.item(3)['ImageName'];
        var len = results.rows.length;
        this.setState({ record: len });
      });
    });
  }

  // ****************  insert homes into property table  and photoName in propertyphotos table    **************************
  insertProperty() {
    for (let i = 0; i < homesJSON.length; i++) {
      db.transaction((tx) => {
        tx.executeSql("INSERT INTO Property(`PropertyName`,`Address`,`Description`,`latitude`,`longitude`,`Amount`,`Status`,`ListingStatus`,`Category`,`Beds`,`TotalBath`,`Area`,`BuildYear`,`NoOfStorey`,`createdDate`,`RatingStatus`,`FavouriteProperty`,`OverallRating`,`SortedRating`) VALUES ('" + homesJSON[i]["PropertyName"] + "','" + homesJSON[i]["Address"] + "','" + homesJSON[i]["Description"] + "'," + homesJSON[i]["latitude"] + "," + homesJSON[i]["longitude"] + "," + homesJSON[i]["Amount"] + ",'" + homesJSON[i]["Status"] + "','" + homesJSON[i]["ListingStatus"] + "','" + homesJSON[i]["Category"] + "'," + homesJSON[i]["Beds"] + "," + homesJSON[i]["TotalBath"] + "," + homesJSON[i]["Area"] + "," + homesJSON[i]["BuildYear"] + "," + homesJSON[i]["NoOfStorey"] + ", CURRENT_TIMESTAMP , 'Unrated','Unfavourite',0,0)");
      })
    }
    for (let i = 0; i < propertyImages.length; i++) {
      db.transaction((tx) => {
        tx.executeSql("INSERT INTO PropertyPhotos(`Header`,`PropertyId`,`PropertyName`,`PropertyImage`,`createdDate`) VALUES ("+propertyImages[i]["Header"]+","+propertyImages[i]["PropertyId"]+",'" + propertyImages[i]["PropertyName"] + "','data:image/jpeg;base64," + propertyImages[i]["blob"] + "',CURRENT_TIMESTAMP)");
      })
    }

  }

  /**
   * 
   * insert profile into profile table
   */

  insertProfile() {
    db.transaction((tx) => {
     /* tx.executeSql('insert into User (username,password,EmailId,LoggedIn,loginCount,loggedInflag) VALUES ("chris","henry","chris@ait-india.com","False",1,1)');
      tx.executeSql('insert into User (username,password,EmailId,LoggedIn,loginCount,loggedInflag) VALUES ("james","jelinski","james@ait-india.com","False",1,1)');
      tx.executeSql('insert into User (username,password,EmailId,LoggedIn,loginCount,loggedInflag) VALUES ("rishikesh","thakur","rishikesh@ait-india.com","False",1,1)');
      tx.executeSql('insert into Profile (FirstName,LastName) values ("","")');
      tx.executeSql("insert into FilterAndSort(username,filterNsortQuery ,Rated ,NotRated ,PriceFrom ,PriceTo ,LowToHighRating ,HighToLowRating ,LowToHighPrice ,HighToLowPrice ,CheckAll ,ParticularCriteria ) values ('james','"+alertMSG.en['FilterString']+"',1,1,100000,600000,0,1,1,0,1,0)");
    */});
  }

  saveUserData(lusername,lpassword,lemailId,lloggedIn,lloginCount) {
    db.transaction((tx) => {
      tx.executeSql('insert into User (username,password,EmailId,LoggedIn,loginCount,loggedInflag,status) VALUES ("'+lusername+'","'+lpassword+'","'+lemailId+'","'+lloggedIn+'",'+lloginCount+',1,"Active")');
       });
  }

  insertCriteria() {
    for (let i = 0; i < criteriaJSON.length; i++) {
      db.transaction((tx) => {
        tx.executeSql('select * from Criteria', [], (tx, results) => {
         // console.log("Query completed");
          var len = results.rows.length;
          let query = 'insert into Criteria (CriteriaName,CriteriaType,Status,createdDate) VALUES ("' + criteriaJSON[i]['CriteriaName'] + '","' + criteriaJSON[i]['CriteriaType'] + '","Inactive",CURRENT_TIMESTAMP)';
          len == 24 ? null : tx.executeSql(query);
        });

      })
    }
  }

  pageDecideModal() {

    try {
      db.transaction((tx) => {
        //tx.executeSql('SELECT * FROM User  where status = "Active"', [this.username, this.password], (tx, results) => {
          tx.executeSql('SELECT * FROM User', [], (tx, results) => {
        
        
          var len = results.rows.length;
          if (len == 0) {
            this.setState({ loggedIn: false, timepassed: true });
          }
          if (len != 0) {
            
          var loggedIn = results.rows.item(0)['LoggedIn'];
          var loginCount = results.rows.item(0)['loginCount'];
          var uName = results.rows.item(0)['username'];
            if (loggedIn === 'false') {
              this.setState({ loggedIn: false, timepassed: true });
            }
            if (loggedIn === 'true') {
              //return 'True';
              this.setState({ loggedIn: true, timepassed: true });
            }
            if (loginCount === 1) {
              this.setState({ loginCount: false });
            }
            if (loginCount === 2) {
              //return 'True';
              this.setState({ loginCount: true });
            }
            if(uName==='Realtor'){
              this.setState({ isRealtor: true });
            } else{
              this.setState({ isRealtor: false });
            }


          }

        });

      });

    }
    catch (error) {
      alert(error);
    }

  }

}



