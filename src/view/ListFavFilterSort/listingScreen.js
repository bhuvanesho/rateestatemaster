
import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native'
//import { ListItem, Button, Icon } from 'react-native-elements';
import { Spinner, Card, Container, Header, Body, Content, CardItem, List, Icon } from 'native-base';
import styles from './styles';
import StarRating from 'react-native-star-rating';
import Property from '../../controller/Property';
import Orientation from "react-native-orientation";

export default class ListingScreen extends Property {
    constructor(props) {
        super(props);
        this.getAllProperty();
    }
    onSelect = data => {
        this.setState(data);
    };
    componentDidMount() {
        Orientation.lockToPortrait();
        this.props.navigation.addListener('willFocus', () => this.setState({ selected: true }));
    }
    render() {
        if (this.state.selected) {
            this.setState({ selected: false, loadingSpinner: true });
            this.getAllProperty();
        }
        var propertyImages = this.state.PropertyImages;

        var swiperImages = [];

        for (let k = 0; k < propertyImages.length; k++) {
            swiperImages.push({
                Id: propertyImages[k]['propertyId'],
                // url: 'data:image/jpeg;base64,' + propertyImages[k]['imageName']
                url: propertyImages[k]['imageName']

            })
        }
        var properties = this.state.propertyInfo;
        return (
            <Container style={styles.container}>
                <Header style={styles.headerBg}>
                    <Body>
                        <Image source={require('../../../assets/Headerlogo.png')} style={styles.headerBGimage} />

                    </Body>

                </Header>
                <View style={styles.filterSortHeaderBG}>
                    <Text style={styles.filterSortHeaderColor}>LISTINGS</Text>
                </View>
                <Content padder bounces={false}>
                    {properties.map(data =>
                        <TouchableOpacity key={data.propertyId} onPress={() => this.props.navigation.navigate('PropertyDetail', { onSelect: this.onSelect, propId: data.propertyId })}>
                            <Card style={styles.cardBackgroundColor} >

                                <Body>
                                    <View style={styles.cardBody} >
                                        {swiperImages.map(swiperImages =>
                                            <View>
                                                {!this.state.imageError ? this.state['imageError' + swiperImages.Id] : this.state['imageError' + swiperImages.Id] = false}
                                                {data.propertyId == swiperImages.Id ?

                                                    <Image
                                                        style={styles.cardImageSize}
                                                        resizeMode="cover"
                                                        source={{ uri: !this.state['imageError' + swiperImages.Id] ? swiperImages.url : 'https://www.krsaddleshop.com/sca-dev-montblanc/img/no_image_available.jpeg?resizeid=6&resizeh=1200&resizew=1200' }}
                                                       
                                                        onError={() => this.setState({ ['imageError' + swiperImages.Id]: true })}
                                                    /> : null
                                                }
                                            </View>
                                        )}
                                        <View style={styles.cardContent}>
                                            {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('PropertyDetail', { onSelect: this.onSelect, propId: data.propertyId })}> */}
                                            <Text style={styles.cardPropertyName}>{data.propertyName}</Text>

                                            <Text style={styles.cardPropertyAddress}>{data.address}</Text>
                                            <Text style={styles.cardPropertyDetails} numberOfLines={2}>{data.description}</Text>
                                            {/* <Text style={styles.cardPropertyDetails}><Icon style={styles. dollarFont} name='dollar' type='FontAwesome' />{this.addCommas(data.amount)}</Text> */}
                                            <Text style={styles.cardPropertyDetails}>${this.addCommas(data.amount)}</Text>
                                            {data.overallRating != null && data.overallRating != 0.0 && data.overallRating != 0 ?
                                                <View style={{ flexDirection: 'row' }}>
                                                    <StarRating
                                                        disabled={true}
                                                        emptyStar="ios-star-outline"
                                                        fullStar="ios-star"
                                                        halfStar="ios-star-half"
                                                        iconSet="Ionicons"
                                                        maxStars={5}
                                                        rating={data.overallRating}
                                                        fullStarColor={styles.starColor}
                                                        halfStarColor={styles.starColor}
                                                        emptyStarColor={styles.starColor}
                                                        halfStarEnabled
                                                        starSize={16}
                                                        starPadding={10}
                                                    />
                                                    <Text style={styles.rangefont}> {data.overallRating == null ? 0 : data.overallRating.toFixed(1)}/5 </Text>
                                                </View>
                                                :
                                                <Text style={styles.newListing}>{data.listingStatus}</Text>
                                            }

                                            {/* </TouchableOpacity>   */}
                                        </View>
                                        {/* <TouchableOpacity onPress={this.deleteListing.bind(this, data)}> */}
                                        <TouchableOpacity style={styles.listingDeleteView} onPress={this.confirmListingDelete.bind(this, data)}>
                                            <Image source={require('../../../assets/deleteButton.png')} style={styles.listingDeleteImage} />
                                        </TouchableOpacity>
                                    </View>
                                </Body>
                            </Card>
                        </TouchableOpacity>
                    )}
                    {/* <Text>{'\n'}</Text>
                    <Text>{'\n'}</Text> */}
                </Content>
                {properties.length == 0 && !this.state.loadingSpinner ?
                    <View style={styles.noFavourites}>
                        <Text style={styles.notfound}>
                            No  property found in the listings.
                </Text>
                    </View> : null}
                {this.state.loadingSpinner ?
                    <View style={styles.noFavourites}>
                        <Spinner color={styles.loadingColor} />
                    </View> : null}
            </Container>


            // implemented without image without header, using ListItem component

        );

    }
}
