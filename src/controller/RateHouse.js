
import React, { Component } from 'react';
import {Alert} from 'react-native';
import PropTypes from 'prop-types';
import alertMSG from '../assets/resources';
import RateHouseModel from '../model/RateHouse';
var sumProduct = require('sum-product');

export default class RateHouse extends RateHouseModel {

constructor(props) {
 super(props);
 const { navigation } = this.props;
 const propId = navigation.getParam('propertyId');
 this.state = {
   propId: propId,
   userId:'',
   rating:'',
 
   criteriaInfo:[],
   criteriaNRating:[],
   propertyId:'',
   propertyName:'',
   address:'',
   description:'',
   amount:'',
   status:'',
   listingStatus:'',
   category:'',
   beds:'',
   totalBath:'',
   area:'',
   buildYear:'',
   noOfStorey:'',  
   image:[],
   propertyInfo:[],
   ratings:[],
   weightage:[],
   percentage:[]
  }
  this.propertyInfo();
  this.calculateOverallRating();
}

propertyInfo(){
  this.getPropertyInfo('select * from Property where Id = '+this.state.propId+'');
}

calculateOverallRating(){
  this.state.weightage.push(4);
  this.state.weightage.push(3);
  this.state.weightage.push(3);
  this.state.weightage.push(3);
  this.state.weightage.push(3);
  this.state.percentage.push(0.31);
  this.state.percentage.push(0.06);
  this.state.percentage.push(0.19);
  this.state.percentage.push(0.25);
  this.state.percentage.push(0.19);
  var overAllRating = sumProduct(this.state.percentage,this.state.weightage);
  alert(overAllRating);
}

}
