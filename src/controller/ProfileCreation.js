
import React, { Component } from 'react';
import { Alert, Animated, View, Text, DatePickerIOS, TouchableOpacity, Picker, Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import ProfileCreationModel from '../model/ProfileCreation';
import alertMSG from '../assets/resources';
import styles from '../view/profile/styles';
import { MKTextField } from 'react-native-material-kit';
import {AsyncStorage} from 'react-native';

var moment = require('moment');
const window = Dimensions.get('window');
//alert(window.height);
import { data,genderValuesC,  maritalStautsC , BudgetedPriceC,QuestionYNC ,TypeOfHomeDesiredC,
  RangeOfHouseholdIncomeC } from '../firebaseConfig/commonConstant';

  import {setParameters,getParameter} from '../view/helper'

export default class ProfileCreation extends ProfileCreationModel {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      birthYear: '1994',
      birthMonth: '10',
      selectedGender: '0',
      selectedMaritalStatus: '0',
      numberOfPeopleInHome: '0',
      selectedAgeRangeOfPeopleFrom: '0',
      selectedAgeRangeOfPeopleTo: '0',
      selectedTypeOfHomeDesired: '0',
      ownVehicle: '',
      sharingAccount: '0',
      sharingUserFirstName: 'Raj',
      sharingUserLastName: 'kumar',

      year: [],
      month: [],
      gender: [],
      maritalStatus: [],
      rangeOfHouseHoldIncome: [],
      incomeHouseHoldRange: '',
      typeOfHomeDesired: [],
      oneTohundred: [],
      yesOrno: [],
      budgetedPrice: [],
      sample: 'Nothing',

      count: 1,
      textInput: [],
      visibleModal: null,
      chosenDate: '',
      clearDate: false,

      numberOfPeople: ['Select', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
      selectedNumberOfPeopleLabel: '',
      selectedNumberOfPeople: '',
      selectedGenderName: '',
      selectedMaritalName: '',
      selectedTypeOfHomeDesiredName: '',
      ownVehicleCount: '',
      incomeHouseHoldRangeLabel: '',
      selectedIncomeHouseHoldRange: '0',
      selectedAgeFrom: '',
      selectedAgeFromLabel: '',
      selectedAgeTo: '',
      selectedAgeToLabel: '',
      ageRangeColor: '#C0C0C0',
      dobColor: '#C0C0C0',
      selectedBudgetedPrice: '',
      selectedBudgetedlabel: '',

      sharingEmail: '',
      secondEmail: '',
      emailArray: [],
      addInputTextCount: 1,
      editableValue: 'true',
      getEmail: [],
      emailShowingcount: 1,
      autoFocusText: true,
      validEmail: true,
      showEmailForwardArrow: true,
      showDoneButton: false,
      selectedItems: [],
      validateEmail: true,
      //noEmailAdded: true,
      genderFloatingLabel: true,

      accountSharedEmail: [],
      onEmailChange: false,

      firstNameLabelFont: 21,
      firstNameLabelFontColor: '#d3d3d3',

      lastNameLabelFont: 21,
      lastNameLabelFontColor: '#d3d3d3',

      dobLabelFont: 21,
      dobLabelFontColor: '#d3d3d3',

      genderLabelFont: 21,
      genderLabelFontColor: '#d3d3d3',

      maritalLabelFont: 21,
      maritalLabelFontColor: '#d3d3d3',

      budgetedPriceFont: 21,
      budgetedPriceFontColor: '#d3d3d3',

      peopleInHouseFont: 21,
      peopleInHouseFontColor: '#d3d3d3',

      incomeHouseFont: 21,
      incomeHouseFontColor: '#d3d3d3',

      ownVehicleFont: 21,
      ownVehicleFontColor: '#d3d3d3',

      firstNameBorderColor: '#dcdcdc',
      lastNameBorderColor: '#dcdcdc',


      onDateChange:false,
      lastdate:'',
      editable:'',
      dropdownDisabled:'',
      disableView:'',
      films: [],
      query: '',
      agentName:'',
      agents:[],
      saveAgentId:'',


    }
    this.validateCreateProfile = this.validateCreateProfile.bind(this);
    this.getOnetoHundred();
    this.getCommonValues('gender');
    this.getCommonValues('maritalStatus')
    this.getCommonValues('RangeOfHouseholdIncome');
    this.getCommonValues('TypeOfHomeDesired');
    this.getCommonValues('QuestionYN');
    this.getCommonValues('BudgetedPrice');
    this.setReadOnlyFlag();
    this.getAccountSharedEmail();
    this.getPersonalProfileDetails() ;
   
    // this.showEmailId();

    this.profilePageCountIncrease = this.profilePageCountIncrease.bind(this);
    this.profilePageCountDecrease = this.profilePageCountDecrease.bind(this);
    this.addTextInput = this.addTextInput.bind(this);
    this.getDOBValue = this.getDOBValue.bind(this);
    this.setDate = this.setDate.bind(this);
    this.getGenderMethod = this.getGenderMethod.bind(this);
    this.cancelModal = this.cancelModal.bind(this);
    this.cancelFamilyModal = this.cancelFamilyModal.bind(this);
    this.cancelPropertyModal = this.cancelPropertyModal.bind(this);
    this.getMaritalMethod = this.getMaritalMethod.bind(this);
    this.getHomeDesiredTypes = this.getHomeDesiredTypes.bind(this);
    this.getNumberOFPeople = this.getNumberOFPeople.bind(this);
    this.selectedGender = this.selectedGender.bind(this);
    this.getVehicleStatus = this.getVehicleStatus.bind(this);
    this.houseHoldRangeIncome = this.houseHoldRangeIncome.bind(this);
    this.selectAgeLimitFrom = this.selectAgeLimitFrom.bind(this);
    this.selectAgeLimitTo = this.selectAgeLimitTo.bind(this);
    this.personalProfileDatas = this.personalProfileDatas.bind(this);
    this.emailSharing = this.emailSharing.bind(this);
    this.getBudgetedPrice = this.getBudgetedPrice.bind(this);
    this.getBudgetedPriceRange = this.getBudgetedPriceRange.bind(this);
    this.showEmailId = this.showEmailId.bind(this);
    this.emailValidate = this.emailValidate.bind(this);
    this.callEmailAdd = this.callEmailAdd.bind(this);
    this.profileSubmitDone = this.profileSubmitDone.bind(this);
    this.onSelectedItemsChangeTypes = this.onSelectedItemsChangeTypes.bind(this);
    this.firstNameValidation = this.firstNameValidation.bind(this);
    this.lastNameValidation = this.lastNameValidation.bind(this);
    this.setReadOnlyFlag=this.setReadOnlyFlag.bind(this);
    this.deletePropertyOfUser=this.deletePropertyOfUser.bind(this);
    this.addPropertyToNewInfluencer=this.addPropertyToNewInfluencer.bind(this);
    this.addAgentIdToNewInfluencer=this.addAgentIdToNewInfluencer.bind(this);
  }



  /**
 * function to get number 1 to 100
 */

  getOnetoHundred() {
    this.state.oneTohundred.push({ value: "Select" });
    for (var i = 1; i <= 100; i++) {
      this.state.oneTohundred.push({ value: i });
    }
  }
   

  setReadOnlyFlag=async()=>{
    try{
      let dropdownDisabledValue =   await AsyncStorage.getItem('dropdownDisabledValue');
      let editableValue =   await AsyncStorage.getItem('editableValue');
      let influencerNameValue =   await AsyncStorage.getItem('influencerNameValue');

      //let dropdownDisabledValue =  getParameter('dropdown');
      //let editableValue =   getParameter('editable');
      //let influencerNameValue =   getParameter('influencer');

      if(editableValue=="false"){ this.setState({ disableView :"none"})}
      if(editableValue=="true"){ this.setState({ disableView :""})}
        this.setState({ editable :editableValue})
        this.setState({ dropdownDisabled :dropdownDisabledValue})
        this.setState({ influencerName :influencerNameValue})
        }catch(error){
          console.log(error)
      }

  }

  validateCreateProfile() {
    this.validate({
      firstName: {},
      lastName: {},
      birthYear: { numbers: true },
      birthMonth: { numbers: true },
      selectedGender: { numbers: true },
      selectedMaritalStatus: { numbers: true },
      selectedRangeOfHouseHoldIncome: { numbers: true },
      numberOfPeopleInHome: { numbers: true },
      selectedAgeRangeOfPeopleFrom: { numbers: true },
      selectedAgeRangeOfPeopleTo: { numbers: true },
      selectedTypeOfHomeDesired: { numbers: true },
      ownVehicle: { numbers: true },
      sharingAccount: { numbers: true },
      sharingUserFirstName: {},
      sharingUserLastName: {}
    });
    if (this.isFormValid()) {
      this.updateProfile();
    }
    if (!this.isFormValid()) {
      Alert.alert('Invalid Input', this.getErrorMessages());
    }
  }


  profileSubmitDone() {
    this.personalProfileDatas();
    this.props.navigation.navigate('Listing');
  }

  profilePageCountIncrease() {

    this.setState({ count: this.state.count + 1 });
    this.personalProfileDatas();

  }
  profilePageCountDecrease() {
    this.setState({ count: this.state.count - 1 });
    this.personalProfileDatas();

  }

  addTextInput = () => {
    var key = this.state.textInput.length;
    let textInput = this.state.textInput;
    let emailShow = this.state.getEmail.length;
    var bgColor = '';
    if (key % 2 == 0) {
      bgColor = "#d4edfc";
    }
    else {

      bgColor = "#ededed";
    }
    if (emailShow != 0 && this.state.emailShowingcount == 1) {
      setTimeout(() => {
        for (var i = 0; i < emailShow; i++) {
          if (i % 2 == 0) {
            bgColor = "#d4edfc";
          }
          else {

            bgColor = "#ededed";
          }
          textInput.push(<View style={{ backgroundColor: bgColor, width: '100%' }} ><View style={styles.textInputStyle1Content}><MKTextField highlightColor='#25a9e0' editable={false} key={key} textInputStyle={styles.emailTextField} placeholder="E mail" onTextChange={(email) => this.setState({ secondEmail: email })} value={this.state.getEmail[i].value} /></View></View>);
        }
        this.setState({ emailShowingcount: 2 });
        this.setState({ validEmail: true });
      }, 500);
    }
    else {
      //textInput.push(<View style={{ backgroundColor: bgColor, width: '100%' }} ><View style={styles.textInputStyle1Content}><MKTextField highlightColor='#25a9e0' key={key} textInputStyle={styles.emailTextField} placeholder="E mail" onTextChange={(email) => this.setState({ secondEmail: email })} onBlur={this.emailValidate} autoFocus={this.state.autoFocusText} /></View></View>);
      textInput.push(<View style={{ backgroundColor: bgColor, width: '100%' }} ><View style={styles.textInputStyle1Content}><MKTextField highlightColor='#25a9e0' key={key} textInputStyle={styles.emailTextField} placeholder="E mail" onTextChange={(email) => this.emailValidate(email)} /></View></View>);
      this.setState({ validEmail: false });
    }

    // textInput.push(<View style={{ backgroundColor: bgColor, width: '100%' }}><View style={styles.textInputStyle1Content}><MKTextField highlightColor='#25a9e0' textInputStyle={styles.emailTextField} placeholder="E mail"  onChangeText={(text) => this.check(text)}  /></View></View>);

    this.setState({ textInput });
    this.state.email = '';
    this.state.emailArray.push(this.state.secondEmail.toString());
  }

  backDrop() {
    this.refs._scrollView.scrollTo(0);
    { this.setState({ visibleModal: null }) }

  }
  backDropProperty() {
    this.refs._scrollViewProperty.scrollTo(0);
    { this.setState({ visibleModal: null }) }

  }
  backDropFamily() {
    this.refs._scrollViewFamily.scrollTo(0);
    { this.setState({ visibleModal: null }) }

  }
  getDOBValue() {
    this.refs._scrollView.scrollTo(window.height >= 667 ? null : 100);
    { this.setState({ clearDate: false, chosenDate:this.state.chosenDate!=''?new Date(this.state.chosenDate):new Date() }) };
    this.setState({ dobLabelFont: 15, dobLabelFontColor: '#25A9E0' });
    { this.setState({ visibleModal: 1 }) };
  }
  // setDate(newDate) {
  //   if (newDate.getFullYear() <= new Date().getFullYear()) {

  //     if (newDate.getMonth() <= new Date().getMonth()) {
  //       if (newDate.getDate() <= new Date().getDate()) {
  //         this.setState({ clearDate: false, chosenDate: newDate })
  //       }
  //       else {
  //         Alert.alert(alertMSG.en['dateValidate'], alertMSG.en['date']);
  //         this.setState({ chosenDate: new Date(), clearDate: true })
  //       }
  //     }
  //     else {
  //       Alert.alert(alertMSG.en['dateValidate'], alertMSG.en['date']);
  //       this.setState({ chosenDate: new Date(), clearDate: true })
  //     }
  //     //  this.setState({ clearDate: false, chosenDate: newDate })
  //   }
  //   else {
  //     Alert.alert(alertMSG.en['dateValidate'], alertMSG.en['date']);
  //     this.setState({ chosenDate: new Date(), clearDate: true })
  //   }
  // }

  setDate(newDate) {
    this.setState({onDateChange:true,lastdate:newDate});
    if (newDate.getFullYear() < new Date().getFullYear()) {

      this.setState({ clearDate: false, chosenDate: newDate });

    }
    else if (newDate.getFullYear() == new Date().getFullYear()) {
      if (newDate.getMonth() < new Date().getMonth()) {

        this.setState({ clearDate: false, chosenDate: newDate })
      } else if (newDate.getMonth() == new Date().getMonth()) {
        if (newDate.getDate() <= new Date().getDate()) {
          this.setState({ clearDate: false, chosenDate: newDate })
        }
        else {
          Alert.alert(alertMSG.en['dateValidate'], alertMSG.en['date']);
          this.setState({ chosenDate: new Date(), clearDate: true,onDateChange:false })
        }
      }
      else {
        Alert.alert(alertMSG.en['dateValidate'], alertMSG.en['date']);
        this.setState({ chosenDate: new Date(), clearDate: true,onDateChange:false  })
      }

    }
    else {
      Alert.alert(alertMSG.en['dateValidate'], alertMSG.en['date']);
      this.setState({ chosenDate: new Date(), clearDate: true,onDateChange:false })
    }
  }
  cancelModal() {
    this.setState({ visibleModal: 0 });
    this.refs._scrollView.scrollTo(0);
  }

  cancelFamilyModal() {
    this.setState({ visibleModal: 0 });
    this.refs._scrollViewFamily.scrollTo(0);
  }

  cancelPropertyModal() {
    this.setState({ visibleModal: 0 });
    this.refs._scrollViewProperty.scrollTo(0);
  }
  getGenderMethod() {

    this.refs._scrollView.scrollTo(window.height >= 667 ? 100 : 150);
    { this.setState({ visibleModal: 2 }) };
  }

  getMaritalMethod() {
    this.refs._scrollView.scrollTo(window.height >= 667 ? 150 : 200);
    { this.setState({ visibleModal: 3 }) };
  }

  getHomeDesiredTypes() {
    { this.setState({ visibleModal: 4 }) };
  }
  getNumberOFPeople() {
    { this.setState({ visibleModal: 5 }) };
  }

  getVehicleStatus() {
    this.refs._scrollViewFamily.scrollTo(window.height >= 667 ? 70 : 170);
    this.setState({ visibleModal: 6 });
  }
  houseHoldRangeIncome() {
    this.refs._scrollViewFamily.scrollTo(window.height >= 667 ? 10 : 110);
    this.setState({ visibleModal: 7 })
  }
  selectAgeLimitFrom() {
    this.refs._scrollViewFamily.scrollTo(window.height >= 667 ? 140 : 190);
    this.setState({ visibleModal: 8 });
  }
  selectAgeLimitTo() {
    this.refs._scrollViewFamily.scrollTo(window.height >= 667 ? 140 : 190);
    this.setState({ visibleModal: 9 });
  }
  getBudgetedPrice() {
    this.refs._scrollViewProperty.scrollTo(window.height >= 667 ? 20 : 100);
    this.setState({ visibleModal: 10 });
  }

  selectedGender = (itemValue, itemIndex) => {
    //alert('hi');
    this.setState({ genderLabelFont: 15, genderLabelFontColor: '#25A9E0' });
    const id = parseInt(itemValue);
    this.setState({ selectedGender: id });
    this.setState({ selectedGenderName: this.state.gender[itemIndex].value });
    //alert(this.state.gender[itemIndex].value);
    { this.state.gender[itemIndex].value == 'Select' ? this.setState({ genderLabelFont: 20, genderLabelFontColor: '#d3d3d3' }) : null }

  }
  selectedMaritalStatus = (itemValue, itemIndex) => {
    this.setState({ maritalLabelFont: 15, maritalLabelFontColor: '#25A9E0' });
    const id = parseInt(itemValue);
    this.setState({ selectedMaritalStatus: id });
    this.setState({ selectedMaritalName: this.state.maritalStatus[itemIndex].value });
    { this.state.maritalStatus[itemIndex].value == 'Select' ? this.setState({ maritalLabelFont: 20, maritalLabelFontColor: '#d3d3d3' }) : null }
  }
  selectedHomeDesiredTypes = (itemValue, itemIndex) => {

    const id = parseInt(itemValue);
    this.setState({ selectedTypeOfHomeDesired: id });
    this.setState({ selectedTypeOfHomeDesiredName: this.state.typeOfHomeDesired[itemIndex].value });
  }
  selectedPeopleCount = (itemValue, itemIndex) => {
    this.setState({ peopleInHouseFont: 15, peopleInHouseFontColor: '#25A9E0' });
    this.setState({ selectedNumberOfPeople: itemValue });
    this.setState({ selectedNumberOfPeopleLabel: this.state.numberOfPeople[itemIndex] });
    { this.state.numberOfPeople[itemIndex] == 'Select' ? this.setState({ peopleInHouseFont: 20, peopleInHouseFontColor: '#d3d3d3' }) : null }
  }
  selectedVehicleStatus = (itemValue, itemIndex) => {
    this.setState({ ownVehicleFont: 15, ownVehicleFontColor: '#25A9E0' });
    const id = parseInt(itemValue);
    this.setState({ ownVehicle: id });
    this.setState({ ownVehicleCount: this.state.yesOrno[itemIndex].value });
    { this.state.yesOrno[itemIndex].value == 'Select' ? this.setState({ ownVehicleFont: 20, ownVehicleFontColor: '#d3d3d3' }) : null }
  }
  houseHoldIncomeRange = (itemValue, itemIndex) => {
    this.setState({ incomeHouseFont: 15, incomeHouseFontColor: '#25A9E0' });
    //const id = parseInt(itemValue);
    this.setState({ selectedIncomeHouseHoldRange: itemValue });
    this.setState({ incomeHouseHoldRangeLabel: this.state.rangeOfHouseHoldIncome[itemIndex].value });
    { this.state.rangeOfHouseHoldIncome[itemIndex].value == 'Select' ? this.setState({ incomeHouseFont: 20, incomeHouseFontColor: '#d3d3d3' }) : null }
  }
  selectAgeFrom = (itemValue, itemIndex) => {

    // const id = parseInt(itemValue);
    if (this.state.selectedAgeTo != '') {
      if (this.state.selectedAgeTo >= itemValue) {
        this.setState({ selectedAgeFrom: itemValue });
        this.setState({ selectedAgeFromLabel: this.state.oneTohundred[itemIndex].value });
        this.setState({ ageRangeColor: '#25A9E0' });
      }
      else {
        Alert.alert('', alertMSG.en['SelectedAgeFrom']);

      }
    }
    else {
      this.setState({ selectedAgeFrom: itemValue });
      this.setState({ selectedAgeFromLabel: this.state.oneTohundred[itemIndex].value });
      this.setState({ ageRangeColor: '#25A9E0' });
    }





  }
  selectAgeTo = (itemValue, itemIndex) => {
    if (this.state.selectedAgeFrom != '') {
      if (this.state.selectedAgeFrom <= itemValue) {
        this.setState({ selectedAgeTo: itemValue })
        this.setState({ selectedAgeToLabel: this.state.oneTohundred[itemIndex].value });
        this.setState({ ageRangeColor: '#25A9E0' });
      }
      else if (this.state.selectedAgeFrom > itemValue) {
        Alert.alert('', alertMSG.en['SelectedAgeTo']);

      }
    }
    else {
      this.setState({ selectedAgeTo: itemValue })
      this.setState({ selectedAgeToLabel: this.state.oneTohundred[itemIndex].value });
      this.setState({ ageRangeColor: '#25A9E0' });
    }
  }

  budgetedPriceChange = (itemValue, itemIndex) => {

    this.setState({ budgetedPriceFont: 15, budgetedPriceFontColor: '#25A9E0' });
    const id = parseInt(itemValue);
    this.setState({ selectedBudgetedPrice: id });
    this.setState({ budgetedPriceChange: this.state.budgetedPrice[itemIndex].value });
    { this.state.budgetedPrice[itemIndex].value == 'Select' ? this.setState({ budgetedPriceFont: 20, budgetedPriceFontColor: '#d3d3d3' }) : null }

  }
  personalProfileDatas() {
    this.updateProfile();

  }

  emailSharing(route) {
    try {
      this.updateEmail(route);
    }
    catch (error) {
      alert(error);
    }
  }

  onSelectedItemsChangeTypes = (selectedItems) => {
    //alert(selectedItems)
    this.setState({ selectedItems });
  }


  showEmailId() {
    setTimeout(() => {
      this.addTextInput();
    }, 1000);
  }


  emailValidate = (email) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    /*if (email.length > 0) {
      this.setState({ noEmailAdded: false });
    }
    else {
      this.setState({ noEmailAdded: true });
    }*/

    if (reg.test(email) === false) {
      this.setState({ validEmail: false });

    }
    else {
      this.setState({ validEmail: true })
      //this.setState({ secondEmail: email });
    }
  }

  callEmailAdd(params) {
    if (params == 'Add') {
      if (!this.state.validEmail) {
        Alert.alert('', 'Please enter the valid Email Id');
      }

    }

    if (params == 'Listing') {
      if (this.state.validEmail == false) {
        Alert.alert('', 'Please enter the valid Email Id');
      }
      else {
        this.emailSharing('Listing');
      }
    }

    if (params == 'Criteria') {
      //page 4 contains family info. so update profile and move ahead
      this.updateProfile();
      this.emailSharing('Criteria');
      //this.props.navigation.navigate('YourCriteria')
    }

    if (params == 'backWard') {
      if (this.state.validEmail == false) {
        Alert.alert('', 'Please enter the valid Email Id');
      }
      else {
        this.emailSharing('backWard');
      }
    }
    if (params == 'forward') {
      if (this.state.validEmail == false) {
        Alert.alert('', 'Please enter the valid Email Id');
      }
      else {
        this.emailSharing('forward');
      }
    }
  }

  pushEmail() {
    var accountSharedEmail = this.state.accountSharedEmail;
    var sharedEmail = [];
    var valid = false;
    for (var i = 0; i < accountSharedEmail.length; i++) {
      let oldid = accountSharedEmail[i]['Id'];
      let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      let emailId = this.state['email_' + oldid];
      // alert(accountSharedEmail.length+" "+emailId);
      if (reg.test(emailId) == false) {
        valid = false;
        this.setState({ validEmail: false });

        Alert.alert('', 'Please enter the valid Email Id');
      }
      else if (reg.test(emailId) == true) {
        valid = true;

      }
    }
    if (valid == true) {
      this.onscreenEmail();
    }

  }



  onscreenEmail() {
    var accountSharedEmail = this.state.accountSharedEmail;
    var sharedEmail = [];
var nextNumber;
    for (var i = 0; i < accountSharedEmail.length; i++) {
      let oldid = accountSharedEmail[i]['Id'];
      var len = accountSharedEmail.length;
      let status = accountSharedEmail[i]['status'];
      let emailId = this.state['email_' + oldid];
      let bgColor;
      if (oldid == 0) {
        oldid = len + oldid;
      }
      // alert(emailId);
      if (oldid % 2 == 0) {
        bgColor = "#ededed";
      }
      else {
        bgColor = "#d4edfc";
      }
      sharedEmail.push({ value: emailId, Id: oldid, bgColor: bgColor, editable: true, status: status });
    }
    if (accountSharedEmail.length % 2 == 0) {
      bgColor = "#d4edfc";
    }
    else {
      bgColor = "#ededed";
    }
    //decide the next number based on the assigned Id. there are chances of deleting inbetween ids
    nextNumber=Math.max.apply(Math, accountSharedEmail.map(function(o) { return o.Id; }));
    if(nextNumber==0){nextNumber=1}
    nextNumber=nextNumber+1;
    sharedEmail.push({ value: '', Id: nextNumber, bgColor: bgColor, editable: true, status: 'New' });
    this.setState({accountSharedEmail: sharedEmail})
    this.setState({onEmailChange: false });
    this.setState({validEmail: true });
  }



  firstNameValidation = (name) => {
    //  var reg = /[`~!@#$%^&*()_|+\-=÷¿?;:'",.<>\{\}\[\]\\\/]/gi;
    //var reg = /^[A-Za-z]+$/;
    var reg = /^[a-zA-Z_ ]*$/;
    if (reg.test(name) === true) {
      { this.setState({ firstName: name, firstNameLabelFont: 21, firstNameLabelFontColor: '#d3d3d3' }) }
    }
    else {
      Alert.alert('', alertMSG.en['InvalidName']);
    }
  }

  lastNameValidation = (name) => {
    // var reg = /[`~!@#$%^&*()_|+\-=÷¿?;:"',.<>\{\}\[\]\\\/]/gi;
    // var reg = /^[A-Za-z]+$/;
    // var  reg = [a-zA-Z]+;
    var reg = /^[a-zA-Z_ ]*$/;
    if (reg.test(name) === true) {
      this.setState({ lastName: name, lastNameLabelFont: 21, lastNameLabelFontColor: '#d3d3d3' })
    }
    else {
      Alert.alert('', alertMSG.en['InvalidName']);
    }
  }

  deleteEmail=(id,username)=>{
    
    var sharedEmail = this.state.accountSharedEmail;
    if(sharedEmail.length==1){
      sharedEmail[0].value="";
      sharedEmail[0].status="New";
      sharedEmail[0].Id=0;
        this.setState({accountSharedEmail: sharedEmail})
        //change influencer account to primary account
        if(username!='')
        {
          this.changeInfluencerAccount(username.toLowerCase().trim())
        }
      }
    if(sharedEmail.length>1){
    sharedEmail = sharedEmail.filter(function (obj) {
        return obj.Id != id;
      });
      this.setState({accountSharedEmail: sharedEmail})
      //change influencer account to primary account
      if(username!='')
      {
        this.changeInfluencerAccount(username.toLowerCase().trim())
      }
    }
    

  }

  getCommonValues =(type) =>{
    var results = '';
   
      switch(type)
      {
        
        case 'gender':
       
            results = genderValuesC;
            var len = results.length;
            for (let i = 0; i < len; i++) {
                //console.log(results[i][id]);
                let id = results[i]['Id'];
                let name = results[i]['value'];
                this.state.gender.push({ value: name, Id: id });
            }
           
                break;
        case 'maritalStatus':
            results = maritalStautsC;
            var len = results.length;
            for (let i = 0; i < len; i++) {
                //console.log(results[i][id]);
                let id = results[i]['Id'];
                let name = results[i]['value'];
                this.state.maritalStatus.push({ value: name, Id: id });
        
            }
                break;
        case 'BudgetedPrice':
            results = BudgetedPriceC;
            var len = results.length;
            for (let i = 0; i < len; i++) {
                //console.log(results[i][id]);
                let id = results[i]['Id'];
                let name = results[i]['value'];
                this.state.budgetedPrice.push({ value: name, Id: id });
        
            }
            break;
            case 'QuestionYN':
            results = QuestionYNC;
            var len = results.length;
            for (let i = 0; i < len; i++) {
                //console.log(results[i][id]);
                let id = results[i]['Id'];
                let name = results[i]['value'];
                this.state.yesOrno.push({ value: name, Id: id });
        
            }
                break;      
              case 'TypeOfHomeDesired':
                results = TypeOfHomeDesiredC;
                var len = results.length;
                for (let i = 0; i < len; i++) {
                   // console.log(results[i][id]);
                    let id = results[i]['Id'];
                    let name = results[i]['name'];
                    this.state.typeOfHomeDesired.push({ name: name, Id: id });
            
                }
                    break;  
                case  "RangeOfHouseholdIncome" :
                  results = RangeOfHouseholdIncomeC;
                  var len = results.length;
                  for (let i = 0; i < len; i++) {
                     // console.log(results[i][id]);
                      let id = results[i]['Id'];
                      let name = results[i]['value'];
                      this.state.rangeOfHouseHoldIncome.push({ value: name, Id: id });
              
                  }
                    break;  
      }
}


}