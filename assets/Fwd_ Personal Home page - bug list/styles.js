import { Dimensions } from 'react-native';
const window = Dimensions.get('window');

//alert(window.height);

export default {
  empty: {
    padding: 30
  },
  container: {
    backgroundColor: "#FFF"
  },
  MainContainer: {
    flex: 2,
    margin: 60,
    justifyContent: 'center',
    //alignItems: 'center',
  },
  RememberMe: {
    justifyContent: 'center',
    marginTop: 11,
    fontSize: 21,
    fontFamily: 'AspiraXXXNar-Regular',
    color: '#727171'
  },
  loginScreenLogo: {
    height: 81.33,
    width: 144,
    marginTop: -120,
  },
  TextInputStyleClass: {
    padding: 10,
    height: 40,
    borderColor: '#ededed',
    borderRadius: 5,
    backgroundColor: "#ededed"
  },
  title: {
    color: "#727171",
    textAlign: "left",
    fontSize: 19,
    fontFamily: 'AspiraXXXNar-Bold'
  },
  SubmitButtonStyle: {
    justifyContent: 'center',
    paddingTop: 8,
    backgroundColor: '#1DA4DF',
    borderRadius: 30,
    borderWidth: 1,
    borderColor: '#fff',
    height: 34.015748031,
    width: 145.181102362,


  },
  TextStyle: {
    // marginTop: -5,
    justifyContent: 'center',
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'AspiraXXXNar-Regular',
    fontSize: 21
  },


  textInputStyle1: {
    // borderBottomWidth: 1,
    //borderBottomColor: "#d3d3d3",
    fontSize: 21,
    //paddingTop: 25,
    height: 23,
    color: '#727171',
    fontFamily: 'AspiraXXXNar-Regular',
    //paddingTop: 40


  },

  emailTextFieldBlue: {
    // borderBottomWidth: 1,
    //borderBottomColor: "#d3d3d3",
    fontSize: 21,
    //paddingTop: 25,

    color: '#25a9e0',
    fontFamily: 'AspiraXXXNar-Regular',



  },
  emailTextField: {
    // borderBottomWidth: 1,
    //borderBottomColor: "#d3d3d3",
    fontSize: 21,
    //paddingTop: 25,

    color: '#727171',
    fontFamily: 'AspiraXXXNar-Regular',



  },
  textInputStyle1Content: {
    flex: 1,
    margin: 15,
    zIndex:5,
  },

  imageIcons: {
    width: 140,
    height: 90
  },
  imageView: {
    width: '100%',
    height: 150,
    backgroundColor: '#25A9E0',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',


  },
  imageBackgroundView: {
    flex: 1,
    flexDirection: 'column',
  },

  alignArrows1: {
    //  left:'85%',
    //flexDirection:'row'
    alignSelf: 'flex-end'
  },
  alignArrows2:
    {
      // left:'50%',
      alignSelf: 'flex-end',
      flexDirection: 'row'
    },

  profileBgImage: {
    width: window.width <= 320 ? 230 : 290,
    height: 150
  },



  //filter and sort pages

  headerBg: {
    backgroundColor: '#fff'
  },
  headerBGimage: {
    width: 50,
    height: 42.3,


  },
  filterSortHeaderBG: {
    backgroundColor: '#1DA4DF',
    height: 36,
    alignItems: 'center',
    justifyContent: 'center',
  },
  filterSortHeaderColor: {
    color: '#FFF',
    fontFamily: 'AspiraXXXNar-Bold',
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
    paddingTop: 7,
    fontSize: 18
  },
  filterCategeoryHeader: {
    backgroundColor: '#d4edfc',
    height: 30,
    justifyContent: 'center'
  },
  filterCategeoryHeaderAlign: {
    left: 25,
    fontFamily: 'AspiraXXXNar-Bold',
    // fontWeight : 'bold' ,
    paddingTop: 5,
    color: "#727171",

  },
  filterMarginAlign: {
    flexDirection: 'column',
    margin: 25
  },
  filterContentAlign: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',

  },
  filterCheckBoxAlign: {
    alignContent: 'center',
    borderRadius: 5

  },
  filterContent: {
    marginTop: 15
  },



  //welcome screen

  welcomeBgimage: {
    width: '100%',
    height: '100%'
  },
  welcomeScreenStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  welcomeScreenLogo: {
    width: 196.66,
    height: 150.33,
    marginTop: -100
  },
  welcomeScreenText1: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 36,
    fontFamily: 'AspiraXXXNar-Regular'
  },
  welcomeScreenText2: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 23,
    fontFamily: 'AspiraXXXNar-Regular'
  },
  welcomeArrowAlign: {
    left: '83%',
    bottom: '5%'
  },
  welcomeArrowImage: {
    width: 32.66,
    height: 33.33
  },

  //listing Screen

  cardBackgroundColor: {
    backgroundColor: '#ededed'
  },
  cardImageSize:
    {
      height: 100,
      width: 144
    },
  cardContent:
    {
      width: 0,
      flexGrow: 1,
      marginLeft: 8,
      paddingTop: 10
    },
  cardPropertyName:
    {
      fontFamily: 'AspiraXXXNar-Regular',
      fontSize: 15,
      color: '#25A9E0'
    },
  cardPropertyDetails:
    {
      fontFamily: 'ArialNarrowMTStd',
      fontSize: 11,
      color: '#727171'
    },
  cardStarImageSize:
    {
      width: 10,
      height: 9
    },
  cardBody:
    {
      flexDirection: 'row',
    },

  propertyDetailsBG: {
    backgroundColor: '#25a9e0',
    height: 36
  },
  propertyDetails: {
    color: '#FFF',
    marginTop: 8,
    fontWeight: 'bold',
    fontSize: 21,
    fontFamily: 'AspiraXXXNar-Regular'
  },
  propertyFlex: { flex: 1, flexDirection: 'row', justifyContent: 'center' },
  iconFlex: { paddingTop: 3, flex: 1, flexDirection: 'row' },
  locationIcon: { marginRight: 5, width: 17, height: 28 },
  favouriteIcon: { marginRight: 5, width: 28, height: 28 },
  filterIcon: { marginRight: 5, width: 30, height: 30 },

  submitHeader:
    {
      color: '#25a9e0',
      fontFamily: 'AspiraXXXNar-Regular',
      fontSize: 21,
      paddingTop: 5
    },
  leftRightArrows:
    {
      width: 30.66,
      height: 31.66,
      margin: 20
    },
  addEmail:
    {
      width: 48,
      height: 18.66
    },
  tabHeader: {
    //fontSize :18.61,

    color: '#25a9e0', fontWeight: 'bold', fontSize: 16.235, fontFamily: 'AspiraXXXNar-Regular'
  },
  filterFont: {
    fontFamily: 'AspiraXXXNar-Regular',
    fontSize: 16.23,
    color: "#727171",
  },
  tabUnderline: {
    borderBottomColor: '#25a9e0',
  },
  activetabheaderfont: { color: '#25a9e0', fontWeight: 'bold', fontSize: 16.235, fontFamily: 'AspiraXXXNar-Regular' },
  tabheaderfont: { color: '#727171', fontWeight: 'bold', fontSize: 16.235, fontFamily: 'AspiraXXXNar-Regular' },
  tabUnderline: { borderColor: '#25a9e0', borderWidth: 2 },


  //Profile Down Modal Popup

  ModalBG: {
    justifyContent: 'flex-end',
    margin: 0
  },
  modalBody: {
    backgroundColor: 'rgba(245, 245, 245, 0.8)',
    //  backgroundColor : '#f8f8f8',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    bottom: window.height > 700 ? window.height / 9 : 55
  },
  confirmCancelButton: {
    flexDirection: 'row', justifyContent: 'space-between'
  },
  modalLabel: {
    justifyContent: 'center',
    //  color: '#fff',
    textAlign: 'center',
    fontFamily: 'AspiraXXXNar-Regular',
    fontSize: 21,
    marginTop: 15
  },
  arrow: { padding: 20 },
  container: {
    backgroundColor: "#FFF"
  },
  arrowless: { padding: 15 },

  floatinglabel: { fontFamily: 'AspiraXXXNar-Regular', fontSize: 20 },






  floatinglabelStyle: { fontFamily: 'AspiraXXXNar-Regular',paddingLeft:0   },
  floatinglabelStyle2: { fontFamily: 'AspiraXXXNar-Regular',paddingLeft:0,fontSize:15,color:'#25A9E0'   },
  floatingInputStyle: {
    borderWidth: 0, height: 23,
    color: '#727171',
    fontFamily: 'AspiraXXXNar-Regular',
    paddingLeft:0,
    marginTop:20
  },
  floatinglabelPropertyStyle: {
  borderBottomWidth: 2,
    borderColor: '#dcdcdc',
    padding: 3,
    
},
buttonInputStyleClass: {
  float: 'left',
  paddingTop: 2,
  backgroundColor: '#ededed',
  borderRadius: 10,
  borderWidth: 1,
  borderColor: '#fff',
  height: 50.015748031,
  width: 305.18110236,
  color:'black',
  
},
loginScreenLogo2: {
  height: 81.33,
  width: 144,
  marginTop: 20,
},
personalButtonStyle2: {
  justifyContent: 'center',
  marginTop:40,
  paddingTop: 2,
  backgroundColor: '#1DA4DF',
  borderRadius: 10,
  borderWidth: 1,
  borderColor: '#fff',
  height: 50.015748031,
  width: 305.18110236
},
TextStyle2: {
  // marginTop: 50, 
   //justifyContent: 'center',
   color: "#727171",
   alignItems: 'center',
   fontFamily: 'AspiraXXXNar-Regular',
   fontSize: 21,
   margin:5,
   marginTop:12,
   left:10,
 },
 shareImage2: {
    width: 35,
    height: 35,
    position: 'absolute',
    right: 10, // Keep some space between your left border and Image
    top:7,
},
};