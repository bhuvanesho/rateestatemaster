import React, { Component } from 'react';
import { Text, View, TextInput, Button, Image, TouchableOpacity, DatePickerIOS, Picker, TouchableWithoutFeedback, KeyboardAvoidingView, Keyboard, ScrollView } from 'react-native';

import { Container, Header, Left, Body, Icon, Right, Item, Input, Content, Footer, FooterTab, Label } from 'native-base';
import styles from './styles';
import { AsyncStorage } from 'react-native';

import { database } from '../../firebaseConfig/config';

const launchscreenLogo1 = require("../../../assets/loginScreenLogo.png");
const backArrow = require("../../../assets/blueBackArrow.png");
import Orientation from "react-native-orientation";



class SharedAccount extends Component {
    constructor(props) {
        super(props)
        const sharedAcc = this.props.navigation.getParam('sharedAcc');
        this.state = {
            allClientsData: [],
            sharedAccountData: sharedAcc,
            primaryClientData: [],
            userId: '',
            firstNameBorderColor: '',
            firstNameLabelFont: 21,
            firstNameLabelFontColor: '#ededed',
            floatinglabelStyle: { fontFamily: 'AspiraXXXNar-Regular', paddingLeft: 0 },
            active: false,
            personalViewActiveCss: '',
            permanetUserId: '',
        }
        this.setState({ sharedAccountData: sharedAcc });
    }
    componentDidMount() {
        try {
            Orientation.lockToPortrait();
        } catch (error) {

        }

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.navigation.state.params.sharedAcc) {
          //this.props.retrieveCurrentUser(this.props.sharedAcc);
          this.setState({ sharedAccountData: nextProps.navigation.state.params.sharedAcc });
        }
      }

    getClients = async () => {
        var agentId = await AsyncStorage.getItem('agentId');
        this.setState({ permanetUserId: agentId })


        var AllClients = [];
        var primaryClients = [];
        var ref1 = database.ref('profile/')
        ref1.orderByChild('agent')
            .startAt(agentId)
            .endAt(agentId)
            .once('value', function (snapshot1) {
                snapshot1.forEach(function (childSnap1) {
                    AllClients.push({
                        "influencerName": childSnap1.val().firstName + " " + childSnap1.val().lastName,
                        "userId": childSnap1.val().userId,
                        "accountId": 0,
                        "influencers": 0,
                    })
                }.bind(this));

                for (let i = 0; i < AllClients.length; i++) {
                    var userId = AllClients[i].userId;

                    var ref = database.ref('User/')
                    ref.orderByChild('userId')
                        .startAt(userId)
                        .endAt(userId)
                        .once('value', function (snapshot) {
                            snapshot.forEach(function (childSnap) {
                                AllClients[i].accountId = childSnap.val().accountId;
                            }.bind(this));
                            this.setState({ allClientsData: AllClients })
                            //filter the primary users for first page display 
                            primaryClients = AllClients.filter(function (obj) {
                                return obj.userId == obj.accountId;
                            });
                            for (j = 0; j < primaryClients.length; j++) {
                                let tot = AllClients.filter(function (obj) { return obj.accountId == primaryClients[j].accountId }).length;
                                primaryClients[j].influencers = tot;
                            }

                            this.setState({ primaryClientData: primaryClients })
                        }.bind(this));

                }

            }.bind(this));
    }

    getSharedAccount = (userId) => {
        var sharedAccount = [];

        var AllClients = this.state.allClientsData;
        sharedAccount = AllClients.filter(function (obj) {
            return obj.accountId == userId;
        });
        this.setState({ sharedAccountData: sharedAccount, count: 2 })
    }

    setId = async (id, name, count) => {
        try {
            await AsyncStorage.setItem('userId', id);
            await AsyncStorage.setItem('influencerNameValue', name);
            this.setState({ userId: id })
            this.setState({ active: true })
            this.props.navigation.navigate('WelcomeSkip');
        } catch (error) {
            //console.log(error)
        }

    }

    setNavigation = async (userId, permanetUserId) => {




        //userId = await this.getParameterValue('user')
        //console.log(userId + '  value pulled from db')
        //permanetUserId = await this.getParameterValue('permanent');

        if (userId === permanetUserId) {
            this.setState({ navigation: 'ProfileUpdate' })
            //set two major flags which control the read only for floating label and dropdowns in all screens
            await AsyncStorage.setItem('editableValue', JSON.stringify(true));
            await AsyncStorage.setItem('dropdownDisabledValue', JSON.stringify(false));

            //setParameters(JSON.stringify(true),'editable');
            //setParameters(JSON.stringify(false),'dropdown');



        }
        else {
            this.setState({ navigation: 'Profile' })
            //set two major flags which control the read only for floating label and dropdowns in all screens
            await AsyncStorage.setItem('editableValue', JSON.stringify(false));
            await AsyncStorage.setItem('dropdownDisabledValue', JSON.stringify(true));

            //setParameters(JSON.stringify(false),'editable');
            //setParameters(JSON.stringify(true),'dropdown');
        }


    }



    getPersonalViewStyle = (touch, id) => {

        if (touch && this.state.permanetUserId === id) {

            return {
                backgroundColor: '#1DA4DF',
                color: '#fff',
                fontFamily: 'AspiraXXXNar-Regular',
                justifyContent: 'center',
                marginTop: 40,
                paddingTop: 15,
                borderRadius: 10,
                borderWidth: 1,
                borderColor: '#fff',
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                fontSize: 21,
                zIndex: 1,

            }

        } if (touch && this.state.permanetUserId !== id) {
            return {
                backgroundColor: '#ededed',
                color: "#727171",
                fontFamily: 'AspiraXXXNar-Regular',
                justifyContent: 'center',
                marginTop: 40,
                paddingTop: 15,
                borderRadius: 10,
                borderWidth: 1,
                borderColor: '#fff',
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                fontSize: 21,
                zIndex: 1,
            }
        }

        if (this.state.permanetUserId === id) {
            return {
                backgroundColor: '#1DA4DF',
                color: '#fff',
                fontFamily: 'AspiraXXXNar-Regular',
                justifyContent: 'center',
                marginTop: 40,
                paddingTop: 15,
                borderRadius: 20,
                borderWidth: 1,
                borderColor: '#1DA4DF',
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                fontSize: 21,
                zIndex: 1,

            }
        }

        if (this.state.permanetUserId !== id) {
            return {
                backgroundColor: '#ededed',
                color: "#727171",
                fontFamily: 'AspiraXXXNar-Regular',
                justifyContent: 'center',
                marginTop: 40,
                paddingTop: 15,
                borderRadius: 10,
                borderWidth: 1,
                borderColor: '#fff',
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                fontSize: 21,
                zIndex: 1,
            }
        }

    }
    getActiveStyle = (touched, id) => {

        if (touched && this.state.userId === id) {
            return {
                backgroundColor: "#1DA4DF",
                color: '#fff',

                fontFamily: 'AspiraXXXNar-Regular',
                fontSize: 21,
                margin: 0,
                marginTop: 0,
                // borderRadius: 10,
                paddingTop: 15,
                paddingLeft: 5,
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                zIndex: 1,
            }
        }
        if (touched && this.state.userId !== id) {
            return {
                backgroundColor: "#ededed",
                color: "#727171",

                fontFamily: 'AspiraXXXNar-Regular',
                fontSize: 21,
                margin: 0,
                marginTop: 0,
                // borderRadius: 10,
                paddingTop: 15,
                paddingLeft: 5,
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                zIndex: 1,

            }
        }
        if (this.state.userId === id) {
            return {
                backgroundColor: "#1DA4DF",
                color: '#fff',

                fontFamily: 'AspiraXXXNar-Regular',
                fontSize: 21,
                margin: 0,
                marginTop: 0,
                // borderRadius: 12,
                paddingTop: 15,
                paddingLeft: 5,
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                zIndex: 1,

            }
        }
        if (this.state.userId !== id) {
            return {
                backgroundColor: "#ededed",
                color: "#727171",
                fontFamily: 'AspiraXXXNar-Regular',
                fontSize: 21,
                margin: 0,
                marginTop: 0,
                //  borderRadius: 12,
                paddingTop: 15,
                paddingLeft: 5,
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                zIndex: 1,

            }
        }

    }

    logout = async () => {
        AsyncStorage.clear();
        setTimeout(() => {
            this.props.navigation.navigate('Login');
        }, 1000);

    }
    goback() {
        this.props.navigation.navigate('RealtorHome');
    }

    render() {
        return (
            <Container style={styles.container}>

                <View style={styles.MainContainer2}>

                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'stretch', marginTop: -100 }}>
                        <Left>
                            <TouchableOpacity onPress={() => this.goback()} >
                                <Image source={backArrow} style={styles.backArrowStyle}></Image>
                            </TouchableOpacity>
                        </Left>
                        <Body>  <Image source={launchscreenLogo1} style={styles.loginScreenLogo3} /></Body>
                        <Right></Right>
                    </View>
                    <ScrollView bounces={false} style={styles.scrollContainer} >

                        <Text style={styles.emailTextFieldBlue}>Select a Shared Account</Text>
                        {this.state.sharedAccountData && this.state.sharedAccountData.map((item, id) => {
                            return (<View style={{ alignItems: 'center' }} >
                                <TouchableOpacity onPress={() => this.setId(item.userId, item.influencerName, item.influencers)} >
                                    <Text style={this.getActiveStyle(this.state.active, item.userId)}>{item.influencerName} </Text>
                                    <Image source={(this.state.userId === item.userId) ? require('../../../assets/shareGrey.png') : require('../../../assets/share.png')} style={styles.shareImage2}></Image>
                                </TouchableOpacity>
                                <Text>{"\n"}</Text>
                            </View>
                            )
                        })}
                    </ScrollView>

                </View>

            </Container>
        )
    }
}


export default SharedAccount;
