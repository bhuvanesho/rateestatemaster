

import { Alert } from 'react-native';
var SQLite = require('react-native-sqlite-storage')
var db = SQLite.openDatabase({ name: "HomeRatingDB.db" });
import ValidationComponent from 'react-native-form-validator';
import alertMSG from '../assets/resources';
var sumProduct = require('sum-product');
import { AsyncStorage } from 'react-native';
import { database } from '../firebaseConfig/config';
export default class criteriaModel extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = {
      record: null
    }
    
    this.calculateOverallRating = this.calculateOverallRating.bind(this);
  }


  /***
     * 
     * function to create get criteria and its info to show in criteria setting 
     * 
  */


  getCriteria = async () => {
    let userId = await AsyncStorage.getItem('userId');
    try {
      let l_criteriaInterior = [];
      let l_criteriaExterior = [];
      let l_criteriaCharecteristics = [];
      let l_criteriaInteriorOther = [];
      let l_criteriaExteriorOther = [];
      let l_criteriaCharacteristicsOther = [];


      // Find  criteria matching the user id.
      var ref = database.ref('criteria/')
      ref.orderByChild('UserId')
        .startAt(userId)
        .endAt(userId)
        .on('value', function (snapshot) {
          snapshot.forEach(function (childSnap) {
            if (childSnap.val().CriteriaType == 'INTERIOR') {
              l_criteriaInterior.push({ criteriaId: childSnap.val().Id, criteriaName: childSnap.val().CriteriaName, status: childSnap.val().Status });
            }
            if (childSnap.val().CriteriaType == 'EXTERIOR') {
              l_criteriaExterior.push({ criteriaId: childSnap.val().Id, criteriaName: childSnap.val().CriteriaName, status: childSnap.val().Status });
            }
            if (childSnap.val().CriteriaType == 'CHARACTERISTICS') {
              l_criteriaCharecteristics.push({ criteriaId: childSnap.val().Id, criteriaName: childSnap.val().CriteriaName, status: childSnap.val().Status });
            }
            if (childSnap.val().CriteriaType == 'OTHER INTERIOR') {
              l_criteriaInteriorOther.push({ criteriaId: childSnap.val().Id, criteriaName: childSnap.val().CriteriaName, status: childSnap.val().Status });
            }
            if (childSnap.val().CriteriaType == 'OTHER EXTERIOR') {
              l_criteriaExteriorOther.push({ criteriaId: childSnap.val().Id, criteriaName: childSnap.val().CriteriaName, status: childSnap.val().Status });
            }
            if (childSnap.val().CriteriaType == 'OTHER CHARACTERISTICS') {
              l_criteriaCharacteristicsOther.push({ criteriaId: childSnap.val().Id, criteriaName: childSnap.val().CriteriaName, status: childSnap.val().Status });
            }
          }.bind(this));

        }.bind(this));
      this.setState({ criteriaInterior: l_criteriaInterior });
      this.setState({ criteriaExterior: l_criteriaExterior });
      this.setState({ criteriaCharacteristics: l_criteriaCharecteristics });
      this.setState({ interiorOther: l_criteriaInteriorOther });
      this.setState({ exteriorOther: l_criteriaExteriorOther });
      this.setState({ characteristicsOther: l_criteriaCharacteristicsOther });
      this.setState({ loadingSpinner: false });
      //reset the variables to avoid auto addition 
      l_criteriaInterior = [];
      l_criteriaExterior = [];
      l_criteriaCharecteristics = [];
      l_criteriaInteriorOther = [];
      l_criteriaExteriorOther = [];
      l_criteriaCharacteristicsOther = [];



    }
    catch (error) {
      alert(error);
    }




    try {
      var len = 0;
      var arr = [];

      var ref = database.ref('criteria/')
      ref.orderByChild('UserId')
        .startAt(userId)
        .endAt(userId)
        .on('value', function (snapshot) {
          snapshot.forEach(function (childSnap) {
            if (childSnap.val().Status == 'Active') {
              len = len + 1;
            }
          });
        }.bind(this));

      //len=arr.length;
      this.setState({ criteriaCount: len });
      len = 0;
    }
    catch (error) {
      alert(error);
    }
  }


  /***
   * 
   * function to update criteria name in the database
   * 
   */


  updateCriteria = async () => {
    let userId = await AsyncStorage.getItem('userId');
    try {
      let query = '';

      // Find  criteria matching the user id.
      var ref = database.ref('FilterAndSort/')
      ref.orderByChild('UserId')
        .startAt(userId)
        .endAt(userId)
        .on('value', function (snapshot) {
          snapshot.forEach(function (childSnap) {
            query = childSnap.val().filterNsortQuery;
          });
        }.bind(this));



      //var query = results.rows.item(0)['filterNsortQuery'];
      //var filterString = alertMSG.en["FilterString"];
      var filterString = "0";
      let criteriaId = this.state.criteriaId;
      if (query != filterString) {
        // Alert.alert('', alertMSG.en['FilterRefresh']);

        database.ref('criteria' + '/' + criteriaId).update(
          {
            "SortedStatus": 'Active',
          }
        )
        database.ref('FilterAndSort' + '/' + userId).update(
          {
            "filterNsortQuery": filterString,
            "Rated": 1,
            "NotRated": 1,
            "PriceFrom": 100000,
            "PriceTo": 6000000,
            "LowToHighRating": 0,
            "HighToLowRating": 1,
            "LowToHighPrice": 1,
            "HighToLowPrice": 0,
            "CheckAll": 1,
            "ParticularCriteria": 0,
            "Favourite": 1,
            "NotFavourite": 1,
          }
        )
      }
      database.ref('criteria' + '/' + criteriaId).update(
        {
          "SortedStatus": 'Active',
          "Status": this.state.criteriaStatus,
        }
      )
      this.setState({ criteriaId: null, criteriaStatus: null });

    }
    catch (error) {
      alert(error)
    }

    /** *getcriteria count */
    try {
      var len = 0;
      var ref = database.ref('criteria/')
      ref.orderByChild('UserId')
        .startAt(userId)
        .endAt(userId)
        .on('value', function (snapshot) {
          snapshot.forEach(function (childSnap) {
            if (childSnap.val().Status == 'Active') {
              len = len + 1;
            }
          });
        }.bind(this));

      //len=arr.length;
      this.setState({ criteriaCount: len });
    }
    catch (error) {
      alert(error);
    }
  }



  updateCriteriaWithParameters = async (criteriaId, criteriaStatus) => {
    let userId = await AsyncStorage.getItem('userId');
    try {
      let query = '';

      // Find  criteria matching the user id.
      var ref = database.ref('FilterAndSort/')
      ref.orderByChild('UserId')
        .startAt(userId)
        .endAt(userId)
        .on('value', function (snapshot) {
          snapshot.forEach(function (childSnap) {
            query = childSnap.val().filterNsortQuery;
          });
        }.bind(this));
      var filterString = "0";

      if (query != filterString) {


        database.ref('criteria' + '/' + criteriaId).update(
          {
            "SortedStatus": 'Active',
          }
        )
        database.ref('FilterAndSort' + '/' + userId).update(
          {
            "filterNsortQuery": filterString,
            "Rated": 1,
            "NotRated": 1,
            "PriceFrom": 100000,
            "PriceTo": 6000000,
            "LowToHighRating": 0,
            "HighToLowRating": 1,
            "LowToHighPrice": 1,
            "HighToLowPrice": 0,
            "CheckAll": 1,
            "ParticularCriteria": 0,
            "Favourite": 1,
            "NotFavourite": 1,
          }
        )
      }
      database.ref('criteria' + '/' + criteriaId).update(
        {
          "SortedStatus": 'Active',
          "Status": criteriaStatus,
        }
      )


    }
    catch (error) {
      alert(error)
    }

    /** *getcriteria count */
    try {
      var len = 0;
      var ref = database.ref('criteria/')
      ref.orderByChild('UserId')
        .startAt(userId)
        .endAt(userId)
        .on('value', function (snapshot) {
          snapshot.forEach(function (childSnap) {
            if (childSnap.val().Status == 'Active') {
              len = len + 1;
            }
          });
        }.bind(this));

      //len=arr.length;
      this.setState({ criteriaCount: len });
    }
    catch (error) {
      alert(error);
    }
  }
  /**
   * 
   * get active criteria for listing
   * 
   */
  getActiveCriteria = async () => {
    try {
      let userId = await AsyncStorage.getItem('userId');
      let criteria = [];
      var ref = database.ref('criteria/')
      ref.orderByChild('UserId')
        .startAt(userId)
        .endAt(userId)
        .once('value', function (snapshot) {
          snapshot.forEach(function (childSnap) {
            if (childSnap.val().Status == 'Active') {
              criteria.push({ criteriaId: childSnap.val().Id, criteriaName: childSnap.val().CriteriaName, status: childSnap.val().Status, weightage: childSnap.val().Weightage });
            }
          });
          //sort data from high to low
          criteria = criteria.sort((a, b) => {
            return b.weightage - a.weightage;
          });
          this.setState({ criteriaInfo: criteria, showMessage: true });
          criteria = [];
        }.bind(this));
    }
    catch (error) {
      alert(error);
    }
  }

  //Weightage Update

  weightageUpdate(id, value) {
    try {
      database.ref('criteria' + '/' + id).update(
        {
          "Weightage": value,
        }
      )

    }
    catch (error) {
      alert(error);
    }
  }

  //percentage update
  percentageUpdate(id, value) {
    try {
      if (!isNaN(value)) {
        database.ref('criteria' + '/' + id).update(
          {
            "Percentage": value,
          }
        )
      }
    }
    catch (error) {
      alert(error);
    }
  }


  //percentage update
  updateOtherCriteria = (criteriaName, criteriaId) => {
    try {
      var currentcriteria = criteriaName;
      database.ref('criteria' + '/' + criteriaId).update(
        {
          "CriteriaName": currentcriteria,
        }
      )
    }
    catch (error) {
      alert(error);
    }
  }

  //refresh/reset filter to default values

  refreshFilter = async () => {
    // db.transaction((tx) => {
    //  tx.executeSql('select * from FilterAndSort where Id=1', [], (tx, results) => {

    let userId = await AsyncStorage.getItem('userId');
    var criteriaId = this.state.criteriaId;
    var ref = database.ref('FilterAndSort/')
    ref.orderByChild('UserId')
      .startAt(userId)
      .endAt(userId)
      .once('value', function (snapshot) {
        snapshot.forEach(function (childSnap) {
          var query = childSnap.val().filterNsortQuery;
          //var filterString = alertMSG.en["FilterString"];
          var filterString = "0";

          if (query != filterString) {
            Alert.alert('', alertMSG.en['WeightageFilterRefresh']);
            database.ref('criteria' + '/' + criteriaId).update(
              {
                "SortedStatus": "Active",
              });

            database.ref('FilterAndSort' + '/' + userId).update(
              {
                "filterNsortQuery": filterString,
                "Rated": 1,
                "NotRated": 1,
                "PriceFrom": 100000,
                "PriceTo": 6000000,
                "LowToHighRating": 0,
                "HighToLowRating": 1,
                "LowToHighPrice": 1,
                "HighToLowPrice": 0,
                "CheckAll": 1,
                "ParticularCriteria": 0,
                "Favourite": 1,
                "NotFavourite": 1,
              });
          }
        });
      }.bind(this));



  }


  /****
   * 
   * recalculating  overall rating
   * 
   * 
   */



  reCalculateOverallRating = () => {
    try {
      db.transaction((tx) => {
        tx.executeSql('select * from Property', [], (tx, result1) => {
          for (let i = 0; i < result1.rows.length; i++) {
            var propertyId = result1.rows.item(i)['Id'];

            tx.executeSql('select * from Criteria where Status = "Active"', [], (tx, result2) => {
              for (let j = 0; j < result2.rows.length; j++) {
                var criteriaId = result2.rows.item(j)['Id'];
                var percentage = result2.rows.item(j)['Percentage'];
                let criteriaPercentage = [];
                let criteriaRating = [];
                tx.executeSql('select * from Ratings where CriteriaId = ' + criteriaId + '  and PropertyId = ' + propertyId + '', [], (tx, result3) => {

                  for (let k = 0; k < result3.rows.length; k++) {
                    var rating = result3.rows.item(k)['Rating'];
                    rating == null ? rating = 0 : null;
                    criteriaPercentage.push(percentage / 100);
                    criteriaRating.push(rating);
                  }
                });
              }
            });
          }
        });
      });

    }
    catch (error) {
      alert(error);
    }
  }

  /****
   * 
   * recalculating  percentage and weightages 
   * 
   */

  reCalculatePercentage = async () => {

    var criteria = [];
    let userId = await AsyncStorage.getItem('userId');

    var ref = database.ref('criteria/')
    ref.orderByChild('UserId')
      .startAt(userId)
      .endAt(userId)
      .on('value', function (snapshot) {
        snapshot.forEach(function (childSnap) {
          if (childSnap.val().Status == 'Active') {
            criteria.push({ Id: childSnap.val().Id, Weightage: childSnap.val().Weightage });
          }
        });
      }.bind(this));


    // var len = results.rows.length;
    var len = criteria.length;

    var weightage = 0;
    var sumWeightage = 0;

    for (let i = 0; i < len; i++) {
      let weightage = criteria[i]['Weightage'];
      weightage == null ? weightage = 0 : null;
      weightage == "" ? weightage = 0 : "";
      weightage = parseInt(weightage);
      sumWeightage = sumWeightage + weightage;
    }

    for (let i = 0; i < len; i++) {
      let weightage = criteria[i]['Weightage'];
      weightage == null ? weightage = 0 : null;
      weightage == "" ? weightage = 0 : "";
      let percentage = parseInt(weightage) / sumWeightage * 100;
      this.percentageUpdate(criteria[i]['Id'], percentage);
    }
    //});
    //});
  }

  /**
   * 
   * 
   * updated overall ratings for all the properties when changed in weightage screen
   */

   calculateUpdatedRatingBhuvan=async()=>
   {
    try {
      let userId = await AsyncStorage.getItem('userId');
      var propertyIds=[];
      var ref = database.ref('UserPropertyMapping/')
      ref.orderByChild('UserId')
        .startAt(userId)
        .endAt(userId)
        .once('value', function (snapshot) {
          snapshot.forEach(function (childSnap) {
            if (childSnap.val().ListingStatus != 'NEW LISTING') {
              propertyIds.push({UserPropertyMappingId:childSnap.val().Id, propertyId: childSnap.val().ListingID})
            }
            });
            for (let i = 0; i < propertyIds.length; i++) {
              this.calculateOverallRating(propertyIds[i]['UserPropertyMappingId'],propertyIds[i]['propertyId']);
            }
          }.bind(this));
          
    }
    catch (error) {
      alert(error);
    }
   }

  calculateNewRatingArun = async () => {
    try {
      var criteria = [];
      let userId = await AsyncStorage.getItem('userId');
      var ref = database.ref('criteria/')
      ref.orderByChild('UserId')
        .startAt(userId)
        .endAt(userId)
        .once('value', function (snapshot) {
          snapshot.forEach(function (childSnap) {
            if (childSnap.val().Status == 'Active') {
              criteria.push({ Id: childSnap.val().Id, Weightage: childSnap.val().Weightage });
            }
          });
        }.bind(this));




      var ref = database.ref('UserPropertyMapping/')
      let sortedCriteria = criteria;
      let sortRating = 0;
      ref.orderByChild('UserId')
        .startAt(userId)
        .endAt(userId)
        .once('value', function (snapshot) {
          snapshot.forEach(function (childSnap) {
            if (childSnap.val().ListingStatus != 'NEW LISTING') {
              let propertyId = childSnap.val().ListingID;
              let UserPropertyMappingId = childSnap.val().Id;
              let criteriaPercentage = [];
              let criteriaRating = [];
              let criteriaId;
              let rowCount = 0;
              for (let i = 0; i < sortedCriteria.length; i++) {
                // alert(this.state.sortedCriteria.length);
                criteriaId = sortedCriteria[i]['Id']
                var ref = database.ref('Rating/')
                ref.orderByChild('CriteriaId')
                  .startAt(criteriaId)
                  .endAt(criteriaId)
                  .once('value', function (snapshot1) {
                    snapshot1.forEach(function (childSnap1) {
                      if (childSnap1.val().PropertyId == propertyId) {
                        rating = childSnap1.val().Rating;
                        criteriaPercentage.push(sortedCriteria[i]['percentage'] / 100);
                        criteriaRating.push(rating);
                        rowCount = rowCount + 1;
                      }
                    }.bind(this));
                    if (rowCount == 0) {
                      rating = 0;
                      criteriaPercentage.push(sortedCriteria[i]['percentage'] / 100);
                      criteriaRating.push(rating);
                    }
                  }.bind(this));
              }
              sortRating = sumProduct(criteriaPercentage, criteriaRating);
                // console.log(propertyId  "property" +sortRating);
                //this.filterRatingPropertyArun(propertyId, sortRating);
                database.ref('UserPropertyMapping' + '/' + UserPropertyMappingId).update(
                  {
                    "OverallRating": sortRating,
                  });
            }
          }.bind(this));
          if (sortedCriteria.length == 0) {
            //this.filterRatingPropertyArun(propertyId, 0);
          }
        }.bind(this));

       

    }
    catch (error) {
      alert(error);
    }
  }


  /**
   * 
   * updating latest rating to the properties
   * 
   */

  filterRatingPropertyArun = async (propertyId, overallRating) => {
    try {

      let userId = await AsyncStorage.getItem('userId');
      let UserPropertyMappingId = '';
      var ref = database.ref('UserPropertyMapping/')
      ref.orderByChild('UserId')
        .startAt(userId)
        .endAt(userId)
        .on('value', function (snapshot1) {
          snapshot1.forEach(function (childSnap1) {
            if (childSnap1.val().ListingID == propertyId) {
              UserPropertyMappingId = childSnap1.val().Id;
            }
          }.bind(this));
        }).bind(this);

      database.ref('UserPropertyMapping' + '/' + UserPropertyMappingId).update(
        {
          "OverallRating": overallRating,
        });


    }
    catch (error) {
      alert(error);
    }
  }

  updateLoginCount = () => {

    db.transaction((tx) => {
      tx.executeSql('Update User set loginCount = 2');
    });
    this.setState({ loginCount: true });

  }

  getMaximumPropertyValue = async () => {

    let userId = await AsyncStorage.getItem('userId');
    var ref = database.ref('UserPropertyMapping/')
    let amountArr = [];
    let maxValue = 0;
    ref.orderByChild('UserId')
      .startAt(userId)
      .endAt(userId)
      .once('value', function (snapshot1) {
        snapshot1.forEach(function (childSnap1) {
          amountArr.push({ amount: childSnap1.val().Amount })
        }.bind(this));
        maxValue = Math.max.apply(Math, amountArr.map(function (o) { return o.amount; }));
        if (maxValue == NaN || maxValue == 0) {
          maxValue = 6000000;
        }
        if (this.state.maxPrice > maxValue) { this.setState({ maxPrice: maxValue }) }
        this.setState({ variablePriceMax: maxValue, vairableStep: maxValue / 10 });
      }.bind(this));
  }

  calculateOverallRating = async (UserPropertyMappingId,propertyId) => {
    try {
     
        //let rowcount = 0;
        //let propertyId = this.state.propertyDetails[0]['propertyId'];
        let criteriaPercentage = [];
        let criteriaRating = [];
        //let criteriaId;
        let userId = await AsyncStorage.getItem('userId');
        let ratingsData = [];
        var ref = database.ref('Ratings/')
        let overallRating = 0.001;
        ref.orderByChild('UserId')
          .startAt(userId)
          .endAt(userId)
          .once('value', function (snapshot) {
            snapshot.forEach(function (childSnap) {
              if (childSnap.val().PropertyId == propertyId) {
                ratingsData.push({ rCriteriaId: childSnap.val().CriteriaId, rPropertyId: childSnap.val().PropertyId, rRating: childSnap.val().Rating })
              }
            }.bind(this));
            //now loop through this array 

            //loop through the ratings for calculation
            for (let j = 0; j < ratingsData.length; j++) {
              for (let i = 0; i < this.state.criteriaDetails.length; i++) {
                if (ratingsData[j].rCriteriaId == this.state.criteriaDetails[i]['criteriaId']) {
                  rating = ratingsData[j].rRating;
                  criteriaPercentage.push(this.state.criteriaDetails[i]['percentage'] / 100);
                  criteriaRating.push(rating);
                }
              }
            }
            overallRating = sumProduct(criteriaPercentage, criteriaRating);
            if (overallRating == 0) { overallRating = 0.001 }
            this.setState({ overallRating: overallRating });
            if (overallRating != 0 && overallRating != 0.0 && overallRating != null) {

              database.ref('UserPropertyMapping' + '/' + UserPropertyMappingId).update(
                {
                  "OverallRating": overallRating.toFixed(1),
                });

            }
           /* else {
              database.ref('UserPropertyMapping' + '/' + UserPropertyMappingId).update(
                {
                  "ListingStatus": "NOT RATED",
                });
            }*/
          }.bind(this));
      
    }
    catch (error) {
      alert(error);
    }
  }
  getRatingsOnLoad = async () => {

    let userId = await AsyncStorage.getItem('userId');
    var criteriaDetails = [];

    let rowcount = 0;
    var ref = database.ref('criteria/')
    ref.orderByChild('UserId')
      .startAt(userId)
      .endAt(userId)
      .on('value', function (snapshot) {
        snapshot.forEach(function (childSnap) {
          if (childSnap.val().Status == 'Active') {
            Id = childSnap.val().Id;
            var ref = database.ref('Ratings/')
            ref.orderByChild('CriteriaId')
              .startAt(Id)
              .endAt(Id)
              .on('value', function (snapshot1) {
                snapshot1.forEach(function (childSnap1) {
                  if (childSnap1.val().PropertyId == this.state.propertyId) {
                    criteriaDetails.push({ criteriaId: childSnap.val().Id, criteriaName: childSnap.val().CriteriaName, status: childSnap.val().Status, percentage: childSnap.val().Percentage, rating: childSnap1.val().Rating, ratingId: childSnap1.val().Id });
                    rowcount = rowcount + 1
                  }
                }.bind(this));
              }.bind(this));
            if (rowcount == 0) {
              criteriaDetails.push({ criteriaId: childSnap.val().Id, criteriaName: childSnap.val().CriteriaName, status: childSnap.val().Status, percentage: childSnap.val().Percentage, rating: 0, ratingId: 0 });
            }
            rowcount = 0;
          }
        }.bind(this));

      }.bind(this));
    this.setState({ criteriaDetails: criteriaDetails });
    criteriaDetails = [];
    
  }

}