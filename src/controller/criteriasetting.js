import React, { Component } from 'react';
import { Alert, AlertIOS, Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import criteriaModel from '../model/criteriasetting';
import alertMSG from '../assets/resources';
export const { width, height } = Dimensions.get('window');
import {AsyncStorage} from 'react-native';
import CriteriaSetting from '../view/criteria/criteriasetting';
import {setParameters,getParameter} from '../view/helper'
export default class criteria extends criteriaModel {

    constructor(props) {
        super(props);
        this.state = {
            bottomHeight: 0,
            weightage: 20,
            percentage: 70,
            userId: '',
            Id: 1,
            sumWeigtage: '',
            criteriaId: null,
            criteriaName: null,
            previous: null,
            criteriaType: null,
            criteriaStatus: null,
            criteriaCount: null,
            criteriaPercentage: [],
            criteriaInfo: [],
            criteriaDetails: [],
            criteriaInterior: [],
            criteriaExterior: [],
            criteriaCharacteristics: [],
            criteriaAll:[],
            interiorOther: [],
            exteriorOther: [],
            characteristicsOther: [],
            onScreenChange: false,
            showMessage: false,
            tabBottom: 0,
            currentTab: '',
            dimensionWidth: '',
            dimensionHeight: '',
            selected: true,
            weightageSelected:true,
            charac: false,
            criteriaExceed: width > 350 ? 21 : 16,
            criteriaPercentage: '',
            criteriaRating: '',
            toolTip:false,
            editable:'',
            dropdownDisabled:'',
            influencerName:'',
            disableView:'',
            sortedCriteria: [],
            loginCount:false,
            navigatedAway : false,
        }
        this.validateCriteria = this.validateCriteria.bind(this);
        
        this.setReadOnlyFlag();
        
       
    }

    setReadOnlyFlag=async()=>{
        try{
            let dropdownDisabledValue  ='';
            let editableValue = '';
            let influencerNameValue = '';
           dropdownDisabledValue =   await AsyncStorage.getItem('dropdownDisabledValue');
           editableValue =   await AsyncStorage.getItem('editableValue');
           influencerNameValue =   await AsyncStorage.getItem('influencerNameValue');

           //dropdownDisabledValue =  getParameter('dropdown');
           //editableValue =   getParameter('editable');
           //influencerNameValue =   getParameter('influencer');


          if(editableValue=="false"){ this.setState({ disableView :"none"})}
          if(editableValue=="true"){ this.setState({ disableView :""})}
            this.setState({ editable :editableValue})
            this.setState({ dropdownDisabled :dropdownDisabledValue})
            this.setState({ influencerName :influencerNameValue})
            }catch(error){
              console.log(error)
          }
    
      }

    backCriteria = () => {
        if (!this.state.onScreenChange) {
            const { navigation } = this.props;
            navigation.goBack();

        } else
            this.criteriaBack();
        // AlertIOS.alert(
        //     '',
        //     alertMSG.en['Save'],
        //     [
        //         {
        //             text: 'Yes',
        //             onPress: () => this.criteriaBack(),
        //         },
        //         {
        //             text: 'No',
        //             onPress: () => this.props.navigation.goBack()
        //         }
        //     ],
        // );
    }

    navigateToYourCriteria=()=>{
        this.props.navigation.navigate('CriteriaSetting');
    }


    /**
     * 
     * calcaulte percentage for criteria
     * 
     */


    // calPercentage() {
    //     let criteriaInfo = this.state.criteriaInfo;
    //     let sumWeightage = 0;
    //     let id = null;
    //     let weightage
    //     for (let i = 0; i < criteriaInfo.length; i++) {
    //         id = criteriaInfo[i]['criteriaId'];
    //         weightage = parseInt(this.state['weightage_' + id] == null ? 0 : this.state['weightage_' + id]);
    //         sumWeightage = sumWeightage + weightage;
    //         this.weightageUpdate(id, weightage);

    //     }
    //     // alert(sumWeightage);
    //     for (let i = 0; i < criteriaInfo.length; i++) {
    //         let criteriaId = criteriaInfo[i]['criteriaId'];
    //         let percentage = parseInt(this.state['weightage_' + criteriaId]) / sumWeightage * 100;
    //         this.percentageUpdate(criteriaId, percentage);
    //     }

    // }




    calPercentage() {
        let criteriaInfo=[];
         criteriaInfo = this.state.criteriaInfo;
        let sumWeightage = 0;
        let id = null;
        let weightage
        let len = criteriaInfo.length;
        for (let i = 0; i < len; i++) {
            id = criteriaInfo[i]['criteriaId'];
            weightage = parseInt(this.state['weightage_' + id] == null ? 0 : this.state['weightage_' + id]);
            weightage = isNaN(weightage) ? 0 : weightage;
            
            sumWeightage = sumWeightage + weightage;
            this.weightageUpdate(id, weightage);

        }
        // alert(sumWeightage);
        for (let i = 0; i < len; i++) {
            let criteriaId = criteriaInfo[i]['criteriaId'];
            let percentage = parseInt(this.state['weightage_' + criteriaId]) / sumWeightage * 100;
            this.percentageUpdate(criteriaId, percentage);
        }

    }

    /**
     * 
     * validation function for  criteria form
     * 
     */

    validateCriteria() {
        this.validate({
            criteriaName: { required: true }
        });
        if (this.isFormValid()) {
            this.createCriteria();
        }
        if (!this.isFormValid()) {
            Alert.alert('Invalid Input', this.getErrorMessages());
        }
    }
    /**
     * 
     * validation function for  criteria weightage and percentage update
     * 
     */

    validateCriteriaWeightageUpdate() {

        this.validate({
            criteriaName: { required: true },
            weightage: { numbers: true, required: true },
            percentage: { numbers: true, required: true },
        });
        if (this.isFormValid()) {
            this.updateCriteriaWeightage();
        }
        if (!this.isFormValid()) {
            Alert.alert('Invalid Input', this.getErrorMessages());
        }
    }



    /**
     * 
     * update status of criteria --validation
     * 
     */

    validateCriteriaUpdate = () => {
        this.setState({loadingSpinner: true });
        let Id = this.state.criteriaId;
        let status = this.state.criteriaStatus;
        let count = this.state.criteriaCount;
        if (count == 10 && status == 'Inactive') {
            this.setState({criteriaStatus:"Inactive"});
            this.updateCriteria();
            Alert.alert(alertMSG.en['criteriaWarn'], alertMSG.en['Exceeded']);
           // this.getCriteria();
            //this.getCriteria('INTERIOR');
            //this.getCriteria('EXTERIOR');
           // this.getCriteria('CHARACTERISTICS');
        }
        else if (count <= 10) {
            if (this.state.criteriaName == 'Other Characteristics 1' || this.state.criteriaName == 'Other Characteristics 2' || this.state.criteriaName == 'Other Interior 1' || this.state.criteriaName == 'Other Interior 2' || this.state.criteriaName == 'Other Exterior 1' || this.state.criteriaName == 'Other Exterior 2') {
                Alert.alert(alertMSG.en['criteriaName'], alertMSG.en['criteriaInfo']);
                //this.getCriteria();
                
            }
            else {
                status == 'Inactive' ? this.setState({ criteriaStatus: 'Active' }) : this.setState({ criteriaStatus: 'Inactive' });
                //update based on criteria count
                this.updateCriteria();
                //please check this below method as it is not updating anything
                //this.reCalculateOverallRating();
               
               
                this.reCalculatePercentage();
                //setTimeout(() => {
                    this.calculateNewRatingArun();
                //}, 1000);
                
            }
        }
        this.setState({loadingSpinner: false });
    }


    validateCriteriaUpdateWithParameters=(criteriaName,criteriaId,criteriaStatus)=>{

        
        let Id = criteriaId;
        let status = criteriaStatus;
        let count = this.state.criteriaCount;
        if (count == 10 && status == 'Inactive') {
            //this.setState({criteriaStatus:"Inactive"});
            this.updateCriteria();
            Alert.alert(alertMSG.en['criteriaWarn'], alertMSG.en['Exceeded']);
          
        }
        else if (count <= 10) {
            if (criteriaName == 'Other Characteristics 1' || criteriaName == 'Other Characteristics 2' || criteriaName == 'Other Interior 1' || criteriaName == 'Other Interior 2' || criteriaName == 'Other Exterior 1' || criteriaName == 'Other Exterior 2') {
                Alert.alert(alertMSG.en['criteriaName'], alertMSG.en['criteriaInfo']);
               
                
            }
            else {
                //status == 'Inactive' ? this.setState({ criteriaStatus: 'Active' }) : this.setState({ criteriaStatus: 'Inactive' });
                status == 'Inactive' ? criteriaStatus= 'Active'  : criteriaStatus= 'Inactive' ;
               
                //update based on criteria count
                this.updateCriteriaWithParameters(criteriaId,criteriaStatus);
                //please check this below method as it is not updating anything
                //this.reCalculateOverallRating();
              
               
                this.reCalculatePercentage();
                //setTimeout(() => {
                    //this.calculateNewRatingArun();
                //}, 1000);
                this.getCriteria();

                //update the count in user db logincount to 2 to bring up the tabs
                this.updateLoginCount();

            }
        }
       


    }

    //setting weightage value through slider

    sliderweightage(id, value) {
      
        this.setState({ onScreenChange: true, ['weightage_' + id]: value })
        this.state.criteriaId=id;
        this.refreshFilter();
    }

    //navigator and insert percentage

    criteriaDone = async() => {
        let userId = await AsyncStorage.getItem('userId');
        this.calPercentage();
        this.getRatingsOnLoad();
        this.calculateUpdatedRatingBhuvan();
        //this.calculateNewRatingArun();
       
        this.setState({ selected: true });
        setTimeout(() => {
            this.props.navigation.navigate('Favourite');

        }, 500);


    }

    criteriaBack = () => {
        this.calPercentage();
        //this.calculateNewRatingArun();
        const { navigation } = this.props;
        navigation.goBack();
    }



    /***
     * updating name of criteria of others
     */

    validateCriteriaOther = (previous) => {
        reWhiteSpace = new RegExp(/^\s+$/);
        if (reWhiteSpace.test(this.state.criteriaName)) {
            Alert.alert('', alertMSG.en['Noempty']);
            var lastChar = previous.substr(previous.length - 1);
            if (lastChar != ' ')
                this.updateOtherCriteria(previous + ' ', this.state.criteriaId);
            else
                this.updateOtherCriteria(previous.trim(), this.state.criteriaId);
            this.updateOtherCriteria(previous + ' ', this.state.criteriaId);
           // this.getCriteria();
            
        }
        else if (this.state.criteriaName == null || this.state.criteriaName == '') {
            Alert.alert('', alertMSG.en['Noempty']);
            var lastChar = previous.substr(previous.length - 1);
            if (lastChar != ' ')
                this.updateOtherCriteria(previous + ' ', this.state.criteriaId);
            else
                this.updateOtherCriteria(previous.trim(), this.state.criteriaId);
                //this.getCriteria();
           
        }
        else if (this.state.criteriaName == 'Other Characteristics 1' || this.state.criteriaName == 'Other Characteristics 2' || this.state.criteriaName == 'Other Interior 1' || this.state.criteriaName == 'Other Interior 2' || this.state.criteriaName == 'Other Exterior 1' || this.state.criteriaName == 'Other Exterior 2') {
            Alert.alert(alertMSG.en['criteriaOtherHeader'], alertMSG.en['criteriaOther']);
            //this.getCriteria();
        }
        else {
            this.updateOtherCriteria(this.state.criteriaName, this.state.criteriaId);
           // this.getCriteria();
        }
    }



}
