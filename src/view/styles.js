export const { width, height } = Dimensions.get('window');
import { Dimensions } from 'react-native';

export default {

  //footer props

  footer: {backgroundColor:'#fff'},
  footerIcon: { width: width/4, height: 55 },
  footerIconReadOnly: { width: width/3, height: 55 },


  // footerbutton 


  buttonedge: {
    borderLeftWidth: 1,
    borderColor: '#25a9e0'
  },

//header style

headerText: {
  color: '#25a9e0',
  fontSize: 20,
  padding: 10,
  fontFamily: 'AspiraXXXNar-Regular'
},
header:{ width: 50.66, height: 38.66 },
loader:{ justifyContent: 'center', height: '100%' },

loadingColor:'#727171',

headerBg: {
  backgroundColor: '#fff'
},
headerBGimage: {
  width: 50,
  height: 42.3,
},

emailTextField: {
  fontSize: 21,
  color: '#727171',
  fontFamily: 'AspiraXXXNar-Regular',
  textAlign:'center',
  width:250,
  paddingTop: 5
},
emailTextFieldOrange: {
  fontSize: 21,
  color: '#F0853C',
  fontFamily: 'AspiraXXXNar-Regular',
  textAlign:'center',
  width:250,
  paddingTop: 5
},
};


