import Tabs from '../TabNavigator';

import { StackNavigator, createAppContainer } from "react-navigation";


import WelcomeScreen from '../welcomescreen/welcome';
import WelcomeScreenSkip from '../welcomescreen/welcomeSkip';
import ProfileScreen from '../profile/profileDetails';
import Filter from '../ListFavFilterSort/filter';
import Listing from '../ListFavFilterSort/listingScreen';
import Favourite from '../ListFavFilterSort/favourite';
import Profile from '../profile/profileDetails';
import YourCriteria from '../criteria/criteriasetting';
import CriteriaWeightage from '../criteria/criteriaweightage';
import PropertyDetail from '../property/propertydetails'
import Login from '../login/login';
import Somepage from '../Register/somePage';

import Home from '../personalView/index';
import Relatorhomepage from '../realtorHomePage/index';
const AppNavigator = StackNavigator(
    {
      
      Login: { screen: Login },
      YourCriteria: { screen: YourCriteria },
      CriteriaWeightage: { screen: CriteriaWeightage },
      Tab: { screen: Tabs },
      PropertyDetail: { screen: PropertyDetail },
      Filter: { screen: Filter },
      List: { screen: Listing },
      ProfileUpdate: { screen: Profile },
      Welcome: { screen: WelcomeScreen },
      WelcomeSkip:  {screen: WelcomeScreenSkip} ,
      Register: { screen: Somepage },
      Home: { screen: Home },
      Relatorhomepage: { screen: Relatorhomepage },
    },
    {
      initialRouteName: 'Tab',
      navigationOptions: { gesturesEnabled: false },
      headerMode: "none",
    },
  
  );

  
  const AppContainer = createAppContainer(AppNavigator);
export default AppContainer;