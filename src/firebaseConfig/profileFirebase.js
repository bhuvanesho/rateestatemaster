import { database } from './config'


export  const profileSaveFirebase =  (item) => {
    console.log("calling from profilefirbase",item.userId)
    var newPostKey = database.ref().child('profile').push().key;
    database.ref('profile/' + newPostKey).set({
        "id":newPostKey,
        "userId":item.userId,
        "firstName" : item.firstName,
        "lastName" : item.lastName,
        "gender" : item.selectedGender,
        "maritalStatus" :item.selectedMaritalStatus,
        "typeOfHomeDesired" : item.selectedTypeOfHomeDesired,
        "typeOfHomeDesired" : item.selectedItems.join(','),
        "rangeOfHouseHoldIncome" : item.selectedIncomeHouseHoldRange,
        "ownVehicle" : item.ownVehicle,
        "numberOfPeopleInHome" : item.selectedNumberOfPeople,
        "ageRangeOfPeopleFrom"  : item.selectedAgeFrom,
        "ageRangeOfPeopleTo" : item.selectedAgeTo,
         "selectedBudgetedPrice" : item.selectedBudgetedPrice,
        "dob" : item.chosenDate,
        "sharingAccount" : item.sharingAccount,
        "sharingUserFirstName" : item.sharingUserFirstName,
        "sharingUserLastName": item.sharingUserLastName
        


    });
};  

// function fetchData(){
	
// 	dbObj.on("value",function(snapshot){
// 		console.log(snapshot.val());
// 	},function(error){
// 		console.log("error code",error.code);
// 	}
// 	);
// }       

export const readProfileFirebaseData =() =>{
    database().ref('profile/').on('value', function (snapshot) {
        alert("from fetching data",snapshot.val())
    },function(error){
		alert("error code",error.code);
	});
}
