import React, { Component } from 'react';
import {Keyboard, ScrollView,Image, Button, TouchableOpacity, StatusBar, TextInput, KeyboardAvoidingView, StyleSheet } from 'react-native';
import {
  Container,
  View,
  Header,
  Title,
  Icon,
  Tabs,
  Tab,
  Right,
  Left,
  Text,
  Body,
  List,
  ListItem
} from "native-base";

import { MKTextField, MKColor, MKIconToggle, MKSwitch } from 'react-native-material-kit';

const Textfield = MKTextField.textfield()
  .withStyle(styles.textfield)
  .withTextInputStyle(styles.textInputStyle)
  .build();

import styles, { height } from './styles';
import criteria from '../../controller/criteriasetting';

export default class App extends criteria {
  constructor(props) {
    super(props);
    this.getCriteria();
    this.keyboardWillShow = this.keyboardWillShow.bind(this)
    this.keyboardWillHide = this.keyboardWillHide.bind(this)
  }
  componentWillMount() {
    this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardWillShow)
    this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardWillHide)
  }

  componentWillUnmount() {
    this.keyboardWillShowSub.remove()
    this.keyboardWillHideSub.remove()
  }

  keyboardWillShow = event => {
   
    this.setState({
      tabBottom: 300
    });
    this.refs.ScrollView.scrollTo(height/2);
  }

  keyboardWillHide = event => {
    // alert(1);
    this.setState({
      tabBottom: 0
    })
  }



  render() {
    var criteriaInterior = this.state.criteriaInterior;
    var criteriaExterior = this.state.criteriaExterior;
    var criteriaCharacteristics = this.state.criteriaCharacteristics;
    var interiorOther = this.state.interiorOther;
    var exteriorOther = this.state.exteriorOther;
    var characteristicsOther = this.state.characteristicsOther;
    return (
      <View style={styles.container}>
        <Header style={styles.headerBg}>
          <Body>
            <Image source={require('../../../assets/Headerlogo.png')} style={styles.headerBGimage} />
          </Body>
        </Header>
        <View style={styles.criteriaBG}>
          <View style={styles.criteriaFlex}>
            <Left />
            <Text style={styles.criteriaColor}>YOUR CRITERIA</Text>
            <Right>
              <TouchableOpacity style={styles.arrowforward} onPress={() => this.props.navigation.navigate('CriteriaWeightage')}>
                <Icon name="ios-arrow-forward" type="Ionicons" style={styles.backIcon} />
              </TouchableOpacity>
            </Right>
          </View>
        </View>
        <View style={styles.tapselectmsg}>
          <Text style={styles.tapselectTxt}>Tap to select up to 10 evaluation criteria from the below categories</Text>
        </View>
        {/* <KeyboardAvoidingView scrollEnabled={false} resetScrollToCoords={{ x: 0, y: 0 }} behavior="padding" style={styles.form}> */}
          <ScrollView 
          ref='ScrollView'>
            <View style={styles.tabView}>
              <View>
                <List
                  dataArray={criteriaInterior}
                  renderRow={criteriaInterior =>
                    <View style={styles.criteriaFlex}>
                      <View style={styles.criteriaView}>
                        <Text style={styles.tabfont}>{criteriaInterior.criteriaName.substring(0, 34)}{criteriaInterior.criteriaName.length > 34 ? '...' : null}</Text>
                      </View>
                      <View style={styles.switchFlex}>
                        <MKSwitch
                          checked={criteriaInterior.status === 'Inactive' ? false : true}
                          trackLength={styles.linelength}
                          thumbRadius={styles.togglethumb}
                          trackSize={styles.toggleline}
                          onColor={styles.ontoggle}
                          thumbOffColor={MKColor.Blue}
                          thumbOnColor={MKColor.Blue}
                          rippleColor={MKColor.Blue}
                          onPress={() => this.setState({ criteriaName: criteriaInterior.criteriaName, criteriaId: criteriaInterior.criteriaId, criteriaStatus: criteriaInterior.status })}
                          onCheckedChange={this.validateCriteriaUpdate}
                        />
                      </View>
                    </View>
                  } />
              </View>
            </View>
            <View style={styles.lineseperator} />

            <View style={styles.tabView}>
              <Text style={styles.otherFont}>Others</Text>
              <View style={styles.textFieldContainer}>
                <List dataArray={interiorOther} renderRow={interiorOther =>
                  <View style={styles.criteriaFlex}>
                    {this.state['current_' + interiorOther.criteriaId] = interiorOther.criteriaName}
                    <View style={styles.criteriaView}>
                      {/* <Text style={styles.tabfont}>{interiorOther.criteriaName.substring(0, 34)}{interiorOther.criteriaName.length > 34 ? '...' : null}</Text> */}
                      <Textfield
                      
                        maxLength={25}
                        placeholder={interiorOther.criteriaName == 'Other Interior 1' || interiorOther.criteriaName == 'Other Interior 2' ? interiorOther.criteriaName : null}
                        highlightColor={MKColor.Blue}
                        onBlur={() => this.state.criteriaName != null ? this.validateCriteriaOther(this.state['current_' + interiorOther.criteriaId]) : null}
                        onChangeText={(text) => this.setState({ criteriaName: text.trim(), criteriaId: interiorOther.criteriaId })}
                        value={interiorOther.criteriaName == 'Other Interior 1' || interiorOther.criteriaName == 'Other Interior 2' ? null : interiorOther.criteriaName} />
                    </View>

                    <View style={styles.switchFlex}>
                      <MKSwitch
                        checked={interiorOther.status === 'Inactive' ? false : true}
                        trackLength={styles.linelength}
                        thumbRadius={styles.togglethumb}
                        trackSize={styles.toggleline}
                        onColor={styles.ontoggle}
                        thumbOffColor={MKColor.Blue}
                        thumbOnColor={MKColor.Blue}
                        rippleColor={MKColor.Blue}
                        onPress={() => this.setState({ criteriaName: interiorOther.criteriaName, criteriaId: interiorOther.criteriaId, criteriaStatus: interiorOther.status })}
                        onCheckedChange={this.validateCriteriaUpdate}
                      />
                    </View>
                  </View>
                } />
              </View>
            </View>
            <View style={{paddingBottom:this.state.tabBottom}}/>
            {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('CriteriaWeightage')} style={styles.tabView}>
                                <Image style={styles.navigator} source={require('../../../assets/rightArrow.png')} />
                            </TouchableOpacity> */}
          </ScrollView>
          
      </View>
    );
  }

  _submit = () => {
    alert(`Confirmation email has been sent to ${this.state.email}`);
  };
}
