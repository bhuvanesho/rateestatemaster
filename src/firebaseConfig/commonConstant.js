export var genderValuesC =[

{
  "value":"Select",
  "Id":0,
},

  {
  "value":"Male",
  "Id":1,
},
 {
  "value":"Female",
  "Id":2,
}
];
export var maritalStautsC =[
  {
    "value":"Select",
    "Id":0,
  },
  
    {
    "value":"Single",
    "Id":1,
  },
   {
    "value":"Married",
    "Id":2,
  },
  {
   "value":"Common Law",
   "Id":3,
 }
];
export let QuestionYNC=[
  {
    "value":"Select",
    "Id":0,
  },
  {
    "value":"Yes",
    "Id":1,
  },
  {
    "value":"No",
    "Id":2,
  }
];

export let TypeOfHomeDesiredC =[
  
  {
    name:"Condo",
    Id:1,
  },
  {
    name:"Town home",
    Id:2,
  },
  {
    name:"Duplex",
    Id:3,
  },
  {
    name:"Detached",
    Id:4,
  },
  {
    name:"Estate home",
    Id:5,
  },
  {
    name:"Undecided",
    Id:6,
  }
];
export let BudgetedPriceC = [
  {
    "value":"Select",
    "Id":0,
  },
  {
    "value": "<$200,000",
    "Id": 1,
  },
  {
    "value": "$200,001 - $400,000",
    "Id": 2,
  },
  {
    "value": "$400,001 - $600,000",
    "Id": 3,
  },
  {
    "value": "$600,001 - $800,000",
    "Id": 4,
  },
  {
    "value": "$800,001 - $1,000,000",
    "Id": 5,
  },
  {
    "value": "$1,000,001 - $1,200,000",
    "Id": 6,
  },
  {
    "value": "$1,200,001 - $1,400,000",
    "Id": 7,
  },
  {
    "value": "$1,400,001 - $1,600,000",
    "Id": 8,
  },
  {
    "value": "$1,600,001 - $1,800,000",
    "Id": 9,
  },
  {
    "value": "$1,800,001 - $2,000,000",
    "Id": 10,
  },
  {
    "value": "$2,000,001 - $3,000,000",
    "Id": 11,
  },
  {
    "value": "$3,000,001 - $4,000,000",
    "Id": 12,
  },
  {
    "value": "$4,000,001 - $5,000,000",
    "Id": 13,
  },
  {
    "value": ">$5,000,000",
    "Id": 14,
  }
]

export let RangeOfHouseholdIncomeC =[
  {
    "value":"Select",
    "Id":0,
  },
  {
    "value":"< $60,000",
    "Id":1,
  },
  {
    "value":"$60,000 - $100,000",
    "Id":2,
  },
  {
    "value":"$100,000 - $150,000",
    "Id":3,
  },
  {
    "value":"$150,000 - $200,000",
    "Id":4,
  },
  {
    "value":"$200,000 - $250,000",
    "Id":5,
  },
  {
    "value":"> $250,000",
    "Id":6,
  }
];

export var data = [
    {
    Gender:{
      "Male":1,
      "Female":2,
      "Other":3
  
    },
    
    
    MaritalStatus:{
      "Single":1,
      "Married":2,
      "Common Law":3
    },
    
    QuestionYN:{
      "Yes":1,
      "No":2
    },
      RangeOfHouseholdIncome:{
      "< $60,000":1,
      "$60,000 - $100,000":2,
      "$100,000 - $150,000" :3,
      "$150,000 - $200,000":4,
      "$200,000 - $250,000": 5,
      "> $250,000":6,
      
    },
      BirthMonth:{
      "January":1,
      "February":2,
      "March":3,
      "April":4,
      "May":5,
      "June":6,
      "July":7,
      "August":8,
      "Septemper":9,
      "October":10,
      "November":11,
      "December":12
    },
      BudgetedPrice:{
      "<$200,000":1, 
      "$200,001 - $400,000":2,
      "$400,001 - $600,000":3,
      "$600,001 - $800,000":4,
      "$800,001 - $1,000,000":5,
      "$1,000,001 - $1,200,000":6,
      "$1,200,001 - $1,400,000":7,
       "$1,400,001 - $1,600,000":8,
      "$1,600,001 - $1,800,000":9,
      "$1,800,001 - $2,000,000":10,
      "$2,000,001 - $3,000,000":11,
      "$3,000,001 - $4,000,000":12,
      "$4,000,001 - $5,000,000":13, 
      ">$5,000,000":13
      
    }
  }

  
    
 ]