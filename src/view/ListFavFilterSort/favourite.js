import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ImageBackground,TextInput,Keyboard } from 'react-native'
//import { ListItem, Button, Icon } from 'react-native-elements';
import { Spinner,Card, Container, Header, Body, Content, CardItem, List, Left, Right,Icon } from 'native-base';
import styles from './styles';
import StarRating from 'react-native-star-rating';
import Property from '../../controller/Property';
import Orientation from "react-native-orientation";
 var propertyImages =[];
var prevProps=[];
var prevState=[];
import mainStyles from '../../view/styles';
import { Alert } from 'react-native';
export default class Favourite extends Property {
    constructor(props) {
        super(props);
       // this.getFavourites();
    }
    componentDidUpdate(prevProps, prevState) {
        Object.entries(this.props).forEach(([key, val]) =>
          prevProps[key] !== val && console.log(`Prop '${key}' changed`)
        );
       
        Object.entries(this.state).forEach(([key, val]) =>
          prevState[key] !== val && console.log(`State '${key}' changed`)
         
        );
       // console.log(Object.entries(this.state).length);
      }
    componentDidMount() {
        Orientation.lockToPortrait();
        this.props.navigation.addListener('willFocus', () => this.setState({ selected: true }));
        this.getHardcodedImageUrlwithToken();
        this.getFavourites("existing");
        this.getAccountUsers();
        
       

    }
    onSelect = data => {
        this.setState(data);
    };

    gotoFilter = () => {
       // if (this.state.favouriteInfo.length > 0) {
            this.props.navigation.navigate('Filter', { onSelect: this.onSelect })
       // }
        //else {
        //    Alert.alert("You cannot filter empty list");
        //}
    }

    render() {
        if (this.state.selected == true) {
            this.setState({ selected: false,loadingSpinner:true });
            this.getFavourites("existing");
            //this.setReadOnlyFlag();
            //this.getAccountUsers();
            
        }
       
       
       
        propertyImages = this.state.PropertyImages;

        var swiperImages = [];
        for (let k = 0; k < propertyImages.length; k++) {
            swiperImages.push({
                Id: propertyImages[k]['propertyId'],
                // url: 'data:image/jpeg;base64,' + propertyImages[k]['imageName']
                url: propertyImages[k]['imageName']
            })
        }
        var favourites = this.state.favouriteInfo;
        return (

            <Container style={styles.container}>
                <Header style={styles.headerBg}>
                        <Left>
                            { this.state.editable=="false" ? 
                            <Image source={require('../../../assets/Headerlogo.png')} style={mainStyles.headerBGimage} >
                            </Image> : null
                            }
                        </Left>
                        <Body>
                            { this.state.editable =="true" ? <Image source={require('../../../assets/Headerlogo.png')} style={mainStyles.headerBGimage} /> : null
                            }
                            { this.state.editable =="false" ? <Text style={mainStyles.emailTextFieldOrange}>{this.state.influencerName}</Text> : null
                            } 
                        </Body>
                        <Right>
                            { this.state.editable =="false" ? <Image source={require('../../../assets/share.png')} style={styles.shareImage2}></Image> : null} 
                        </Right>
                </Header>
                {/* <View style={[styles.filterSortHeaderBG,{flexDirection:'row'}]}>
                <View style={styles.filterSortHeaderBG}>
                        <Text style={styles.filterSortHeaderColor}>MY FAVOURITES</Text>
                       </View>
                        <View style={{flex:1,alignItems:'flex-end'}} >
                            <TouchableOpacity>
                                <Image source={require('../../../assets/filter.png')} style={{ height: 20.33, width: 22 }} resizeMode='cover' />
                            </TouchableOpacity>
                        </View>
                   
                </View> */}

                <View style={styles.propertyDetailsBG} >
                    <View style={styles.propertyFlex}>
                        <Left></Left>
                        <Text style={styles.propertyDetails}>SHOWINGS</Text>
                        <Right >
                            <TouchableOpacity onPress={() => this.gotoFilter()}>
                                <View style={styles.iconFlex}>
                                    <Image style={this.state.filterActive?styles.filterIcon:styles.filterIcon} source={this.state.filterActive?require('../../../assets/filterActive.png'):require('../../../assets/filter.png')} />

                                </View>
                            </TouchableOpacity >
                        </Right>
                    </View>
                </View>

               


                <View >
                    <View>
                        <Text></Text>
                        <View style={{flexDirection:"row"}}>
                            <View style={{flex:4}}>
                                
                                <TextInput editable={this.state.editable} required={true}
                        autoCapitalize="none"
                        placeholder="SEARCH BY Listing ID"
                        onChangeText={(text) => this.setState({ mlsId: text})}
                        style={styles.TextInputStyleClassShowings} 
                        value={this.state.mlsId}
                        />
                            </View>
                            <View style={{flex:1}}   pointerEvents={this.state.disableView}>
                                <TouchableOpacity onPress={() => this.addPropertyToUser()} style={{ justifyContent: 'flex-end' }}>
                                    <Image source={require('../../../assets/addIcon.png')} style={styles.addEmail} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>

                </View>


                <Content padder bounces={false}>
                    {favourites.map((data,index) =>
                        <TouchableOpacity key={index} onPress={() => this.props.navigation.navigate('PropertyDetail', { onSelect: this.onSelect, propId: data.propertyId })}>
                            <Card style={styles.cardBackgroundColor}>
                                <Body>
                                    <View style={styles.cardBodyFavourite}  >
                                        {swiperImages.map(swiperImages =>
                                            <View>
                                                {data.propertyId == swiperImages.Id  ?
                                                    <Image
                                                    style={styles.cardImageSize}
                                                    resizeMode="cover"
                                                    source={{ uri: !this.state['imageError' + swiperImages.Id] ? swiperImages.url : 'https://www.krsaddleshop.com/sca-dev-montblanc/img/no_image_available.jpeg?resizeid=6&resizeh=1200&resizew=1200' }}
                                                    onError={() => this.setState({ ['imageError' + swiperImages.Id]: true })}
                                                /> : null
                                                }
                                            </View>
                                        )}
                                        <View style={styles.cardContent}>
                                            <Text style={styles.cardPropertyName}> Listing ID: {data.listingId}</Text>
                                            <Text style={styles.cardPropertyName}>{data.propertyName}</Text>
                                            
                                            <Image source={data.favouriteProperty==''?require('../../../assets/favouriteGrey.png'):require('../../../assets/favouriteOrange.png')} style={styles.favHeartImage} />
                                             
                                            <Text style={styles.cardPropertyAddress}>{data.address}</Text>
                                            {/*<Text style={styles.cardPropertyDetails} numberOfLines={2}>{data.description}</Text>*/}
                                            {/* <Text style={styles.cardPropertyDetails}><Icon style={styles. dollarFont} name='dollar' type='FontAwesome' /> {this.addCommas(data.amount)}</Text> */}
                                            <Text style={styles.cardPropertyDetails}>${this.addCommas(data.amount)}</Text>

                                            {data.overallRating != null && data.overallRating != 0.0 && data.overallRating != 0 ?
                                                <View style={{ flexDirection: 'row' }}>
                                                <View style={{flex:1}}>
                                                <ImageBackground style={styles.favImage} source={require('../../../assets/Housesmall.png')} >
                                                        <View>
                                                            <Text style={styles.homePercentValue}>{data.overallRating == null ? 0 : data.overallRating}</Text>
                                                        </View>
                                                    </ImageBackground>
                                                    </View>
                                                    <View style={{flex:1}}></View>
                                                    <View style={{flex:3,top:5}}>
                                                    <StarRating
                                                        disabled={true}
                                                        emptyStar="ios-star-outline"
                                                        fullStar="ios-star"
                                                        halfStar="ios-star-half"
                                                        iconSet="Ionicons"
                                                        maxStars={5}
                                                        rating={data.overallRating}
                                                        fullStarColor={styles.starColor}
                                                        halfStarColor={styles.starColor}
                                                        emptyStarColor={styles.starColor}
                                                        halfStarEnabled
                                                        starSize={16}
                                                        starPadding={10}
                                                    />
                                                    </View>
                                                    <View style={{flex:4}}></View>
                                                </View>
                                                : <Text style={styles.newListing}>{data.listingStatus}</Text>}
                                        </View>
                                        <TouchableOpacity style={styles.listingDeleteView} onPress={this.confirmListingDelete.bind(this, data)} >
                                            <Image source={require('../../../assets/deleteButton.png')} style={styles.listingDeleteImage} />
                                        </TouchableOpacity>

                                    </View>
                                </Body>
                            </Card>
                        </TouchableOpacity>

                    )}

                    {/* <Text>{'\n'}</Text>
                    <Text>{'\n'}</Text> */}
                </Content>
                {favourites.length == 0 && !this.state.loadingSpinner?
                    <View style={styles.noFavourites}>
                        <Text style={styles.notfound}>
                            No Properties found. Please search for the property and add to the list.
                             </Text>
                    </View> : null}
                    {this.state.loadingSpinner?
                    <View style={styles.noFavourites}>
                        <Spinner color={styles.loadingColor}/>
                    </View> : null}
            </Container>


            // implemented without image without header, using ListItem component

        );
    }
}
