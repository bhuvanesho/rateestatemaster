import React, { Component } from 'react';
import { Text, View, Image, ImageBackground, TouchableOpacity } from 'react-native';
import styles from './styles';
import { Container, Content ,Spinner} from 'native-base';
import LoginController from '../../controller/Login';
import Orientation from "react-native-orientation";
 
 
export default class Welcome extends LoginController {
    constructor (props) {
        super(props);
       
    }

    componentDidMount() {
        this.welcomeName();
        
        Orientation.lockToPortrait();
        setTimeout(function () {
            this.setState({ loading: false });
          }.bind(this), 1000);
          this.setNavigation();
        }
  
       /* {this.state.welcomeName.charAt(0).toUpperCase() + this.state.welcomeName.slice(1)}  */

  

    render() {
       
        return (
           
            <Container>
                {this.state.loading ?
                
                    <View style={styles.welcomeloader}>
                        <Spinner color={styles.loadingColor} />
                    </View>
                    : <ImageBackground style={styles.welcomeBgimage} source={require("../../../assets/bluebackgroundRepeatSmall.png")}>
                        <View style={styles.welcomeScreenStyle}>
                            <Image source={require("../../../assets/welcomePageLogo.png")} style={styles.welcomeScreenLogo} />
                            <Text>{'\n'}{'\n'}</Text>
                            <Text style={styles.welcomeScreenText1}>Welcome to RateEstate!</Text>
                            <Text></Text>
                            <Text style={styles.welcomeScreenText2}>We are exicted to be part of your house search</Text>
                        </View>

                        <View style={styles.welcomeArrowAlign} >
                            <TouchableOpacity onPress={() =>{this.state.loginCount? this.props.navigation.navigate('Favourite'):this.props.navigation.navigate(this.state.navigation)} }>
                                <Image source={require('../../../assets/welcomePageArrow.png')} style={styles.welcomeArrowImage} resizeMode='cover' />
                            </TouchableOpacity>
                        </View>
                      
                    </ImageBackground>
                }
            </Container>


        );
    }

}










