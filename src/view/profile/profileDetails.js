import React, { Component } from 'react';
import { Text, View, TextInput, Image, TouchableOpacity, DatePickerIOS, Picker, TouchableWithoutFeedback, KeyboardAvoidingView, Keyboard, ScrollView,StyleSheet } from 'react-native';
import Orientation from "react-native-orientation";
import { Container, Header, Button, Left, Body, Icon, Right, Item, Input, Content, Footer, FooterTab, Label } from 'native-base';
import styles from './styles';
import { MKTextField } from 'react-native-material-kit';
import ProfileCreation from '../../controller/ProfileCreation';
import Modal from 'react-native-modal';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import FloatingLabel from 'react-native-floating-labels';
import { data,genderValuesC,  maritalStautsC , BudgetedPriceC,QuestionYNC ,TypeOfHomeDesiredC,
    RangeOfHouseholdIncomeC } from '../../firebaseConfig/commonConstant';
var moment = require('moment');
import SimplePicker from 'react-native-simple-picker';

import Autocomplete from 'react-native-autocomplete-input';
var realtorJSON = require('../../assets/realtors.json');
const API = 'https://swapi.co/api';
const sty = StyleSheet.create({
    container: {
      backgroundColor: '#d4edfc',
      flex: 1,
      padding: 16,
      
      fontSize: 21,
      color: '#25a9e0',
      fontFamily: 'AspiraXXXNar-Regular',
    },
    autocompleteContainer: {
      backgroundColor: '#d4edfc',
      borderWidth: 0,
      color: '#25a9e0',
      fontSize: 21,
      fontFamily: 'AspiraXXXNar-Regular',
      zIndex:20,

    },
    descriptionContainer: {
      flex: 1,
      justifyContent: 'center',
      fontSize: 21,
      fontFamily: 'AspiraXXXNar-Regular',
    },
    itemText: {
      fontSize: 21,
      paddingTop: 5,
      paddingBottom: 5,
      margin: 2,
      color: '#727171',
      fontFamily: 'AspiraXXXNar-Regular',
      backgroundColor: '#ffffff',
      opacity:1,
    },
    infoText: {
        flex: 1,
      padding: 16,
        backgroundColor: '#d4edfc',
        color: '#25a9e0',
      fontSize: 21,
      fontFamily: 'AspiraXXXNar-Regular',
    },
  });
  import mainStyles from '../../view/styles';
export default class ProfileDetails extends ProfileCreation {

    

    componentDidMount() {
        Orientation.lockToPortrait();
        this.props.navigation.addListener('willFocus', () => this.setState({ count: 1 }));
        //this.getAccountSharedEmail();
    
        this.getAgents();
        //convr
      //this.setState({ realtors:realtorJSON });
       // this.setState({ realtors:this.state.agents });
    }


    findRealtor(query) {
        //method called everytime when we change the value of the input
        if (query === '') {
          //if the query is null then return blank
          return [];
        }
        var realtors = this.state.realtors;
        //making a case insensitive regular expression to get similar value from the realtor json
        const regex = new RegExp(`${query.trim()}`, 'i');
        //return the filtered realtor array according the query from the input
        return realtors.filter(realtor => realtor.lastName.search(regex) >= 0);
      }

    render() {

        const { query } = this.state;
        const realtors = this.findRealtor(query);
        const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();
        
        var accountSharedEmail = this.state.accountSharedEmail;
        if (this.state.count == 1) {
           
            return (
                <Container style={styles.container}>
                    <Header style={styles.container}>
                        <Left>
                            { this.state.editable=="false" ? 
                            <Image source={require('../../../assets/Headerlogo.png')} style={mainStyles.headerBGimage} >
                            </Image> : null
                            }
                        </Left>
                        <Body>
                            { this.state.editable =="true" ? <Image source={require('../../../assets/Headerlogo.png')} style={mainStyles.headerBGimage} /> : null
                            }
                            { this.state.editable =="false" ? <Text style={mainStyles.emailTextFieldOrange}>{this.state.influencerName} <Image source={require('../../../assets/share.png')} style={styles.shareImage2}></Image></Text> : null
                            } 
                        </Body>
                           
                        <Right>
                            {this.state.showDoneButton ?
                                <TouchableOpacity onPress={this.profileSubmitDone}>
                                    <Text style={styles.submitHeader}>Done</Text>
                                </TouchableOpacity> : null
                            }
                        </Right>
                            
                        
                    </Header>
                    <View style={styles.imageBackgroundView}>
                        <View style={styles.imageView}>
                        <View
                            style={styles.empty}>
                        </View>
                        <Image source={require('../../../assets/Account.png')} style={styles.profileBgImage} resizeMode='cover' />
                        {this.state.showEmailForwardArrow ?
                            <TouchableOpacity style={styles.arrow} onPress={() => this.callEmailAdd('forward')}>
                                <Icon style={{ color: '#fff' }} name="arrow-forward" />
                            </TouchableOpacity> : <View
                                style={styles.empty}>
                            </View>
                            }
                        </View>
                        
                        <Content >
                        <View style={styles.textInputStyle1Content}>
                            <Text style={styles.emailTextFieldBlue}>SELECT YOUR AGENT</Text>
                        </View>
                        <View style={sty.container}   pointerEvents={this.state.disableView}>
                            <Autocomplete
                            autoCapitalize="none"
                            autoCorrect={false}
                            containerStyle={sty.autocompleteContainer}
                            inputContainerStyle={sty.autocompleteContainer}
                            listStyle={{position: 'relative'}}
                            //data to show in suggestion
                            data={realtors.length === 1 && comp(query, realtors[0].lastName) ? [] : realtors}
                            //default value if you want to set something in input
                            defaultValue={this.state.agentName}
                            //onchange of the text changing the state of the query which will trigger
                            //the findRealtor method to show the suggestions
                            onChangeText={text => this.setState({ query: text })}
                            placeholder={this.state.agentName=''? 'ENTER AGENT NAME (Lastname, Firstname)': this.state.agentName}
                            renderItem={({ lastName,firstName, agentId }) => (
                                //you can change the view you want to show in suggestion from here
                                <TouchableOpacity style={sty.itemText} onPress={() => this.setState({ query: lastName+', '+firstName,agentName: lastName+', '+firstName, saveAgentId:agentId })}>
                                <Text style={sty.itemText}>
                                    {lastName}, {firstName}
                                </Text>
                                </TouchableOpacity>
                               
                            )}
                            />
                           
                        </View>

                            <View style={styles.textInputStyle1Content}>
                                <Text style={styles.emailTextFieldBlue}>Share your account [Optional]</Text>
                                <Text style={styles.emailTextField}>Share your account with other users by entering their RateEstate userID / e-mail below</Text>
                            </View>

                            {accountSharedEmail.map(accountSharedEmail => (
                                <View style={{ backgroundColor: accountSharedEmail.bgColor, width: '100%' }} >
                                    {!this.state.onEmailChange ? this.state['email_' + accountSharedEmail.Id] = accountSharedEmail.value : this.state['email_' + accountSharedEmail.Id] = this.state['email_' + accountSharedEmail.Id]}
                                    {emailString = this.state['email_' + accountSharedEmail.Id]}
                                    <View style={styles.textInputStyle1Content}>
                                        {/*  <MKTextField highlightColor='#25a9e0' editable={accountSharedEmail.editable} textInputStyle={styles.emailTextField} placeholder="E mail" onTextChange={(email) => this.setState({ ['email_' + accountSharedEmail.Id]: email, onEmailChange: true, validEmail: true })} value={emailString} />
                                        */}
                                        <MKTextField highlightColor='#25a9e0' editable={this.state.editable} textInputStyle={styles.emailTextField} placeholder="E mail" onTextChange={(email) => this.setState({ ['email_' + accountSharedEmail.Id]: email, onEmailChange: true, validEmail: true })} value={emailString} />
                                        <TouchableOpacity style={styles.listingDeleteView}   onPress={()=>this.deleteEmail(accountSharedEmail.Id,accountSharedEmail.value)} >
                                            <Image source={require('../../../assets/deleteButton.png')} style={styles.listingDeleteImage} />
                                        </TouchableOpacity>

                                    </View>
                                </View>
                            ))}
                            
                            <View style={styles.textInputStyle1Content} pointerEvents={this.state.disableView}>
                                <TouchableOpacity onPress={() => this.pushEmail()} style={{ alignSelf: 'flex-end' }}>
                                    <Image source={require('../../../assets/addIcon.png')} style={styles.addEmail} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.empty}>
                            </View>

                        </Content>

                    </View>


                </Container>

            );
        }


        if (this.state.count == 2) {
            return (
                <Container style={styles.container}>
                    <Header style={styles.container}>
                    <Left>
                            { this.state.editable=="false" ? 
                            <Image source={require('../../../assets/Headerlogo.png')} style={mainStyles.headerBGimage} >
                            </Image> : null
                            }
                        </Left>
                        <Body>
                            { this.state.editable =="true" ? <Image source={require('../../../assets/Headerlogo.png')} style={mainStyles.headerBGimage} /> : null
                            }
                            { this.state.editable =="false" ? <Text style={mainStyles.emailTextField}>{this.state.influencerName}<Image source={require('../../../assets/share.png')} style={styles.shareImage2}></Image></Text> : null
                            } 
                        </Body>

                        <Right>
                            {this.state.showDoneButton ?
                                <TouchableOpacity onPress={this.profileSubmitDone}>
                                    <Text style={styles.submitHeader}>Done</Text>
                                </TouchableOpacity> : null
                            }
                        </Right>

                    </Header>

                    <View style={styles.imageBackgroundView}>
                        <View style={styles.imageView}>
                            
                            <TouchableOpacity style={styles.arrow} onPress={this.profilePageCountDecrease} >
                                <Icon style={{ color: '#fff' }} name="arrow-back" />
                            </TouchableOpacity>
                            <Image source={require('../../../assets/personalProfile.png')} style={styles.profileBgImage} resizeMode='cover' />
                            <TouchableOpacity style={styles.arrow} onPress={this.profilePageCountIncrease}>
                                <Icon style={{ color: '#fff' }} name="arrow-forward" />
                            </TouchableOpacity>

                        </View>
                        <ScrollView ref='_scrollView' style={styles.textInputStyle1Content}>
                            <FloatingLabel
                            editable={this.state.editable}
                                labelStyle={(this.state.firstName == '') ? [styles.floatinglabelStyle, { fontSize: this.state.firstNameLabelFont, color: this.state.firstNameLabelFontColor }] : styles.floatinglabelStyle2}
                                inputStyle={styles.floatingInputStyle}
                                style={[styles.floatinglabelPropertyStyle, { borderColor: this.state.firstNameBorderColor }]}
                                maxLength={25}
                                onTouchStart={() => { this.setState({ firstNameLabelFont: 15, firstNameLabelFontColor: '#25A9E0', firstNameBorderColor: '#25A9E0' }) }}
                                onBlur={() => this.setState({ firstNameLabelFont: 21, firstNameLabelFontColor: '#d3d3d3', firstNameBorderColor: '#dcdcdc' })}
                                // onChangeText={(userFirstName) => this.setState({ firstName: userFirstName, firstNameLabelFont: 21, firstNameLabelFontColor: '#d3d3d3' })} value={this.state.firstName}
                                onChangeText={(userFirstName) => { this.firstNameValidation(userFirstName) }} value={this.state.firstName}
                            >First Name</FloatingLabel>



                            <FloatingLabel
                            editable={this.state.editable}
                                //  labelStyle={(this.state.lastName=='')?[styles.floatinglabelStyle,{fontSize:this.state.firstNameLabelFont,color:this.state.firstNameLabelFontColor}]:styles.floatinglabelStyle2}
                                labelStyle={(this.state.lastName == '') ? [styles.floatinglabelStyle, { fontSize: this.state.lastNameLabelFont, color: this.state.lastNameLabelFontColor }] : [styles.floatinglabelStyle2]}
                                inputStyle={styles.floatingInputStyle}
                                style={[styles.floatinglabelPropertyStyle, { borderColor: this.state.lastNameBorderColor }]}
                                maxLength={25}
                                onTouchStart={() => { this.setState({ lastNameLabelFont: 15, lastNameLabelFontColor: '#25A9E0', lastNameBorderColor: '#25A9E0' }) }}
                                onBlur={() => this.setState({ lastNameLabelFont: 21, lastNameLabelFontColor: '#d3d3d3', lastNameBorderColor: '#dcdcdc' })}
                                //onChangeText={(userLastName) => this.setState({ lastName: userLastName, lastNameLabelFont: 21, lastNameLabelFontColor: '#d3d3d3' })} value={this.state.lastName}>
                                onChangeText={(userLastName) => this.lastNameValidation(userLastName)} value={this.state.lastName}>
                                Last Name</FloatingLabel>
                            <TouchableOpacity onPress={this.getDOBValue} disabled={JSON.parse(this.state.dropdownDisabled)}>
                                <FloatingLabel labelStyle={(this.state.chosenDate == '') ? [styles.floatinglabelStyle, { fontSize: this.state.dobLabelFont, color: this.state.dobLabelFontColor }] : styles.floatinglabelStyle2}
                                    inputStyle={styles.floatingInputStyle}
                                    style={styles.floatinglabelPropertyStyle} pointerEvents="none" value={this.state.onDateChange?moment(this.state.lastdate).format('DD-MMM-YYYY'):moment(this.state.chosenDate).format('DD/MM/YYYY') == 'Invalid date' ? '' : moment(this.state.chosenDate).format('DD-MMM-YYYY')}>Date of Birth</FloatingLabel>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={this.getGenderMethod} disabled={JSON.parse(this.state.dropdownDisabled)}>
                                <FloatingLabel labelStyle={(this.state.selectedGenderName == 'Select' || this.state.selectedGenderName == '') ? [styles.floatinglabelStyle, { fontSize: this.state.genderLabelFont, color: this.state.genderLabelFontColor }] : styles.floatinglabelStyle2}
                                    inputStyle={styles.floatingInputStyle}
                                    style={styles.floatinglabelPropertyStyle} pointerEvents="none" value={(this.state.selectedGenderName == 'Select') ? '' : this.state.selectedGenderName}>Gender</FloatingLabel>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.getMaritalMethod} disabled={JSON.parse(this.state.dropdownDisabled)}>
                                <FloatingLabel labelStyle={(this.state.selectedMaritalName == 'Select' || this.state.selectedMaritalName == '') ? [styles.floatinglabelStyle, { fontSize: this.state.maritalLabelFont, color: this.state.maritalLabelFontColor }] : styles.floatinglabelStyle2}
                                    inputStyle={styles.floatingInputStyle}
                                    style={styles.floatinglabelPropertyStyle} pointerEvents="none" value={(this.state.selectedMaritalName == 'Select' || this.state.selectedMaritalName == '') ? '' : this.state.selectedMaritalName} >Marital Status</FloatingLabel>
                            </TouchableOpacity>

                            <View style={styles.empty}>
                            </View>


                            {/* </Content> */}
                        </ScrollView>
                        {/* <View style={styles.alignArrows1} >
                            <TouchableOpacity onPress={this.profilePageCountIncrease}>
                                <Image source={require('../../../assets/rightArrow.png')} style={styles.leftRightArrows} resizeMode='cover' />
                            </TouchableOpacity>
                        </View> */}
                        <Modal backdropColor='transparent' isVisible={this.state.visibleModal === 1} onBackdropPress={this.backDrop.bind(this)} style={styles.ModalBG}>
                            {/* {this._renderModalContent()} */}
                            <View style={styles.modalBody}>

                                <View>

                                    <DatePickerIOS
                                        date={this.state.chosenDate}
                                        onDateChange={this.setDate}
                                        mode="date"
                                        maxDate={new Date()}
                                    />

                                </View>
                            </View>
                        </Modal>

                        <Modal backdropColor='transparent' isVisible={this.state.visibleModal === 2} onBackdropPress={this.backDrop.bind(this)} style={styles.ModalBG}>
                           
                            <View style={styles.modalBody}>
                                <View>
                                    <TouchableOpacity onPress={this.cancelModal}>
                                        <Text style={styles.doneButton}>Done</Text>
                                    </TouchableOpacity>
                                </View>
                                <Picker
                                    selectedValue={this.state.selectedGender}
                                    onValueChange={this.selectedGender.bind(this)}>
                                    {this.state.gender.map(item => {
                                        return (<Picker.Item label={item.value} value={item.Id} key={item.Id} />);
                                    })
                                    }
                                </Picker>

                            </View>
                        </Modal>



       

                        <Modal backdropColor='transparent' isVisible={this.state.visibleModal === 3} onBackdropPress={this.backDrop.bind(this)} style={styles.ModalBG}>

                            <View style={styles.modalBody}>

                                <View>
                                    <TouchableOpacity onPress={this.cancelModal}>
                                        <Text style={styles.doneButton}>Done</Text>
                                    </TouchableOpacity>
                                </View>

                                <Picker
                                    selectedValue={this.state.selectedMaritalStatus}
                                    onValueChange={this.selectedMaritalStatus.bind(this)}>

                                    {this.state.maritalStatus.map(item => {
                                        return (<Picker.Item label={item.value} value={item.Id} key={item.Id} />);
                                    })
                                    }
                                </Picker>

                            </View>

                        </Modal>

                    </View>

                </Container>
            );
        }
        if (this.state.count == 3) {
            return (
                <Container style={styles.container}>
                    <Header style={styles.container}>
                    <Left>
                            { this.state.editable=="false" ? 
                            <Image source={require('../../../assets/Headerlogo.png')} style={mainStyles.headerBGimage} >
                            </Image> : null
                            }
                        </Left>
                        <Body>
                            { this.state.editable =="true" ? <Image source={require('../../../assets/Headerlogo.png')} style={mainStyles.headerBGimage} /> : null
                            }
                            { this.state.editable =="false" ? <Text style={mainStyles.emailTextField}>{this.state.influencerName}<Image source={require('../../../assets/share.png')} style={styles.shareImage2}></Image></Text> : null
                            } 
                        </Body>

                        <Right>
                            {this.state.showDoneButton ?
                                <TouchableOpacity onPress={this.profileSubmitDone}>
                                    <Text style={styles.submitHeader}>Done</Text>
                                </TouchableOpacity> : null
                            }
                        </Right>

                    </Header>
                    <View style={styles.imageBackgroundView}>
                        <View style={styles.imageView}>
                            <TouchableOpacity style={styles.arrow} onPress={this.profilePageCountDecrease} >
                                <Icon style={{ color: '#fff' }} name="arrow-back" />
                            </TouchableOpacity>
                            <Image source={require('../../../assets/Property.png')} style={styles.profileBgImage} resizeMode='cover' />
                            <TouchableOpacity style={styles.arrow} onPress={this.profilePageCountIncrease}>
                                <Icon style={{ color: '#fff' }} name="arrow-forward" />
                            </TouchableOpacity>

                        </View>
                        <View   pointerEvents={this.state.disableView}>
                        <ScrollView ref='_scrollViewProperty'>
                            <Content style={styles.textInputStyle1Content}>

                                <SectionedMultiSelect
                                disabled={JSON.parse(this.state.dropdownDisabled)}
                                
                                    items={this.state.typeOfHomeDesired}
                                    uniqueKey='name'
                                    selectText='Type of Home Desired (choose all that apply)'
                                    // placeholder = 'Type of Home Desired (choose all that apply)'
                                    hideSearch={true}
                                    // showDropDowns={true}
                                    onSelectedItemsChange={this.onSelectedItemsChangeTypes}
                                    selectedItems={this.state.selectedItems}
                                    itemFontFamily={{ textAlign: 'center' }}
                                    confirmFontFamily={{ fontFamily: 'AspiraXXXNar-Regular' }}

                                />

                                <View
                                    style={{
                                        borderBottomColor: '#dcdcdc',
                                        borderBottomWidth: 2,
                                        bottom: 0
                                    }}
                                />
                                <TouchableOpacity onPress={this.getBudgetedPrice} disabled={JSON.parse(this.state.dropdownDisabled)}>
                                    <FloatingLabel labelStyle={(this.state.budgetedPriceChange == 'Select' || this.state.budgetedPriceChange == '' || this.state.budgetedPriceChange == undefined) ? [styles.floatinglabelStyle, { fontSize: this.state.budgetedPriceFont, color: this.state.budgetedPriceFontColor }] : styles.floatinglabelStyle2}
                                        inputStyle={styles.floatingInputStyle}
                                        style={styles.floatinglabelPropertyStyle} pointerEvents="none" value={(this.state.budgetedPriceChange == 'Select' || this.state.budgetedPriceChange == '' || this.state.budgetedPriceChange == undefined) ? '' : this.state.budgetedPriceChange} >Your Budgeted Price Range</FloatingLabel>
                                </TouchableOpacity>

                                <View style={styles.empty}>
                                </View>
                            </Content>
                        </ScrollView>
                    </View>

                        <Modal backdropColor='transparent' isVisible={this.state.visibleModal === 4} onBackdropPress={() => { this.setState({ visibleModal: null }) }} style={styles.ModalBG}>

                            <View style={styles.modalBody}>
                                <View>
                                    <TouchableOpacity onPress={this.cancelPropertyModal}>
                                        <Text style={styles.doneButton}>Done</Text>
                                    </TouchableOpacity>
                                </View>
                                <Picker
                                    selectedValue={this.state.selectedTypeOfHomeDesired}
                                    onValueChange={this.selectedHomeDesiredTypes.bind(this)}>

                                    {this.state.typeOfHomeDesired.map(item => {
                                        return (<Picker.Item label={item.value} value={item.Id} key={item.Id} />);
                                    })
                                    }
                                </Picker>

                            </View>
                        </Modal>
                        <Modal backdropColor='transparent' isVisible={this.state.visibleModal === 10} onBackdropPress={this.backDropProperty.bind(this)} style={styles.ModalBG}>

                            <View style={styles.modalBody}>
                                <View>
                                    <TouchableOpacity onPress={this.cancelPropertyModal}>
                                        <Text style={styles.doneButton}>Done</Text>
                                    </TouchableOpacity>
                                </View>
                                <Picker
                                    selectedValue={this.state.selectedBudgetedPrice}
                                    onValueChange={this.budgetedPriceChange.bind(this)}>
                                    {this.state.budgetedPrice.map(item => {
                                        return (<Picker.Item label={item.value} value={item.Id} key={item.Id} />);
                                    })
                                    }
                                </Picker>
                            </View>
                        </Modal>
                    </View>
                </Container>
            );
        }

        if (this.state.count == 4) {
            return (
                <Container style={styles.container}>
                    <Header style={styles.container}>
                    <Left>
                            { this.state.editable=="false" ? 
                            <Image source={require('../../../assets/Headerlogo.png')} style={mainStyles.headerBGimage} >
                            </Image> : null
                            }
                        </Left>
                        <Body>
                            { this.state.editable =="true" ? <Image source={require('../../../assets/Headerlogo.png')} style={mainStyles.headerBGimage} /> : null
                            }
                            { this.state.editable =="false" ? <Text style={mainStyles.emailTextField}>{this.state.influencerName}<Image source={require('../../../assets/share.png')} style={styles.shareImage2}></Image></Text> : null
                            } 
                        </Body>
                        <Right>
                            {this.state.showDoneButton ?
                                <TouchableOpacity onPress={() => this.callEmailAdd('Listing')}>
                                    <Text style={styles.submitHeader}>Done</Text>
                                </TouchableOpacity> : null
                            }
                        </Right>


                    </Header>
                    <View style={styles.imageBackgroundView}>
                        <View style={styles.imageView}>

                            <TouchableOpacity style={styles.arrow} onPress={this.profilePageCountDecrease} >
                                <Icon style={{ color: '#fff' }} name="arrow-back" />
                            </TouchableOpacity>
                            <Image source={require('../../../assets/Family.png')} style={styles.profileBgImage} resizeMode='cover' />
                            <TouchableOpacity style={styles.arrow} onPress={() => this.callEmailAdd('Criteria')}>
                                <Icon style={{ color: '#fff' }} name="arrow-forward" />
                            </TouchableOpacity>

                        </View>
                        <ScrollView ref='_scrollViewFamily'>
                            <Content style={styles.textInputStyle1Content}>
                                <TouchableOpacity onPress={this.getNumberOFPeople} disabled={JSON.parse(this.state.dropdownDisabled)}>
                                    <FloatingLabel labelStyle={(this.state.selectedNumberOfPeopleLabel == 'Select' || this.state.selectedNumberOfPeopleLabel == '') ? [styles.floatinglabelStyle, { fontSize: this.state.peopleInHouseFont, color: this.state.peopleInHouseFontColor }] : styles.floatinglabelStyle2}
                                        inputStyle={styles.floatingInputStyle}
                                        style={styles.floatinglabelPropertyStyle} pointerEvents="none" value={(this.state.selectedNumberOfPeopleLabel == 'Select' || this.state.selectedNumberOfPeopleLabel == '') ? '' : this.state.selectedNumberOfPeopleLabel} >Number of People in Home</FloatingLabel>
                                </TouchableOpacity>
                                <View
                                    style={{
                                        borderBottomColor: '#dcdcdc',
                                        borderBottomWidth: 2,
                                        bottom: 2
                                    }}
                                />
                                

                                <TouchableOpacity onPress={this.houseHoldRangeIncome} disabled={JSON.parse(this.state.dropdownDisabled)}>
                                    <FloatingLabel labelStyle={(this.state.incomeHouseHoldRangeLabel == 'Select' || this.state.incomeHouseHoldRangeLabel == '') ? [styles.floatinglabelStyle, { fontSize: this.state.incomeHouseFont, color: this.state.incomeHouseFontColor }] : styles.floatinglabelStyle2}
                                        inputStyle={styles.floatingInputStyle}
                                        style={styles.floatinglabelPropertyStyle} pointerEvents="none" value={(this.state.incomeHouseHoldRangeLabel == 'Select' || this.state.incomeHouseHoldRangeLabel == '') ? '' : this.state.incomeHouseHoldRangeLabel} >Range of Household Income</FloatingLabel>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={this.getVehicleStatus} disabled={JSON.parse(this.state.dropdownDisabled)}>
                                    <FloatingLabel labelStyle={(this.state.ownVehicleCount == 'Select' || this.state.ownVehicleCount == '') ? [styles.floatinglabelStyle, { fontSize: this.state.ownVehicleFont, color: this.state.ownVehicleFontColor }] : styles.floatinglabelStyle2}
                                        inputStyle={styles.floatingInputStyle}
                                        style={styles.floatinglabelPropertyStyle} pointerEvents="none" value={(this.state.ownVehicleCount == 'Select' || this.state.ownVehicleCount == '') ? '' : this.state.ownVehicleCount} >Do you Own a vehicle?</FloatingLabel>
                                </TouchableOpacity>

                                <Text style={{ color: (this.state.selectedAgeToLabel.toString() != '' && this.state.selectedAgeFromLabel.toString() != '' && this.state.selectedAgeToLabel > 0 && this.state.selectedAgeFromLabel.toString() > 0) ? '#25A9E0' : '#d3d3d3', fontFamily: 'AspiraXXXNar-Regular', fontSize: (this.state.selectedAgeToLabel.toString() != '' && this.state.selectedAgeFromLabel.toString() != '' && this.state.selectedAgeToLabel > 0 && this.state.selectedAgeFromLabel.toString() > 0) ? 15 : 21, paddingTop: 10 }}>Age Range of People living in Home</Text>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', flex: 1, paddingRight: 100 }}>
                                    <TouchableOpacity onPress={this.selectAgeLimitFrom} disabled={JSON.parse(this.state.dropdownDisabled)}>
                                        <MKTextField maxLength={3} highlightColor='#25a9e0' textInputStyle={{
                                            fontSize: 21,
                                            paddingLeft: 30,
                                            paddingRight: 35,
                                            color: '#727171',
                                            height: 23,
                                            marginTop: 9,
                                            textAlign: 'center',
                                            fontFamily: 'AspiraXXXNar-Regular', margin: 0,
                                        }} pointerEvents='none' value={(this.state.selectedAgeFromLabel.toString() == 'Select' || this.state.selectedAgeFromLabel.toString() == '' ? '' : this.state.selectedAgeFromLabel.toString())} />
                                    </TouchableOpacity>
                                    <Text style={{ marginLeft: -10, fontFamily: 'AspiraXXXNar-Regular', fontSize: 21, paddingTop: 20, color: '#c0c0c0' }}>to</Text>
                                    <TouchableOpacity onPress={this.selectAgeLimitTo} disabled={JSON.parse(this.state.dropdownDisabled)}>
                                        <MKTextField maxLength={1} highlightColor='#25a9e0' textInputStyle={{
                                            fontSize: 21,
                                            color: '#727171',
                                            height: 23,
                                            marginTop: 9,
                                            textAlign: 'center',
                                            justifyContent: 'center',
                                            paddingLeft: 30,
                                            paddingRight: 35,
                                            fontFamily: 'AspiraXXXNar-Regular', margin: 0
                                        }} pointerEvents='none' value={(this.state.selectedAgeToLabel.toString() == 'Select' || this.state.selectedAgeToLabel.toString() == '' ? '' : this.state.selectedAgeToLabel.toString())} />
                                    </TouchableOpacity>
                                </View>

                                <View style={styles.empty}>
                                </View>
                            </Content>
                        </ScrollView>


                        <Modal backdropColor='transparent' isVisible={this.state.visibleModal === 5} onBackdropPress={() => { this.setState({ visibleModal: null }) }} style={styles.ModalBG}>
                            <View style={styles.modalBody}>
                                <View>
                                    <TouchableOpacity onPress={this.cancelFamilyModal}>
                                        <Text style={styles.doneButton}>Done</Text>
                                    </TouchableOpacity>
                                </View>
                                <Picker
                                    selectedValue={this.state.selectedNumberOfPeople}
                                    onValueChange={this.selectedPeopleCount.bind(this)}>

                                    {this.state.numberOfPeople.map(item => {
                                        return (<Picker.Item label={item} value={item} key={item} />);
                                    })
                                    }
                                </Picker>

                            </View>
                        </Modal>
                        <Modal backdropColor='transparent' isVisible={this.state.visibleModal === 6} onBackdropPress={this.backDropFamily.bind(this)} style={styles.ModalBG}>
                            <View style={styles.modalBody}>

                                <View>
                                    <TouchableOpacity onPress={this.cancelFamilyModal}>
                                        <Text style={styles.doneButton}>Done</Text>
                                    </TouchableOpacity>
                                </View>
                                <Picker
                                    selectedValue={this.state.ownVehicle}
                                    onValueChange={this.selectedVehicleStatus.bind(this)}>

                                    {this.state.yesOrno.map(item => {
                                        return (<Picker.Item label={item.value} value={item.Id} key={item.Id} />);
                                    })
                                    }
                                </Picker>

                            </View>
                        </Modal>

                        <Modal backdropColor='transparent' isVisible={this.state.visibleModal === 7} onBackdropPress={this.backDropFamily.bind(this)} style={styles.ModalBG}>
                            <View style={styles.modalBody}>
                                <View>
                                    <TouchableOpacity onPress={this.cancelFamilyModal}>
                                        <Text style={styles.doneButton}>Done</Text>
                                    </TouchableOpacity>
                                </View>
                                <Picker
                                    selectedValue={this.state.selectedIncomeHouseHoldRange}
                                    onValueChange={this.houseHoldIncomeRange.bind(this)}>

                                    {this.state.rangeOfHouseHoldIncome.map(item => {
                                        return (<Picker.Item label={item.value} value={item.Id} key={item.Id} />);
                                    })
                                    }
                                </Picker>

                            </View>
                        </Modal>
                        <Modal backdropColor='transparent' isVisible={this.state.visibleModal === 8} onBackdropPress={this.backDropFamily.bind(this)} style={styles.ModalBG}>
                            <View style={styles.modalBody}>
                                <View>
                                    <TouchableOpacity onPress={this.cancelFamilyModal}>
                                        <Text style={styles.doneButton}>Done</Text>
                                    </TouchableOpacity>
                                </View>
                                <Text style={styles.modalLabel}>From Age</Text>
                                <Picker
                                    selectedValue={this.state.selectedAgeFrom}
                                    onValueChange={this.selectAgeFrom.bind(this)}>

                                    {this.state.oneTohundred.map(item => {
                                        return (<Picker.Item label={(item.value).toString()} value={item.value} key={item.value} />);
                                    })
                                    }
                                </Picker>

                            </View>
                        </Modal>
                        <Modal backdropColor='transparent' isVisible={this.state.visibleModal === 9} onBackdropPress={this.backDropFamily.bind(this)} style={styles.ModalBG}>
                            <View style={styles.modalBody}>
                                <View>
                                    <TouchableOpacity onPress={this.cancelFamilyModal}>
                                        <Text style={styles.doneButton}>Done</Text>
                                    </TouchableOpacity>
                                </View>
                                <Text style={styles.modalLabel}>To Age</Text>
                                <Picker
                                    selectedValue={this.state.selectedAgeTo}
                                    onValueChange={this.selectAgeTo.bind(this)}>

                                    {this.state.oneTohundred.map(item => {
                                        return (<Picker.Item label={(item.value).toString()} value={item.value} key={item.value} />);
                                    })
                                    }
                                </Picker>

                            </View>
                        </Modal>
                    </View>
                </Container>
            );
        }

       

    }
}










