export const { width, height } = Dimensions.get('window');
import { Dimensions } from 'react-native';
export default {

  loader: { justifyContent: 'center', height: '100%' },

  loadingColor: '#727171',

  noFavourites: {
    height: height / 1.2,
    justifyContent: 'center',
    alignItems: 'center'
  },
  notfound: {
    padding: 5,
    color: '#25a9e0',
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: 19,
    fontFamily: 'AspiraXXXNar-Regular'
  },
  containerStyle: { backgroundColor: '#fff' },
  container: {
    backgroundColor: "#FFF"
  },
  criteriafont: {
    textAlign: 'center',
    color: '#25a9e0',
    fontSize: 15.611,
    fontFamily: 'ArialNarrowMTStd'
  },
  MainContainer: {
    flex: 2,
    margin: 60,
    justifyContent: 'center',
    //alignItems: 'center',
  },
  RememberMe: {
    justifyContent: 'center',
    marginTop: 11,
    fontSize: 21,
    fontFamily: 'AspiraXXXNar-Regular',
    color: '#727171'
  },
  loginScreenLogo: {
    height: 81.33,
    width: 144,
    marginTop: -120,
  },
  TextInputStyleClass: {
    padding: 10,
    height: 40,
    borderColor: '#ededed',
    borderRadius: 5,
    backgroundColor: "#ededed"
  },
  TextInputStyleClassShowings: {
    padding: 10,
    height: 40,
    width:250,
    borderColor: '#ededed',
    borderRadius: 5,
    backgroundColor: "#ededed",
    justifyContent:'flex-start',
    marginLeft:10,
    fontFamily: 'AspiraXXXNar-Regular',
    fontSize: 19,
  },
  title: {
    color: "#727171",
    textAlign: "left",
    fontSize: 19,
    fontFamily: 'AspiraXXXNar-Bold'
  },
  SubmitButtonStyle: {
    justifyContent: 'center',
    paddingTop: 8,
    backgroundColor: '#1DA4DF',
    borderRadius: 30,
    borderWidth: 1,
    borderColor: '#fff',
    height: 34.015748031,
    width: 145.181102362,


  },
  TextStyle: {
    // marginTop: -5,
    justifyContent: 'center',
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'AspiraXXXNar-Regular',
    fontSize: 21
  },


  textInputStyle1: {
    // borderBottomWidth: 1,
    //borderBottomColor: "#d3d3d3",
    fontSize: 21,
    padding: 2,
    color: '#727171',
    fontFamily: 'AspiraXXXNar-Regular',

  },
  textInputStyle1Content: {
    flex: 1,
    margin: 20
  },

  imageIcons: {
    width: 140,
    height: 90
  },
  imageView: {
    width: '100%',
    height: 200,
    backgroundColor: '#25A9E0',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageBackgroundView: {
    flex: 1,
    flexDirection: 'column',
  },

  alignArrows1: {
    //  left:'85%',
    //flexDirection:'row'
    alignSelf: 'flex-end'
  },
  alignArrows2:
    {
      // left:'50%',
      alignSelf: 'flex-end',
      flexDirection: 'row'
    },

  profileBgImage: {
    width: 414,
    height: 210.66
  },



  //filter and sort pages

  headerBg: {
    backgroundColor: '#fff'
  },
  headerBGimage: {
    width: 50,
    height: 42.3,


  },
  filterSortHeaderBG: {
    backgroundColor: '#1DA4DF',
    height: 36,
    alignItems: 'center',
    justifyContent: 'center',
  },
  filterSortHeaderColor: {
    color: '#FFF',
    fontFamily: 'AspiraXXXNar-Bold',
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
    paddingTop: 7,
    fontSize: 18
  },
  filterCategeoryHeader: {
    backgroundColor: '#d4edfc',
    height: 30,
    justifyContent: 'center'
  },
  filterCategeoryHeaderAlign: {
    left: 25,
    fontFamily: 'AspiraXXXNar-Bold',
    // fontWeight : 'bold' ,
    paddingTop: 5.1,
    color: "#727171",

  },
  filterEvaluationCriteria: {
    flexDirection: 'column',
    marginLeft: 25,
    marginRight: 25,
    marginTop: 10,
    marginBottom: 10,
    height: height > 600 ? height / 2.1 : height / 5,
  },
  filterMarginAlign: {
    flexDirection: 'column',
    marginLeft: 25,
    marginRight: 25,
    marginTop: 10,
    marginBottom: 10
  },
  filterContentAlign: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 40

  },
  filterCheckBoxAlign: {
    alignContent: 'center',
    borderRadius: 5

  },
  filterContent: {
    marginTop: 15
  },



  //welcome screen

  welcomeBgimage: {
    width: '100%',
    height: '100%'
  },
  welcomeScreenStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  welcomeScreenLogo: {
    width: 196.66,
    height: 150.33,
    marginTop: -100
  },
  welcomeScreenText1: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 36,
    fontFamily: 'AspiraXXXNar-Regular'
  },
  welcomeScreenText2: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 23,
    fontFamily: 'AspiraXXXNar-Regular'
  },
  welcomeArrowAlign: {
    left: '83%',
    bottom: '5%'
  },
  welcomeArrowImage: {
    width: 32.66,
    height: 33.33
  },

  //listing Screen

  cardBackgroundColor: {
    backgroundColor: '#ededed',
  },
  cardImageSize:
    {
      height: 100,
      width: 144
    },
  cardBodyFavourite: { flexDirection: 'row' },
  cardContent:
    {
      width: 0,
      flexGrow: 1,
      marginLeft: 8,
      paddingTop: 10
    },
  cardPropertyName:
    {
      fontFamily: 'AspiraXXXNar-Regular',
      fontSize: 15,
      color: '#25A9E0'
    },
  cardPropertyDetails:
    {
      fontFamily: 'ArialNarrowMTStd',
      fontSize: 11,
      color: '#727171',
      top:5
    }
  ,
  newListing:
    {
      fontFamily: 'AspiraNar-LightIt',
      fontSize: 11,
      color: '#f7797f',
      top:5
    },
  cardStarImageSize:
    {
      width: 10,
      height: 9
    },
  cardBody:
    {
      flexDirection: 'row',
    },

  propertyDetailsBG: {
    backgroundColor: '#25a9e0',
    height: 36
  },
  propertyDetails: {
    color: '#FFF',
    marginTop: 8,
    alignSelf: 'center',
    fontWeight: 'bold',
    fontSize: 18,
    fontFamily: 'AspiraXXXNar-Regular'
  },
  propertyFlex: { flex: 1, flexDirection: 'row', justifyContent: 'center' },
  iconFlex: { paddingTop: 3, flex: 1, flexDirection: 'row' },
  locationIcon: { marginRight: 5, width: 17, height: 28 },
  favouriteIcon: { marginRight: 5, width: 28, height: 28 },
  filterIcon: { marginRight: 5, width: 28, height: 28 },
  filterActiveIcon: { tintColor: 'rgb(255,127,39)', marginRight: 5, width: 28, height: 28 },
  submitHeader:
    {
      color: '#25a9e0',
      fontFamily: 'AspiraXXXNar-Regular',
      fontSize: 21,
      paddingTop: 5
    },
  leftRightArrows:
    {
      width: 30.66,
      height: 31.66,
      marginRight: 20
    },
  addEmail:
    {
      margin:10,
      width: 48,
      height: 18.66
    },
  tabHeader: {
    //fontSize :18.61,

    color: '#25a9e0', fontWeight: 'bold', fontSize: 16.235, fontFamily: 'AspiraXXXNar-Regular'
  },
  filterFont: {
    fontFamily: 'AspiraXXXNar-Regular',
    fontSize: 16.23,
    color: "#727171",
  },
  //tab styles
  tabUnderline: {
    borderBottomColor: '#25a9e0',
  },
  activetabheaderfont: { paddingTop: 2, color: '#25a9e0', fontWeight: 'bold', fontSize: 16.235, fontFamily: 'AspiraXXXNar-Regular' },
  tabheaderfont: { paddingTop: 2, color: '#727171', fontWeight: 'bold', fontSize: 16.235, fontFamily: 'AspiraXXXNar-Regular', },
  tabUnderline: { borderColor: '#25a9e0', borderWidth: 2 },

  tabBgc: { backgroundColor: '#fff' },
  tabHeight: { height: 40 },

  rangefont: {
    color: '#727171',
    fontSize: 15.611,
    fontFamily: 'ArialNarrowMTStd',
    paddingTop: 3
  },
  starColor: '#25a9e0',
  homePercentValue:
    {
      fontFamily: 'ArialNarrowMTStd',
      fontSize: 12,
      color: '#fff',
      marginTop: 10,
      textAlign: 'center',
      fontWeight: '800'
    },
  favImage: { width: 24, height: 23, bottom: 0, position: "absolute", left: 0, top:1 },
  favHeartImage: { position: 'absolute', right: 5, top: 5, width: 90 / 3.5, height: 90 / 3.5 },
  listingDeleteImage: { right:4, width: 34/2, height: 42/2 },
  listingDeleteView: { position: 'absolute', paddingRight: 4, paddingBottom: 4, right: 0, bottom: 0, },
  cardPropertyAddress:
    {
      fontFamily: 'ArialNarrowMTStd',
      fontSize: 11,
      color: '#b8bbc1',
    },
  dollarFont: {
    
    color: '#727171',
    fontSize: 11,
    margin: 5,
    fontFamily: 'ArialNarrowMTStd'

  },

  shareImage2: {
    width: 30,
    height: 30,
    justifyContent:'flex-end',
  },

};