export const { width, height } = Dimensions.get('window');
import { Dimensions } from 'react-native';
import { Left } from 'native-base';
export default {
  header: {
    paddingTop: 20 ,
    padding: 20,
    backgroundColor: '#336699',
  },
  description: {
    fontSize: 14,
    color: 'white',
  },
  tooltipSettingContainer:{zIndex:5,flexDirection:'row',justifyContent:'space-between',width:'100%',top:'60%', position:'absolute', backgroundColor: '#ededed', borderRadius: 5 },
  tooltipContainer:{zIndex:5,flexDirection:'row',justifyContent:'space-between',width:'180%',padding:5,top:'100%', position:'absolute', backgroundColor: '#ededed', borderRadius: 5 },
  toolTip: {
    textAlign:'center',
    color: '#727171',
    fontSize: 15.611,
    fontFamily: 'ArialNarrowMTStd'
  },
  toolTipClose:{
    color: '#727171',
    fontSize: 30.611,
    fontFamily: 'ArialNarrowMTStd'
  },
  input: {
    margin: 20,
    marginBottom: 0,
    height: 34,
    paddingHorizontal: 10,
    borderRadius: 4,
    borderColor: '#ccc',
    borderWidth: 1,
    fontSize: 16,
  },
  legal: {
    margin: 10,
    color: '#333',
    fontSize: 12,
    textAlign: 'center',
  },
  form: {
    flex: 1,
    justifyContent: 'space-between',
  },
  noCriteria: {
    height: height / 1.2,
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: { flex: 1, backgroundColor: '#fff' },
  arrowback: { paddingLeft: 10, paddingRight: 20 },
  arrowforward: { paddingLeft: 20, paddingRight: 10 },
  submitHeader:
  {
    color: '#25a9e0',
    fontFamily: 'AspiraXXXNar-Regular',
    fontSize: 21,
    paddingTop: 5
  },
  headerBg: {
    backgroundColor: '#fff'
  },
  headerBGimage: {
    width: 50,
    height: 42.3,
  },

  emailTextField: {
    fontSize: 21,
    color: '#727171',
    fontFamily: 'AspiraXXXNar-Regular',
    textAlign:'center',
    width:250,
    paddingTop: 5
  },
  //footer props

  footer: {
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderColor: '#25a9e0'
  },
  footerIcon: {
    color: '#25a9e0'
  },
  footerText: {
    color: '#25a9e0',
    fontfamily: 'AspiraXXXNar-Regular'
  },


  // footerbutton 


  buttonedge: {
    borderLeftWidth: 1,
    borderColor: '#25a9e0'
  },

  //swipable tab styles
  activetabheaderfont: {
    color: '#25a9e0',
    fontWeight: 'bold',
    fontSize: width > 320 ? 16.235 : 14.235,
    fontFamily: 'AspiraXXXNar-Regular'
  },
  tabheaderfont: {
    color: '#727171',
    fontWeight: 'bold',
    fontSize: width > 320 ? 16.235 : 14.235,
    fontFamily: 'AspiraXXXNar-Regular'
  },
  tabUnderline: { borderColor: '#25a9e0', borderWidth: 1.5 }
  ,
  tabView: {
    flex:2,
    padding:20,
  },
  weightageContainer: {
    paddingRight: 15,
    paddingLeft: 15,
    paddingTop: 2
  },
  weightageView: {
    flexDirection:'row',
    padding: 10,
  },
  weightagefont: {
    color: '#25a9e0',
    //textDecorationLine: 'underline',
    fontSize: 18.611,
    fontFamily: 'AspiraXXXNar-Regular'
  },
  otherFont: {
    color: '#25a9e0',
    fontSize: 18.611,
    fontFamily: 'AspiraXXXNar-Regular'
  },
  newrangefont: {
    color: '#727171',
    fontWeight: '500',
    fontSize: 18.611,
    margin: 5,
    fontFamily: 'AspiraXXXNar-Regular'
  },
  dot: {
    color: '#727171',
    fontSize: 18.611,
    marginTop:10,
    marginLeft:2,
    fontFamily: 'AspiraXXXNar-Regular'
  },
  tabnameActivefont: {
    color: 'black',
    fontSize: 18.611,
    margin: 5,
    fontFamily: 'AspiraXXXNar-Regular',
    marginTop:13
  },
  tabfont: {
    color: '#727171',
    fontSize: 18.611,
    margin: 5,
    fontFamily: 'AspiraXXXNar-Regular',
    marginTop:13
  },
  tabweightagefont: {
    color: '#727171',
    fontSize: 18.611,
    margin: 5,
    fontFamily: 'AspiraXXXNar-Regular',
  },
  tapselectTxt: {
    color: '#727171',
    fontSize: 19,
    alignSelf: 'center',
    fontFamily: 'AspiraXXXNar-Regular'
  },
  tapselectmsg: {
    padding: 5,
    backgroundColor: '#fff'
  },
  //Text fields 

  textfield: {
    width: 260,
  },
  textFieldContainer: {
  },
  textInputStyle: { flex: 1, fontSize: 18.611, fontFamily: 'AspiraXXXNar-Regular' },
  // line seperator 
  lineseperator: { borderColor: '#25a9e0', borderWidth: 0.5 },

  //TextField styles

  highlightTextfield: { color: '#25a9e0', },

  //togglestyles

  ontoggle: "#ccf2ff",

  toggleline: 15,

  togglethumb: 6,

  linelength: 40,

  //flex criteria

  criteriaFlex: { flex: 2, flexDirection: 'row' },
  otherFlex: { flex: 0.3, flexDirection: 'row' },

  //switch flex
  switchFlex: { flex: 1, flexDirection: 'row', justifyContent: 'flex-end',paddingTop:-2 },

  //slider styles
  sliderFlex: { paddingTop: 5, flex: 1, flexDirection: 'row', justifyContent: 'flex-end' },

  slidertorange: { paddingTop: 3.6 },
  sliderwidth: { width: width > 320 ? width / 2.4 : width / 3 },
  slidercolor: '#25a9e0',

  textSwitch: { paddingTop: 20, flex: 1, flexDirection: 'row', justifyContent: 'flex-end' },

  criteriaView: { flexDirection:'row', justifyContent: 'center',paddingTop:3 },
  navigator: {
    alignSelf: 'flex-end',
    width: 30.66,
    height: 31.66
  },
  weightageBg: { backgroundColor: '#fff' },



  //scale msg font

  scalemsg: {
    padding: 5,
    color: '#727171',
    textAlign: 'center',
    fontSize: 19,
    fontFamily: 'AspiraXXXNar-Regular'
  },
  notfound: {
    padding: 5,
    color: '#25a9e0',
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: 19,
    fontFamily: 'AspiraXXXNar-Regular'
  },

  //criteria heading styles

  criteriaBG: {
    backgroundColor: '#25a9e0',
    height: 36,
    alignItems: 'center',
    justifyContent: 'center'
  },
  criteriaColor: {
    color: '#FFF',
    marginTop: 10,
    fontWeight: 'bold',
    fontSize: 18,
    fontFamily: 'AspiraXXXNar-Regular'
  },
  evaluationCriteria: {
    color: '#FFF',
    marginTop: 10,
    fontWeight: 'bold',
    fontSize: 18,
    fontFamily: 'AspiraXXXNar-Regular'
  },
  backIcon: {
    color: '#fff',
  },
  backNav: {
    padding: 5,
    marginBottom: 20
  }
  ,
  noFavourites: {
    height: height / 1.2,
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor:'green'
  },
  loadingColor: '#727171',
  //tab color,
  tabBgc: { flex:1,backgroundColor: '#fff' },
  tabHeight: { height: 30 },
  shareImage2: {
    width: 30,
    height: 30,
    justifyContent:'flex-end',
  },
};
