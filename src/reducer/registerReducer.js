import { REGISTER_ATTEMPT , REGISTER_SUCCESS, REGISTER_FAILED } from '../actions/actionType';

const INITIAL_STATE = { loading: false, register: null, error: '' };


export default  registerReducer = (state = INITIAL_STATE, action) =>{
    switch (action.type) {
        case REGISTER_ATTEMPT:
            return { ...INITIAL_STATE, loading: true };
        case REGISTER_SUCCESS: 
            return { ...INITIAL_STATE, register: action.payload };
        case REGISTER_FAILED:
            return { ...INITIAL_STATE, error: 'Register Failed' };
        default: return state;
    }
};