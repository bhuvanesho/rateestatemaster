import { LOGIN_ATTEMPT , LOGIN_SUCCESS, LOGIN_FAILED, LOGIN_DATA , ERROR_MESSAGE} from './actionType';

import * as firebase from "firebase";


export const signIn = (username, password) => dispatch =>{
   //console.log(username, password)
    firebase.auth().signInWithEmailAndPassword(username, password).then(function(user) {
       // console.log(user.user.uid);

      dispatch({
        type: LOGIN_DATA,
        id:user.user.uid,
        payload: {
            status:true,
            message:"" 
        }
      })
       return true;
       
     }).catch(function(error) {
        var errorMessage = error.message;
        dispatch({
            type: ERROR_MESSAGE,
            id:null,
            payload: {
                status:false,
                message:errorMessage 
            }
          })
        var errorCode = error.code;
        
        console.log(errorMessage);
        return false ;
      });
}

// export const login = () => dispatch =>{
//     var ref = database.ref("User");
//     ref.orderByKey().on("child_added", function(snapshot) {
//         const obj = snapshot.val();

//       dispatch({
//             type: LOGIN_DATA,
//             id:obj.userId,
//             payload: obj
//           })
//          // AsyncStorage.setItem('user_info', JSON.stringify(obj.id))
//         //  .catch(error => dispatch({
//         //     type: ERROR_MESSAGE,
//         //     payload: error
//         //   }));
//         //   .catch((error) =>{
//         //     dispatch({ type: ERROR_MESSAGE, 
//         //         payload: error 
//         //     });
//         //   })
       
//     })
// }


export const loggedIn = (user) => {
    return ({ type: LOGIN_SUCCESS, payload: user });
}