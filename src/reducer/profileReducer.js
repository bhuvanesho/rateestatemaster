import { CREATE_PROFILE_ATTEMPT, CREATE_PROFILE ,GET_PROFILE_DATA,
    PROFILE_ERROR_MESSAGE,
    UPDATE_PROFILE  } from '../actions/actionType';

const INITIAL_STATE = { 
    loading: false, 
    profile: null, 
    userData : [],
    updatedData: [],
    error: '' 
};


export default  profileReducer = (state = INITIAL_STATE, action) =>{
    //console.log("profile reducer ",action.payload);
    // console.log(action.type)
    switch (action.type) {
        // case CREATE_PROFILE_ATTEMPT:
        //     return { ...INITIAL_STATE, loading: true };
        case GET_PROFILE_DATA: 
        if ( action && action.payload ) {
            return { ...INITIAL_STATE, profile: action.payload  };
        }
        case CREATE_PROFILE:
        return { ...INITIAL_STATE, userData: action.payload  };
        case UPDATE_PROFILE:
        return { ...INITIAL_STATE, updatedData: action.payload  };
            // return { ...INITIAL_STATE, profile: action.payload };
        case PROFILE_ERROR_MESSAGE:
            return { ...INITIAL_STATE, error: 'Profile Creation Failed' };
        default: return state;
    }
};