
import criteria from '../../controller/criteriasetting';
import { KeyboardAwareScrollView as ScrollView } from 'react-native-keyboard-aware-scroll-view'
import React, { Component } from "react";
import { Keyboard, ScrollView as SV, Image, TouchableOpacity, KeyboardAvoidingView } from 'react-native';
import {
    Container,
    View,
    Header,
    Title,
    Button,
    Icon,
    Tabs,
    Tab,
    Right,
    Left,
    Text,
    Body,
    List,
    ListItem,
    Content,Spinner
} from "native-base";
import { MKTextField, MKColor, MKIconToggle, MKSwitch } from 'react-native-material-kit';
import Orientation from "react-native-orientation";

import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');

const Textfield = MKTextField.textfield()
    .withStyle(styles.textfield)
    .withTextInputStyle(styles.textInputStyle)
    .build();

import styles from './styles';
import mainStyles from '../../view/styles';
var prevProps=[];
var prevState=[];
/**
* criteria screen with all type criteria in tab
*
*/

class CriteriaScreen extends criteria {
    constructor(props) {
        super(props);
       
        this.keyboardWillShow = this.keyboardWillShow.bind(this)
        this.keyboardWillHide = this.keyboardWillHide.bind(this)
        if (this.state.selected) {
            this.setState({ activeTabValue: 0, selected: false,loadingSpinner:true });
            this.getCriteria();
        }
       
    }
   

    componentDidUpdate(prevProps, prevState) {
        Object.entries(this.props).forEach(([key, val]) =>
          prevProps[key] !== val && console.log(`Prop '${key}' changed`)
        );
       
        Object.entries(this.state).forEach(([key, val]) =>
          prevState[key] !== val && console.log(`State '${key}' changed`)
         
        );
        //console.log(Object.entries(this.state).length);
      }

    componentWillMount() {
        Orientation.lockToPortrait();
        this.keyboardWillShowSub = Keyboard.addListener('keyboardWillChangeFrame', this.keyboardWillShow)
        this.keyboardWillHideSub = Keyboard.addListener('keyboardWillChangeFrame', this.keyboardWillHide)
    }

    componentWillUnmount() {
        this.keyboardWillShowSub.remove()
        this.keyboardWillHideSub.remove()
    }
    keyboardWillShow = event => {
        if (this.state.currentTab == "Exterior") {
            this.setState({
                tabBottom: height / 4
            });
            this.refs.Exterior.scrollTo(height > 600 ? this.state.dimensionHeight / 2.6 : this.state.dimensionHeight - 70);
        }
    }

    keyboardWillHide = event => {
        if (this.state.currentTab == "Exterior") {
            this.setState({
                tabBottom: 0
            })
            this.refs.Exterior.scrollToEnd(0);
        }
    }

    find_dimesions(layout) {
        const { x, y, width, height } = layout;
        this.setState({ dimensionHeight: height });
    }

    hideToolTip() {
        var criteriaCharacteristics = this.state.criteriaCharacteristics;
        for (let i = 0; i < criteriaCharacteristics.length; i++) {
            this.setState({ ['toolTip_' + criteriaCharacteristics[i]['criteriaId']]: false });
        }
    }

    componentDidMount() {
        this.props.navigation.addListener('willFocus', () => this.setState({ selected: true }));
        setTimeout(function () {
            this.getCriteria();
        }.bind(this), 1000);
       
    }
    _scrollToInput(reactNode) {
        this.scroll.scrollToFocusedInput(reactNode)
    }

    gotoWeightage=()=>{
        this.getActiveCriteria();
        this.setState({
            navigatedAway : true
          },
          () => {
            this.props.navigation.navigate('CriteriaWeightage');
          }
        );
    }
    render() {
        if (this.state.selected) {
            this.setState({ activeTabValue: 0, selected: false,loadingSpinner:true });
            this.getCriteria();
        }
        
        var criteriaInterior = this.state.criteriaInterior;
        var criteriaExterior = this.state.criteriaExterior;
        var criteriaCharacteristics = this.state.criteriaCharacteristics;
        var interiorOther = this.state.interiorOther;
        var exteriorOther = this.state.exteriorOther;
        var characteristicsOther = this.state.characteristicsOther;
        
        return (
           
            <Container style={styles.container}>
                <Header style={styles.headerBg}>
                        <Left>
                            { this.state.editable=="false" ? 
                            <Image source={require('../../../assets/Headerlogo.png')} style={mainStyles.headerBGimage} >
                            </Image> : null
                            }
                        </Left>
                        <Body>
                            { this.state.editable =="true" ? <Image source={require('../../../assets/Headerlogo.png')} style={mainStyles.headerBGimage} /> : null
                            }
                            { this.state.editable =="false" ? <Text style={mainStyles.emailTextFieldOrange}>{this.state.influencerName}&nbsp;&nbsp;<Image source={require('../../../assets/share.png')} style={styles.shareImage2}></Image></Text>  : null
                            } 
                        </Body>
                    <Right></Right>
                </Header>
                <View style={styles.criteriaBG}>
                    <View style={styles.criteriaFlex}>
                        <Left />
                        <Text style={styles.criteriaColor}>YOUR CRITERIA</Text>
                        <Right>
                            <TouchableOpacity style={styles.arrowforward} onPress={() => this.gotoWeightage()}>
                                <Icon name="ios-arrow-forward" type="Ionicons" style={styles.backIcon} />
                            </TouchableOpacity>
                        </Right>
                    </View>
                </View>
                <View style={styles.tapselectmsg}>
                    <Text style={styles.tapselectTxt}>Tap to select up to 10 evaluation criteria </Text>
                    {/* from the below categories */}
                </View>
                <Tabs initialPage={0.01} page={this.state.activeTabValue} onChangeTab={({ i }) => this.setState({ activeTabValue: i })} tabContainerStyle={styles.tabHeight} style={{ elevation: 0 }} tabBarUnderlineStyle={styles.tabUnderline}  >
                    {/* <Tabs initialPage={this.state.activeTabValue} onChangeTab={({ i, ref, from }) => this.setState({ activeTabValue: i })} tabContainerStyle={styles.tabHeight} style={{ elevation: 0 }} tabBarUnderlineStyle={styles.tabUnderline}> */}
                    <Tab tabStyle={styles.tabBgc} activeTabStyle={styles.tabBgc} heading="INTERIOR" textStyle={styles.tabheaderfont} activeTextStyle={styles.activetabheaderfont}>
                        <Content bounces={false} innerRef={ref => { this.scroll = ref }} >
                            <ScrollView >
                                <View style={styles.tabView} pointerEvents={this.state.disableView}>
                                    <View>
                                        <List
                                            dataArray={criteriaInterior}
                                            renderRow={criteriaInterior =>
                                                <View style={styles.criteriaFlex} key={criteriaInterior.criteriaId}>
                                                    <View style={styles.criteriaView}>
                                                        <Text style={criteriaInterior.status === 'Inactive' ? styles.tabfont : styles.tabnameActivefont}>{criteriaInterior.criteriaName.substring(0, 34)} <TouchableOpacity>
                                                            <Text>{criteriaInterior.criteriaName.length > 34 ? '...' : null} </Text>
                                                        </TouchableOpacity>
                                                        </Text>
                                                    </View>
                                                    <View style={styles.switchFlex}>
                                                        <MKSwitch
                                                            checked={criteriaInterior.status === 'Inactive' ? false : true}
                                                            trackLength={styles.linelength}
                                                            thumbRadius={styles.togglethumb}
                                                            trackSize={styles.toggleline}
                                                            onColor={styles.ontoggle}
                                                            thumbOffColor={criteriaInterior.status == 'Inactive' ? "#fff" : MKColor.Blue}
                                                            thumbOnColor={criteriaInterior.status == 'Inactive' ? "#fff" : MKColor.Blue}
                                                            rippleColor={MKColor.Blue}
                                                            //onPress={() => this.setState({ criteriaName: criteriaInterior.criteriaName, criteriaId: criteriaInterior.criteriaId, criteriaStatus: criteriaInterior.status })}
                                                            onCheckedChange={() =>this.validateCriteriaUpdateWithParameters(criteriaInterior.criteriaName,criteriaInterior.criteriaId,criteriaInterior.status)}
                                                        />
                                                    </View>
                                                </View>
                                            } />
                                    </View>
                                </View>
                                <View style={styles.lineseperator} />

                                <View style={styles.tabView}  pointerEvents={this.state.disableView}>
                                    <Text style={styles.otherFont}>Others</Text>
                                    <View style={styles.textFieldContainer}>
                                        <List dataArray={interiorOther} renderRow={interiorOther =>
                                            <View style={styles.criteriaFlex}>
                                                {this.state['current_' + interiorOther.criteriaId] = interiorOther.criteriaName}
                                                <View style={styles.criteriaView}>
                                                    {/* <Text style={styles.tabfont}>{interiorOther.criteriaName.substring(0, 34)}{interiorOther.criteriaName.length > 34 ? '...' : null}</Text> */}
                                                    <Textfield
                                                        //onFocus={(event) => {
                                                            // `bind` the function if you're using ES6 classes
                                                           // this._scrollToInput(ReactNative.findNodeHandle(event.target))
                                                       // }}
                                                        onFocus={() => this.setState({ currentTab: 'Interior' })}
                                                        maxLength={25}
                                                        placeholder={interiorOther.criteriaName == 'Other Interior 1' || interiorOther.criteriaName == 'Other Interior 2' ? interiorOther.criteriaName : null}
                                                        highlightColor={MKColor.Blue}
                                                        onBlur={() => this.state.criteriaName != null ? this.validateCriteriaOther(this.state['current_' + interiorOther.criteriaId]) : null}
                                                        onChangeText={(text) => this.setState({ criteriaName: text.trim(), criteriaId: interiorOther.criteriaId })}
                                                        value={interiorOther.criteriaName == 'Other Interior 1' || interiorOther.criteriaName == 'Other Interior 2' ? null : interiorOther.criteriaName} />
                                                </View>

                                                <View style={styles.switchFlex}>
                                                    <MKSwitch
                                                        checked={interiorOther.status === 'Inactive' ? false : true}
                                                        trackLength={styles.linelength}
                                                        thumbRadius={styles.togglethumb}
                                                        trackSize={styles.toggleline}
                                                        onColor={styles.ontoggle}
                                                        thumbOffColor={interiorOther.status == 'Inactive' ? "#fff" : MKColor.Blue}
                                                        thumbOnColor={interiorOther.status == 'Inactive' ? "#fff" : MKColor.Blue}
                                                        rippleColor={MKColor.Blue}
                                                        //onPress={() => this.setState({ criteriaName: interiorOther.criteriaName, criteriaId: interiorOther.criteriaId, criteriaStatus: interiorOther.status })}
                                                        //onCheckedChange={this.validateCriteriaUpdate}
                                                        onCheckedChange={() =>this.validateCriteriaUpdateWithParameters(this.state.criteriaName,interiorOther.criteriaId,interiorOther.status)}
                                                        
                                                    />
                                                </View>
                                            </View>
                                        } />
                                    </View>
                                </View>
                               
                            </ScrollView>
                            {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('CriteriaWeightage')} style={styles.tabView}>
<Image style={styles.navigator} source={require('../../../assets/rightArrow.png')} />
</TouchableOpacity> */}
                        </Content>
                    </Tab>
                    <Tab tabStyle={styles.tabBgc} activeTabStyle={styles.tabBgc} heading="EXTERIOR" textStyle={styles.tabheaderfont} activeTextStyle={styles.activetabheaderfont}>
                        <Content bounces={false}>
                            <SV bounces={false} ref='Exterior' onLayout={(event) => { this.find_dimesions(event.nativeEvent.layout) }}>
                                <View style={styles.tabView}  pointerEvents={this.state.disableView}>
                                    <View>
                                        <List bounces={false} dataArray={criteriaExterior} renderRow={criteriaExterior =>
                                            <View style={styles.criteriaFlex}>
                                                <View style={styles.criteriaView}>
                                                    <Text style={criteriaExterior.status == 'Inactive' ? styles.tabfont : styles.tabnameActivefont}>{criteriaExterior.criteriaName.substring(0, 34)} <TouchableOpacity>
                                                        <Text>{criteriaExterior.criteriaName.length > 34 ? '...' : null} </Text>
                                                    </TouchableOpacity>
                                                    </Text>
                                                </View>
                                                <View style={styles.switchFlex}>
                                                    <MKSwitch
                                                        checked={criteriaExterior.status === 'Inactive' ? false : true}
                                                        trackLength={styles.linelength}
                                                        thumbRadius={styles.togglethumb}
                                                        trackSize={styles.toggleline}
                                                        onColor={styles.ontoggle}
                                                        thumbOffColor={criteriaExterior.status == 'Inactive' ? "#fff" : MKColor.Blue}
                                                        thumbOnColor={criteriaExterior.status == 'Inactive' ? "#fff" : MKColor.Blue}
                                                        rippleColor={MKColor.Blue}
                                                        //onPress={() => this.setState({ criteriaName: criteriaExterior.criteriaName, criteriaId: criteriaExterior.criteriaId, criteriaStatus: criteriaExterior.status })}
                                                        //onCheckedChange={this.validateCriteriaUpdate}
                                                        onCheckedChange={() =>this.validateCriteriaUpdateWithParameters(criteriaExterior.criteriaName,criteriaExterior.criteriaId,criteriaExterior.status)}
                                                       
                                                    />
                                                </View>
                                            </View>
                                        } />
                                    </View>
                                </View>
                                <View style={styles.lineseperator} />

                                <View style={styles.tabView}  pointerEvents={this.state.disableView}>
                                    <Text style={styles.otherFont}>Others</Text>
                                    <View style={styles.textFieldContainer}>
                                        <List bounces={false} dataArray={exteriorOther} renderRow={exteriorOther =>
                                            <View style={styles.criteriaFlex}>
                                                {this.state['current_' + exteriorOther.criteriaId] = exteriorOther.criteriaName}
                                                <View style={styles.criteriaView}>
                                                    <Textfield maxLength={25}
                                                        onFocus={() => this.setState({ currentTab: 'Exterior' })}
                                                        placeholder={exteriorOther.criteriaName == 'Other Exterior 1' || exteriorOther.criteriaName == 'Other Exterior 2' ? exteriorOther.criteriaName : null}
                                                        highlightColor={MKColor.Blue}
                                                        onBlur={() => this.state.criteriaName != null ? this.validateCriteriaOther(this.state['current_' + exteriorOther.criteriaId]) : null}
                                                        onChangeText={(text) => this.setState({ criteriaName: text.trim(), criteriaId: exteriorOther.criteriaId })}
                                                        value={exteriorOther.criteriaName == 'Other Exterior 1' || exteriorOther.criteriaName == 'Other Exterior 2' ? null : exteriorOther.criteriaName} />
                                                </View>
                                                <View style={styles.switchFlex}>
                                                    <MKSwitch
                                                        checked={exteriorOther.status === 'Inactive' ? false : true}
                                                        trackLength={styles.linelength}
                                                        thumbRadius={styles.togglethumb}
                                                        trackSize={styles.toggleline}
                                                        onColor={styles.ontoggle}
                                                        thumbOffColor={exteriorOther.status == 'Inactive' ? "#fff" : MKColor.Blue}
                                                        thumbOnColor={exteriorOther.status == 'Inactive' ? "#fff": MKColor.Blue}
                                                        rippleColor={MKColor.Blue}
                                                        //onPress={() => this.setState({ criteriaName: exteriorOther.criteriaName, criteriaId: exteriorOther.criteriaId, criteriaStatus: exteriorOther.status })}
                                                        //onCheckedChange={this.validateCriteriaUpdate}
                                                        onCheckedChange={() =>this.validateCriteriaUpdateWithParameters(this.state.criteriaName,exteriorOther.criteriaId,exteriorOther.status)}
                                                       
                                                    />
                                                </View>
                                            </View>
                                        } />
                                        />
                                   </View>
                                </View>
                                <View style={{ padding: this.state.tabBottom }} />
                            </SV>
                        </Content>
                    </Tab>

                    <Tab tabStyle={styles.tabBgc} activeTabStyle={styles.tabBgc} heading="CHARACTERISTICS" textStyle={styles.tabheaderfont} activeTextStyle={styles.activetabheaderfont}>

                        <Content bounces={false} innerRef={ref => { this.scroll = ref }}  >

                            <ScrollView >
                                <TouchableOpacity onPress={() => this.hideToolTip()} activeOpacity={1}>
                                    <View style={styles.tabView}  pointerEvents={this.state.disableView}>
                                        {criteriaCharacteristics.map(criteriaCharacteristics => (
                                            <View style={styles.criteriaFlex}>
                                                <View style={styles.criteriaView} >
                                                    {this.state['toolTip_' + criteriaCharacteristics.criteriaId] && criteriaCharacteristics.criteriaName.length > 32 ?
                                                        <View style={styles.tooltipSettingContainer}>
                                                            <Text style={styles.toolTip}>{criteriaCharacteristics.criteriaName}</Text>
                                                        </View>
                                                        : null
                                                    }
                                                    <TouchableOpacity activeOpacity={0.7} onPressIn={() => this.hideToolTip()} onPressOut={() => this.setState({ ['toolTip_' + criteriaCharacteristics.criteriaId]: true })}>
                                                        <Text style={criteriaCharacteristics.status == 'Inactive' ? styles.tabfont : styles.tabnameActivefont}>{criteriaCharacteristics.criteriaName.substring(0, 32)}
                                                            <Text style={criteriaCharacteristics.status == 'Inactive' ? styles.tabfont : styles.tabnameActivefont}>{criteriaCharacteristics.criteriaName.length > 32 ? '...' : null}</Text>
                                                        </Text>
                                                    </TouchableOpacity>
                                                </View>
                                                <View style={styles.switchFlex}>
                                                    <MKSwitch
                                                        checked={criteriaCharacteristics.status === 'Inactive' ? false : true}
                                                        trackLength={styles.linelength}
                                                        thumbRadius={styles.togglethumb}
                                                        trackSize={styles.toggleline}
                                                        onColor={styles.ontoggle}
                                                        thumbOffColor={criteriaCharacteristics.status == 'Inactive' ? "#fff" : MKColor.Blue}
                                                        thumbOnColor={criteriaCharacteristics.status == 'Inactive' ? "#fff" : MKColor.Blue}
                                                        rippleColor={MKColor.Blue}
                                                        //onPress={() => this.setState({ criteriaName: criteriaCharacteristics.criteriaName, criteriaId: criteriaCharacteristics.criteriaId, criteriaStatus: criteriaCharacteristics.status })}
                                                       // onCheckedChange={this.validateCriteriaUpdate}
                                                        onCheckedChange={() =>this.validateCriteriaUpdateWithParameters(criteriaCharacteristics.criteriaName,criteriaCharacteristics.criteriaId,criteriaCharacteristics.status)}
                                                       
                                                    />
                                                </View>
                                            </View>
                                        ))}

                                    </View>
                                    <View style={styles.lineseperator} />
                                    <View style={styles.tabView}  pointerEvents={this.state.disableView}>
                                        <Text style={styles.otherFont}>Others</Text>
                                        <View style={styles.textFieldContainer}>
                                            <List dataArray={characteristicsOther} renderRow={characteristicsOther =>
                                                <View style={styles.criteriaFlex}>
                                                    {this.state['current_' + characteristicsOther.criteriaId] = characteristicsOther.criteriaName}
                                                    <View style={styles.criteriaView}>
                                                        <Textfield maxLength={25}
                                                            //onFocus={(event) => {
                                                                // `bind` the function if you're using ES6 classes
                                                              //  this._scrollToInput(ReactNative.findNodeHandle(event.target))
                                                           // }}
                                                            onFocus={() => this.setState({ currentTab: 'Characteristics' })}
                                                            placeholder={characteristicsOther.criteriaName == 'Other Characteristics 1' || characteristicsOther.criteriaName == 'Other Characteristics 2' ? characteristicsOther.criteriaName : null}
                                                            highlightColor={MKColor.Blue}
                                                            onBlur={() => this.state.criteriaName != null ? this.validateCriteriaOther(this.state['current_' + characteristicsOther.criteriaId]) : null}
                                                            onChangeText={(text) => this.setState({ criteriaName: text.trim(), criteriaId: characteristicsOther.criteriaId })}
                                                            value={characteristicsOther.criteriaName == 'Other Characteristics 1' || characteristicsOther.criteriaName == 'Other Characteristics 2' ? null : characteristicsOther.criteriaName}
                                                        />
                                                    </View>
                                                    <View style={styles.switchFlex}>
                                                        <MKSwitch
                                                            checked={characteristicsOther.status === 'Inactive' ? false : true}
                                                            trackLength={styles.linelength}
                                                            thumbRadius={styles.togglethumb}
                                                            trackSize={styles.toggleline}
                                                            onColor={styles.ontoggle}
                                                            thumbOffColor={characteristicsOther.status == 'Inactive' ? "#fff" : MKColor.Blue}
                                                            thumbOnColor={characteristicsOther.status == 'Inactive' ? "#fff": MKColor.Blue}
                                                            rippleColor={MKColor.Blue}
                                                            //onPress={() => this.setState({ criteriaName: characteristicsOther.criteriaName, criteriaId: characteristicsOther.criteriaId, criteriaStatus: characteristicsOther.status })}
                                                            //onCheckedChange={this.validateCriteriaUpdate}
                                                            onCheckedChange={() =>this.validateCriteriaUpdateWithParameters(this.state.criteriaName,characteristicsOther.criteriaId,characteristicsOther.status)}
                                                       
                                                        />
                                                    </View>
                                                </View>
                                            } />
                                            />
                                    </View>
                                    </View>
                                </TouchableOpacity>
                            </ScrollView>
                        </Content>
                    </Tab>
                </Tabs>
                {this.state.loadingSpinner ?
                    <View style={styles.noFavourites}>
                        <Spinner color={styles.loadingColor} />
                    </View> : null}
            </Container>

        );
    }
}

export default CriteriaScreen;

