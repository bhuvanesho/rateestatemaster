
import React, { Component } from 'react';
import { Alert,Animated } from 'react-native';
import PropTypes from 'prop-types';
import LoginModel from '../model/Login';
import { AsyncStorage } from 'react-native';


export default class LoginController extends LoginModel {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      loginCheckedStatus: true,
      loginStatus: '',
      welcomeName: '',
      loggedIn: null,
      timepassed: false,
      loading: true,
      navigation:'',
     
      spinner: false,
     

    }
    this.signInValidation = this.signInValidation.bind(this);
    this.loginChecked = this.loginChecked.bind(this);
    this.checkPageDecide = this.checkPageDecide.bind(this);
    this.welcomeName = this.welcomeName.bind(this);
    this.setNavigation = this.setNavigation.bind(this);
    
  }

  
  loginChecked() {
    if (this.state.loginCheckedStatus) {
      this.setState({ loginCheckedStatus: false });
    }
    else {
      this.setState({ loginCheckedStatus: true });
    }
}

  signInValidation() {

    try {
      if (this.state.username == '' || this.state.password == '') {
        Alert.alert('Invalid Input', this.getErrorMessages());
      }
      else {
       
        this.signIn();
        
      }

    }
    catch (exception) {
      alert(exception);
    }
  }

  checkPageDecide() {
    return this.pageDecideModal();
  }

  welcomeName() {
    return this.welcomeNameModel();
  }

 


}

