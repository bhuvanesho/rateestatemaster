

import { Alert, Keyboard } from 'react-native';

var SQLite = require('react-native-sqlite-storage')
var db = SQLite.openDatabase({ name: "HomeRatingDB.db" });
import ValidationComponent from 'react-native-form-validator';
import alertMSG from '../assets/resources';
var sumProduct = require('sum-product');
import { AsyncStorage } from 'react-native';
import { database, storage } from '../firebaseConfig/config';
export default class propertyModel extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = {
    }
    this.getRatingsOnLoad();
  }


  getRatingsOnLoad = async () => {

    let userId = await AsyncStorage.getItem('userId');
    var criteriaDetails = [];

    let rowcount = 0;
    var ref = database.ref('criteria/')
    ref.orderByChild('UserId')
      .startAt(userId)
      .endAt(userId)
      .on('value', function (snapshot) {
        snapshot.forEach(function (childSnap) {
          if (childSnap.val().Status == 'Active') {
            Id = childSnap.val().Id;
            var ref = database.ref('Ratings/')
            ref.orderByChild('CriteriaId')
              .startAt(Id)
              .endAt(Id)
              .on('value', function (snapshot1) {
                snapshot1.forEach(function (childSnap1) {
                  if (childSnap1.val().PropertyId == this.state.propertyId) {
                    criteriaDetails.push({ criteriaId: childSnap.val().Id, criteriaName: childSnap.val().CriteriaName, status: childSnap.val().Status, percentage: childSnap.val().Percentage, rating: childSnap1.val().Rating, ratingId: childSnap1.val().Id });
                    rowcount = rowcount + 1
                  }
                }.bind(this));
              }.bind(this));
            if (rowcount == 0) {
              criteriaDetails.push({ criteriaId: childSnap.val().Id, criteriaName: childSnap.val().CriteriaName, status: childSnap.val().Status, percentage: childSnap.val().Percentage, rating: 0, ratingId: 0 });
            }
            rowcount = 0;
          }
        }.bind(this));

      }.bind(this));
    this.setState({ criteriaDetails: criteriaDetails });
    criteriaDetails = [];
    this.getAllProperty = this.getAllProperty.bind(this);
    this.getFilteredProperty = this.getFilteredProperty.bind(this);
    this.getSortedProperty = this.getSortedProperty.bind(this);
  }

  /**
   * 
   * 
   * fucntion to fetch filter selection details
   * 
   */

  getFilterDatas = async () => {
    let userId = await AsyncStorage.getItem('userId');
    var query = '';
    var rated1 = '';
    var unrated1 = '';
    var minPrice1 = '';
    var maxPrice1 = '';
    var lowRate1 = '';
    var highRate1 = '';
    var lowPrice1 = '';
    var highPrice1 = '';
    var checkAll1 = '';
    var ParticularCriteria1 = '';
    var favourite1 = '';
    var notfavourite1 = '';
    var ref = database.ref('FilterAndSort/')
    ref.orderByChild('UserId')
      .startAt(userId)
      .endAt(userId)
      .once('value', function (snapshot) {
        snapshot.forEach(function (childSnap) {
          query = childSnap.val().filterNsortQuery;
          rated1 = childSnap.val().Rated;
          unrated1 = childSnap.val().NotRated;
          minPrice1 = childSnap.val().PriceFrom;
          maxPrice1 = childSnap.val().PriceTo;
          lowRate1 = childSnap.val().LowToHighRating;
          highRate1 = childSnap.val().HighToLowRating;
          lowPrice1 = childSnap.val().LowToHighPrice;
          highPrice1 = childSnap.val().HighToLowPrice;
          checkAll1 = childSnap.val().CheckAll;
          ParticularCriteria1 = childSnap.val().ParticularCriteria;
          favourite1 = childSnap.val().Favourite;
          notfavourite1 = childSnap.val().NotFavourite;
          // this.setState({ currentQuery: query });
          this.setState({ rated: (rated1 == 0) ? false : true });
          this.setState({ unrated: (unrated1 == 0) ? false : true });
          this.setState({ minPrice: (minPrice1 == 100000) ? 100000 : minPrice1 });
          this.setState({ maxPrice: (maxPrice1 == 6000000) ? 6000000 : maxPrice1 });
          this.setState({ lowRate: (lowRate1 == 0) ? false : true });
          this.setState({ highRate: (highRate1 == 0) ? false : true });
          this.setState({ lowPrice: (lowPrice1 == 0) ? false : true });
          this.setState({ highPrice: (highPrice1 == 0) ? false : true });
          this.setState({ checkAll: checkAll1 });
          this.setState({ particularCriteria: ParticularCriteria1 });
          this.setState({ favourite: favourite1 });
          this.setState({ notfavourite: notfavourite1 });
        }.bind(this));
      }.bind(this));



    //get listed properties
    var property = [];
    var ref = database.ref('UserPropertyMapping/')
    ref.orderByChild('UserId')
      .startAt(userId)
      .endAt(userId)
      .once('value', function (snapshot1) {
        snapshot1.forEach(function (childSnap1) {
          if (childSnap1.val().ListingStatus != 'NEW LISTING') {
            property.push({ propertyId: childSnap1.val().ListingID, Id: childSnap1.val().Id })
          }
        }.bind(this));
        this.setState({ propertyData: property })
      }.bind(this));

    property = [];

    //get array of ratings for the particular user
    var ratings3 = [];
    var ref = database.ref('Ratings/')
    ref.orderByChild('UserId')
      .startAt(userId)
      .endAt(userId)
      .once('value', function (snapshot3) {
        snapshot3.forEach(function (childSnap3) {
          ratings3.push({ rating: childSnap3.val().Rating, criteriaId: childSnap3.val().CriteriaId, propertyId: childSnap3.val().PropertyId })
        }.bind(this));
        this.setState({ ratingsData: ratings3 })
      }.bind(this));


    if (checkAll1 == 0) {
      checkAll1 = false;
    }
    if (ParticularCriteria1 == 0) {
      ParticularCriteria1 = false;
    }
    query = query.replace(/@/g, '"');

    { checkAll1 ? this.checkAllCriteriaFetch() : null }



  }
  propertyViewed = async () => {

    let UserPropertyMappingId = '';
    let listingStatus = '';
    let userId = await AsyncStorage.getItem('userId');
    var ref = database.ref('UserPropertyMapping/')
    ref.orderByChild('UserId')
      .startAt(userId)
      .endAt(userId)
      .on('value', function (snapshot1) {
        snapshot1.forEach(function (childSnap) {
          if (childSnap.val().ListingID == this.state.propertyId) {
            UserPropertyMappingId = childSnap.val().Id;
            listingStatus = childSnap.val().ListingStatus;
          }
        }.bind(this));
      }.bind(this));

    if (listingStatus == 'NEW LISTING') {
      this.setState({ listingStatus: 'NOT RATED' })

      database.ref('UserPropertyMapping' + '/' + UserPropertyMappingId).update(
        {
          "ListingStatus": "NOT RATED",
        });
    }
    //});
  }

  checkAtleastOneCriteria = async () => {
    try {


      let userId = await AsyncStorage.getItem('userId');
      var ref = database.ref('criteria/')
      ref.orderByChild('UserId')
        .startAt(userId)
        .endAt(userId)
        .once('value', function (snapshot) {
          snapshot.forEach(function (childSnap) {
            if (childSnap.val().SortedStatus == 'Active') {
              this.setState({ atleastOneChecked: true });
            }
          }.bind(this));
        }.bind(this));
    }

    catch (error) {
      alert(error);
    }

  }

  getActiveCriteria = async () => {
    try {

      var criteria = [];
      let userId = await AsyncStorage.getItem('userId');
      var ref = database.ref('criteria/')
      ref.orderByChild('UserId')
        .startAt(userId)
        .endAt(userId)
        .once('value', function (snapshot) {
          criteria.push({ criteriaId: 0, criteriaName: 'ALL' })
          snapshot.forEach(function (childSnap) {
            if (childSnap.val().Status == 'Active') {
              criteria.push({ criteriaId: childSnap.val().Id, criteriaName: childSnap.val().CriteriaName, status: childSnap.val().Status, weightage: childSnap.val().Weightage, percentage: childSnap.val().Percentage, sortedStatus: childSnap.val().SortedStatus });
            }
          }.bind(this));
          this.setState({ criteriaInfo: criteria });
        }.bind(this));


      //get this validation for the filter screen
      criteria = criteria.filter(function (obj) {
        return obj.sortedStatus == "Active";
      });
      if (criteria.length == 0) {
        this.setState({ atleastOneChecked: false });
      }
      criteria = [];
    }
    catch (error) {
      alert(error);
    }
  }

  //** controller to call model function which gets all property list 

  getAllProperty = async () => {
    this.getPropertyHeaderImages();
    let userId = await AsyncStorage.getItem('userId');
    var propertyInfo = [];
    let img = this.state.image;
    var ref = database.ref('UserPropertyMapping/')
    ref.orderByChild('UserId')
      .startAt(userId)
      .endAt(userId)
      .on('value', function (snapshot) {
        snapshot.forEach(function (childSnap) {
          if (childSnap.val().FavouriteProperty != 'Favourite' && childSnap.val().Status != 'Inactive') {
            propertyInfo.push({ propertyId: childSnap.val().ListingID, propertyName: childSnap.val().PropertyName, address: childSnap.val().Address, description: childSnap.val().Description, amount: childSnap.val().Amount, status: childSnap.val().Status, listingStatus: childSnap.val().ListingStatus, category: childSnap.val().Category, beds: childSnap.val().Beds, totalBath: childSnap.val().TotalBath, area: childSnap.val().Area, buildYear: childSnap.val().BuildYear, noOfStorey: childSnap.val().NoOfStorey, overallRating: childSnap.val().OverallRating, image: img });
          }
        });
      }.bind(this));
    this.setState({ propertyInfo: propertyInfo, loadingSpinner: false });
    this.setState({ selected: false });
  }

  checkPropertyAndAdded = async (mlsId) => {
    let userId = await AsyncStorage.getItem('userId');
    var retVal = 'false';
    var ref = database.ref('UserPropertyMapping/')
    ref.orderByChild('UserId')
      .startAt(userId)
      .endAt(userId)
      .once('value', function (snapshot1) {
        snapshot1.forEach(function (childSnap1) {
          if (childSnap1.val().ListingID == mlsId) {
           retVal='true';
          }
        }.bind(this));

        if(retVal=='true')
        {
          Alert.alert('Property already added for the given Listing Id!')
          this.getFavourites("existing");
          this.setState({ loadingSpinner: false, mlsId: '' });
          return;
        }

        if(retVal=='false'){
        // if property is not added in the listing 
        //search if given mls id is present in the property table. If present add the same to userpropertymapping table
        let rowcount = 0;
        let ListingID = '';
        let PropertyName = '';
        let Address = '';
        let Description = '';
        let latitude = '';
        let longitude = '';
        let Amount = '';
        let Status = '';
        let Beds = '';
        let TotalBath = '';
        let Area = '';
        let Category = '';
        let createdDate = '';
        let BuildYear = '';
        let NoOfStorey = '';
        let MatrixId = '';
        let NoOfPhotos = 0;
        var ref = database.ref('PropertyTable/')
        ref.orderByChild('MLSNumber')
          .startAt(mlsId)
          .endAt(mlsId)
          .on('value', function (snapshot) {
            snapshot.forEach(function (childSnap) {
              ListingID = childSnap.val().MLSNumber;
              PropertyName = childSnap.val().AddressDisplay;
              Address = childSnap.val().ListOfficeName;
              Description = childSnap.val().PublicRemarks;
              latitude = childSnap.val().Latitude;
              longitude = childSnap.val().Longitude;
              Amount = childSnap.val().ListPrice;
              Status = childSnap.val().Status;
              Beds = childSnap.val().BedsTotal;
              TotalBath = childSnap.val().BathsTotal;
              Area = childSnap.val().LotSizeArea;
              Category = childSnap.val().BuildingType;
              createdDate = childSnap.val().OriginalEntryTimestamp;
              BuildYear = childSnap.val().YrBuilt;
              NoOfStorey = childSnap.val().TotalFloors;
              MatrixId = childSnap.val().Matrix_Unique_ID;
              NoOfPhotos = childSnap.val().PhotoCount;
              rowcount = rowcount + 1;
            });
            if (rowcount == 0) {

              Alert.alert("No Property matching Listing ID found! please try again..");
              this.setState({ loadingSpinner: false });
              return;
            } else {

              //handle undefined values here

              if (PropertyName == undefined) { PropertyName = '' }
              if (Address == undefined) { Address = '' }
              if (Description == undefined) { Description = '' }
              if (latitude == undefined) { latitude = '' }
              if (longitude == undefined) { longitude = '' }
              if (Amount == undefined) { Amount = '' }
              if (Status == undefined) { Status = '' }
              if (Beds == undefined) { Beds = '' }
              if (TotalBath == undefined) { TotalBath = '' }
              if (Area == undefined) { Area = '' }
              if (Category == undefined) { Category = '' }
              if (BuildYear == undefined) { BuildYear = '' }
              if (createdDate == undefined) { createdDate = '' }
              if (NoOfStorey == undefined) { NoOfStorey = '' }


              let accountUsers = [];
              accountUsers = this.state.accountUsersData;
              let len = accountUsers.length;
              for (let i = 0; i < len; i++) {
                //make loop and add data
                var newPostKey = database.ref().child('UserPropertyMapping').push().key;

                var dt = new Date();
                database.ref('UserPropertyMapping/' + newPostKey).set({
                  "Id": newPostKey,
                  "UserId": accountUsers[i].userId,
                  "ListingID": ListingID,
                  "PropertyName": PropertyName,
                  "Address": Address,
                  "Description": Description,
                  "latitude": latitude,
                  "longitude": longitude,
                  "Amount": Amount,
                  "Status": Status,
                  "Beds": Beds,
                  "TotalBath": TotalBath,
                  "Area": Area,
                  "OverallRating": 0.0,
                  "SortedRating": 0.0,
                  "Category": Category,
                  "createdDate": dt.getTime(),
                  "BuildYear": BuildYear,
                  "NoOfStorey": NoOfStorey,
                  "RatingStatus": 'Unrated',
                  "FavouriteProperty": '',
                  "ListingStatus": 'NEW LISTING',
                  "Comments": '',
                  "MatrixId": MatrixId,
                  "NoOfPhotos": NoOfPhotos,
                })
              }
              //add properpty related images to PropertyPhotos table
              this.insertPropertyPhotos(ListingID, MatrixId, NoOfPhotos)
            }
            this.getFavourites("new");
            this.setState({ loadingSpinner: false, mlsId: '' });
            this.clearAll();
            this.doneFilter();
          }.bind(this));
        }
      }.bind(this));
    
  }


  getFavourites = async (type) => {
    try {



      var favouriteInfo = [];
      let userId = await AsyncStorage.getItem('userId');
      var query = '';
      var rated1 = '';
      var unrated1 = '';
      var minPrice1 = '';
      var maxPrice1 = '';
      var lowRate1 = '';
      var highRate1 = '';
      var lowPrice1 = '';
      var highPrice1 = '';
      var checkAll1 = '';
      var ParticularCriteria1 = '';
      var favourite1 = '';
      var notfavourite1 = '';

      var ref = database.ref('FilterAndSort/')
      ref.orderByChild('UserId')
        .startAt(userId)
        .endAt(userId)
        .once('value', function (snapshot) {
          snapshot.forEach(function (childSnap) {
            query = childSnap.val().filterNsortQuery;
            rated1 = childSnap.val().Rated;
            unrated1 = childSnap.val().NotRated;
            minPrice1 = childSnap.val().PriceFrom;
            maxPrice1 = childSnap.val().PriceTo;
            lowRate1 = childSnap.val().LowToHighRating;
            highRate1 = childSnap.val().HighToLowRating;
            lowPrice1 = childSnap.val().LowToHighPrice;
            highPrice1 = childSnap.val().HighToLowPrice;
            checkAll1 = childSnap.val().CheckAll;
            favourite1 = childSnap.val().Favourite;
            notfavourite1 = childSnap.val().NotFavourite;
            ParticularCriteria1 = childSnap.val().ParticularCriteria;


            // pull property table data and filter as per the filter and sort table meta data
            var ref = database.ref('UserPropertyMapping/')
            ref.orderByChild('UserId')
              .startAt(userId)
              .endAt(userId)
              .once('value', function (snapshot1) {
                snapshot1.forEach(function (childSnap1) {
                  if (childSnap1.val().Status != 'Inactive') {
                    if (ParticularCriteria1 == 1) {
                      favouriteInfo.push({ propertyId: childSnap1.val().ListingID, propertyName: childSnap1.val().PropertyName, address: childSnap1.val().Address, description: childSnap1.val().Description, amount: childSnap1.val().Amount, status: childSnap1.val().Status, listingStatus: childSnap1.val().ListingStatus, category: childSnap1.val().Category, beds: childSnap1.val().Beds, totalBath: childSnap1.val().TotalBath, area: childSnap1.val().Area, buildYear: childSnap1.val().BuildYear, noOfStorey: childSnap1.val().NoOfStorey, overallRating: childSnap1.val().SortedRating, favouriteProperty: childSnap1.val().FavouriteProperty, listingId: childSnap1.val().ListingID, createdDate: childSnap1.val().createdDate });
                    }
                    else {
                      favouriteInfo.push({ propertyId: childSnap1.val().ListingID, propertyName: childSnap1.val().PropertyName, address: childSnap1.val().Address, description: childSnap1.val().Description, amount: childSnap1.val().Amount, status: childSnap1.val().Status, listingStatus: childSnap1.val().ListingStatus, category: childSnap1.val().Category, beds: childSnap1.val().Beds, totalBath: childSnap1.val().TotalBath, area: childSnap1.val().Area, buildYear: childSnap1.val().BuildYear, noOfStorey: childSnap1.val().NoOfStorey, overallRating: childSnap1.val().OverallRating, favouriteProperty: childSnap1.val().FavouriteProperty, listingId: childSnap1.val().ListingID, createdDate: childSnap1.val().createdDate });
                    }
                  }
                }.bind(this));
                // once the favouriteINfo array is formed sort the data based on the parameters
                if (type == "existing") {
                  //now perform the filtering of the data on the array


                  if (rated1 == 1 && unrated1 != 1) {
                    favouriteInfo = favouriteInfo.filter(function (obj) {
                      return obj.overallRating > 0;
                    });
                  }
                  if (rated1 != 1 && unrated1 == 1) {
                    favouriteInfo = favouriteInfo.filter(function (obj) {
                      return obj.overallRating == 0;
                    });
                  }


                  if (favourite1 == 1 && notfavourite1 != 1) {
                    favouriteInfo = favouriteInfo.filter(function (obj) {
                      return obj.favouriteProperty == "Favourite";
                    });
                  }
                  if (notfavourite1 == 1 && favourite1 != 1) {
                    favouriteInfo = favouriteInfo.filter(function (obj) {
                      return obj.favouriteProperty.trim() == "";
                    });
                  }

                  if (maxPrice1 >= 0) {
                    favouriteInfo = favouriteInfo.filter(function (obj) {
                      return obj.amount <= maxPrice1;
                    });
                  }



                  // sort the array
                  if (highRate1 == 1 && lowPrice1 == 1) {
                    favouriteInfo.sort(function (a, b) {
                      return b.overallRating - a.overallRating || a.amount - b.amount;
                    });
                  }

                  if (highRate1 == 1 && highPrice1 == 1) {
                    favouriteInfo.sort(function (a, b) {
                      return b.overallRating - a.overallRating || b.amount - a.amount;
                    });
                  }


                  if (lowRate1 == 1 && lowPrice1 == 1) {
                    favouriteInfo.sort(function (a, b) {
                      return a.overallRating - b.overallRating || a.amount - b.amount;
                    });
                  }

                  if (lowRate1 == 1 && highPrice1 == 1) {
                    favouriteInfo.sort(function (a, b) {
                      return a.overallRating - b.overallRating || b.amount - a.amount;
                    });
                  }
                }

                if (type == "new") {
                  favouriteInfo = favouriteInfo.sort((a, b) => {
                    return b.createdDate - a.createdDate;
                  });
                }

                this.setState({ favouriteInfo: favouriteInfo, loadingSpinner: false }, () => { this.getPropertyHeaderImages(); });
                favouriteInfo = [];

                if (query != "1") {
                  this.setState({ filterActive: false });
                }
                else {
                  this.setState({ filterActive: true });
                }


              }.bind(this));


          }.bind(this));
        }.bind(this));

    }
    catch (error) {
      alert(error);
    }
    this.setState({ selected: false });
  }


  /***
  * function to get property list based on filters of criteria, price and ratings
  */

  getFilteredProperty = async () => {

    this.getPropertyHeaderImages();
    let userId = await AsyncStorage.getItem('userId');
    var propertyInfo = [];
    var ref = database.ref('UserPropertyMapping/')
    ref.orderByChild('UserId')
      .startAt(userId)
      .endAt(userId)
      .on('value', function (snapshot) {
        snapshot.forEach(function (childSnap) {
          if (childSnap.val().Status != 'Inactive') {
            propertyInfo.push({ propertyId: childSnap.val().ListingID, propertyName: childSnap.val().PropertyName, address: childSnap.val().Address, description: childSnap.val().Description, amount: childSnap.val().Amount, status: childSnap.val().Status, listingStatus: childSnap.val().ListingStatus, category: childSnap.val().Category, beds: childSnap.val().Beds, totalBath: childSnap.val().TotalBath, area: childSnap.val().Area, buildYear: childSnap.val().BuildYear, noOfStorey: childSnap.val().NoOfStorey, overallRating: childSnap.val().OverallRating, image: this.state.image });
          }
        });
      }.bind(this));
    this.setState({ propertyInfo: propertyInfo, loadingSpinner: false });


  }

  /***
  * function to get property list based on sort of from low to high and high to low
  */

  getSortedProperty = async () => {

    this.getPropertyHeaderImages();
    let userId = await AsyncStorage.getItem('userId');
    var propertyInfo = [];
    var ref = database.ref('UserPropertyMapping/')
    ref.orderByChild('UserId')
      .startAt(userId)
      .endAt(userId)
      .on('value', function (snapshot) {
        snapshot.forEach(function (childSnap) {
          if (childSnap.val().Status != 'Inactive') {
            propertyInfo.push({ propertyId: childSnap.val().ListingID, propertyName: childSnap.val().PropertyName, address: childSnap.val().Address, description: childSnap.val().Description, amount: childSnap.val().Amount, status: childSnap.val().Status, listingStatus: childSnap.val().ListingStatus, category: childSnap.val().Category, beds: childSnap.val().Beds, totalBath: childSnap.val().TotalBath, area: childSnap.val().Area, buildYear: childSnap.val().BuildYear, noOfStorey: childSnap.val().NoOfStorey, overallRating: childSnap.val().OverallRating, image: this.state.image });
          }
        });
      }.bind(this));
    this.setState({ propertyInfo: propertyInfo, loadingSpinner: false });
  }

  /** added new method to get the property header image */
  getPropertyHeaderImages = () => {
    var token = this.state.tokenValue;
    var image = [];
    var bucket = storage.ref().bucket;
    var firebaseUrl = "https://firebasestorage.googleapis.com/v0/b/" + bucket + "/o/";
    var finalUrl = ''

    var ref = database.ref('PropertyPhotos/')
    ref.orderByChild('Header')
      .startAt(1)
      .endAt(1)
      .on('value', function (snapshot) {
        snapshot.forEach(function (childSnap) {
          //construct the url dynamically here
          finalUrl = firebaseUrl + childSnap.val().blob + "?alt=media&token=" + token;
          image.push({ propertyId: childSnap.val().PropertyId, propertyName: childSnap.val().PropertyName, imageName: finalUrl });

        });
        this.setState({ PropertyImages: image });
        image = [];
      }.bind(this));


  }

  async getHardcodedImageUrlwithToken() {
    var bucket = storage.ref().bucket;
    var firebaseUrl = "https://firebasestorage.googleapis.com/v0/b/" + bucket + "/o/";
    var finalUrl = firebaseUrl + "LargePhoto32179131-0.jpeg";
    return fetch(finalUrl)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({ tokenValue: responseJson.downloadTokens })
      })
      .catch((error) => {
        console.error(error);
      });
  }


  getProperties(query) {
    try {
      db.transaction((tx) => {

        this.getPropertyHeaderImages();
        tx.executeSql(query, [], (tx, results) => {
          var len = results.rows.length;
          var propertyInfo = [];
          for (let i = 0; i < len; i++) {

            this.setState({ propertyId: results.rows.item(i)['Id'], propertyName: results.rows.item(i)['PropertyName'], address: results.rows.item(i)['Address'], description: results.rows.item(i)['Description'], amount: results.rows.item(i)['Amount'], status: results.rows.item(i)['Status'], listingStatus: results.rows.item(i)['ListingStatus'], category: results.rows.item(i)['Category'], beds: results.rows.item(i)['Beds'], totalBath: results.rows.item(i)['TotalBath'], area: results.rows.item(i)['Area'], buildYear: results.rows.item(i)['BuildYear'], noOfStorey: results.rows.item(i)['NoOfStorey'], overallRating: results.rows.item(i)['OverallRating'] })
            propertyInfo.push({ propertyId: this.state.propertyId, propertyName: this.state.propertyName, address: this.state.address, description: this.state.description, amount: this.state.amount, status: this.state.status, listingStatus: this.state.listingStatus, category: this.state.category, beds: this.state.beds, totalBath: this.state.totalBath, area: this.state.area, buildYear: this.state.buildYear, noOfStorey: this.state.noOfStorey, overallRating: this.state.overallRating, image: this.state.image });
          }
          this.setState({ propertyInfo: propertyInfo, loadingSpinner: false });
        });
      });
    }
    catch (error) {
      alert(error);
    }
  }




  /***
   * 
   *
   * funtion to get all details of selected property along with its image and ratings
   * 
   * 
   */

  getPropertyDetails = async () => {
    //db.transaction((tx) => {
    let userId = await AsyncStorage.getItem('userId');
    var propertyDetails = [];
    var image = [];
    let propertyId = this.state.propertyId;
    var ref = database.ref('UserPropertyMapping/')
    ref.orderByChild('UserId')
      .startAt(userId)
      .endAt(userId)
      .once('value', function (snapshot) {
        snapshot.forEach(function (childSnap) {
          if (childSnap.val().ListingID == propertyId) {
            propertyDetails.push({ UserPropertyMappingId: childSnap.val().Id, propertyId: childSnap.val().ListingID, propertyName: childSnap.val().PropertyName, address: childSnap.val().Address, description: childSnap.val().Description, amount: childSnap.val().Amount, latitude: childSnap.val().latitude, longitude: childSnap.val().longitude, status: childSnap.val().Status, listingStatus: childSnap.val().ListingStatus, category: childSnap.val().Category, beds: childSnap.val().Beds, totalBath: childSnap.val().TotalBath, area: childSnap.val().Area, buildYear: childSnap.val().BuildYear, noOfStorey: childSnap.val().NoOfStorey, overallRating: childSnap.val().OverallRating, favourite: childSnap.val().FavouriteProperty, comments: childSnap.val().Comments, Accomodation: '', Education: '', Arts: '' });
          }
        }.bind(this));
        this.setState({ propertyDetails: propertyDetails }, () => { this.calculateOverallRating(this.state.propertyDetails[0]['UserPropertyMappingId']) });
      }.bind(this));

    propertyDetails = [];

    var token = this.state.tokenValue;
    var image = [];
    var bucket = storage.ref().bucket;
    var firebaseUrl = "https://firebasestorage.googleapis.com/v0/b/" + bucket + "/o/";
    var finalUrl = ''
    var ref = database.ref('PropertyPhotos/')
    ref.orderByChild('PropertyId')
      .startAt(propertyId)
      .endAt(propertyId)
      .once('value', function (snapshot1) {
        snapshot1.forEach(function (childSnap1) {
          if (childSnap1.val().Header != 2) {
            finalUrl = firebaseUrl + childSnap1.val().blob + "?alt=media&token=" + token;
          } else {
            finalUrl = childSnap1.val().blob;
          }
          image.push({ imageName: finalUrl });
        });

        this.setState({ PropertyImages: image });
        var len = image.length * 100;
        setTimeout(() => {
          this.setState({ spinner: false });
        }, len);

        if (this.state.lastAdded) {
          this.setState({ currentPic: image.length - 1 });
        }
        image = [];
      }.bind(this));


    this.setState({ selected: false })
  }

  /***
   * 
   * insert or update ratings for the property 
   * 
   */


  ratingProperty = async (propertyId, criteriaId, rating, ratingId) => {

    try {

      let userId = await AsyncStorage.getItem('userId');

      if (ratingId != 0) {
        database.ref('Ratings' + '/' + ratingId).update(
          {
            "Rating": rating,
          });
      }
      if (ratingId == 0) {
        var newPostKey = database.ref().child('Ratings').push().key;
        //insert if the row count is zero 
        database.ref('Ratings/' + newPostKey).set({
          "Id": newPostKey,
          "UserId": userId,
          "CriteriaId": criteriaId,
          "PropertyId": propertyId,
          "Rating": rating,
          "createdDate": Date.now(),
          "updatedDate": Date.now(),
        });
        //recreate the active criteria array as new ratings got added
        this.getRatingsOnLoad();
      }
    }
    catch (error) {
    }
  }

  /*
  *
  * calculate overall rating for property
  * 
  */


  calculateOverallRating = async (UserPropertyMappingId) => {
    try {
      if (this.state.listingStatus != 'NEW LISTING') {
        let rowcount = 0;
        let propertyId = this.state.propertyDetails[0]['propertyId'];
        let criteriaPercentage = [];
        let criteriaRating = [];
        let criteriaId;
        let userId = await AsyncStorage.getItem('userId');
        let ratingsData = [];
        var ref = database.ref('Ratings/')
        let overallRating = 0.001;
        ref.orderByChild('UserId')
          .startAt(userId)
          .endAt(userId)
          .once('value', function (snapshot) {
            snapshot.forEach(function (childSnap) {
              if (childSnap.val().PropertyId == propertyId) {
                ratingsData.push({ rCriteriaId: childSnap.val().CriteriaId, rPropertyId: childSnap.val().PropertyId, rRating: childSnap.val().Rating })
              }
            }.bind(this));
            //now loop through this array 

            //loop through the ratings for calculation
            for (let j = 0; j < ratingsData.length; j++) {
              for (let i = 0; i < this.state.criteriaDetails.length; i++) {
                if (ratingsData[j].rCriteriaId == this.state.criteriaDetails[i]['criteriaId']) {
                  rating = ratingsData[j].rRating;
                  criteriaPercentage.push(this.state.criteriaDetails[i]['percentage'] / 100);
                  criteriaRating.push(rating);
                }
              }
            }
            overallRating = sumProduct(criteriaPercentage, criteriaRating);
            if (overallRating == 0) { overallRating = 0.001 }
            this.setState({ overallRating: overallRating });
            if (overallRating != 0 && overallRating != 0.0 && overallRating != null) {

              database.ref('UserPropertyMapping' + '/' + UserPropertyMappingId).update(
                {
                  "OverallRating": overallRating.toFixed(1),
                });

            }
            else {
              database.ref('UserPropertyMapping' + '/' + UserPropertyMappingId).update(
                {
                  "ListingStatus": "NOT RATED",
                });
            }
          }.bind(this));
      }
    }
    catch (error) {
      alert(error);
    }
  }


  /**
   * 
   * function to update overall rating of property 
   * 
   */

  overallRatingProperty = async (propertyId, overallRating) => {
    if (overallRating == 0) {
      overallRating = null;
    }
    try {

      let userId = await AsyncStorage.getItem('userId');
      let UserPropertyMappingId = '';
      var ref = database.ref('UserPropertyMapping/')
      ref.orderByChild('UserId')
        .startAt(userId)
        .endAt(userId)
        .on('value', function (snapshot1) {
          snapshot1.forEach(function (childSnap1) {
            if (childSnap1.val().ListingID == propertyId) {
              UserPropertyMappingId = childSnap1.val().Id;
            }
          }.bind(this));
        }).bind(this);

      database.ref('UserPropertyMapping' + '/' + UserPropertyMappingId).update(
        {
          "OverallRating": overallRating,
        });


    }
    catch (error) {
      alert(error);
    }
  }

  /**
   * 
   * favourite property 
   */

  favouriteProperty = async (propertyId, favourite) => {

    try {
      let userId = await AsyncStorage.getItem('userId');
      if (favourite == 'Favourite') {
        var ref = database.ref('FilterAndSort/')
        ref.orderByChild('UserId')
          .startAt(userId)
          .endAt(userId)
          .once('value', function (snapshot) {
            snapshot.forEach(function (childSnap) {
              var query = childSnap.val().filterNsortQuery;
              //var filterString = alertMSG.en["FilterString"];
              var filterString = "0";
              if (query != filterString) {
                Alert.alert('', alertMSG.en['WeightageFilterRefresh']);
                database.ref('criteria' + '/' + userId).update(
                  {
                    "SortedStatus": "Active",
                  });

                database.ref('FilterAndSort' + '/' + userId).update(
                  {
                    "filterNsortQuery": filterString,
                    "Rated": 1,
                    "NotRated": 1,
                    "PriceFrom": 100000,
                    "PriceTo": this.state.variablePriceMax,
                    "LowToHighRating": 0,
                    "HighToLowRating": 1,
                    "LowToHighPrice": 1,
                    "HighToLowPrice": 0,
                    "CheckAll": 1,
                    "ParticularCriteria": 0,
                    "Favourite": 1,
                    "NotFavourite": 1,
                  });
              }
            });
          }.bind(this));
      }
      let UserPropertyMappingId = '';
      var ref = database.ref('UserPropertyMapping/')
      ref.orderByChild('UserId')
        .startAt(userId)
        .endAt(userId)
        .on('value', function (snapshot1) {
          snapshot1.forEach(function (childSnap1) {
            if (childSnap1.val().ListingID == propertyId) {
              UserPropertyMappingId = childSnap1.val().Id;
            }
          }.bind(this));
        }.bind(this));

      database.ref('UserPropertyMapping' + '/' + UserPropertyMappingId).update(
        {
          "FavouriteProperty": favourite,
        });

    }
    catch (error) {
      alert(error);
    }
  }

  getNewRatings = async () => {

    try {

      let userId = await AsyncStorage.getItem('userId');
      var ratingDetails = [];
      var ref = database.ref('Ratings/')
      ref.orderByChild('UserId')
        .startAt(userId)
        .endAt(userId)
        .on('value', function (snapshot) {
          snapshot.forEach(function (childSnap) {
            if (childSnap.val().PropertyId == this.state.propertyId) {
              ratingDetails.push(childSnap.val().Rating);
            }
          });
        }.bind(this));
      this.setState({ ratingDetails: ratingDetails });
    }
    catch (error) {
      alert(error);
    }
  }



  /*
  *
  * calculate filtered rating for property
  * 
  */


  calculateSortedRating = async (userId) => {


    try {

      let sortedRatingArray = [];
      //loop through each property for calculation purpose
      var property1 = [];

      property1 = this.state.propertyData;

      var criteria2 = [];
      //get array of criteria for particular user
      var ref = database.ref('criteria/')
      ref.orderByChild('UserId')
        .startAt(userId)
        .endAt(userId)
        .once('value', function (snapshot2) {
          snapshot2.forEach(function (childSnap2) {
            if (childSnap2.val().Status == 'Active' && childSnap2.val().SortedStatus == 'Active') {
              criteria2.push({ criteriaId: childSnap2.val().Id, criteriaName: childSnap2.val().CriteriaName, sortedStatus: childSnap2.val().SortedStatus, status: childSnap2.val().Status, weightage: childSnap2.val().Weightage, percentage: childSnap2.val().filterPercentage });
            }
          }.bind(this));
        }.bind(this));


      //get array of ratings for the particular user

      ratings3 = this.state.ratingsData;


      //loop through the properties
      for (let k = 0; k < property1.length; k++) {
        let propertyId = property1[k].propertyId;
        let UserPropertyMappingId = property1[k].Id;
        //array to hold percentage and rating
        let criteriaPercentage = [];
        let criteriaRating = [];
        let criteriaId;

        //match the criteria for the given property
        for (let i = 0; i < criteria2.length; i++) {
          count = 0;

          for (let j = 0; j < ratings3.length; j++) {
            if (ratings3[j].criteriaId == criteria2[i].criteriaId && ratings3[j].propertyId == propertyId) {
              rating = ratings3[j].rating;
              criteriaPercentage.push(criteria2[i].percentage / 100);
              criteriaRating.push(rating);
              count = count + 1;
            }
          }//calculate the sorted rating
          if (count == 0) {
            rating = 0;
            criteriaPercentage.push(criteria2[i].percentage / 100);
            criteriaRating.push(rating);
          }
        }

        let sortedRating = sumProduct(criteriaPercentage, criteriaRating);


        if (sortedRating == 0 || sortedRating == null || sortedRating == 0.0) {
          database.ref('UserPropertyMapping' + '/' + UserPropertyMappingId).update(
            {
              "SortedRating": sortedRating.toFixed(1),
              "ListingStatus": 'NOT RATED',
            });
        }
        else {
          database.ref('UserPropertyMapping' + '/' + UserPropertyMappingId).update(
            {
              "SortedRating": sortedRating.toFixed(1),
              "ListingStatus": 'RATED',
            });
        }

      }
      //reset all arrays
      property1 = [];
      ratings3 = [];
      criteria2 = [];

    }
    catch (error) {
      alert(error);
    }
  }


  


  //filter rating property

  filterRatingProperty = async (propertyId, sortedRating) => {


    try {
      let userId = await AsyncStorage.getItem('userId');
      var ref = database.ref('UserPropertyMapping/')
      let UserPropertyMappingId = '';
      ref.orderByChild('UserId')
        .startAt(userId)
        .endAt(userId)
        .on('value', function (snapshot1) {
          snapshot1.forEach(function (childSnap1) {
            if (childSnap1.val().ListingID == propertyId) {
              UserPropertyMappingId = childSnap1.val().Id;
            }
          }.bind(this));
        }.bind(this));
      if (sortedRating == 0 || sortedRating == null || sortedRating == 0.0) {
        database.ref('UserPropertyMapping' + '/' + UserPropertyMappingId).update(
          {
            "SortedRating": sortedRating.toFixed(1),
            "ListingStatus": 'NOT RATED',
          });
      }
      else {
        database.ref('UserPropertyMapping' + '/' + UserPropertyMappingId).update(
          {
            "SortedRating": sortedRating.toFixed(1),
            "ListingStatus": 'RATED',
          });
      }
    }
    catch (error) {
      alert(error);
    }
  }


  selectedCriteria = async (criteriaId, status) => {
    //this.setState({ ['criteria_' + criteriaId]: this.state['criteria_' + criteriaId] ? false : true, checkAll: false, onScreenChange: true })
    try {

      var SortedStatus = '';
      if (status.checked != undefined) {
        if (status.checked.toString() === 'true') {
          SortedStatus = 'Active';
        }
        if (status.checked.toString() === 'false') {
          SortedStatus = 'Inactive';
        }

      }

      if (status.checked == undefined) {
        if (status.toString() == 'true') {
          SortedStatus = 'Active';
        }
        if (status.toString() == 'false') {
          SortedStatus = 'Inactive';
        }

      }

      database.ref('criteria' + '/' + criteriaId).update(
        {
          "SortedStatus": SortedStatus,
        });
      //this.checkAtleastOneCriteria();
      this.setState({ particularCriteria: true, checkAll: false })
      if (this.state.atleastOneChecked == false) {
        if (status.checked != undefined) {
          this.setState({ atleastOneChecked: status.checked.toString() == "true" ? true : false, queryChange: "1" })
        }
        if (status.checked == undefined) {
          this.setState({ atleastOneChecked: status.toString() == "true" ? true : false, queryChange: "0" })
        }
      }
      //check second condition where you uncheck the criteria individually

      if (status.checked != undefined) {
        if (status.checked.toString() == 'false') {
          this.setState({ queryChange: "1" });
          //decide based on the total Actives in the criteria array
          this.getActiveCriteria()
        }
      }

      if (status.checked == undefined) {
        if (status.toString() == 'false') {
          this.setState({ queryChange: "1" });
          //decide based on the total Actives in the criteria array
          this.getActiveCriteria()
        }
      }
    }



    catch (error) {
      alert(error);
    }
  }

  allCriteria = async (status) => {
    try {
      var criteria = this.state.criteriaInfo;
      for (let i = 1; i < criteria.length; i++) {
        var SortedStatus = '';
        if (status.checked != undefined) {
          if (status.checked.toString() === 'true') {
            SortedStatus = 'Active';

          }
          if (status.checked.toString() === 'false') {
            SortedStatus = 'Inactive';
          }
          criteria[i].sortedStatus = SortedStatus;

        }
        else
        {
          //when Clear All is clicked
          if(status==='true'){
            SortedStatus = 'Active';
           
          }
        }
        database.ref('criteria' + '/' + criteria[i].criteriaId).update(
          {
            "SortedStatus": SortedStatus,
          });
      }
      if (status.checked != undefined) {
          this.setState({ particularCriteria: !(status.checked), checkAll: status.checked, atleastOneChecked: status.checked, queryChange: "1" })
      }
      //when Clear All is clicked
      else if(status==='true')
      {
        this.getActiveCriteria();
      }
    }
    catch (error) {
      alert(error);
    }
  }



  addComments = async () => {
    try {
      var comments = this.state.comments;
      reWhiteSpace = new RegExp(/^\s+$/);
      if (reWhiteSpace.test(comments)) {
        comments = '';
      }
      if (comments == null) {
        comments = '';
      }


      let userId = await AsyncStorage.getItem('userId');
      var ref = database.ref('UserPropertyMapping/')
      let propertyId = this.state.propertyId;
      let UserPropertyMappingId = '';
      ref.orderByChild('UserId')
        .startAt(userId)
        .endAt(userId)
        .on('value', function (snapshot1) {
          snapshot1.forEach(function (childSnap1) {
            if (childSnap1.val().ListingID == propertyId) {
              UserPropertyMappingId = childSnap1.val().Id;
            }
          }.bind(this));
        }.bind(this));

      database.ref('UserPropertyMapping' + '/' + UserPropertyMappingId).update(
        {
          "Comments": comments,
        });


      this.getPropertyDetails();
    }
    catch (error) {
      alert(error);
    }
  }


  //Filter Done 
  filterDone = async (query, rated, unrated, minPrice, maxPrice, lowRate, highRate, lowPrice, highPrice, checkAll, particularCriteria, favourite, notfavourite) => {
    try {


      // try to call the calculate sorted rating function here


      let userId = await AsyncStorage.getItem('userId');
      database.ref('FilterAndSort' + '/' + userId).update(
        {
          "filterNsortQuery": query,
          "Rated": rated,
          "NotRated": unrated,
          "PriceFrom": minPrice,
          "PriceTo": maxPrice,
          "LowToHighRating": lowRate,
          "HighToLowRating": highRate,
          "LowToHighPrice": lowPrice,
          "HighToLowPrice": highPrice,
          "CheckAll": checkAll,
          "ParticularCriteria": particularCriteria,
          "Favourite": favourite,
          "NotFavourite": notfavourite,
        });
    }
    catch (error) {
      alert(error);
    }
    // this.setState({ favouriteSelected: true });
    this.setState({ filterNsort: false });
  }


  refreshFilter = async () => {



    let userId = await AsyncStorage.getItem('userId');
    var query = '';
    var filterString = '';
    var ref = database.ref('FilterAndSort/')
    ref.orderByChild('UserId')
      .startAt(userId)
      .endAt(userId)
      .on('value', function (snapshot) {
        snapshot.forEach(function (childSnap) {
          query = childSnap.val().filterNsortQuery;

          filterString = "0";
        });
      });
    if (query != filterString) {
      //Alert.alert('', alertMSG.en['RatingRefreshFilter']);
      database.ref('criteria' + '/' + userId).update(
        {
          "SortedStatus": "Active",
        });

      database.ref('FilterAndSort' + '/' + userId).update(
        {
          "filterNsortQuery": filterString,
          "Rated": 1,
          "NotRated": 1,
          "PriceFrom": 100000,
          "PriceTo": this.state.variablePriceMax,
          "LowToHighRating": 0,
          "HighToLowRating": 1,
          "LowToHighPrice": 1,
          "HighToLowPrice": 0,
          "CheckAll": 1,
          "ParticularCriteria": 0,
          "Favourite": 1,
          "NotFavourite": 1,
        });
    }

  }

  //looks like method not used
  clearFilter = async () => {
    var filterString = 'select * from Property where FavouriteProperty = @Favourite@ order by OverallRating  asc, Amount desc';
    let userId = await AsyncStorage.getItem('userId');
    database.ref('criteria' + '/' + userId).update(
      {
        "SortedStatus": "Active",
      });

    database.ref('FilterAndSort' + '/' + userId).update(
      {
        "filterNsortQuery": filterString,
        "Rated": 1,
        "NotRated": 1,
        "PriceFrom": 100000,
        "PriceTo": this.state.variablePriceMax,
        "LowToHighRating": 0,
        "HighToLowRating": 1,
        "LowToHighPrice": 1,
        "HighToLowPrice": 0,
        "CheckAll": 1,
        "ParticularCriteria": 0,
        "Favourite": 1,
        "NotFavourite": 1,
      });
    this.setState({ checkAll: true });


  }

  listingStatusChange = (propertyId) => {

    try {

      let accountUsers = [];
      accountUsers = this.state.accountUsersData;
      let len = accountUsers.length;
      for (let i = 0; i < len; i++) {


        let userId = accountUsers[i].userId;
        var ref = database.ref('UserPropertyMapping/')
        let UserPropertyMappingId = '';
        ref.orderByChild('UserId')
          .startAt(userId)
          .endAt(userId)
          .once('value', function (snapshot1) {
            snapshot1.forEach(function (childSnap1) {
              if (childSnap1.val().ListingID == propertyId) {
                database.ref('UserPropertyMapping' + '/' + childSnap1.val().Id).remove();
              }
            }.bind(this));
          }.bind(this));
      }
    }
    catch (error) {
      alert(error);
    }
  }

  /*****
   * 
   * 
   * recalculating percentage for the filter
   * 
   * 
   * 
   */





  calculatefilterPercentage = (userId) => {
    try {

      //let userId=await AsyncStorage.getItem('userId');
      var criteria = [];
      var weightage = 0;
      var sumWeightage = 0;
      count = 0;
      var ref = database.ref('criteria/')
      ref.orderByChild('UserId')
        .startAt(userId)
        .endAt(userId)
        .on('value', function (snapshot) {
          //calculate sumWeightage
          snapshot.forEach(function (childSnap) {
            if (childSnap.val().Status == 'Active' && childSnap.val().SortedStatus == 'Active') {
              criteria.push({ "Id": childSnap.val().Id, "Weightage": childSnap.val().Weightage });
            }
          }.bind(this));

        }.bind(this));


      let weightage = 0;

      for (i = 0; i < criteria.length; i++) {
        //calculate sumWeightage
        weightage = criteria[i].Weightage;
        weightage == null ? weightage = 0 : null;
        weightage = parseInt(weightage);
        sumWeightage = sumWeightage + weightage;
      }

      //reset variable
      weightage = 0;
      //update the percentage
      for (j = 0; j < criteria.length; j++) {
        //calculate sumWeightage
        weightage = criteria[j].Weightage;
        weightage == null ? weightage = 0 : null;
        let percentage = parseInt(weightage) / sumWeightage * 100;
        this.percentageUpdate(criteria[j].Id, percentage);
      }
      criteria = [];
    }
    catch (error) {
      alert(error);
    }
  }


  /****
   * 
   * updating criteria percentage 
   * 
   */

  percentageUpdate(id, value) {
    try {

      if (!isNaN(value)) {
        database.ref('criteria' + '/' + id).update(
          {
            "filterPercentage": value,
          }
        )
      }
    }
    catch (error) {
      alert(error);
    }

  }

  insertNewPhotos() {

    try {
      //header 2 represents images added by user
      var newPostKey = database.ref().child('PropertyPhotos').push().key;
      database.ref('PropertyPhotos/' + newPostKey).set({
        "PropertyId": this.state.currentPropertyId,
        "PropertyName": this.state.currentPropertyName,
        "blob": 'data:image/jpeg;base64,' + this.state.ImageSource,
        "Header": 2,
        "ImageName": '',
      });
      Alert.alert("Image added to the property");
    }
    catch (error) {
      alert(error);
    }
    this.setState({ lastAdded: true });
    this.getPropertyDetails();
  }

  insertPropertyPhotos(propertyId, MatrixId, NoOfPhotos) {

    try {

      var ref = database.ref('PropertyPhotos/')
      ref.orderByChild('PropertyId')
        .startAt(propertyId)
        .endAt(propertyId)
        .on('value', function (snapshot) {
          if (!snapshot.exists()) {
            for (i = 0; i < NoOfPhotos; i++) {
              var Header = 0;
              if (i == 0) { Header = 1 }
              var newPostKey = database.ref().child('PropertyPhotos').push().key;
              database.ref('PropertyPhotos/' + newPostKey).set({
                "PropertyId": propertyId,
                "PropertyName": '',
                "blob": 'LargePhoto' + MatrixId + '-' + i + '.jpeg',
                "Header": Header,
                "ImageName": '',
              });

            }
          }
        });



    }
    catch (error) {
      alert(error);
    }

  }

  checkPropertyImages(propertyId) {

    var ref = database.ref('PropertyPhotos/')
    ref.orderByChild('PropertyId')
      .startAt(propertyId)
      .endAt(propertyId)
      .on('value', function (snapshot) {
        return snapshot.exists();

      });

  }


  //This method adds property to the list for the logged in user
  addPropertyToUser = () => {
    Keyboard.dismiss();
    let mlsId = this.state.mlsId;
    if (mlsId.trim() == '') {
      Alert.alert('Listing ID cannot be empty!')
      return;
    }
    mlsId = mlsId.replace('c', 'C');
    if (mlsId.substring(0, 1) != 'C') {
      mlsId = 'C' + mlsId;
    }
    
    this.setState({ loadingSpinner: true });
    //before calling the add check if the property already exists
    //you have to take it from the database as the current state would display filtered data
    this.checkPropertyAndAdded(mlsId);
   
  }

 



  getAccountUsers = async () => {
    var userId = await AsyncStorage.getItem('accountId');
    var influencers = [];
    var ref = database.ref('User/')
    ref.orderByChild('accountId')
      .startAt(userId)
      .endAt(userId)
      .on('value', function (snapshot) {
        snapshot.forEach(function (childSnap) {
          var ref1 = database.ref('profile/')
          ref1.orderByChild('userId')
            .startAt(childSnap.val().userId)
            .endAt(childSnap.val().userId)
            .once('value', function (snapshot1) {
              snapshot1.forEach(function (childSnap1) {

                influencers.push({
                  "influencerName": childSnap1.val().firstName + " " + childSnap1.val().lastName,
                  "userId": childSnap1.val().userId,
                  "accountId": childSnap.val().accountId
                })

              }.bind(this));
              this.setState({ accountUsersData: influencers })
            }.bind(this));

        }.bind(this));

      }.bind(this));


  }

  getMaximumPropertyValue = async () => {

    let userId = await AsyncStorage.getItem('userId');
    var ref = database.ref('UserPropertyMapping/')
    let amountArr = [];
    let maxValue = 0;
    ref.orderByChild('UserId')
      .startAt(userId)
      .endAt(userId)
      .once('value', function (snapshot1) {
        snapshot1.forEach(function (childSnap1) {
          amountArr.push({ amount: childSnap1.val().Amount })
        }.bind(this));
        if(amountArr.length>0){
          maxValue = Math.max.apply(Math, amountArr.map(function (o) { return o.amount; }));
        }
        if (maxValue == NaN || maxValue == 0) {
          maxValue = 6000000;
        }
        if (this.state.maxPrice > maxValue) { this.setState({ maxPrice: maxValue }) }
        this.setState({ variablePriceMax: maxValue, vairableStep: maxValue / 10 });
      }.bind(this));
  }

}
