import React, { Component } from "react";
import { ImageBackground, View, Text, Dimensions } from "react-native";
import LoginController from '../../controller/Login';
import Orientation from "react-native-orientation";

import styles from "./styles";
import { Button } from "native-base";


const launchscreenLogo = require("../../../assets/splashscreenLogo.png");

class Home extends Component {

  componentDidMount() {
    Orientation.lockToPortrait();
  }
  render() {
    return (
      <View style={styles.splashScreen}>
        <ImageBackground source={launchscreenLogo} style={styles.splashScreenlogo} />
      </View>

    );
  }
}

export default Home;


// render() {
//   if (this.state.loggedIn == false) {
//     return (
//       <Login />
//     );
//   }
//   if (this.state.loggedIn == true) {
//     return (
//       <Tab />
//     );
//   }
//   if (this.state.loggedIn == null) {
//     return (
//       <View style={styles.splashScreen}>
//         <ImageBackground source={launchscreenLogo} style={styles.splashScreenlogo} />
//       </View>

//     );
//   }
// }