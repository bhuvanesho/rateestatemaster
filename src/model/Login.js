var SQLite = require('react-native-sqlite-storage')
var db = SQLite.openDatabase({ name: "HomeRatingDB.db" });
import ValidationComponent from 'react-native-form-validator';
import { database } from '../firebaseConfig/config';
import { AsyncStorage } from 'react-native';
export default class LoginModel extends ValidationComponent {

  constructor(props) {
    super(props);
    this.state = {
      record: null,
      loggedIn: false,

    }

    this.welcomeNameModel = this.welcomeNameModel.bind(this);

  }

  /****sigining in method depricated
  signIn() {

    this.username = this.state.username;
    this.password = this.state.password;
    this.loginStatus = this.state.loginCheckedStatus;


    try {
      db.transaction((tx) => {

        tx.executeSql('SELECT * FROM User where username = ? and password = ?', [this.username, this.password], (tx, results) => {
         var len = results.rows.length;
          if (len == '' || len == 0) {
            Alert.alert(alertMSG.en['Failure'], alertMSG.en['LoginFail']);
          }
          var loginCount = results.rows.item(0)['loginCount'];
          this.setState({ record: len });

          if (len != '' && this.loginStatus == true) {
            tx.executeSql('UPDATE User set status = "Active" where username= "' + this.username + '" and password="' + this.password + '" ');
            tx.executeSql('UPDATE User set status = "Inactive" where username != "' + this.username + '" and password!="' + this.password + '" ');
            tx.executeSql('Update User set LoggedIn = "True",loginCount = 2 where username = "' + this.username + '" and password = "' + this.password + '"');

            if (this.username == 'james') {
              var homesJSON = homes;
              var propertyImages = propImages;
            }
            if (this.username == 'chris') {
              var homesJSON = homes1;
              var propertyImages = propImages1;
            }
            if (this.username == 'rishikesh') {
              var homesJSON = homes2;
              var propertyImages = propImages2;
            }

            for (let i = 0; i < homesJSON.length; i++) {
              db.transaction((tx) => {
                tx.executeSql("INSERT INTO Property(`PropertyName`,`Address`,`Description`,`latitude`,`longitude`,`Amount`,`Status`,`ListingStatus`,`Category`,`Beds`,`TotalBath`,`Area`,`BuildYear`,`NoOfStorey`,`createdDate`,`RatingStatus`,`FavouriteProperty`,`OverallRating`,`SortedRating`,`Education`,`Arts`,`Accomodation`) VALUES ('" + homesJSON[i]["PropertyName"] + "','" + homesJSON[i]["Address"] + "','" + homesJSON[i]["Description"] + "'," + homesJSON[i]["latitude"] + "," + homesJSON[i]["longitude"] + "," + homesJSON[i]["Amount"] + ",'" + homesJSON[i]["Status"] + "','" + homesJSON[i]["ListingStatus"] + "','" + homesJSON[i]["Category"] + "'," + homesJSON[i]["Beds"] + "," + homesJSON[i]["TotalBath"] + "," + homesJSON[i]["Area"] + "," + homesJSON[i]["BuildYear"] + "," + homesJSON[i]["NoOfStorey"] + ", CURRENT_TIMESTAMP , 'Unrated','Unfavourite',0,0," + homesJSON[i]["Education"] + "," + homesJSON[i]["Arts"] + "," + homesJSON[i]["Accomodation"] + ")");
              })
            }
            for (let i = 0; i < propertyImages.length; i++) {
              db.transaction((tx) => {
                tx.executeSql("INSERT INTO PropertyPhotos(`Header`,`PropertyId`,`PropertyName`,`PropertyImage`,`createdDate`) VALUES (" + propertyImages[i]["Header"] + "," + propertyImages[i]["PropertyId"] + ",'" + propertyImages[i]["PropertyName"] + "','" + propertyImages[i]["blob"] + "',CURRENT_TIMESTAMP)");
              })
              if (i === propertyImages.length-1) {
                this.setState({loading:false});
              }

            }


            if (loginCount == 1) {
              { this.props.navigation.navigate('Welcome') };

            }
            else {
              { this.props.navigation.navigate('Listing') };
            }
          }
          if (len != '' && this.loginStatus == false) {
            tx.executeSql('UPDATE User set status = "Active" where username= "' + this.username + '" and password="' + this.password + '" ');
            tx.executeSql('UPDATE User set status = "Inactive" where username != "' + this.username + '" and password!="' + this.password + '" ');


            tx.executeSql('Update User set LoggedIn = "False" ,loginCount = 2 where username = "' + this.username + '" and password = "' + this.password + '"');

            if (loginCount == 1) {

              { this.props.navigation.navigate('Welcome') };

            }
            else {
              { this.props.navigation.navigate('Listing') };
            }
          }
        });
      });
    }
    catch (error) {
      alert(error);
    }
  } */

  /***method to show welcome message */
  welcomeNameModel() {

    try {
      db.transaction((tx) => {
        tx.executeSql('SELECT * FROM User ', [], (tx, results) => {
          if (results.rows.length > 0) {
            var name = results.rows.item(0)['username'];
            if (results.rows.item(0)['loginCount'] == 2) { var v_loginCount = true; }
            { this.setState({ welcomeName: name, loginCount: v_loginCount }) };
          }
        });

      });
    }
    catch (error) {
      alert(error);
    }
  }

  setAccountId = async (userId) => {
    let accountId = '';
    var ref1 = database.ref('User/')
    ref1.orderByChild('userId')
      .startAt(userId)
      .endAt(userId)
      .on('value', function (snapshot1) {
        snapshot1.forEach(function (childSnap1) {
          AsyncStorage.setItem('accountId', childSnap1.val().accountId)
        }.bind(this));
      }.bind(this));
  }

  checkRealtor = async (realtorId) => {
    var ref = database.ref('Realtor/')
    ref.orderByChild('agentId')
      .startAt(realtorId)
      .endAt(realtorId)
      .once('value', function (snapshot) {
        snapshot.forEach(function (childSnap) {
          AsyncStorage.setItem('agentId', realtorId);
          this.setState({ isRealtor: true })
          setTimeout(() => {
            if (childSnap.val().agentId == realtorId) {
              this.props.navigation.navigate('RealtorHome')
              this.setState({ spinner: false })
            }
          }, 1000);
        }.bind(this));
      }.bind(this));


  }

  loginUser = async (userId, username, password) => {
    if (!this.state.isRealtor) {
      AsyncStorage.clear()
      AsyncStorage.setItem('userId', userId);
      AsyncStorage.setItem('permanetUserId', userId);
      this.setAccountId(userId);
      this.setState({ username: username })
      if (this.state.loginCheckedStatus == true) {
        try {
          db.transaction((tx) => {
            tx.executeSql('SELECT * FROM User', [], (tx, results) => {
              var len = results.rows.length;
              if (len == 0) {
                tx.executeSql('insert into User (username,password,EmailId,LoggedIn,loginCount,loggedInflag,status) VALUES ("' + username + '","' + password + '","' + username + '","false",1,1,"Active")');
                this.props.navigation.navigate('Welcome')
                this.setState({ spinner: false })
              }
              if (len > 0) {
                tx.executeSql('Update User set LoggedIn = "true"');

                //based on the second time login navigate user to corresponding screen
                var loginCount = results.rows.item(0)['loginCount'];
                if (loginCount == 2) {
                  this.setNavigation();
                  setTimeout(() => {
                    this.props.navigation.navigate('Favourite')
                  }, 1000);


                } else {
                  this.props.navigation.navigate('Welcome')
                }
                this.setState({ spinner: false })
              }
            });
          });
        } catch (error) {
          alert(error);
        }
      } else {
        try {
          db.transaction((tx) => {
            tx.executeSql('SELECT * FROM User', [], (tx, results) => {
              var len = results.rows.length;
              if (len == 0) {
                tx.executeSql('insert into User (username,password,EmailId,LoggedIn,loginCount,loggedInflag,status) VALUES ("' + username + '","' + password + '","' + username + '","false",1,1,"Active")');
                this.props.navigation.navigate('Welcome')
                this.setState({ spinner: false })
              } else {
                //based on the second time login navigate user to corresponding screen
                var loginCount = results.rows.item(0)['loginCount'];
                if (loginCount == 2) {
                  this.setNavigation();
                  setTimeout(() => {
                    this.props.navigation.navigate('Favourite')
                  }, 1000);
                } else {
                  this.props.navigation.navigate('Welcome')
                }
                this.setState({ spinner: false })
              }
            });
          });
        } catch (error) {
          alert(error);
          this.setState({ spinner: false })
        }

      }
    }

  }

  setNavigation = async () => {

    let permanetUserId = '';
    let userId = '';
    userId = await AsyncStorage.getItem('userId');
    permanetUserId = await AsyncStorage.getItem('permanetUserId');


    if (userId === permanetUserId) {
      this.setState({ navigation: 'ProfileUpdate' })
      //set two major flags which control the read only for floating label and dropdowns in all screens
      await AsyncStorage.setItem('editableValue', JSON.stringify(true));
      await AsyncStorage.setItem('dropdownDisabledValue', JSON.stringify(false));

    }
    else {
      this.setState({ navigation: 'Profile' })
      //set two major flags which control the read only for floating label and dropdowns in all screens
      await AsyncStorage.setItem('editableValue', JSON.stringify(false));
      await AsyncStorage.setItem('dropdownDisabledValue', JSON.stringify(true));

    }


  }

}
