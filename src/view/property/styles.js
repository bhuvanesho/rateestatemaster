export const { width, height } = Dimensions.get('window');
import { Dimensions } from 'react-native';
export default {
  propertyaddress: {
    color: '#b8bbc1',
    fontSize: 15.611,
    fontFamily: 'ArialNarrowMTStd'
  },
  close: { alignSelf: 'flex-end' },
  closeSwiper: {position:'absolute',alignSelf:'flex-end',top:'5%' },

  imagearrow: 10,
  loadingColor: '#727171',
  overallRatingText: { marginTop: 6, color: '#fff', fontSize: 15.611, fontWeight: 'bold' },

  overallRating: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },


  criterialine: { zIndex: 2, justifyContent: 'center', left: width / 2.6, marginBottom: 4 },

  houseRating: { width: width / 8, height: height / 16, position: "absolute" },

  iconspacing: { padding: 5 },

  cameraiconspacing: { paddingLeft: 9,paddingTop:5 },

  mapnFav: { flexDirection: 'column', paddingTop: 20 },

  propertyInfoFlex: { flexDirection: 'row', flex: 1 },

  maptooltip: { backgroundColor: '#fff', borderRadius: 5 },

  tooltip: { margin: 5 },

  dot: '#fff',
  dotModal: '#727171',
  activedot: '#25a9e0',
  imageSwiper: height - 450,
  modalimageSwiper:height - 450,
  imageslide: { width: '100%', height: height - 450 },
  modalImageSwiper: { width: width, height: height - 450 },
  modalLandscape:{width:'100%',height:'100%'},
  arrow: { paddingLeft: 10, paddingRight: 10 },
  leftRightArrows:
    {
      width: 30.66,
      height: 31.66,
      marginRight: 20
    },

  headerBg: {
    backgroundColor: '#fff'
  },
  headerBGimage: {
    width: 50,
    height: 42.3,
  },
  propertyDetailsBG: {
    backgroundColor: '#25a9e0',
    height: 36
  },
  propertyDetails: {
    color: '#FFF',
    marginTop: 8,
    alignSelf: 'center',
    fontWeight: 'bold',
    fontSize: 18,
    fontFamily: 'AspiraXXXNar-Regular'
  },
  propertyFlex: { flex: 1, flexDirection: 'row', justifyContent: 'center' },
  iconFlex: { paddingTop: 3, flex: 1, flexDirection: 'row' },
  locationIcon: { alignSelf: 'center', width: 20, height: 30 },
  favouriteIcon: { width: 90 / 3, height: 90 / 3 },
  favouriteactiveIcon: { tintColor: '#ff707e', width: 90 / 3, height: 90 / 3 },
  activefavouriteIcon: { marginRight: 10, width: 30, height: 30, tintColor: '#ffb6da' },
  propertyView: {
    margin: 16,
    width: width / 1.3
  },

  locationProperty: {
    flexDirection: 'row'
  },

  starView: {
    flexDirection: 'row',
    marginLeft: 3,
    width: 100
  },
  ratestarView: {
  },
  starPadding: {
    marginRight: 5
  },
  propertyName: {
    color: '#25a9e0',
    fontWeight: 'bold',
    fontSize: 15.611,
    paddingTop: 5,
    fontFamily: 'ArialNarrowMTStd'
  },
  propertyInfo: {
    color: '#727171',
    fontSize: 15.611,
    // paddingBottom: 3,
    fontFamily: 'ArialNarrowMTStd'
  },
  servicetitle: {
    color: '#25a9e0',
    fontWeight: 'bold',
    fontSize: 15.611,
    padding: 5,
    fontFamily: 'ArialNarrowMTStd'
  },
  services: {
    color: '#727171',
    fontSize: 15.611,
    // paddingBottom: 3,
    fontFamily: 'ArialNarrowMTStd',alignSelf:'center'
  },
  
  moreFont: {
    color: '#f7797f',
    fontSize: 13.611,
    marginBottom: 3,
    // fontFamily: 'ArialNarrowMTStd',
    fontStyle: 'italic'
  },
  closeFont: {
    color: '#fff'
  },
  closeImageFont: {
    color: '#727171',
},
  propertyfont: {
    color: '#727171',
    fontSize: 15.611,
    margin: 5,
    fontFamily: 'ArialNarrowMTStd'
  },
  newrangefont: {
    color: '#727171',
    fontWeight: '500',
    fontSize: 18.611,
    margin: 5,
    fontFamily: 'AspiraXXXNar-Regular'
  },
  rangefont: {
    color: '#727171',
    paddingTop: 5,
    fontSize: 15.611,
    fontFamily: 'ArialNarrowMTStd'
  },

  //property screen background 

  propertyBg: { backgroundColor: '#fff' },
  //criteria Flex
  criteriaBorder: { borderColor: '#25a9e0', borderWidth: 0.5, borderRadius: 10 },
  criteriaView: { paddingLeft: 16, paddingRight: 16 },
  criteriaFlex: { padding: 8, flex: 1, flexDirection: 'row' },
  //slider styles
  sliderFlex: { flex: 1, flexDirection: 'row', justifyContent: 'flex-end' },
  slidertorange: {},
  sliderwidth: { width: 160 },
  slidercolor: '#25a9e0',
  //criteria font
  criteriafont: {
    textAlign: 'center',
    color: '#25a9e0',
    fontSize: 15.611,
    fontFamily: 'ArialNarrowMTStd'
  },
  tooltipContainer: { zIndex: 6, flexDirection: 'row', justifyContent: 'space-between', width: '180%', padding: 5, left: '10%', top: '50%', position: 'absolute', backgroundColor: '#ededed', borderRadius: 5 },
  toolTip: {
    textAlign: 'center',
    color: '#727171',
    fontSize: 15.611,
    fontFamily: 'ArialNarrowMTStd'
  },
  toolTipClose: {
    position: 'relative',
    bottom: 10,
    textAlign: 'right',
    color: '#727171',
    fontSize: 30.611,
    fontFamily: 'ArialNarrowMTStd'
  },
  TextInputStyleClass: {
    margin: 1,
    height: 92,
    borderColor: '#ededed',
    borderRadius: 10,
    backgroundColor: "#ededed",
    fontSize: 15.611,
    fontFamily: 'ArialNarrowMTStd',
    color: '#727171',
  },

  placeholderColor: '#727171',

  starColor: '#25a9e0',

  locationIconFont: {
    color: '#25a9e0',
    marginLeft: 5
  },

  //back button
  navigator: {
    alignSelf: 'flex-end',
    width: 30.66,
    height: 31.66
  }, backNav: {
    padding: 16,
    marginBottom: 20
  },
  //back icon 
  backIcon: {
    color: '#fff',
    margin: -20, 
    padding: 20,
    position: "relative",
  },
  modalContent: {
    height: height,
    padding: 22,
    justifyContent: "center",
    alignItems: "center",
    borderColor: "rgba(0, 0, 0, 0.1)"
  },
  mapView: {
    justifyContent: 'center',
    paddingBottom: 10,
    borderRadius: 10,
    height: '95%',
    width: '90%',
  },
  mapContainer: {
    height: '100%',
    width: '100%',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  shareImage2: {
    width: 30,
    height: 30,
    justifyContent:'flex-end',
    
  },

}