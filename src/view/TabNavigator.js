import styles from './styles';
import Profile from '../view/profile/profileDetails';
import Filter from './ListFavFilterSort/filter';
import CriteriaSetting from '../view/criteria/criteriasetting';
import CriteriaWeightage from '../view/criteria/criteriaweightage';
import Listing from '../view/ListFavFilterSort/listingScreen';
import Favourite from '../view/ListFavFilterSort/favourite';
import Home from '../view/personalView/index';
import RealtorHome from '../view/realtorHomePage/index';
import SharedAccount from '../view/realtorHomePage/sharedAccount';
import { AsyncStorage } from 'react-native';
//import React from 'react';
import { Text, View, Image, TouchableOpacity as Button, TouchableOpacity } from 'react-native';
import { TabNavigator, StackNavigator } from 'react-navigation';
//import Icon from 'react-native-vector-icons/FontAwesome';
import { Footer, FooterTab, Icon, Text as NBText } from 'native-base'
import React, { Component } from 'react';

var agentId = ''; var userId = ''; var permanetUserId = ''; 
var profileDisable ='init'; var agentDisable='true'; 
var favDisable='false'; criteriaDisable='false';

setUserIds = async () => {
  agentId = await AsyncStorage.getItem('agentId');
  userId = await AsyncStorage.getItem('userId');
  permanetUserId = await AsyncStorage.getItem('permanetUserId');
 //when not logged in by agent
 if(agentId==null || agentId=="")
 {
  agentDisable='true';
  criteriaDisable='false';
  favDisable='false';
   if(userId==permanetUserId && permanetUserId!="")
   {
     profileDisable='false';
   } 
   if(userId!=permanetUserId)
   {
     profileDisable='true';
   } 
 } 
// when logged in by agent
 if(agentId!=null && agentId!="")
 {
  profileDisable='true';
  agentDisable='false';
  favDisable='true';
  criteriaDisable='true';
   if(userId!="" && userId!=null)
   {
     favDisable='false';
     criteriaDisable='false';
   } 
  
 } 

}



export default TabNavigator ({
  Profile: { screen: Profile },
  Home: { screen: Home },
  CriteriaSetting: { screen: CriteriaSetting },
  CriteriaWeightage: { screen: CriteriaWeightage },
  //Listing: { screen: Listing },
  Favourite: { screen: Favourite },
  RealtorHome: { screen: RealtorHome },
  SharedAccount: { screen: SharedAccount },
},
  {
    initialRouteName: 'Favourite',
    //initialRouteName: 'Profile',
    tabBarPosition: 'bottom',
    swipeEnabled: false,
    lazy: true,
   
    navigationOptions: {
      headerTitle: (
        <TouchableOpacity >
          <Image style={styles.header} source={require('../../assets/Headerlogo.png')} />
        </TouchableOpacity>
      )
    },
   
    tabBarComponent: props => {
      setUserIds();
      active = props.navigationState.index;
      
        
      
       
if(active!=3){
      return (
  
        <Footer style={styles.footer}>
          <FooterTab >
          
            <Button
              vertical
              onPress={() => {(agentDisable=='false') ? props.navigation.navigate("RealtorHome"): props.navigation.navigate("Home")}}>
              <Image style={styles.footerIcon} source={active != 1 ? require('../../assets/homeMenu.png') : require('../../assets/homeMenuActive.png')} />
            </Button>  
            {/*<Button
              onPress={() => {(profileDisable=='true' && agentDisable=='true' ) ? alert('Profile cannot be viewed!'):props.navigation.navigate("Profile")}}>
               
                 {(profileDisable=='true' ) ?
                <Image style={styles.footerIcon} source= {require('../../assets/profileDisable.png')} />: null }
             
              {(profileDisable=='false' || profileDisable=='init' ) ?
                <Image style={styles.footerIcon} source={active != 0 ? require('../../assets/profile.png') : require('../../assets/profileActive.png')} /> :null}
              
            </Button>*/}

            <Button
              vertical
              onPress={() => {(favDisable=='true') ? null : props.navigation.navigate("Profile")}}>  
                <Image style={styles.footerIcon} source={active != 0 ? require('../../assets/profile.png') : require('../../assets/profileActive.png')} /> :null}
              </Button>
            <Button
              vertical
              onPress={() => {(criteriaDisable=='false') ? ((userId==permanetUserId && permanetUserId!="") ? props.navigation.navigate("CriteriaWeightage"):props.navigation.navigate("CriteriaWeightage")):null}}>
              <Image style={styles.footerIcon} source={(active != 2 && active !=3)? require('../../assets/criteria.png') : require('../../assets/criteriaActive.png')} />
            </Button>
            <Button
              vertical
              onPress={() => {(favDisable=='false') ? props.navigation.navigate("Favourite"):null}}>
              <Image style={styles.footerIcon} source={active != 4 ? require('../../assets/showings.png') : require('../../assets/showingsActive.png')} />
            </Button>
          </FooterTab>
        </Footer>






      );
} else{ return (false);}

     

      {/* if (agentId != "" && agentId != null && userId == null && userId == null) {
          return (
            <Footer style={styles.footer}>
              <FooterTab >
  
                <Button
                  vertical
                  onPress={() => props.navigation.navigate("RealtorHome")}>
                  <Image style={styles.footerIconReadOnly} source={active != 5 ? require('../../assets/homeMenu.png') : require('../../assets/homeMenuActive.png')} />
                
                </Button>
                <Button
                  vertical
                  onPress={() => props.navigation.navigate("CriteriaWeightage")}>
                  <Image style={styles.footerIconReadOnly} source={active != 3 ? require('../../assets/criteria.png') : require('../../assets/criteriaActive.png')} />
                
                </Button>
                <Button
                  vertical
                  onPress={() => props.navigation.navigate("Favourite")}>
                  <Image style={styles.footerIconReadOnly} source={active != 4 ? require('../../assets/showings.png') : require('../../assets/showingsActive.png')} />
                
                </Button>
  
              </FooterTab>
            </Footer>
          );
  
        }
  
        if ((userId == permanetUserId || permanetUserId == "") && (agentId == null || agentId == "")) {
          return (
  
            <Footer style={styles.footer}>
              <FooterTab >
                <Button
                  onPress={() => props.navigation.navigate("Profile")}>
                  <Image style={styles.footerIcon} source={active != 0 ? require('../../assets/profile.png') : require('../../assets/profileActive.png')} />
                  
                </Button>
                <Button
                  vertical
                  onPress={() => props.navigation.navigate("Home")}>
                  <Image style={styles.footerIcon} source={active != 1 ? require('../../assets/homeMenu.png') : require('../../assets/homeMenuActive.png')} />
                 
                </Button>
                <Button
                  vertical
                  onPress={() => props.navigation.navigate("CriteriaSetting")}>
                  <Image style={styles.footerIcon} source={active != 2 ? require('../../assets/criteria.png') : require('../../assets/criteriaActive.png')} />
                 
                </Button>
                <Button
                  vertical
                  onPress={() => props.navigation.navigate("Favourite")}>
                  <Image style={styles.footerIcon} source={active != 4 ? require('../../assets/showings.png') : require('../../assets/showingsActive.png')} />
                 
                </Button>
  
              </FooterTab>
            </Footer>
          );
        }
        if ((userId != permanetUserId || permanetUserId != "")) {
          return (
            <Footer style={styles.footer}>
              <FooterTab >
  
                <Button
                  vertical
                  onPress={() => {(agentId!="" && agentId !=null) ? props.navigation.navigate("RealtorHome"): props.navigation.navigate("Home")}}>
                  <Image style={styles.footerIconReadOnly} source={active != 1 ? require('../../assets/homeMenu.png') : require('../../assets/homeMenuActive.png')} />
                 
                </Button>
                <Button
                  vertical
                  onPress={() => props.navigation.navigate("CriteriaWeightage")}>
                  <Image style={styles.footerIconReadOnly} source={active != 3 ? require('../../assets/criteria.png') : require('../../assets/criteriaActive.png')} />
                
                </Button>
                <Button
                  vertical
                  onPress={() => props.navigation.navigate("Favourite")}>
                  <Image style={styles.footerIconReadOnly} source={active != 4 ? require('../../assets/showings.png') : require('../../assets/showingsActive.png')} />
                 
                </Button>
  
              </FooterTab>
            </Footer>
          );
  
        }
      return(null)*/}
    }
  });



  /*export default class TabNavigator1 extends Component {
    render() {
      return (
        <Navigation />
      );
    }
  }*/
