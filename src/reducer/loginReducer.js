import { LOGIN_ATTEMPT , LOGIN_SUCCESS, LOGIN_FAILED, LOGIN_DATA , ERROR_MESSAGE} from '../actions/actionType';

const INITIAL_STATE = { loading: false, login: null, error: '' };


export default  loginReducer = (state = INITIAL_STATE, action) =>{
    //console.log(action.payload);
    switch (action.type) {
        // case LOGIN_ATTEMPT:
        //     return { ...INITIAL_STATE, loading: true };
        case LOGIN_DATA:
        if ( action.payload && action.payload ) {
            return { ...INITIAL_STATE, login: action.payload };
        }
            
        case ERROR_MESSAGE:
            return { ...INITIAL_STATE, error: action.payload };
        default: return state;
    }
};
