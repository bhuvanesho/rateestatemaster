import { REGISTER_ATTEMPT , REGISTER_SUCCESS, REGISTER_FAILED } from './actionType';
import {database } from '../firebaseConfig/config';
import * as firebase from "firebase";
var criteriaJSON = require('../assets/criteria.json');
var realtorJSON = require('../assets/realtors.json');
export const register = ({username, password }) => {
  console.log(criteriaJSON)
    return (dispatch) => {
        dispatch({ type: REGISTER_ATTEMPT });
       // var newPostKey = database.ref().child('User').push().key;
        firebase.auth().createUserWithEmailAndPassword(username, password)
        .then(function(user) {




          // user signed in
          console.log("createdid",user.user.uid);
          console.log("user creation starts")
          database.ref('User/' + user.user.uid).set({
            "accountId":user.user.uid,
            "userId":user.user.uid,
            "username":username.toLowerCase().trim(),
            "EmailId":username.toLowerCase().trim(),
            "phoneNumber":"",
            "createdDate":Date.now(),
            "updatedDate":Date.now(),
            "LoggedIn":"i",
            "loginCount":"1",
            "loggedInflag":"1",
            "status":"A"
        })
        console.log("User creation ends")
        //Profile page
        console.log("Profile creation starts")
        database.ref('profile/' + user.user.uid).set({   
            "id":user.user.uid,
            "userId":user.user.uid,
            "firstName" : '',
            "lastName" : '',
            "gender" : '',
            "maritalStatus" :'',
            "typeOfHomeDesired" : '',
            "rangeOfHouseHoldIncome" : '',
            "ownVehicle" : '',
            "numberOfPeopleInHome" : '',
            "ageRangeOfPeopleFrom"  : '',
            "ageRangeOfPeopleTo" : '',
            "BudgetedPrice" : '',
            "dob" : '',
            "sharingAccount" : '',
            "sharingUserFirstName" : '',
            "sharingUserLastName": ''
            
        })
        console.log("Profile creation ends")

          //FilterAndSort table creation start
        console.log("Filter and Sort table creation starts")
        database.ref('FilterAndSort/' + user.user.uid).set({   
            "Id":user.user.uid,
            "UserId":user.user.uid,
            "CheckAll": '1',
            "HighToLowPrice": '0',
            "HighToLowRating": '1',
            "LowToHighPrice": '1',
            "LowToHighRating": '0',
            "NotRated": '1',
            "ParticularCriteria": '0',
            "PriceFrom": '100000',
            "PriceTo": '6000000',
            "Rated": '1',
           // "filterNsortQuery": alertMSG.en['FilterString'],
            "filterNsortQuery": "0",
            "username": ''
        })
        console.log("filter and sort creation ends")
        
        for (let i = 0; i < criteriaJSON.length; i++) {

          var newPostKey = database.ref().child('criteria').push().key;
           database.ref('criteria/' + newPostKey).set({
            "Id":newPostKey,
            "CriteriaName":criteriaJSON[i]['CriteriaName'] ,
            "CriteriaType":criteriaJSON[i]['CriteriaType'],
            "Weightage":0,
            "Percentage":0,
            "filterPercentage":0,
            "UserId":user.user.uid,
            "createdDate":"",
            "updatedDate":"",
            "SortedStatus":"",
            "Status":"Inactive",
        })
        }


       
         /* code for creating only retailors manually. This needs to be automated in future
           database.ref('Realtor/' + user.user.uid).set({
            "agentId":user.user.uid,
            "firstName":"" ,
            "lastName":"",
            "Status":"A",
            "address":"",
        })*/
       
        
        return true;
       })
        .catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      if (errorCode == 'auth/weak-password') {
        alert('The password is too weak.');
      } else {
        alert(errorMessage);
        return false;
      }
      console.log(error);
    });
   
    
    }   
}



export const registered = (register) => {
    return ({ type: REGISTER_SUCCESS, payload: register });
}
