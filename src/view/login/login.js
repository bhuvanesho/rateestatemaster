import React, { Component } from 'react';
import {
    AppRegistry, StyleSheet, TextInput, View, Text, TouchableOpacity, Image, KeyboardAvoidingView, Animated,
    Linking, Keyboard, TouchableWithoutFeedback, Alert
} from 'react-native';
import styles, { IMAGE_HEIGHT, IMAGE_HEIGHT_SMALL } from './styles';
import { Container, Content } from 'native-base';
import { MKCheckbox } from 'react-native-material-kit';
import LoginController from '../../controller/Login';
import Orientation from "react-native-orientation";
import { AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import * as firebase from "firebase";

import { login, signIn } from '../../actions/loginAction';
var customData = require('../../firebaseConfig/commonConstant.js');
import helper from '../helper'
//import data from '../../firebaseConfig/commonConstant.json'
const launchscreenLogo1 = require("../../../assets/loginScreenLogo.png");
var SQLite = require('react-native-sqlite-storage')
var db = SQLite.openDatabase({ name: "HomeRatingDB.db" });
import Spinner from 'react-native-loading-spinner-overlay';

class Login extends LoginController {
    constructor(props) {
        super(props)
        this.state = {
            isRealtor:false,

        }
    }
    handleSignIn = async () => {
        this.setState({ spinner: true })
        const { username, password } = this.state
        firebase
            .auth()
            .signInWithEmailAndPassword(username, password)
            .then((user) => {
               this.checkRealtor(user.user.uid);
               setTimeout(() => {
                 if(this.state.isRealtor){
                     this.setState({ spinner: false })
                    } 
                 else {
                    this.loginUser(user.user.uid,username,password);  
                    }
               }, 4000);
                
            }).catch((error) => {
                this.errorMessage(error);
            })


    }

    errorMessage = (error) => {
        this.setState({ spinner: false })
        setTimeout(() => {
             alert("Invalid Credentials " + error);
        }, 1000);
       

    }

   

    openRegisterPage = () => {
        this.props.navigation.navigate("Somepage")

    };
    render() {
        let loginStatus1 = this.props.state.login
        //console.log("message", this.props.state.login)
        let loginStatus = this.props.state.login.login && this.props.state.login.login.status
       // console.log(loginStatus)


        return (
            <TouchableWithoutFeedback onPress={() => { Keyboard.dismiss() }}>
                <Container style={styles.container}><Spinner visible={this.state.spinner} />
                    {/* <KeyboardAvoidingView behavior="padding" style={{ flex: 1 }}> */}

                    <View style={styles.MainContainer}>
                        <View style={{ alignItems: 'center' }}>
                            <Image source={launchscreenLogo1} style={styles.loginScreenLogo} />
                        </View>
                        <Text>{"\n"}</Text>
                        <View >
                            <Text style={styles.title}>USERNAME (E-mail)</Text>
                            <TextInput required={true}
                                autoCapitalize="none"
                                onChangeText={(text) => this.setState({ username: text })}
                                style={styles.TextInputStyleClass}
                            />
                        </View>
                        <Text>{"\n"}</Text>
                        <View >
                            <Text style={styles.title}>PASSWORD</Text>
                            <TextInput secureTextEntry={true}
                                autoCapitalize="none"
                                onChangeText={(password) => this.setState({ password: password })}
                                style={styles.TextInputStyleClass} />
                        </View>
                        <Text></Text>
                        <View style={{ flexDirection: 'row' }}>
                            <MKCheckbox checked={this.state.loginCheckedStatus}
                                borderOnColor='#1DA4DF'
                                fillColor='#1DA4DF'
                                onCheckedChange={this.loginChecked}
                                style={styles.filterCheckBoxAlign} />
                            <Text style={styles.RememberMe}>Remember me</Text>

                        </View>
                        <View>
                            <Text style={styles.noAccount}
                                onPress={() => this.openRegisterPage()}>No Account? Create one!</Text>
                        </View>
                        <Text>{'\n'}</Text>

                        <View style={{ alignItems: 'flex-end' }}>
                            <TouchableOpacity
                                style={styles.SubmitButtonStyle}
                                onPress={() => this.handleSignIn()}>
                                <Text style={styles.TextStyle}> LOG IN </Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    {/* </KeyboardAvoidingView> */}
                </Container>
            </TouchableWithoutFeedback>
        );
    }
};
const mapStateToProps = state => {
    console.log(state.login.error)
    return {
        state: state,
    };
}

export default connect(mapStateToProps, {
    // /
    signIn
})(Login);

