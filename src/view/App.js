/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Image,
  Platform,
  StyleSheet,
  Text,
  View,
  YellowBox,
  TouchableOpacity
} from 'react-native';
import { Spinner } from 'native-base';
import Orientation from "react-native-orientation";
import Database from '../Db/database';
import Loginscreen from './login/login';
import WelcomeScreen from './welcomescreen/welcome';
import ProfileScreen from './profile/profileDetails';
import Filter from './ListFavFilterSort/filter';
import Listing from './ListFavFilterSort/listingScreen';
import Favourite from './ListFavFilterSort/favourite';
import Profile from '../view/profile/profileDetails';
import YourCriteria from './criteria/criteriasetting';
import CriteriaWeightage from './criteria/criteriaweightage';
import PropertyDetail from '../view/property/propertydetails'
import Login from '../view/login/login';
import Somepage from '../view/Register/somePage';
import Agreement from '../view/Agreement/Agreement';
import Home from '../view/personalView/index';
import Relatorhomepage from '../view/realtorHomePage/index';
import SharedAccount from '../view/realtorHomePage/sharedAccount';

import WelcomeScreenSkip from './welcomescreen/welcomeSkip';
import WelcomeRealtor from './welcomescreen/welcomeRealtor';

// import ProfileData from '../view/profile/profile'
import Launch from './launchscreen/index';

import Tabs from './TabNavigator';

import styles from './styles';

import { StackNavigator, DrawerNavigator } from "react-navigation";
import { Button } from 'react-native-elements';
import criteria from '../controller/criteriasetting';
var SQLite = require('react-native-sqlite-storage')
var db = SQLite.openDatabase({ name: "HomeRatingDB.db" });

const AppNavigator = StackNavigator(
  {
    //Launch: { screen: Launch },

    Login: { screen: Login },
    // ProfileData: { screen: ProfileData },
    YourCriteria: { screen: YourCriteria },
    CriteriaWeightage: { screen: CriteriaWeightage },
    Tab: { screen: Tabs },
    PropertyDetail: { screen: PropertyDetail },
    Filter: { screen: Filter },
    List: { screen: Listing },
    ProfileUpdate: { screen: Profile },
    Login: { screen: Loginscreen },
    Somepage: { screen: Somepage },
    Agreement: { screen: Agreement },
    Welcome: { screen: WelcomeScreen },
    WelcomeSkip: { screen: WelcomeScreenSkip },
    WelcomeRealtor: { screen: WelcomeRealtor },
    Home: { screen: Home },
    Relatorhomepage: { screen: Relatorhomepage },
    SharedAccount: { screen: SharedAccount },
  },
  {
    initialRouteName: 'Login',
    navigationOptions: { gesturesEnabled: false },
    headerMode: "none",
  }
);

const AppNavigator1 = StackNavigator(
  {
    //Launch: { screen: Launch },


    Login: { screen: Login },
    YourCriteria: { screen: YourCriteria },
    CriteriaWeightage: { screen: CriteriaWeightage },
    Tab: { screen: Tabs },
    PropertyDetail: { screen: PropertyDetail },
    Filter: { screen: Filter },
    List: { screen: Listing },
    ProfileUpdate: { screen: Profile },
    Login: { screen: Loginscreen },
    Register: { screen: Somepage },
    Welcome: { screen: WelcomeScreen },
    WelcomeSkip: { screen: WelcomeScreenSkip },
    WelcomeRealtor: { screen: WelcomeRealtor },
    Home: { screen: Home },
    Relatorhomepage: { screen: Relatorhomepage },
    SharedAccount: { screen: SharedAccount },
    

  },
  {
    initialRouteName: 'Tab',
    navigationOptions: { gesturesEnabled: false },
    headerMode: "none",
  },

);




export default class App extends Database {
  constructor(props) {
    super(props);


    super.createTable();
    //super.insertcodeType();
    //super.insertContext();
    //super.insertcodeValue();
    // super.insertProperty();
   // super.insertProfile();
    // super.insertCriteria();
    //super.sqliteMaster();
    super.pageDecideModal();
  }

  componentWillMount() {
    setTimeout(function () {
      this.setState({ time: true });
    }.bind(this), 4000);
  }
  componentDidMount() {
    Orientation.lockToPortrait();
  }


  render() {
    if (!this.state.timepassed) {
      return (
        // <Launch />
        <View style={styles.loader}>
          <Spinner color={styles.loadingColor} />
        </View>
      )
    }
    //else {
    if (this.state.loggedIn == false) {
      return (
        <AppNavigator />
      );
    }
    
    if (this.state.loggedIn == true) {
      return (
        <AppNavigator1 />
      );
    }
    // }
  }
}
