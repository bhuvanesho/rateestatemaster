
import criteria from '../../controller/criteriasetting';
import Orientation from "react-native-orientation";
import React, { Component } from "react";
import { ScrollView, Image, TouchableOpacity } from 'react-native';
import {
    Container,
    View,
    Header,
    Title,
    Button,
    Icon,
    Tabs,
    Tab,
    Right,
    Left,
    Text,
    Body,
    List,
    ListItem
} from "native-base";
import { MKTextField, MKColor, MKIconToggle, MKSwitch, MKSlider } from 'react-native-material-kit';

import styles from './styles';
import mainStyles from '../../view/styles';
import {NavigationEvents} from 'react-navigation';


class Criteriaweightage extends criteria {
    constructor(props) {
        super(props)
        this.calPercentage();
        if (this.state.weightageSelected) {
            this.setState({weightageSelected: false});
            this.getActiveCriteria();
        }
    }
    componentDidMount(){
        Orientation.lockToPortrait();
        this.props.navigation.addListener('willFocus', () => this.setState({ weightageSelected: true }));
        setTimeout(function () {
            this.getActiveCriteria();
        }.bind(this), 1000);
    }
    //added as the active weightage was not updating active criteria selected
      componentDidUpdate(prevProps, prevState) {
        Object.entries(this.props).forEach(([key, val]) =>
        prevProps[key] !== val && console.log(`Prop '${key}' changed`)
      );
     
      Object.entries(this.state).forEach(([key, val]) =>
        prevState[key] !== val && console.log(`State '${key}' changed`)
       
      );
      }
    
    hideToolTip(){
        var activeCriteria = this.state.criteriaInfo;
        for (let i = 0; i < activeCriteria.length; i++) {
         this.setState({['toolTip_'+activeCriteria[i]['criteriaId']]:false});
        }
    }
    render() {
       {/*} <NavigationEvents onDidFocus={() =>  this.getActiveCriteria()} />*/}
       if (this.state.weightageSelected) {
        this.setState({ weightageSelected: false});
        this.getActiveCriteria();
    }
        var activeCriteria = this.state.criteriaInfo;
        return (

            <Container style={styles.weightageBg} >
                <Header style={styles.headerBg}>
                   

                        <Left>
                            { this.state.editable=="false" ? 
                            <Image source={require('../../../assets/Headerlogo.png')} style={mainStyles.headerBGimage} >
                            </Image> : null
                            }
                        </Left>
                        <Body>
                            { this.state.editable =="true" ? <Image source={require('../../../assets/Headerlogo.png')} style={mainStyles.headerBGimage} /> : null
                            }
                            { this.state.editable =="false" ? <Text style={mainStyles.emailTextFieldOrange}>{this.state.influencerName}</Text> : null
                            } 
                        </Body>
                        
                    <Right>
                        <TouchableOpacity onPress={this.criteriaDone}>
                            { this.state.editable =="true" ?<Text style={styles.submitHeader}>Done</Text>:null
                            }
                            { this.state.editable =="false" ? <Text style={styles.submitHeader}>Done</Text>:null}
                           {/* <Image source={require('../../../assets/share.png')} style={styles.shareImage2}></Image> */}
                            
                        </TouchableOpacity>
                    </Right>
                </Header>
                <View style={styles.criteriaBG}>
                    <View style={styles.criteriaFlex}>
                        <Left>
                            <TouchableOpacity style={styles.arrowback} onPress={() => this.navigateToYourCriteria()}>
                                <Icon name="ios-arrow-back" type="Ionicons" style={styles.backIcon} />
                            </TouchableOpacity>
                        </Left>
                        <Text style={styles.evaluationCriteria}>EVALUATION CRITERIA</Text>
                        <Right />
                    </View>
                </View>
                <View onTouchStart={()=>this.hideToolTip()}>
                    {this.state.showMessage == true ?
                        activeCriteria.length != 0 ?
                            <Text style={styles.scalemsg}>
                                Please rate your selected criteria based on how important they are to you (Scale of 1 low to 5 high)
                       </Text>
                            :
                            <View style={styles.noCriteria}>
                                <Text style={styles.notfound}>
                                    No criteria found to evaluate. Please go back to 'Your Criteria' and select criteria to evaluate.
                      </Text>
                            </View> :
                        null
                    }
                </View>
                <ScrollView onTouchStart={()=>this.hideToolTip()} bounces={false} style={styles.weightageContainer} >

                    <View style={styles.weightageBg}  pointerEvents={this.state.disableView}>
                        {activeCriteria.map(activeCriteria => (
                            <View style={styles.criteriaFlex}>
                                <View style={styles.weightageView} >
                                    {this.state['toolTip_' + activeCriteria.criteriaId] && activeCriteria.criteriaName.length > this.state.criteriaExceed ?
                                        <View style={styles.tooltipContainer}>
                                            <Text style={styles.toolTip}>{activeCriteria.criteriaName}</Text>
                                        </View>
                                        : null
                                    }
                                    <TouchableOpacity activeOpacity={0.7} onPressIn={()=>this.hideToolTip()} onPressOut={() => this.setState({ ['toolTip_' + activeCriteria.criteriaId]: true })}>
                                        <Text style={styles.weightagefont}>{activeCriteria.criteriaName.substring(0, 21)}
                                            <Text style={styles.weightagefont}>{activeCriteria.criteriaName.length > 21 ? '...' : null}</Text>
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                                {!this.state.onScreenChange ? this.state['weightage_' + activeCriteria.criteriaId] = activeCriteria.weightage : null}
                                {weightage = this.state['weightage_' + activeCriteria.criteriaId]}
                                <View style={styles.sliderFlex}>
                                    <View style={styles.slidertorange}>
                                        <Text style={styles.newrangefont}>{weightage == null || 0 ? 0 : weightage}</Text>
                                    </View>
                                    <View style={styles.slidertorange} >
                                        <MKSlider
                                            thumbRadius={3}
                                            lowerTrackColor={styles.slidercolor}
                                            min={0}
                                            max={5}
                                            value={weightage == null ? 0 : weightage}
                                            style={styles.sliderwidth}
                                            onChange={(curValue) => this.sliderweightage(activeCriteria.criteriaId, curValue.toFixed(0))}
                                        />
                                    </View>
                                    <View style={styles.slidertorange}>
                                        <Text style={styles.tabweightagefont}>5</Text>
                                    </View>
                                </View>
                            </View>
                        ))}
                    </View>
                    {/* back naviagtion button image */}
                    {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('CriteriaSetting')} style={styles.backNav}>
                        <Image style={styles.navigator} source={require('../../../assets/leftArrow.png')} />
                    </TouchableOpacity> */}
                </ScrollView>
            </Container >
        );
    }
}

export default Criteriaweightage;
