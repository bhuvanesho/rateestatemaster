import { CREATE_PROFILE, CREATE_PROFILE_ATTEMPT , GET_PROFILE_DATA,
    PROFILE_ERROR_MESSAGE,
    UPDATE_PROFILE } from './actionType';
import {database } from '../firebaseConfig/config';
import {AsyncStorage} from 'react-native';


export const secondPageProfileUpdate = (data) => async dispatch =>{
    console.log("actio page2", data);
    try {
        const userId = await AsyncStorage.getItem('userId');
        const id = await AsyncStorage.getItem('profileId');
        if (userId !== null  ) {
          console.log("update",data.typeOfHomeDesired,userId)
            database.ref('profile'+'/'+userId).update(
            {
                "BudgetedPrice" : 2,
                "typeOfHomeDesired:" : 1
            }
            
        )
        console.log('update ends2')
      
          dispatch({
            type:UPDATE_PROFILE,
            payload:{
                "BudgetedPrice":userI2d,
                "usertypeOfHomeDesiredId":use1rId
            }
        })
        }
        
      } catch (error) {
        console.log('Error in Update Profile2:',error);
        throw (error);
      }   


}
export const doInsertProfile = (data) => async dispatch =>{
    console.log("actio page", data);
    try {

        console.log("firstName:",data.firstName);
        console.log("lastName:",data.lastName);
        console.log("selectedGender:",data.selectedGender);
        console.log("selectedMaritalStatus:",data.selectedMaritalStatus);
        console.log("chosenDate:",data.chosenDate);
        
        

        const userId = await AsyncStorage.getItem('userId');
        const id = await AsyncStorage.getItem('profileId');
        console.log("value==>>>>>",userId)
        console.log("value==>>>>>",id)
        if (userId !== null  ) {
          // We have data!!
          console.log("update",data.typeOfHomeDesired,userId)
          //console.log(database.ref('profile/'+id)).update({"firstName":'asasaaa'})
        //   database.ref('profile').child(id).update(
            
            database.ref('profile'+'/'+userId).update(
            {
                //"id":userId,
                //"userId":userId,
                "firstName" : data.firstName,
                "lastName" : data.lastName,
                "gender" : data.selectedGender,
                "maritalStatus" :data.selectedMaritalStatus,
                "dob" : data.chosenDate,
            }
            
        )
        console.log('update ends')
      
          dispatch({
            type:UPDATE_PROFILE,
            payload:{
                "id":userId,
                "userId":userId,
                "firstName" : data.firstName,
                "lastName" : data.lastName,
                "gender" : data.selectedGender,
                "maritalStatus" :data.maritalStatus,
                "dob" : data.chosenDate
            }
        })
        }
        
      } catch (error) {
        console.log('Error in Update Profile:',error);
        throw (error);
      }   


}

export const createProfile = (id) => dispatch => {
    console.log("from action", id)
   database.ref('profile/')
                .once('value', function (snapshot) {
                    snapshot.forEach((item) => {
                        console.log("From CreateProfilefunction",item);
                        let data = item.val();
                        console.log("from actio data", data)
                        if(id === data.userId){
                           // console.log("from action page",data);
                            try {
                                 AsyncStorage.setItem('IsProfileExists', "true");
                              } catch (error) {
                                // Error saving data
                              }
                            dispatch({ 
                                type: GET_PROFILE_DATA,
                                payload: data
                            })
                        }               

                    })
                });
    
}

export const profileData = (data) => {
    return ({ type: GET_PROFILE_DATA, payload: data });
}