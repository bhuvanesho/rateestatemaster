
import React, { Component } from 'react';
import {Alert} from 'react-native';

export default class Home extends Component {

constructor(props) {
 super(props);
 this.navigateToCriteriaCreation = this.navigateToCriteriaCreation.bind(this);
 this.navigateToHousesYetToRate = this.navigateToHousesYetToRate.bind(this);
 this.navigateToRatedHouses = this.navigateToRatedHouses.bind(this);
 this.navigateToRatingSummary = this.navigateToRatingSummary.bind(this);

 }
  
  

 navigateToCriteriaCreation()
  {
  this.props.navigation.navigate("Criteria");
 }

 navigateToHousesYetToRate()
  {
    this.props.navigation.navigate("Property",{RatingStatus:'Unrated'});
 }

 navigateToRatedHouses()
  {
    this.props.navigation.navigate("Property",{RatingStatus:'Rated'});
 }

 navigateToRatingSummary()
  {
    this.props.navigation.navigate("RatingSummary");
 }

}