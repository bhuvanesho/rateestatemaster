import React, { Component } from 'react';
import { Text, View, TextInput, Button, Image, TouchableOpacity, DatePickerIOS, Picker, TouchableWithoutFeedback, KeyboardAvoidingView, Keyboard, ScrollView } from 'react-native';
import Orientation from "react-native-orientation";
import { Container, Header, Left, Body, Icon, Right, Item, Input, Content, Footer, FooterTab, Label } from 'native-base';
import styles from './styles';
import { MKTextField } from 'react-native-material-kit';
import FloatingLabel from 'react-native-floating-labels';
import { AsyncStorage } from 'react-native';
import { realtorHomePage } from '../../actions/actionRealtorHomePage';
import { connect } from 'react-redux';
import { database } from '../../firebaseConfig/config';
import * as firebase from "firebase";
import { Alert } from 'react-native';
import helper from '../helper'
import Login from '../../model/Login';
const launchscreenLogo1 = require("../../../assets/loginScreenLogo.png");
const backArrow = require("../../../assets/blueBackArrow.png");

class RealtorHome extends Component {
    constructor(props) {

        super(props)
        this.getClients();
        this.state = {
            allClientsData: [],
            sharedAccountData: [],
            primaryClientData: [],
            userId: '',
            firstNameBorderColor: '',
            firstNameLabelFont: 21,
            firstNameLabelFontColor: '#ededed',
            floatinglabelStyle: { fontFamily: 'AspiraXXXNar-Regular', paddingLeft: 0 },
            active: false,
            personalViewActiveCss: '',
            permanetUserId: '',

        }

    }
    async componentDidMount() {
        try {
           

        } catch (error) {

        }

    }

    getClients = async () => {
        var agentId = await AsyncStorage.getItem('agentId');
        this.setState({ permanetUserId: agentId })

        // pull all clients with matching agent id
        var AllClients = [];
        var primaryClients = [];
        var ref1 = database.ref('profile/')
        ref1.orderByChild('agent')
            .startAt(agentId)
            .endAt(agentId)
            .once('value', function (snapshot1) {
                snapshot1.forEach(function (childSnap1) {
                    AllClients.push({
                        "influencerName": childSnap1.val().firstName + " " + childSnap1.val().lastName,
                        "userId": childSnap1.val().userId,
                        "accountId": 0,
                        "influencers": 0,
                    })
                }.bind(this));

                //loop through all the users and check if this userid matches with account id
                //if it matches they are primary account holders
                for (let i = 0; i < AllClients.length; i++) {
                    var userId = AllClients[i].userId;

                    var ref = database.ref('User/')
                    ref.orderByChild('userId')
                        .startAt(userId)
                        .endAt(userId)
                        .once('value', function (snapshot) {
                            snapshot.forEach(function (childSnap) {
                                AllClients[i].accountId = childSnap.val().accountId;
                            }.bind(this));
                            this.setState({ allClientsData: AllClients })

                            //filter the primary users for first page display 
                            primaryClients = AllClients.filter(function (obj) {
                                return obj.userId == obj.accountId;
                            });
                            for (j = 0; j < primaryClients.length; j++) {
                                let tot = AllClients.filter(function (obj) 
                                { return obj.accountId == primaryClients[j].accountId }).length;
                                primaryClients[j].influencers = tot;
                            }

                            this.setState({ primaryClientData: primaryClients })
                        }.bind(this));

                }

            }.bind(this));
    }

    getSharedAccount = (userId) => {
        var sharedAccount = [];

        var AllClients = this.state.allClientsData;
        sharedAccount = AllClients.filter(function (obj) {
            return obj.accountId == userId;
        });

        this.setState({ sharedAccountData: sharedAccount })
    }

    setId = async (id, name, count) => {
        try {
            await AsyncStorage.setItem('userId', id);
            await AsyncStorage.setItem('influencerNameValue', name);

            this.setState({ userId: id })
            this.setState({ active: true })

            if (count > 1) {
                this.getSharedAccount(id);
                this.props.navigation.navigate('SharedAccount',{sharedAcc:this.state.sharedAccountData});
            }
            else {
                this.props.navigation.navigate('WelcomeSkip');
            }
        } catch (error) {
           // console.log(error)
        }

    }

    setNavigation = async (userId, permanetUserId) => {




        //userId = await this.getParameterValue('user')
        //console.log(userId + '  value pulled from db')
        //permanetUserId = await this.getParameterValue('permanent');

        if (userId === permanetUserId) {
            this.setState({ navigation: 'ProfileUpdate' })
            //set two major flags which control the read only for floating label and dropdowns in all screens
            await AsyncStorage.setItem('editableValue', JSON.stringify(true));
            await AsyncStorage.setItem('dropdownDisabledValue', JSON.stringify(false));

            //setParameters(JSON.stringify(true),'editable');
            //setParameters(JSON.stringify(false),'dropdown');



        }
        else {
            this.setState({ navigation: 'Profile' })
            //set two major flags which control the read only for floating label and dropdowns in all screens
            await AsyncStorage.setItem('editableValue', JSON.stringify(false));
            await AsyncStorage.setItem('dropdownDisabledValue', JSON.stringify(true));

            //setParameters(JSON.stringify(false),'editable');
            //setParameters(JSON.stringify(true),'dropdown');
        }


    }



    getPersonalViewStyle = (touch, id) => {

        if (touch && this.state.permanetUserId === id) {

            return {
                backgroundColor: '#1DA4DF',
                color: '#fff',
                fontFamily: 'AspiraXXXNar-Regular',
                justifyContent: 'center',
                marginTop: 40,
                paddingTop: 15,
                borderRadius: 10,
                borderWidth: 1,
                borderColor: '#fff',
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                fontSize: 21,
                zIndex: 1,

            }

        } if (touch && this.state.permanetUserId !== id) {
            return {
                backgroundColor: '#ededed',
                color: "#727171",
                fontFamily: 'AspiraXXXNar-Regular',
                justifyContent: 'center',
                marginTop: 40,
                paddingTop: 15,
                borderRadius: 10,
                borderWidth: 1,
                borderColor: '#fff',
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                fontSize: 21,
                zIndex: 1,
            }
        }

        if (this.state.permanetUserId === id) {
            return {
                backgroundColor: '#1DA4DF',
                color: '#fff',
                fontFamily: 'AspiraXXXNar-Regular',
                justifyContent: 'center',
                marginTop: 40,
                paddingTop: 15,
                borderRadius: 20,
                borderWidth: 1,
                borderColor: '#1DA4DF',
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                fontSize: 21,
                zIndex: 1,

            }
        }

        if (this.state.permanetUserId !== id) {
            return {
                backgroundColor: '#ededed',
                color: "#727171",
                fontFamily: 'AspiraXXXNar-Regular',
                justifyContent: 'center',
                marginTop: 40,
                paddingTop: 15,
                borderRadius: 10,
                borderWidth: 1,
                borderColor: '#fff',
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                fontSize: 21,
                zIndex: 1,
            }
        }

    }
    getActiveStyle = (touched, id) => {

        if (touched && this.state.userId === id) {
            return {
                backgroundColor: "#1DA4DF",
                color: '#fff',

                fontFamily: 'AspiraXXXNar-Regular',
                fontSize: 21,
                margin: 0,
                marginTop: 0,
                // borderRadius: 10,
                paddingTop: 15,
                paddingLeft: 5,
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                zIndex: 1,
            }
        }
        if (touched && this.state.userId !== id) {
            return {
                backgroundColor: "#ededed",
                color: "#727171",

                fontFamily: 'AspiraXXXNar-Regular',
                fontSize: 21,
                margin: 0,
                marginTop: 0,
                // borderRadius: 10,
                paddingTop: 15,
                paddingLeft: 5,
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                zIndex: 1,

            }
        }
        if (this.state.userId === id) {
            return {
                backgroundColor: "#1DA4DF",
                color: '#fff',

                fontFamily: 'AspiraXXXNar-Regular',
                fontSize: 21,
                margin: 0,
                marginTop: 0,
                // borderRadius: 12,
                paddingTop: 15,
                paddingLeft: 5,
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                zIndex: 1,

            }
        }
        if (this.state.userId !== id) {
            return {
                backgroundColor: "#ededed",
                color: "#727171",
                fontFamily: 'AspiraXXXNar-Regular',
                fontSize: 21,
                margin: 0,
                marginTop: 0,
                //  borderRadius: 12,
                paddingTop: 15,
                paddingLeft: 5,
                height: 50.015748031,
                width: 305.18110236,
                textAlign: 'center',
                zIndex: 1,

            }
        }

    }

    logout = async () => {
       // await AsyncStorage.setItem('userId', '');
       // await AsyncStorage.setItem('influencerNameValue', '');
       // await AsyncStorage.setItem('agentId', '');
        AsyncStorage.clear();
        setTimeout(() => {
            this.props.navigation.navigate('Login');
        }, 1000);

    }

    render() {
       
            return (
                <Container style={styles.container}>
                     <Header style={styles.headerBg}>
                        <Left>
                        </Left>
                        <Body>
                        </Body>
                    <Right></Right>
                </Header>
                     <View style={styles.propertyDetailsBG} >
                        <View style={styles.propertyFlex}>
                            <Left></Left>
                            <Text style={styles.propertyDetails}>AGENT HOME</Text>
                            <Right >
                                <TouchableOpacity onPress={() => this.logout()}>
                                    <Image style={styles.filterIcon} source={require('../../../assets/logoutIcon.png')} />
                                </TouchableOpacity >
                            </Right>
                        </View>
                    </View>
                    <View style={styles.MainContainer}>
                        <View style={{ alignItems: 'center' }}>
                            <Image source={launchscreenLogo1} style={styles.loginScreenLogo2} />
                        </View>
                       
                        <Text style={styles.emailTextFieldBlue}>{this.state.primaryClientData.length>0 ? 'Select Client View':'No Client Available!'}</Text>
                     <ScrollView>
                        {this.state.primaryClientData && this.state.primaryClientData.map((item, id) => {
                            return (<View style={{ alignItems: 'center' }} >
                                <TouchableOpacity onPress={() => this.setId(item.userId, item.influencerName, item.influencers)} >
                                    <Text style={this.getActiveStyle(this.state.active, item.userId)}>{item.influencerName} </Text>
                                    <Image source={(item.influencers > 1) ? ((this.state.userId === item.userId) ? require('../../../assets/shareGrey.png') : require('../../../assets/share.png')) : null} style={styles.shareImage2}></Image>
                                </TouchableOpacity>
                                <Text>{"\n"}</Text>
                            </View>
                            )
                        })}
                        </ScrollView>
                    </View>
                </Container>
            )
    }
}
const mapStateToProps = state => {
    return {
        state: state

    };
}

export default connect(mapStateToProps, { realtorHomePage })(RealtorHome);
