import React, { Component } from 'react';
import { AppRegistry, StyleSheet, TextInput, View, Text, TouchableOpacity, Image, KeyboardAvoidingView, Animated, 
 Linking,Keyboard ,TouchableWithoutFeedback, Alert } from 'react-native';
import styles,{ IMAGE_HEIGHT, IMAGE_HEIGHT_SMALL } from './styles';
import { Container, Content } from 'native-base';
import { MKCheckbox } from 'react-native-material-kit';
import LoginController from '../../controller/Login';
import Orientation from "react-native-orientation";
import {AsyncStorage} from 'react-native';
import { connect } from 'react-redux';
import * as firebase from "firebase";

import { login , signIn } from '../../actions/loginAction';
var customData = require('../../firebaseConfig/commonConstant.js');

//import data from '../../firebaseConfig/commonConstant.json'
const launchscreenLogo1 = require("../../../assets/loginScreenLogo.png");



class Login extends LoginController {
    constructor(props){
        super(props)
        this.state = {
            username:'',
            password:'',
             

        }
    }
    handleSignIn = async () => {
        const { username, password } = this.state
        firebase
          .auth()
          .signInWithEmailAndPassword(username, password)
          .then((user) => {
             // console.log(user)
              AsyncStorage.clear()
            AsyncStorage.setItem('userId', user.user.uid);
            AsyncStorage.setItem('permanetUserId', user.user.uid);

            this.setUserIds(user.user.uid,'user');
            this.setUserIds(user.user.uid,'permanent');
            
            this.props.navigation.navigate('Welcome')
          }).catch(error => alert("Invalid Credentials"))
          
      }

 

openRegisterPage = () =>{
    this.props.navigation.navigate("Somepage")

};
    render() {
        let loginStatus1 = this.props.state.login
      // console.log("message", this.props.state.login)
       let loginStatus  =  this.props.state.login.login && this.props.state.login.login.status 
       //console.log(loginStatus)
    
        
        return (
            <TouchableWithoutFeedback onPress={() => {Keyboard.dismiss()}}>
                <Container style={styles.container}>
                {/* <KeyboardAvoidingView behavior="padding" style={{ flex: 1 }}> */}

                    <View style={styles.MainContainer}>
                        <View style={{ alignItems: 'center' }}>
                            <Image source={launchscreenLogo1} style={styles.loginScreenLogo} />
                        </View>
                        <Text>{"\n"}</Text>
                        <View >
                            <Text style={styles.title}>USERNAME (E - mail)</Text>
                            <TextInput required={true}
                                autoCapitalize="none"
                                onChangeText={(text) => this.setState({ username: text })}
                                style={styles.TextInputStyleClass} 
                                />
                        </View>
                        <Text>{"\n"}</Text>
                        <View >
                            <Text style={styles.title}>PASSWORD</Text>
                            <TextInput secureTextEntry={true}
                                autoCapitalize="none"
                                onChangeText={(password) => this.setState({ password: password })}
                                style={styles.TextInputStyleClass} />
                        </View>
                        <Text></Text>
                        <View style={{ flexDirection: 'row' }}>
                            <MKCheckbox checked={this.state.loginCheckedStatus}
                                borderOnColor='#1DA4DF'
                                fillColor='#1DA4DF'
                                onCheckedChange={this.loginChecked}
                                style={styles.filterCheckBoxAlign} />
                            <Text style={styles.RememberMe}>Remember me</Text>

                        </View>
                        <View> 
                        <Text style={styles.noAccount}
                        onPress={ ()=> this.openRegisterPage() }>No Account ? Create one!</Text>
                        </View>
                        <Text>{'\n'}</Text>
                        
                            <View style={{ alignItems: 'flex-end' }}>
                                <TouchableOpacity
                                    style={styles.SubmitButtonStyle}
                                    onPress={() => this.handleSignIn()}
                                    // onPress = {() =>{
                                    //     this.props.signIn(this.state.username, this.state.password).then(() =>{
                                    //         console.log("data")
                                    //     })
                                    //     console.log(this.props.state.login)
                                    //     if(this.props.state.login == null){
                                    //         Alert.alert("Invalid Credentials", this.getErrorMessages());
                                    //     }else if(this.props.state.login.login && this.props.state.login.login.status == true){
                                    //         this.props.navigation.navigate('Profile');
                                    //     }else{
                                    //         Alert.alert("Invalid Credentials", this.getErrorMessages());
                                    //     }
                                       
                                    //     console.log(this.props.state.login)
                                    // }}
                                    // onPress={() =>{
                                    //   let ids =  AsyncStorage.setItem('userId', this.props.state.login.login.id);
                                    //   console.log(ids)
                                    // try {
                                    //     const value =  AsyncStorage.setItem('profileId', this.props.state.login.login.userId);
                                    //     if (value !== null) {
                                    //       // We have data!!
                                    //       let {username, password} = this.state
                                    //       if(!username || !password){
                                    //           Alert.alert('Invalid Input', this.getErrorMessages());
                                    //           return;
                                    //       }else if(this.props.state.login.login.EmailId !== username ){
                                    //           Alert.alert("Invalid Email", this.getErrorMessages());
                                    //           return;
                                    //       }else if(this.props.state.login.login.password !== password){
                                    //           Alert.alert("Invalid Password", this.getErrorMessages());  
                                    //           return;                                          
                                    //       }else{
                                    //           AsyncStorage.setItem('userId', this.props.state.login.login.userId);
                                    //          // AsyncStorage.setItem('customData', customData.Gender);
                                    //          //  AsyncStorage.setItem('', this.props.state.login.login.id);
                                    //           this.props.navigation.navigate('Profile')
                                    //       }
                                    //     }
                                    //   }catch (error) {
                                    //     // Error retrieving data
                                    //   }
                                        
                                    // }}
                                >
                                    <Text style={styles.TextStyle}> LOG IN </Text>
                                </TouchableOpacity>
                            </View>
                    </View>

                {/* </KeyboardAvoidingView> */}
            </Container>
            </TouchableWithoutFeedback>
        );
    }
};
const mapStateToProps = state => {
 //console.log(state.login.error)
    return {
        state: state,
    };
}

export default connect(mapStateToProps, {
    // /
    signIn})(Login);

