# RateEstate 
> RateEstate app is real estate app, which will be having properties in listings
> you can set selected property as favorite and those will displayed in favourites tab/list
> favourites can be filtered and sorted using filter  and sort screen.
> after filter favourites will display properties based on filter and sort 
> RateEsate contains  four tabs and fiter and sort screen.
> In property details screen, user can rate the property and add photos to property by taking new photo or upload from the phone gallery .


## Installation

*	**Clone and install packages**

cd RateEstate
```

*	**Run on iOS**
	*	Opt #1:
	    *   npm install
		*   react-native link
		*	Open the project in Xcode from `ios/RateEstate.xworkspace`
		*	Click `run` button to simulate
	*	Opt #2:
		*	Run `react-native run-ios` in your terminal


*	**Run on Android**
    *   npm install
	*   react-native link
	*   install react-native-maps  from  https://github.com/react-community/react-native-maps
	*	Make sure you have an `Android emulator` installed and running
	*	Run `react-native run-android` in your terminal

Technology Used 

React Native, SQlite.

   
	 In RateEstate folder
	    
		  Package.json - which manages packages for project

          index.js - which register whole js object into app

		  App.js which renders app main component
     
	PATH:RateEstate/src/view/App.js

	        which imports all app components and has config of  react navigation  of components and creates database, tables and insert data into sqlite by calling following functions from 
			
			path  RateEstate/src/Db/database.js

			        createTable -- It creates all tables for the database
                    insertcodeType -- It inserts codetype datas into codetype table
                    insertcodeValue -- It inserts codevalue datas into codevalue table
                    insertProfile -- It Insert codevalue datas into profile table
                    insertCriteria -- It inserts criteria datas into criteria table
                    pageDecideModal -- function which decides to render page based on the login details
	
	CONTROLLER

     RateEstate/src/controller/Login.js 

	        Functions

			      loginChecked -- function which checks login details whether user has logged in or not
			      signInValidation -- function to validates input
			      checkPageDecide -- function which decides to render page based on the login details
			      welcomeName -- function which deceides to show welcome message with username

	 RateEstate/src/controller/criteriasetting.js
	      
		     Functions
			 
					backCriteria - functions which goes back from 'evaluation criteria' to 'your criteria' screen
					validateCriteria - validate criteria from input boxes
					sliderweightage - function which sets criteria value to slider dynamically.
					validateCriteriaOther - validating other criteria fields.
					validateCriteriaUpdate - validates criteria from your criteria
					validateCriteriaWeightageUpdate - validates criteria weightage from evaluation criteria.
					calPercentage - percetage calculator for all weightage.
				
	 RateEstate/src/controller/ProfileCreation.js
	         
			 Functions
			        
					lastNameValidation -  function to validate last name from form input
					firstNameValidation - function to validate first name from form input
					onscreenEmail - which gets prevoius email id of shared user.
					pushEmail - this pushes mail to the email array.
					callEmailAdd - adds email to the array.
					emailValidate- validates email
					showEmailId - shows emailid in particular time
					personalProfileDatas - which call model to update data in sqlite
					
	 RateEstate/src/controller/Property.js
	         
			 Functions
			        
					checkAllCriteria -  function which checks all criteria and done calculations 
					checkAllCriteriaFetch - function which checks all criteria functions
					checkParticular - function which checks particular criteria active or inactive , then updates status of                      criteria  in sqlite
					clearAll - clear all selection in filter screen
					doneFunction - which updates criteria status and update percentage in sqlite
					addCommas - function to add commas to price value 
					sliderRating - function which sets criteria value to slider dynamically.
					favourite - function which makes property as favourite and non favourite in sqlite
					doneFilter - function which calculates percentage and updates in the sqlite.
					ratedCheck -  function which checks rated has checked or not
					unRatedCheck - function which checks unrated has checked or not
					confirmListingDelete -  confirmation message to delete property in the records
					deleteListing -  deletes property from record.
					sliderChange - function which catches values when changes in UI

	MODELS

	  RateEstate/src/model/criteriasetting.js
	          
			  Functions
			      
				     getCriteria - function which gets all criteria among interior, exterior, characteristics
					               and active criteria count
				     
					 updateCriteria - update criteria status in the sqlite and gets active criteria count
					 getActiveCriteria - get all active criteria from criteria table
					 weightageUpdate - it updates weightage for criteria in sqlite
					 percentageUpdate - it updates percentage of criteria in sqlite
					 updateOtherCriteria -  update other criteria name in sqlite
					 refreshFilter -  it refeshes filter in favourite screen when changes in your criteria,
					                  evaluation criteria
					 calculateNewRatingArun - it calculates new rating with percentage of all criteria and all property
					 filterRatingPropertyArun - updates calculates overall rating in sqlite for property
					 reCalculatePercentage - recalculates percentage of the property
					 reCalculateOverallRating -  recalculates overall rating of the property

	  RateEstate/src/model/Login.js
	          
			  Functions

					  signIn - allows user to sign in and insert property from json file based on the user,insert different property for different users	     
					  welcomeNameModel -  shows welcome message with username 
					  getBudgetedPriceRange - function which gets budgeted price range
					  getSharedEmail - function to get all shared email details of shared users
					  updateNinsertEmail - update old email and insert new email in sqlite
					  getMonth - function toget month to bind in the UI Form
					  getGender - It gets all gender from sqlite
					  getMaritalStatus - gets marital status from code value
					  getHouseHoldIncome - gets householdincome ranges from codevalue
					  getDesiredHome -  gets getDesiredHome  from codevalue
					  getYesOrNo - gets Yes or NO  ranges from codevalue 
					  updateProfile - function to update entered profile details from form
					  getPersonalProfileDetails - function to get previously saved user information 
					  showEmailSharingForwardArrow - function to getarrow symbol in the UI 


       RateEstate/src/model/Property.js
	          
			  Functions
			         
					   insertNewPhotos - insert new photo to the property added from the UI
					   percentageUpdate - function which updates  property percentage criteria
					   calculatefilterPercentage - calculates overall percentage and  updates in the UI
					   clearFilter - it clear filter selection in the db
					   refreshFilter - updates filter selection to inactive defaultly
					   addComments - update comments for the property in th sqlite
					   selectedCriteria - gets criteria from filter selection
					   calculateSortedRating - calculate sorted criteria percentage and calculates rating
					   filterRatingProperty -  updates rating in the property criteria table sqlite
					   overallRatingProperty - update overall rating property in the property table
					   getNewRatings - it gets new rating from property details screen
					   favouriteProperty - it favourites and unfavourites property
					   ratingProperty -  it rates property by property Id
					   getPropertyDetails - funciton which gets all  details about property
					   getProperties - it gets all properties
					   getFavourites - it gets all favourite property

RateEstate/src/View
        where all view components are there

RateEstate/src/assets
        It has all json files including property, property image, criteria 

RateEstate/assets
       All local images are available here

  ____________________________________________________________________________________________________________________________  




	  
	    











