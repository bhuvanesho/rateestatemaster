import React, { Component } from "react";
import { Image, View, Text, TouchableOpacity, ScrollView, Alert } from 'react-native';
import {
    Container,
    Header,
    Title,
    Button,
    Icon,
    Tabs,
    Tab,
    Right,
    Left,
    Body,
    Thumbnail
} from "native-base";
import { MKCheckbox, MKRangeSlider, MKSlider, MKRadioButton, MKColor } from 'react-native-material-kit';
import Property from '../../controller/Property';
import styles from './styles';
import Messages from '../../assets/resources'
import Orientation from "react-native-orientation";

import alertMSG from '../../assets/resources';

export default class Filter extends Property {
    
    constructor(props) {
        super(props);
        this.getActiveCriteria();
        this.getFilterDatas();
        this.getMaximumPropertyValue();
        
    }
    componentDidMount() {
        Orientation.lockToPortrait();
        this.getActiveCriteria();
     
    }
    render() {
        var activeCriteria = this.state.criteriaInfo;
       
        return (
            <Container style={styles.containerStyle}>
                < Header style={styles.container}>
                    <Left>
                        <TouchableOpacity onPress={() => { this.clearAll() } }  >
                            <Text style={styles.submitHeader}>Clear All</Text>
                        </TouchableOpacity>

                    </Left>
                    <Body>
                        <Image source={require('../../../assets/Headerlogo.png')} style={styles.headerBGimage} />
                    </Body>
                    <Right>
                        <TouchableOpacity onPress={() => this.filterGoBack()}>
                            <Text style={styles.submitHeader}>Done</Text>
                        </TouchableOpacity>
                    </Right>


                </Header>
                <View style={styles.filterSortHeaderBG}>
                    <Text style={styles.filterSortHeaderColor}>FILTER   &  SORT</Text>
                </View>
                {/* locked={this.state.scrollDisable} */}
                <Tabs locked={true} page={this.state.activeTab} onChangeTab={({ i }) => this.state.noCriteriaFound && this.state.activeTab == 1 && this.state.showMessage ? (
                    this.setState({ activeTab: i }), Alert.alert(
                        '',
                        alertMSG.en['SortMessage'],
                        [
                            {
                                text: 'OK',
                                onPress: () => this.setState({ activeTab: 1,showMessage:false }),
                            }
                        ],
                    )) :(this.state.noCriteriaFound?this.setState({ activeTab: i, showMessage:true  }):this.setState({ activeTab: i }))} tabContainerStyle={styles.tabHeight} style={{ elevation: 0 }} tabBarUnderlineStyle={styles.tabUnderline}>
                    {/****** filter tab content  *******/}
                    <Tab
                        tabStyle={styles.tabBgc} activeTabStyle={styles.tabBgc} heading="FILTER" textStyle={styles.tabheaderfont} activeTextStyle={styles.activetabheaderfont} >
                        <Container>
                            <View style={styles.filterContent} pointerEvents={this.state.disableView}>
                                <View style={styles.filterCategeoryHeader}>
                                    <Text style={styles.filterCategeoryHeaderAlign}>RATING</Text>
                                </View>
                                <View style={styles.filterMarginAlign}>
                                    <View style={styles.filterContentAlign}>
                                        <Text style={styles.filterFont}>Rated</Text>
                                        <MKCheckbox
                                            checked={this.state.rated}
                                            onCheckedChange={() => this.ratedCheck()}
                                            borderOnColor='#1DA4DF'
                                            fillColor='#1DA4DF'
                                            rippleColor='#1DA4DF'
                                            style={styles.filterCheckBoxAlign} />
                                    </View>
                                    <View style={styles.filterContentAlign}>
                                        <Text style={styles.filterFont}>Not Rated</Text>
                                        <MKCheckbox
                                            checked={this.state.unrated}
                                            onCheckedChange={() => this.unRatedCheck()}
                                            borderOnColor='#1DA4DF'
                                            fillColor='#1DA4DF'
                                            rippleColor='#1DA4DF'
                                            style={styles.filterCheckBoxAlign} />
                                    </View>
                                </View>
                                <View style={styles.filterCategeoryHeader}>
                                    <Text style={styles.filterCategeoryHeaderAlign}>FAVOURITE</Text>
                                </View>
                                <View style={styles.filterMarginAlign}>
                                    <View style={styles.filterContentAlign}>
                                        <Text style={styles.filterFont}>Favourite</Text>
                                        <MKCheckbox
                                            checked={this.state.favourite}
                                            onCheckedChange={() => this.favouriteCheck()}
                                            borderOnColor='#1DA4DF'
                                            fillColor='#1DA4DF'
                                            rippleColor='#1DA4DF'
                                            style={styles.filterCheckBoxAlign} />
                                    </View>
                                    <View style={styles.filterContentAlign}>
                                        <Text style={styles.filterFont}>Not Favourite</Text>
                                        <MKCheckbox
                                            checked={this.state.notfavourite}
                                            onCheckedChange={() => this.notfavouriteCheck()}
                                            borderOnColor='#1DA4DF'
                                            fillColor='#1DA4DF'
                                            rippleColor='#1DA4DF'
                                            style={styles.filterCheckBoxAlign} />
                                    </View>
                                </View>
                                <View style={styles.filterCategeoryHeader}>
                                    <Text style={styles.filterCategeoryHeaderAlign}>MAX PRICE</Text>
                                </View>
                                <View style={styles.filterMarginAlign}>
                                    <Text style={styles.filterFont}>$ {this.addCommas(this.state.maxPrice.toFixed(0))} </Text>
                                    <MKSlider
                                        step={this.state.vairableStep}
                                        //onStart={() => this.setState({ scrollDisable: true })}
                                        // onChange={(curValue) => this.setState({
                                        //     minPrice: curValue.min > curValue.max ? 0 : curValue.min,
                                        //     maxPrice: curValue.max,
                                        // })}
                                        onConfirm={() => this.setState({ scrollDisable: false })}
                                        lowerTrackColor={'#25a9e0'}
                                        min={0}
                                        max={this.state.variablePriceMax}
                                        value={this.state.maxPrice}
                                        onChange={(curValue) => this.setState({ scrollDisable: true, maxPrice: curValue, queryChange: "1"  })}
                                    />

                                </View>
                            </View>
                        </Container>
                    </Tab>


                    {/* sort content tab */}


                    <Tab tabStyle={styles.tabBgc} activeTabStyle={styles.tabBgc} heading="SORT" textStyle={styles.tabheaderfont} activeTextStyle={styles.activetabheaderfont} >
                        <Container>
                            <View style={styles.filterContent} pointerEvents={this.state.disableView}>
                                <View style={styles.filterCategeoryHeader}>
                                    <Text style={styles.filterCategeoryHeaderAlign}>RATING</Text>
                                </View>
                                <View style={styles.filterMarginAlign}>
                                    <View style={styles.filterContentAlign}>
                                        <Text style={styles.filterFont}>Low to High</Text>
                                        <MKRadioButton
                                            checked={this.state.lowRate}
                                            onCheckedChange={() => this.setState({ lowRate: true, highRate: false, queryChange: "1"  })}
                                            borderOffColor='#727171'
                                            rippleColor='#1DA4DF'
                                            borderOnColor='#1DA4DF'
                                            fillColor='#1DA4DF'
                                        />
                                    </View>
                                    <View style={styles.filterContentAlign}>
                                        <Text style={styles.filterFont}>High to Low</Text>
                                        <MKRadioButton
                                            checked={this.state.highRate}
                                            onCheckedChange={() => this.setState({ lowRate: false, highRate: true, queryChange: "1"  })}
                                            borderOffColor='#727171'
                                            rippleColor='#1DA4DF'
                                            borderOnColor='#1DA4DF'
                                            fillColor='#1DA4DF'
                                        />
                                    </View>
                                </View>
                                <View style={styles.filterCategeoryHeader}>
                                    <Text style={styles.filterCategeoryHeaderAlign}>EVALUATION CRITERIA</Text>
                                </View>

                                {activeCriteria.length == 1 ?
                                    <View padding={20}>
                                        <Text style={styles.criteriafont}>{Messages.en['CriteriaNull']}</Text>
                                    </View>
                                    :
                                    <ScrollView bounces={false} style={styles.filterEvaluationCriteria}>

                                        {activeCriteria.map(activeCriteria => (
                                            <View
                                                borderOnColor='#1DA4DF'
                                                fillColor='#1DA4DF'
                                                style={styles.filterContentAlign}>
                                              {/* {!this.state.onScreenChange ? this.state['criteria_' + activeCriteria.criteriaId] = false : null}
                                               {criteria = this.state['criteria_' + activeCriteria.criteriaId]}*/}
                                                <Text style={styles.filterFont}>{activeCriteria.criteriaName}</Text>
                                                <MKCheckbox
                                                 //checked={activeCriteria.criteriaId == 0 ? this.state.checkAll : criteria}
                                                    checked={activeCriteria.sortedStatus == "Active" ? true : false}
                                                    //onCheckedChange={(e) => activeCriteria.criteriaId == 0 ? this.checkAllCriteria() : this.checkParticular(activeCriteria.criteriaId, criteria)}
                                                   onCheckedChange={(e) => activeCriteria.criteriaId == 0 ? this.allCriteria(e) : this.selectedCriteria(activeCriteria.criteriaId, e)}
                                                    
                                                    borderOnColor='#1DA4DF'
                                                    fillColor='#1DA4DF'
                                                    rippleColor='#1DA4DF'
                                                    style={styles.filterCheckBoxAlign} />
                                            </View>
                                        ))
                                        }
                                    </ScrollView>
                                }

                               {/*} <View style={styles.filterCategeoryHeader}>
                                    <Text style={styles.filterCategeoryHeaderAlign}>PRICE</Text>
                                </View>
                                <View style={styles.filterMarginAlign}>
                                    <View style={styles.filterContentAlign}>
                                        <Text style={styles.filterFont}>Low to High</Text>
                                        <MKRadioButton
                                            checked={this.state.lowPrice}
                                            onCheckedChange={() => this.setState({ lowPrice: true, highPrice: false, queryChange: "1"  })}
                                            borderOffColor='#727171'
                                            borderOnColor='#1DA4DF'
                                            rippleColor='#1DA4DF'
                                            fillColor='#1DA4DF'
                                        />
                                    </View>
                                    <View style={styles.filterContentAlign}>
                                        <Text style={styles.filterFont}>High to Low</Text>
                                        <MKRadioButton
                                            checked={this.state.highPrice}
                                            onCheckedChange={() => this.setState({ lowPrice: false, highPrice: true, queryChange: "1"  })}
                                            borderOffColor='#727171'
                                            borderOnColor='#1DA4DF'
                                            rippleColor='#1DA4DF'
                                            fillColor='#1DA4DF'
                                        />
                                    </View>
                                </View>*/}
                            </View>
                        </Container>
                    </Tab>

                </Tabs>
            </Container>
        );
    }
}


