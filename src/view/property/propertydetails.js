import criteria from '../../controller/criteriasetting';
import Overlay from 'react-native-elements';
import React, { Component } from "react";
import Orientation from "react-native-orientation";
import Slideshow from 'react-native-slideshow';
import { Modal as RNModal, PixelRatio, WebView, Keyboard, Dimensions, ScrollView, Image, TouchableOpacity, YellowBox, TextInput, ImageBackground } from 'react-native';
import ImagePicker from 'react-native-image-picker';
export const { width, height } = Dimensions.get('window');
import {
   
    Container,
    View,
    Header,
    Title,
    Button,
    Icon,
    Tabs,
    Tab,
    Right,
    Left,
    Text,
    Body,
    List,
    ListItem,
    Textarea, ActivityIndicator,
} from "native-base";
import ImageViewer from 'react-native-image-zoom-viewer';
import { MKTextField, MKColor, MKIconToggle, MKSwitch, MKSlider } from 'react-native-material-kit';

import Swiper from 'react-native-swiper';

import property from '../../controller/Property';

import StarRating from 'react-native-star-rating';

import Modal from "react-native-modal";

import MapView, { PROVIDER_GOOGLE, Callout, PROVIDER_DEFAULT, Marker, Polyline } from 'react-native-maps';
import styles from './styles';

import { colors } from 'react-native-elements';
import Messages from '../../assets/resources'
var home = require('../../../assets/home.png');
var cityscape = require('../../../assets/cityscape.png');
var prevProps=[];
var prevState=[];
var count=0;
import mainStyles from '../../view/styles';
import Spinner from 'react-native-loading-spinner-overlay';
export default class PropertyDetails extends property {

    constructor(props) {
        super(props);
        state = {
            spinner: true
          };
       
        this._keyboardDidShow = this._keyboardDidShow.bind(this)
        this._keyboardDidHide = this._keyboardDidHide.bind(this)
    }
    componentDidUpdate(prevProps, prevState) {
        Object.entries(this.props).forEach(([key, val]) =>
          prevProps[key] !== val && console.log(`Prop '${key}' changed`)
        );
       
        Object.entries(this.state).forEach(([key, val]) =>
          prevState[key] !== val && console.log(`State '${key}' changed`)
         
        );
        console.log(Object.entries(this.state).length + '  total objects ');
      }
    componentDidMount() {
        this.props.navigation.addListener('willFocus', () => this.setState({ selected: true }));
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
        this.getPropertyDetails();
        // this.calculateOverallRating();
         this.propertyViewed();
       
        
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow() {
        this.refs.PropertyView.scrollTo(this.state.dimensionHeight > 600 ? this.state.dimensionHeight / 1.5 : this.state.dimensionHeight / 1.12);
    }

    _keyboardDidHide() {
        this.refs.PropertyView.scrollToEnd(0);
    }
    closeImageModal() {
        Orientation.lockToPortrait();
        this.setState({ swiperModal: false });
    }
    openImageModal() {
        Orientation.unlockAllOrientations();
        this.setState({ swiperModal: true });
    }

    renderCallout(name, bed, bath, storey, year, Education, Arts, Accomodation) {
        return (
            <MapView.Callout tooltip>
                <View style={styles.maptooltip}>
                    <View style={styles.tooltip}>
                        <Text style={styles.propertyName} >{name}</Text>
                        <Text style={styles.propertyInfo}>Bedroom : {bed == null ? 1 : bed}</Text>
                        <Text style={styles.propertyInfo}>Bathroom : {bath == null ? 1 : bath}</Text>
                        <Text style={styles.propertyInfo}>No of storey : {storey == null ? 1 : storey}</Text>
                        <Text style={styles.propertyInfo}>Build year : {year == null ? 1990 : year}</Text>
                        {/* <Text style={styles.propertyName}>Service Available</Text>
                        <Text style={styles.propertyInfo}>Education/Health/Social : {Education == null ? 'NA' : Education}</Text>
                        <Text style={styles.propertyInfo}>Arts/Entertainment/Recreation  : {Arts == null ? "NA" : Arts}</Text>
                        <Text style={styles.propertyInfo}>Accomodation/Food  : {Accomodation == null ? 'NA' : Accomodation}</Text>
                        {/* <Text>{name}</Text> */}
                    </View>
                </View>
            </MapView.Callout>
        );
    }

    hideToolTip() {
        var activeCriteria = this.state.criteriaDetails;
        for (let i = 0; i < activeCriteria.length; i++) {
            this.setState({ ['toolTip_' + activeCriteria[i]['criteriaId']]: false });
        }
    }


    //image upload


    selectPhotoTapped() {
        const options = {
            title: 'Add Image to the Property Gallery',
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };

        ImagePicker.showImagePicker(options, (response) => {
          //  console.log('Response = ', response);

            if (response.didCancel) {
               // console.log('User cancelled photo picker');
            }
            else if (response.error) {
                //console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                //console.log('User tapped custom button: ', response.customButton);
            }
            else {
                // let source = { uri: response.uri };

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.setState({
                    ImageSource: response.data.toString()
                });
                // alert(response.data.toString());
                this.insertNewPhotos();

            }
        });
    }




    render() {
        count=count+1;
       // console.log(count+'render details');
        var activeCriteria = this.state.criteriaDetails;
        var propertyDetails = this.state.propertyDetails;
        var propertyImages = this.state.PropertyImages;
       
        var swiperImages = [];
        if (this.state.selected == true) {
            this.setState({selected: false, spinner:true });
        }
        for (let k = 0; k < propertyImages.length; k++) {
            swiperImages.push({
                // url: 'data:image/jpeg;base64,' + propertyImages[k]['imageName']
                url: propertyImages[k]['imageName']
            })
        }
        return (
           
            <Container style={styles.propertyBg}> <Spinner visible={this.state.spinner}/>
                <Header style={styles.headerBg}>
                    {/* <Left>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={require('../../../assets/leftArrow.png')} style={styles.leftRightArrows} resizeMode='cover' />
                        </TouchableOpacity>
                    </Left> */}
                    <Left>
                            { this.state.editable=="false" ? 
                            <Image source={require('../../../assets/Headerlogo.png')} style={mainStyles.headerBGimage} >
                            </Image> : null
                            }
                        </Left>
                        <Body>
                            { this.state.editable =="true" ? <Image source={require('../../../assets/Headerlogo.png')} style={mainStyles.headerBGimage} /> : null
                            }
                            { this.state.editable =="false" ? <Text style={mainStyles.emailTextFieldOrange}>{this.state.influencerName}</Text> : null
                            } 
                        </Body>
                        <Right>{ this.state.editable =="false" ? <Image source={require('../../../assets/share.png')} style={styles.shareImage2}></Image> : null} </Right>
                </Header>
                {propertyDetails.map((propertyDetails, index) => (
                    <Container key={index}>

                        {/* map modal  */}

                        <Modal
                            animationIn={'fadeIn'}
                            animationOut={'fadeOut'}
                            isVisible={this.state.modalvisible}
                        >
                            {/* <WebView
                                source={{ uri: 'https://matrix.crebtools.com/Matrix/Public/InteractiveMapPopup.aspx?RecordID=87892088&TableID=9&L=1&pbs=1' }}
                            /> */}
                            <View style={styles.mapContainer}>
                                <TouchableOpacity style={styles.close} onPress={() => this.setState({ modalvisible: false })} >
                                    <Icon style={styles.closeFont} name='close' type='MaterialIcons' />
                                </TouchableOpacity>
                                <MapView
                                    style={styles.mapView}
                                    provider={PROVIDER_GOOGLE}
                                    initialRegion={{
                                        latitude: propertyDetails.latitude,
                                        longitude: propertyDetails.longitude,
                                        latitudeDelta: 0.0922,
                                        longitudeDelta: 0.0421,
                                    }}


                                >



                                    <MapView.Marker
                                        coordinate={{ latitude: propertyDetails.latitude, longitude: propertyDetails.longitude }}
                                        image={home}
                                        centerOffset={{ x: 0, y: 0 }}
                                    >

                                        {this.renderCallout(propertyDetails.propertyName, propertyDetails.beds, propertyDetails.totalbath, propertyDetails.noOfStorey, propertyDetails.buildYear, propertyDetails.Education, propertyDetails.Arts, propertyDetails.Accomodation)}
                                    </MapView.Marker>

                                    {/* <Image source={cityscape} style={styles.closeFont}/> */}
                                </MapView>
                                {
                                    this.state.showServices ?
                                        <View style={{ borderRadius:5,padding:10, position: "absolute", alignSelf: 'center', backgroundColor: '#fff' }}>
                                            <TouchableOpacity onPress={() => this.setState({ showServices: false })} >
                                                <Icon style={{alignSelf:'flex-end', color:'grey',fontSize:20}} name='close' type='MaterialIcons' />
                                            </TouchableOpacity>
                                            <Text style={styles.servicetitle} >Services available in this locality</Text>
                                            <Text style={styles.services}>Education/Health/Social </Text>
                                            <Text style={styles.services}>{propertyDetails.Education == null ? 'NA' : propertyDetails.Education}</Text>
                                            <Text style={styles.services}>Arts/Entertainment/Recreation </Text>
                                            <Text style={styles.services}>
                                            {propertyDetails.Arts == null ? "NA" : propertyDetails.Arts}</Text>
                                            <Text style={styles.services}>Accomodation/Food </Text>
                                            <Text style={styles.services}>  {propertyDetails.Accomodation == null ? 'NA' : propertyDetails.Accomodation}</Text>
                                            {/* <Text>{name}</Text>
                                   <Text>{name}</Text> */}
                                        </View> : null
                                }
                                <TouchableOpacity style={{ position: "absolute", bottom: 5, alignSelf: 'center' }} onPress={() => this.setState({ showServices: !this.state.showServices })} >
                                    <Image source={cityscape} style={{ height: 30, width: 30 }} />
                                </TouchableOpacity>
                            </View>
                            {/* <View>
                                <TouchableOpacity onPress={() => this.setState({ modalvisible: false })} >
                                    <Text style={styles.propertyName}>Close</Text>
                                </TouchableOpacity>
                            </View> */}
                        </Modal>
                        {/* map modal end */}



                        {/* image swiper modal starts */}
                        <RNModal
                            animationIn={'fadeIn'}
                            animationOut={'fadeOut'}
                            visible={this.state.swiperModal}
                            supportedOrientations={['portrait', 'landscape']}
                            onOrientationChange={() => Dimensions.get('window').width > Dimensions.get('window').height ? this.setState({ landscape: true }) : this.setState({ landscape: false })}
                        >
                            <ImageViewer onChange={(index) => this.setState({ currentPic: index })} enableSwipeDown={true} onSwipeDown={() => this.closeImageModal()} saveToLocalByLongPress={false} renderIndicator={() => null} index={this.state.currentPic} imageUrls={swiperImages} 
                            /*loadingRender={() => (
                                <ActivityIndicator
                                  color={colors.grey5}
                                  size="large"
                                  style={{
                                    height: Dimensions.get('window').height,
                                    alignItems: 'center',
                                    justifyContent: 'center'
                                  }}
                                />
                            )}*/ />
                            <TouchableOpacity style={styles.closeSwiper} onPressIn={() => Orientation.lockToPortrait()} onPressOut={() => this.setState({ swiperModal: false })} >
                                <Icon style={styles.closeFont} name='close' type='MaterialIcons' />
                            </TouchableOpacity>
                        </RNModal>

                        {/* image swiper modal ends */}

                        {/*image modal 2 starts */}

                        {/* <ImageView
                            onClose={() => this.closeImageModal()}
                            images={swiperImages}
                            imageIndex={this.state.currentPic}
                            isVisible={this.state.swiperModal}>
                        </ImageView> */}

                        {/*image modal 2 ends */}


                        {/*---------------------------------------------------------------------------------*/}
                        {/*upload modal 1 starts */}
                        {/* <Modal
                            isVisible={this.state.uploadModal}
                        >
                            <TouchableOpacity style={styles.close} onPress={() => this.setState({ uploadModal: false })} >
                                <Icon style={styles.closeFont} name='close' type='MaterialIcons' />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>

                                <View style={{
                                    borderRadius: 10,
                                    width: 250,
                                    height: 250,
                                    borderColor: '#9B9B9B',
                                    borderWidth: 1 / PixelRatio.get(),
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    backgroundColor: '#CDDC39',
                                }}>

                                    {this.state.ImageSource === null ? <Text>Select a Photo</Text> :
                                        <Image style={{
                                            borderRadius: 10,
                                            width: 250,
                                            height: 250,
                                            borderColor: '#9B9B9B',
                                            borderWidth: 1 / PixelRatio.get(),
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            backgroundColor: '#CDDC39',
                                        }} source={this.state.ImageSource} />
                                    }

                                </View>

                            </TouchableOpacity>
                        </Modal> */}




                        {/*upload modal  ends */}







                        <View style={styles.propertyDetailsBG} onTouchStart={() => this.hideToolTip()}>
                            <View style={styles.propertyFlex}>
                                <Left>
                                    <TouchableOpacity style={styles.arrow} onPress={() => this.goBack()}>
                                        <Icon name="ios-arrow-back" type="Ionicons" style={styles.backIcon} />
                                    </TouchableOpacity>
                                </Left>
                                <Text style={styles.propertyDetails}>PROPERTY DETAILS</Text>
                                <Right >
                                    <View style={styles.iconFlex}>
                                        {!this.state.onScreenChange ? this.state.favo = propertyDetails.favourite : null}
                                        {/* <Image style={styles.locationIcon} source={require('../../../assets/Locationicon.png')} /> */}
                                        {/* <TouchableOpacity onPressIn={() => this.setState({ onScreenChange: true, favo: this.state.favo == 'Unfavourite' ? 'Favourite' : 'Unfavourite' })} onPressOut={() => this.favourite(this.state.favo)}>
                                            <Image style={this.state.favo == 'Favourite' ? styles.activefavouriteIcon : styles.favouriteIcon} source={require('../../../assets/Favouriteicon.png')} />
                                        </TouchableOpacity> */}
                                    </View>
                                </Right>
                            </View>
                        </View>
                        <ScrollView onTouchStart={() => this.hideToolTip()} bounces={false} ref="PropertyView" onMomentumScrollEnd={(event) => this.setState({ dimensionHeight: height + event.nativeEvent.contentOffset.y })}>
                            <ScrollView bounces={false}>
                                <Slideshow
                                    position={this.state.currentPic}
                                    indicatorSelectedColor={styles.activedot}
                                    arrowSize={30}
                                    arrowLeft={<Icon name="chevron-thin-left" type="Entypo" style={styles.backIcon} />}
                                    arrowRight={<Icon name="chevron-thin-right" type="Entypo" style={styles.backIcon} />}
                                    onPress={() => this.openImageModal()}
                                    dataSource={swiperImages}
                                    onPositionChanged={position => this.setState({ currentPic: position })}

                                />
                              
                                {/* <Swiper prevButton={<View padding={styles.imagearrow}><Icon name="chevron-thin-left" type="Entypo" style={styles.backIcon} /></View>} nextButton={<View padding={styles.imagearrow}><Icon name="chevron-thin-right" type="Entypo" style={styles.backIcon} /></View>} activeDotColor={styles.activedot} dotColor={styles.dot} height={styles.imageSwiper} showsButtons>

                                    {
                                        propertyImage.map((propertyImage, index) => (
                                            <TouchableOpacity onPressOut={() => Orientation.unlockAllOrientations()} onPressIn={() => this.setState({ swiperModal: true, currentPic: index })}>
                                                <View>
                                                    <Image style={styles.imageslide} source={propertyImage.image} />
                                                </View>
                                            </ TouchableOpacity>
                                        ))
                                    }

                                </Swiper> */}
                                <View style={styles.propertyInfoFlex}>
                                    <View style={styles.propertyView}>
                                        <Text style={styles.propertyName} >{propertyDetails.propertyName}</Text>
                                        {/* <TouchableOpacity onPress={() => this.setState({ modalvisible: true })}>
                                            <Image style={styles.locationIcon} source={require('../../../assets/Locationicon.png')} />
                                        </TouchableOpacity> */}
                                        <Text style={styles.propertyaddress}>{propertyDetails.address}</Text>
                                        <Text numberOfLines={this.state.numberOfLines} style={styles.propertyInfo}>Description : {propertyDetails.description}</Text>
                                        <TouchableOpacity onPress={() => this.setState({ numberOfLines: this.state.numberOfLines == 2 ? null : 2, moreless: this.state.numberOfLines == 2 ? 'less..' : 'more..' })}><Text style={styles.moreFont}>{this.state.moreless}</Text></TouchableOpacity>
                                        {/* <Text style={styles.propertyInfo} >List Price - <Icon style={styles.propertyfont} name='dollar' type='FontAwesome' /> {this.addCommas(propertyDetails.amount)}</Text> */}
                                        <Text style={styles.propertyInfo} >List Price - ${this.addCommas(propertyDetails.amount)}</Text>
                                        {/* {!this.state.onScreenChange ? this.state.overallRating = propertyDetails.overallRating : null} */}
                                    </View>
                                    <View style={styles.mapnFav}>
                                        <View style={styles.iconspacing}   pointerEvents={this.state.disableView}>
                                            <TouchableOpacity onPress={() => this.setState({ modalvisible: true })}>
                                                <Image style={styles.locationIcon} source={require('../../../assets/Locationmap.png')} />
                                            </TouchableOpacity>
                                        </View>
                                        <View style={styles.iconspacing}   pointerEvents={this.state.disableView}>
                                            <TouchableOpacity onPressIn={() => this.setState({ onScreenChange: true, favo: this.state.favo == '' ? 'Favourite' : '' })} onPressOut={() => this.favourite(this.state.favo)}>
                                                <Image style={styles.favouriteIcon} source={this.state.favo == 'Favourite' ? require('../../../assets/favouriteOrange.png') : require('../../../assets/favouriteGrey.png')} />
                                                {/* <Image style={this.state.favo == 'Favourite' ? styles.favouriteactiveIcon : styles.favouriteIcon} source={require('../../../assets/FavouriteProperty.png')} /> */}
                                            </TouchableOpacity>
                                        </View>
                                        <View style={styles.cameraiconspacing}   pointerEvents={this.state.disableView}>
                                            <TouchableOpacity onPressOut={this.selectPhotoTapped.bind(this)} onPressIn={() => this.setState({ currentPropertyId: propertyDetails.propertyId, currentPropertyName: propertyDetails.propertyName })}>
                                                <Icon name='camera' style={{ color: '#25a9e0' }} />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>

                                <View style={styles.criteriaView}>
                                    <View style={styles.criterialine}>
                                        <ImageBackground style={styles.houseRating} source={require('../../../assets/Housebig.png')} >
                                            <View style={styles.overallRating}>
                                                <Text style={styles.overallRatingText}>{this.state.overallRating == null ? 0 : this.state.overallRating.toFixed(1)}</Text>
                                            </View>
                                        </ImageBackground>
                                    </View>
                                    <View style={styles.criteriaBorder}>
                                        <View padder>
                                            {activeCriteria.length == 0 ?
                                                <View padder>
                                                    <Text style={styles.criteriafont}>{Messages.en['CriteriaNull']}</Text>
                                                </View> : null}
                                            {activeCriteria.map(activeCriteria => (
                                                <View style={styles.criteriaFlex}>
                                                    <View style={{ justifyContent: 'center' }}>
                                                        {this.state['toolTip_' + activeCriteria.criteriaId] && activeCriteria.criteriaName.length > this.state.criteriaExceed ?
                                                            <View style={styles.tooltipContainer}>
                                                                <Text style={styles.toolTip}>{activeCriteria.criteriaName}</Text>
                                                            </View>
                                                            : null
                                                        }
                                                        {/* <TouchableOpacity  activeOpacity={0.7} onPressOut={() => this.setState({ ['toolTip_' + activeCriteria.criteriaId]: false })} onPressIn={() => this.setState({ ['toolTip_' + activeCriteria.criteriaId]: true })}> */}
                                                        <TouchableOpacity activeOpacity={0.7} onPress={() => this.setState({ ['toolTip_' + activeCriteria.criteriaId]: true })}>
                                                            <Text style={styles.criteriafont}>{width < 350 ? activeCriteria.criteriaName.substring(0, 16) : activeCriteria.criteriaName.substring(0, 21)}

                                                                <Text style={styles.criteriafont}>{width < 350 ? activeCriteria.criteriaName.length > 16 ? '...' : null : activeCriteria.criteriaName.length > 21 ? '...' : null}</Text>

                                                            </Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                    {!this.state.onScreenChange ? this.state['rating_' + activeCriteria.criteriaId] = activeCriteria.rating : null}
                                                    {rating = this.state['rating_' + activeCriteria.criteriaId]}
                                                    <View style={styles.sliderFlex}   pointerEvents={this.state.disableView}>
                                                        <View style={styles.ratestarView}>
                                                            <StarRating

                                                                starStyle={styles.starPadding}
                                                                emptyStar="ios-star-outline"
                                                                fullStar="ios-star"
                                                                halfStar="ios-star-half"
                                                                iconSet="Ionicons"
                                                                maxStars={5}
                                                                rating={rating == null ? 0 : rating}
                                                                fullStarColor={styles.starColor}
                                                                halfStarColor={styles.starColor}
                                                                emptyStarColor={styles.starColor}
                                                                starSize={23}
                                                                starPadding={10}
                                                                selectedStar={(rating) => this.sliderRating(activeCriteria.criteriaId, rating,activeCriteria.ratingId)}
                                                            />
                                                        </View>
                                                        {/* <View style={styles.slidertorange}>
                                                    <Text style={styles.newrangefont}>{rating == null ? 0 : rating}</Text>
                                                </View>
                                                <View style={styles.slidertorange}>
                                                    <MKSlider
                                                        thumbRadius={3}
                                                        lowerTrackColor={styles.slidercolor}
                                                        min={0}
                                                        max={5}
                                                        value={rating == null ? 0 : rating}
                                                        style={styles.sliderwidth}
                                                        onChange={(curValue) => this.sliderRating(activeCriteria.criteriaId, curValue.toFixed(0))}
                                                    />
                                                </View>
                                                <View style={styles.slidertorange}>
                                                    <Text style={styles.rangefont}>5</Text>
                                                </View> */}
                                                    </View>
                                                </View>
                                            ))}
                                        </View>
                                    </View>
                                </View>
                                <View   pointerEvents={this.state.disableView}>
                                    {/* <View style={{ margin: 28 }}>
                                <Text style={styles.propertyfont}>Comments</Text>
                            </View> */}
                                    <View style={{ margin: 16 }}>
                                        <Textarea
                                            onFocus={() => this.setState({ comments: propertyDetails.comments })}
                                            onBlur={() => this.addComments()}
                                            onChangeText={(Text) => this.setState({ comments: Text })}
                                            style={styles.TextInputStyleClass}
                                            placeholderTextColor={styles.placeholderColor}
                                            placeholder={'Comments'}
                                            value={propertyDetails.comments == null || propertyDetails.comments == '' ? null : propertyDetails.comments}
                                        />
                                        {/* <Textarea
                                            style={styles.TextInputStyleClass}
                                            placeholderTextColor={styles.placeholderColor}
                                           
                                            value={this.state.ImageSource}
                                        /> */}
                                    </View>
                                </View>
                            </ScrollView>
                        </ScrollView>
                    </Container>
                ))}
                {/* <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.backNav}>
                        <Image style={styles.navigator} source={require('../../../assets/leftArrow.png')} />
                    </TouchableOpacity> */}
                     
            </Container>
        );
    }
}