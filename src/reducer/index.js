import { combineReducers } from 'redux';
import registerReducer from './registerReducer';
import loginReducer from './loginReducer';
import profileReducer from './profileReducer';

const rootReducer = combineReducers({
    register: registerReducer,
    login:loginReducer,
    profile: profileReducer,
});
export default rootReducer;