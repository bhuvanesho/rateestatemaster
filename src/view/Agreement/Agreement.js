import React , { Component } from 'react';
import { AppRegistry, StyleSheet, TextInput, View, Text, TouchableOpacity, Image, KeyboardAvoidingView, Animated, 
Linking,Keyboard ,TouchableWithoutFeedback} from 'react-native';
import { Container, Content } from 'native-base';
import { MKCheckbox } from 'react-native-material-kit';
import styles,{ IMAGE_HEIGHT, IMAGE_HEIGHT_SMALL } from '../login/styles';
import { database } from '../../firebaseConfig/config';
const launchscreenLogo1 = require("../../../assets/loginScreenLogo.png");
class Agreement extends Component{
       
    handleNextPage = () =>{
        this.props.navigation.navigate("Login")
    }
      
        
             render(){
                 return(
                    <TouchableWithoutFeedback onPress={() => {Keyboard.dismiss()}}>
                        <Container style={styles.container}>
                            <View style={styles.MainContainer}>
                            <Text></Text>
                                <View style={{ alignItems: 'center' }}>
                                    <Image source={launchscreenLogo1} style={styles.loginScreenLogo} />
                                </View>
                                <Text style={styles.paragraph}>Pivacy Policy</Text>
                                <Text></Text>
                                <View >
                                    <Text style={styles.paragraphText}>
                                    Your privacy is protected by RateEstate. 
                                    Any personal information, such as your name, gender, other profile information 
                                    is used solely for communication with your realtor per your Realtor Contract. 
                                    <Text>{"\n"}</Text> <Text>{"\n"}</Text>
                                    Demographic information entered into your app may be anonymized 
                                    and consolidated into group data by RateEstate, which may be shared on 
                                    an aggregated basis with members of the Calgary Real Estate Board. 
                                    The shared data will not disclose your personal information.
                                </Text>

                                

                            
                                </View>
                                <Text>{"\n"}</Text>
                        
                                    <View style={{ alignItems: 'flex-end' }}>
                                        <TouchableOpacity
                                            style={styles.SubmitButtonStyle}
        
                                            onPress={this.handleNextPage}
                                        >
                                            <Text style={styles.TextStyle}> I Agree </Text>
                                        </TouchableOpacity>
                                    </View>
                            </View>
        
                        {/* </KeyboardAvoidingView> */}
                    </Container>
                    </TouchableWithoutFeedback>
        
        
                 );
             }
         }





// export default class Agreement extends Component{
//     render(){
//         return(
//             <Container>
//             <View>
//     <Text>
//“Your Privacy is protected by RateEsate. Any personal information
//     such as you name, gender, other profile information is used solely	
//     for communication with your realtor per your Relator Contract.
//     Demographic information entered into your app may b anonymized
//     and consolidated into group data by RateEstate which may be shared 
//     on an aggregated basis with members of the Calgary Real Estate Board.
//     The shared data will not disclose your personal information.”</Text>
    
//     </View>

//             </Container>
//     // <Text>{'\n'}</Text>
//     //   <View style={{ alignItems: 'flex-end' }}>
//     //   <TouchableOpacity
//     //       style={styles.SubmitButtonStyle}

//     //       onPress={this.signInValidation}
//     //   >
//     //       <Text>I Agree </Text>
//     //   </TouchableOpacity>
//   //</View>
//         )
        
//     }
// };


export default Agreement;