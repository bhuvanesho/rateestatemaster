// Register Action Types
export const REGISTER_ATTEMPT = "REGISTER_ATTEMPT";
export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const REGISTER_FAILED =  "REGISTER_FAILED";
/////////////////////////////////////////////////

//Login Action Types
export const LOGIN_DATA = "LOGIN_DATA";
export const LOGIN_ATTEMPT = "LOGIN_ATTEMPT";
export const LOGIN_SUCESS = "LOGIN_SUCESS";
export const LOGIN_FAILED = "LOGIN_FAILED";
export const ERROR_MESSAGE = "ERROR_MESSAGE";
export const LOGIN_EMAIL = "LOGIN_EMAIL";
///////////////////////////////////////////

//Profile Acion Types
export const CREATE_PROFILE_ATTEMPT = "CREATE_PROFILE_ATTEMPT"
export const CREATE_PROFILE = "CREATE_PROFILE";
export const GET_PROFILE_DATA = "GET_PROFILE_DATA";
export const UPDATE_PROFILE = "UPDATE_PROFILE ";
export const PROFILE_ERROR_MESSAGE = "PROFILE_ERROR_MESSAGE";
/////////////////////////////////////////////////////////////

export const PERSONAL_VIEW_GET = "PERSONAL_VIEW_GET";
