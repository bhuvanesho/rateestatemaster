
import React, { Component } from 'react';
import { Alert } from 'react-native';
import PropTypes from 'prop-types';
var SQLite = require('react-native-sqlite-storage')
var db = SQLite.openDatabase({ name: "HomeRatingDB.db" });
import alertMSG from '../assets/resources';
import propertyModel from '../model/Property';


export default class RateHouseModel extends propertyModel {

  constructor(props) {
    super(props);
    db.transaction((tx) => {
      tx.executeSql('select * from User where LoggedIn = "True"', [], (tx, results) => {
        //console.log("Query completed");
        // Get rows with Web SQL Database spec compliance.
        var userId = results.rows.item(0)['Id'];
        var len = results.rows.length;
        this.setState({ userId: userId });
        //alert(len);
      });

    });

    //this.getCriteria();
    //this.getCriteriaNRating();
    //this.createRating();
    //this.updateCriteria();
  }

  /**
   * 
   * funciton to get all criteia with its id and weightage
   * 
   */

  getCriteria() {
    try {
      db.transaction((tx) => {
        tx.executeSql('select * from Criteria', [], (tx, results) => {
          var len = results.rows.length;
          for (let j = 0; j < len; j++) {
            this.state.criteriaInfo.push({ criteriaId: results.rows.item(j)['Id'], criteriaName: results.rows.item(j)['CriteriaName'], weightage: results.rows.item(j)['Weightage'] });

          }
          this.state.ratings.push({ criteriaId: 1, userId: 1, propertyId: 1, rating: 3 });
          this.state.ratings.push({ criteriaId: 2, userId: 2, propertyId: 2, rating: 5 });
        });
      });
    }
    catch (error) {

    }
  }

  /**
   * 
   * funciton to get all criteia with its id and weightage and its ratings
   * 
   */

  getCriteriaNRating() {
    try {
      db.transaction((tx) => {
        query = 'select c.Id as CriteriaId,c.CriteriaName as CriteriaName,c.Weightage as Weightage,r.Rating as Rating,r.PropertyId as PropertyId  from Criteria c join Ratings r on c.Id = r.CriteriaId where r.PropertyId = ' + this.state.propId + '';
        tx.executeSql(query, [], (tx, results) => {
          var len = results.rows.length;
          var item = results.rows.item(0)['CriteriaId'];
          for (let j = 0; j < len; j++) {
            this.state.criteriaInfo.push({ propertyId: results.rows.item(j)['PropertyId'], criteriaId: results.rows.item(j)['CriteriaId'], criteriaName: results.rows.item(j)['CriteriaName'], weightage: results.rows.item(j)['Weightage'], rating: results.rows.item(j)['Rating'] });
          }
        });
      });
    }
    catch (error) {

    }
  }

  /**
   * 
   * funciton to get insert newest rating for property's criteria
   * 
   */


  createRating() {
    try {
      // alert(this.state.userId);
      db.transaction((tx) => {
        for (let j = 0; j < this.state.ratings.length; j++) {
          //alert(this.state.ratings[j]['propertyId']);
          let criteriaId = this.state.ratings[j]['criteriaId'];
          let userId = this.state.ratings[j]['userId'];
          let propertyId = this.state.ratings[j]['propertyId'];
          let rating = this.state.ratings[j]['rating'];
          let query = 'insert into Ratings(CriteriaId,PropertyId,UserId,Rating,createdDate) VALUES (' + criteriaId + ',' + propertyId + ',' + userId + ',' + rating.toFixed(1) + ',CURRENT_TIMESTAMP)';
          tx.executeSql(query, (tx, results) => {
          },
            function (results) {
              Alert.alert(alertMSG.en['Success']);
              tx.executeSql('update Property set RatingStatus = "Rated" where Id = ' + propertyId + '')
            },
            function (error) {
              Alert.alert(alertMSG.en['Failure']);
            });
        }
      });
    }
    catch (error) {

    }
  }

  /**
   * 
   * function to update criteria's rating
   * 
   */

  updateCriteria() {
    try {
      db.transaction((tx) => {
        for (let j = 0; j < this.state.ratings.length; j++) {
          //alert(this.state.ratings[j]['propertyId']);
          let criteriaId = this.state.ratings[j]['criteriaId'];
          let userId = this.state.ratings[j]['userId'];
          let propertyId = this.state.ratings[j]['propertyId'];
          let rating = this.state.ratings[j]['rating'];
          let query = 'UPDATE Ratings set Rating = ' + rating + ' , updatedDate = CURRENT_TIMESTAMP  where CriteriaId = ' + criteriaId + ' and PropertyId =' + propertyId + ' and UserId = ' + userId + '';
          //alert(query);
          tx.executeSql(query, (tx, results) => {
          },
            function (results) {
              Alert.alert(alertMSG.en['Success'] + "UP");
            },
            function (error) {
              Alert.alert(alertMSG.en['Failure'] + "UP");
            });
        }
      });
    }
    catch (error) {

    }
  }
}

