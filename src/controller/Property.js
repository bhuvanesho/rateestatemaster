import React, { Component } from 'react';
import { Alert, Dimensions, AlertIOS } from 'react-native';
import PropTypes from 'prop-types';
import propertyModel from '../model/Property';
import alertMSG from '../assets/resources';
var sumProduct = require('sum-product')
export const { width, height } = Dimensions.get('window');
import Orientation from "react-native-orientation";
import {AsyncStorage} from 'react-native';
export default class property extends propertyModel {
   
    constructor(props) {
        super(props);
        const { navigation } = this.props;
        const propId = navigation.getParam('propId');
        this.state = {
            allProperty: [],
            propertyId: propId,
            image: [],
            propertyInfo: [],
            favouriteInfo: [],
            propertyDetails: [],
            criteriaInfo: [],
            criteriaDetails: [],
            selectedCriteria: [],
            ratingDetails: [],
            activeCriteria: [],
            filter: '',
            sort: '',
            sortedCriteria: [],
            onScreenChange: false,
            modalvisible: false,
            load: false,
            selected: false,
            filteredRating: '',

            activeTab: 0,

            scrollDisable: false,
            rated: true,
            unrated: true,
            favourite:true,
            notfavourite:true,
            queryChange: '0',
            minPrice: 0,
            maxPrice: 0,
            checkAll: false,
            particularCriteria: false,
            favouriteSelected: false,
            lowRate: false,
            lowPrice: false,
            highRate: false,
            highPrice: false,
            sorting: false,
            decodedQuery: '',
            //currentQuery: '',

            filterNsort: false,

            numberOfLines: 2,
            moreless: 'more..',

            comments: '',
            dimensionHeight: height,

            criteriaExceed: width > 350 ? 21 : 16,
            overallRating: 0.0,
            listingStatus: '',
            filterActive: false,
            loadingSpinner: true,

            swiperModal: false,
            swiperImages: [],
            currentPic: 0,
            modalImageWidth: width,
            modalImageHeight: height,
            landscape: false,
            uploadModal: false,
            ImageSource: null,
            currentPropertyName: '',
            currentPropertyId: '',
            PropertyImages: [],
            sample: '',
            lastAdded: false,
            noCriteriaFound: false,
            showMessage: false,
            imageError: false,
            showServices:false,
            mlsId:'',
            accountUsersData:[],
            editable:'',
            dropdownDisabled:'',
            influencerName:'',
            disableView:'',
            propertyData:[],
            ratingsData:[],
            atleastOneChecked:true,
            variablePriceMax:6000000,
            vairableStep:100,
        }
        this.setReadOnlyFlag();
        this.navigateRateHouse = this.navigateRateHouse.bind(this);
        
        this.setState({ propertyId: propId });
        this.setReadOnlyFlag=this.setReadOnlyFlag.bind(this);
    }

    setReadOnlyFlag=async()=>{
        try{
            let dropdownDisabledValue  ='';
            let editableValue = '';
            let influencerNameValue = '';
           dropdownDisabledValue =   await AsyncStorage.getItem('dropdownDisabledValue');
           editableValue =   await AsyncStorage.getItem('editableValue');
           influencerNameValue =   await AsyncStorage.getItem('influencerNameValue');

           //dropdownDisabledValue =  getParameter('dropdown');
           //editableValue =   getParameter('editable');
           //influencerNameValue =   getParameter('influencer');


          if(editableValue=="false"){ this.setState({ disableView :"none"})}
          if(editableValue=="true"){ this.setState({ disableView :""})}
            this.setState({ editable :editableValue})
            this.setState({ dropdownDisabled :dropdownDisabledValue})
            this.setState({ influencerName :influencerNameValue})
            }catch(error){
              console.log(error)
          }
    
      }

    goBack() {
        Orientation.lockToPortrait();
        const { navigation } = this.props;
        navigation.goBack();
        navigation.state.params.onSelect({ selected: true });
    }

    //filter go back


    filterGoBack() {
        //check if all criteria has been selected
        
        //  if(this.state.atleastOneChecked==true){
              this.setState({particularCriteria:true})
                this.doneFilter();
         // }
         // else
        //  {
          //    Alert.alert("Select at least one Evaluvation criteria!")
         // }
    }

    /*
    **check ALL criteria
    **
    */
    checkAllCriteria = (status) => {
        var criteria = this.state.criteriaInfo;
        for (let i = 0; i < criteria.length; i++) {
          {/*  !this.state.checkAll ? this.selectedCriteria(criteria[i]['criteriaId'], "Active") : this.selectedCriteria(criteria[i]['criteriaId'], "Unactive");
            !this.state.checkAll ? this.setState({ ['criteria_' + criteria[i]['criteriaId']]: true, particularCriteria: false, checkAll: true, onScreenChange: true })
                :
                this.setState({ particularCriteria: true, checkAll: false, onScreenChange: true });
        */}
                if(status==true){ this.selectedCriteria(criteria[i].criteriaId, "Active");}
                if(status==false){ this.selectedCriteria(criteria[i].criteriaId, "Inactive");}
            }

        // if (this.state.checkAll == true) {
        //     for (let i = 0; i < criteria.length; i++) {
        //         // this.selectedCriteria(criteria[i]['criteriaId'], "Inactive")
        //     }
        // }



        this.calculatefilterPercentage();
        setTimeout(() => {
            //this.calculateSortedRating();
        }, 1000);

    }

    /***fetches criteria */
    checkAllCriteriaFetch = (status) => {

        var criteria = this.state.criteriaInfo;
        for (let i = 0; i < criteria.length; i++) {

          //  this.state.checkAll ? this.setState({ ['criteria_' + criteria[i]['criteriaId']]: true, checkAll: true, onScreenChange: true })
            //    :
             //   this.setState({ ['criteria_' + criteria[i]['criteriaId']]: false, checkAll: false, onScreenChange: true });



                if(status==true){ this.selectedCriteria(criteria[i]['criteriaId'], "Active");}
                if(status==false){ this.selectedCriteria(criteria[i]['criteriaId'], "Inactive");}

        }

       


    }

    /*
    **check particular criteria
    **
    */


    checkParticular = (criteriaId, status) => {
        this.setState({ lastCriteria: criteriaId });
        // alert(this.state.checkAll +" ----" +this.state.particularCriteria);

        this.setState({ particularCriteria: true });
        var criteria = this.state.criteriaInfo;
        // var truecount = 0;
        // for (let i = 1; i < criteria.length; i++) {
        //     console.log(criteria[i]['criteriaId']+" ---  "+this.state['criteria_' + criteria[i]['criteriaId']]);
        //     if (this.state['criteria_' + criteria[i]['criteriaId']] == "true" );
        //     {
        //     truecount = truecount + 1;
        //     }
        //     console.log(truecount);
        // }
        {/*if (!this.state['criteria_' + criteriaId]) {
            // alert(this.state['criteria_' + criteriaId]);
            this.selectedCriteria(criteriaId, "Active");
        }
        if (this.state['criteria_' + criteriaId]) {
            this.selectedCriteria(criteriaId, "Inactive");
        }*/}

        if(status==true){ this.selectedCriteria(criteriaId, "Active");}
        if(status!=false){ this.selectedCriteria(criteriaId, "Inactive");}

        this.calculatefilterPercentage();
        setTimeout(() => {
            //this.calculateSortedRating();
        }, 1000);
    }


    checkParticularFetch = (criteriaId, status) => {

        //var criteria = this.state.criteriaInfo;
        if (status == 'Active') {

            this.setState({ checkAll: false });
        }
    }

    /**
     *
     * funciton to navigate to rate a House screen
     *  
     */

    navigateRateHouse() {
        //alert();
        this.props.navigation.navigate("Rating", { propertyId: 1 })
    }

    /***clear selection */
    clearAll = () => {
        // if (this.state.activeTab == 0) {
        this.setState({
            scrollDisable: false,
            rated: true,
            unrated: true,
            queryChange: '0',
            minPrice: 100000,
            maxPrice: this.state.variablePriceMax,
            favourite:true,
            notfavourite:true,
        })
        this.allCriteria('true')
        // this.clearFilter();
        // }
        //  if (this.state.activeTab == 1) {

        var criteria = this.state.criteriaInfo;
        for (let i = 0; i < criteria.length; i++) {
            this.selectedCriteria(criteria[i]['criteriaId'], true)
        }
        //for (let i = 0; i < criteria.length; i++) {
          //  this.setState({ ['criteria_' + criteria[i]['criteriaId']]: true });
       // }
        this.setState({
            scrollDisable: false,
            checkAll: true,
            checkedAll: true,
            lowRate: false,
            lowPrice: true,
            highRate: true,
            highPrice: false,
            particularCriteria: false,
            noCriteriaFound: false,
            filterActive:false,

        })
        // this.clearFilter();
        // }

    }

    /**save function to do calculation */
    doneFunction = () => {
        var criteria = this.state.criteriaInfo;
        for (let i = 0; i < criteria.length; i++) {
            this.selectedCriteria(criteria[i]['criteriaId'], "Inactive")
        }
        //this.calculateSortedRating();
        this.props.navigation.navigate('Favourite')
    }

    //add comma function
    addCommas(nStr) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    /****slider rate fucntion */
    sliderRating(id, value,ratingId) {
        let propertyId = this.state.propertyDetails[0]['propertyId'];
        this.setState({ onScreenChange: true, ['rating_' + id]: value })
        this.ratingProperty(propertyId, id, value,ratingId)
        this.calculateOverallRating(this.state.propertyDetails[0]['UserPropertyMappingId']);
        //this.setState({ load: true });
        this.refreshFilter();
    }




    /***
     * 
     * favourite
     */

    favourite = (favourite) => {
        let propertyId = this.state.propertyDetails[0]['propertyId'];
        this.favouriteProperty(propertyId, favourite);
    }




    /**
     *
     * 
     * 
     * filtering query genearting function
     * 
     * 
     * 
     * 
     */

    doneFilter = async()=> {
        const { navigation } = this.props;
        var query = this.state.queryChange;
        var conditions = '';
        var rated = this.state.rated;
        var unrated = this.state.unrated;
        var minPrice = this.state.minPrice;
        var maxPrice = this.state.maxPrice;
        var lowRate = this.state.lowRate;
        var highRate = this.state.highRate;
        var lowPrice = this.state.lowPrice;
        var highPrice = this.state.highPrice;
        var checkAll = this.state.checkAll;
        var particularCriteria = this.state.particularCriteria;
        var favourite=this.state.favourite;
        var notfavourite=this.state.notfavourite;

        //alert(rated+' '+unrated);

        //filter rating field
        // if (rated == true && unrated == true) {
        //     conditions = '';
        // }
         if (rated == true && unrated == false) {
 
             conditions = ' AND listingStatus = @RATED@';
         }
         if (rated == false && unrated == true) {
             conditions = ' AND listingStatus != @RATED@';
         }

        //filter price field

        conditions = conditions + ' AND Amount <= ' + maxPrice;

        //sorting string construction
        let RatingField = '';
        if (checkAll == true) {
            RatingField = "OverallRating";
            if (rated == true && unrated == false) {

                conditions = conditions + ' AND OverallRating > 0';
            }
            if (rated == false && unrated == true) {
                conditions = conditions + ' AND OverallRating = 0';
            }
        }
        else {
            RatingField = "SortedRating";
            if (rated == true && unrated == false) {

                conditions = conditions + ' AND SortedRating > 0';
            }
            if (rated == false && unrated == true) {
                conditions = conditions + ' AND SortedRating = 0';
            }
        }

        //check if only price sort is opted out during the first visit
       let priceSort = false;
        let rateSort = true;
        if (lowRate == false && highRate == false) {
            if (lowPrice == true || highPrice == true) {
                priceSort = true;
            }
            rateSort = false;
        }

        if (rateSort == true) {
            if (lowRate == true) {
                conditions = conditions + ' ORDER BY ' + RatingField + ' ASC';
            }
            if (highRate == true) {
                conditions = conditions + ' ORDER BY ' + RatingField + ' DESC';
            }
            if (lowPrice == true) {
                conditions = conditions + ', Amount ASC';
            }
            if (highPrice == true) {
                conditions = conditions + ', Amount DESC';
            }
        }
        if (priceSort == true) {
            if (lowPrice == true) {
                conditions = conditions + ' ORDER BY Amount ASC';
            }
            if (highPrice == true) {
                conditions = conditions + ' ORDER BY Amount DESC';
            }
        }



       
        if (rated == true) {
            rated = 1;
        }
        else {
            rated = 0
        }
        if (unrated == true) {
            unrated = 1;
        }
        else {
            unrated = 0;
        }

        if (favourite == true) {
            favourite = 1;
        }
        else {
            favourite = 0
        }
        if (notfavourite == true) {
            notfavourite = 1;
        }
        else {
            notfavourite = 0;
        }

        if (lowRate == true) {
            lowRate = 1;
        }
        else {
            lowRate = 0;
        }
        if (highRate == true) {
            highRate = 1;
        }
        else {
            highRate = 0;
        }
        if (lowPrice == true) {
            lowPrice = 1;
        }
        else {
            lowPrice = 0;
        }
        if (highPrice == true) {
            highPrice = 1;
        }
        else {
            highPrice = 0;
        }
        if (checkAll == true) {
            checkAll = 1;
        }
        else {
            checkAll = 0;
        }


        
        if (checkAll == 1) {
            particularCriteria = false;
        }

        if (particularCriteria == true) {
            particularCriteria = 1;
            checkAll = 0;
        }
        else {
            particularCriteria = 0;

        }
        

       
        
        // alert(query);
        this.filterDone(query, rated, unrated, minPrice, maxPrice, lowRate, highRate, lowPrice, highPrice, checkAll, particularCriteria,favourite,notfavourite);
       let userId=await AsyncStorage.getItem('userId');
        this.calculatefilterPercentage(userId);
        //setTimeout(() => {
            this.calculateSortedRating(userId);
        //}, 1000);
        if (!this.state.noCriteriaFound)
            navigation.goBack();
        if (this.state.noCriteriaFound)
            Alert.alert('', alertMSG.en['SortMessage']);
        // navigation.state.params.onSelect({ selected: true, rated1: rated, unrated1: unrated, minPrice1: minPrice, maxPrice1: maxPrice, lowRate1: lowRate, highRate1: highRate, lowPrice1: lowPrice, highPrice1: highPrice });

    }

    ratedCheck() {

        if (this.state.rated == true && this.state.unrated == false) {
            Alert.alert('', alertMSG.en['FilterRatingValidation']);
            this.setState({ rated: true, unrated: true });
        }
        this.setState({ rated: this.state.rated ? false : true, queryChange: "1" });

    }
    unRatedCheck() {

        if (this.state.rated == false && this.state.unrated == true) {
            Alert.alert('', alertMSG.en['FilterRatingValidation']);
            this.setState({ rated: true, unrated: true });
        }
        this.setState({ unrated: this.state.unrated ? false : true, queryChange: "1" })
    }

    favouriteCheck() {

        if (this.state.favourite == true && this.state.notfavourite == false) {
            Alert.alert('', alertMSG.en['FilterRatingValidation']);
            this.setState({ favourite: true, notfavourite: true });
        }
        this.setState({ favourite: this.state.favourite ? false : true, queryChange: "1" });

    }
    notfavouriteCheck() {

        if (this.state.favourite == false && this.state.notfavourite == true) {
            Alert.alert('', alertMSG.en['FilterRatingValidation']);
            this.setState({ favourite: true, notfavourite: true });
        }
        this.setState({ notfavourite: this.state.notfavourite ? false : true, queryChange: "1" })
    }

    confirmListingDelete(item) {
        AlertIOS.alert(
            '',
            alertMSG.en['ListingitemDelete'],

            [
                {
                    text: 'No',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                {
                    text: 'Yes',
                    onPress: () => this.deleteListing(item),
                },
            ]
        );
    }

    deleteListing(item) {
        this.listingStatusChange(item.propertyId);
        this.getFavourites("existing");
        const newState = this.state.propertyInfo.slice();
        if (newState.indexOf(item) > -1) {
            newState.splice(newState.indexOf(item), 1);
            this.setState({ propertyInfo: newState });
        }
    }

    sliderChange(curValue) {
        this.setState({
            minPrice: curValue.min > curValue.max ? 0 : curValue.min,
            maxPrice: curValue.max,
        });

    }





}
