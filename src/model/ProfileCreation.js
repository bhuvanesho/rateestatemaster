


import { AsyncStorage } from 'react-native';
var SQLite = require('react-native-sqlite-storage')
var db = SQLite.openDatabase({ name: "HomeRatingDB.db" });
import ValidationComponent from 'react-native-form-validator';
import { database } from '../firebaseConfig/config';
import {
  data, genderValuesC, maritalStautsC, BudgetedPriceC, QuestionYNC, TypeOfHomeDesiredC,
  RangeOfHouseholdIncomeC
} from '../firebaseConfig/commonConstant';
import { Alert } from 'react-native';
import * as firebase from "firebase";
var criteriaJSON = require('../assets/criteria.json');


export default class ProfileCreationModel extends ValidationComponent {

  constructor(props) {
    super(props);
    const userId = '';
    this.state = {
    }
  }

  getBudgetedPriceRange() {
    try {
      db.transaction((tx) => {
        tx.executeSql('select * from CodeValue where CodeTypeId = 7', [], (tx, results) => {
          //  var len = results.rows.item(11)['Name'];
          var len = results.rows.length;
          this.state.budgetedPrice.push({ value: "Select", Id: 0 });
          for (let i = 0; i < len; i++) {
            let id = results.rows.item(i)['Id'];
            let name = results.rows.item(i)['Name'];
            this.state.budgetedPrice.push({ value: name, Id: id });
          }
        });
      });
    } catch (error) {
      alert(error);
    }

  }


  updateEmail = async (route) => {
    this.updateNinsertEmail();
    try {
      let userId = await AsyncStorage.getItem('userId');
      if (this.state.validEmail && route == 'backWard') {
        this.updateProfile();
        this.setState({ count: this.state.count - 1 });
      }
      if (this.state.validEmail && route == 'forward') {
        this.updateProfile();
        this.setState({ count: this.state.count + 1 });
      }
      if (this.state.validEmail && route != 'backWard' && route != 'forward') {
        database.ref('User' + '/' + userId).update(
          {
            "loggedInflag": 2
          }
        )
        if (route == 'Criteria') { this.props.navigation.navigate('CriteriaSetting') }
        if (route == 'Listing') { this.props.navigation.navigate('Listing') }

      }
    } catch (error) {
      alert(error);
    }

  }

  getSharedEmail() {
    try {
      db.transaction((tx) => {
        tx.executeSql('select * from SharedProfile where ProfileId=1', [], (tx, results) => {
          var len = results.rows.length;
          for (let i = 0; i < len; i++) {
            let id = results.rows.item(i)['Id'];
            let emailId = results.rows.item(i)['emailId'];
            this.state.getEmail.push({ value: emailId, Id: id });
          }
        });
      });
    } catch (error) {
      alert(error);
    }
  }



  getAccountSharedEmail = async () => {
    try {

      let counter = 0;
      let sharedEmail = [];
      let userId = await AsyncStorage.getItem('userId');
      let ref = database.ref('User/')
      ref.orderByChild('accountId')
        .startAt(userId)
        .endAt(userId)
        .once('value', function (snapshot) {
          snapshot.forEach(function (childSnap) {
            if (childSnap.val().accountId !== childSnap.val().userId) {
              counter = counter + 1;
              let id = counter;
              let emailId = childSnap.val().EmailId;
              let bgColor;
              if (counter % 2 == 0) {
                bgColor = "#ededed";
              }
              else {
                bgColor = "#d4edfc";
              }
              sharedEmail.push({ value: emailId, Id: id, bgColor: bgColor, editable: true, status: 'Old' });
            }
          }.bind(this));
          //If rows are added newly
          if (counter % 2 == 0) {
            bgColor = "#d4edfc";
          }
          else {
            bgColor = "#ededed";
          }
          if (counter == 0) {
            sharedEmail.push({ value: '', Id: 0, bgColor: bgColor, editable: true, status: 'New' });
          }
          this.setState({ accountSharedEmail: sharedEmail, validEmail: true, loadingSpinner: false });
          sharedEmail = [];
        }.bind(this));

    } catch (error) {
      alert(error);
    }
  }


  updateNinsertEmail = async () => {
    try {
      let userId = await AsyncStorage.getItem('userId');
      //db.transaction((tx) => {
      var accountSharedEmail = this.state.accountSharedEmail;
      var sharedEmail = [];
      for (var i = 0; i < accountSharedEmail.length; i++) {
        let oldid = accountSharedEmail[i]['Id'];
        let status = accountSharedEmail[i]['status'];
        var len = accountSharedEmail.length;
        let emailId = this.state['email_' + oldid];
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (emailId == '' && oldid == 0) {
          break;
        }

        if (reg.test(emailId) == false && typeof emailId !== 'undefined') {
          Alert.alert('', 'Please enter the valid Email Id under Account sharing');
          this.setState({ validEmail: false });
          this.getAccountSharedEmail();
          break;
        }


        if (reg.test(emailId) == true) {

          let bgColor;
          if (oldid == 0) {
            oldid = len + oldid;
          }

          if (oldid % 2 == 0) {
            bgColor = "#ededed";
          }
          else {
            bgColor = "#d4edfc";
          }
          sharedEmail.push({ value: emailId, Id: oldid, bgColor: bgColor, editable: true, status: status });


          //hardcoded password for influencer
          let password = "admin@123";
          // insert the new email id
          if (status == "New" && emailId != '') {
            var success = this.register(emailId, password);
            if (success == true) {
              //also identify for the primary user which properties are assigned same needs to be assigned for influencer
              Alert.alert('', 'New login was created for the shared email Id with default password as "admin@123"');
            }
            if (success == false) {
              sharedEmail.splice(i, 1);
            }
          }
          //update the old email Id
          //right now commenting as once the email is added user cannot alter it. he needs to delete it
          //and add the new one
          {/*if (status == "Old" && oldid != 0 && emailId != '') {
            database.ref('User' + '/' + userId).update(
              {
                "EmailId": emailId,
                "username":emailId,
              }
            )
          }*/}
        }
      }
      if (sharedEmail.length > 0) {
        this.setState({ accountSharedEmail: sharedEmail });
      }
      this.setState({ onEmailChange: false, validEmail: true });
      //set state to refresh the page
      //this.setState({ accountSharedEmail: accountSharedEmail });
      //});
    }
    catch (error) {
      alert(error);
    }
  }



  /**
   * function to get month for birth month dropdown
  */


  getMonth() {
    try {
      db.transaction((tx) => {
        // tx.executeSql('select * from CodeValue where CodeTypeId = 6', [], (tx, results) => {
        tx.executeSql('select * from Profile where Id = 1', [], (tx, results) => {
          //  var len = results.rows.item(11)['Name'];
          var len = results.rows.length;
          let id = results.rows.item(0)['LastName'];

        });
      });
    }
    catch (error) {
      alert(error);
    }
  }

  /**
   * function to get gender for gender dropdown
  */
  getGender() {

    try {
      db.transaction((tx) => {
        tx.executeSql('select * from CodeValue where CodeTypeId = 1', [], (tx, results) => {
          //  var len = results.rows.item(11)['Name'];
          var len = results.rows.length;
          this.state.gender.push({ value: "Select", Id: 0 });
          for (let i = 0; i < len; i++) {
            let id = results.rows.item(i)['Id'];
            let name = results.rows.item(i)['Name'];
            this.state.gender.push({ value: name, Id: id });

          }
        });
      });
    } catch (error) {
      alert(error);
    }
  }

  /**
   * function to get marital status  of  from CodeValue for marital status dropdown
  */
  getMaritalStatus() {
    // alert("hi");
    try {
      db.transaction((tx) => {
        tx.executeSql('select * from CodeValue where CodeTypeId = 2', [], (tx, results) => {
          //  var len = results.rows.item(11)['Name'];
          var len = results.rows.length;
          this.state.maritalStatus.push({ value: "Select", Id: 0 });
          for (let i = 0; i < len; i++) {
            let id = results.rows.item(i)['Id'];
            let name = results.rows.item(i)['Name'];
            this.state.maritalStatus.push({ value: name, Id: id });
          }
        });
      });
    } catch (error) {
      alert(error);
    }
  }

  /**
   * function to get range Of House Hold Income   from CodeValue 
  */
  getHouseHoldIncome() {
    try {
      db.transaction((tx) => {
        tx.executeSql('select * from CodeValue where CodeTypeId = 5', [], (tx, results) => {
          //  var len = results.rows.item(11)['Name'];
          var len = results.rows.length;
          this.state.rangeOfHouseHoldIncome.push({ value: "Select", Id: 0 });
          for (let i = 0; i < len; i++) {
            let id = results.rows.item(i)['Id'];
            let name = results.rows.item(i)['Name'];
            this.state.rangeOfHouseHoldIncome.push({ value: name, Id: id });
            // alert(name);
          }
        });
      });
    }
    catch (error) {
      alert(error);
    }
  }


  /**
   * function to get type Of Home Desired   from CodeValue 
  */
  getDesiredHome() {
    try {
      db.transaction((tx) => {
        tx.executeSql('select * from CodeValue where CodeTypeId = 3', [], (tx, results) => {
          //  var len = results.rows.item(11)['Name'];
          var len = results.rows.length;
          //alert(len);
          // this.state.typeOfHomeDesired.push({ name: "Select", Id: 0 });
          for (let i = 0; i < len; i++) {
            let id = results.rows.item(i)['Id'];
            let value = results.rows.item(i)['Name'];
            this.state.typeOfHomeDesired.push({ name: value, Id: id });
          }
        });
      });
    }
    catch (error) {
      alert(error);
    }
  }


  /**
   * function to get yesOrNO  from CodeValue 
  */
  getYesOrNo() {
    try {
      db.transaction((tx) => {
        tx.executeSql('select * from CodeValue where CodeTypeId = 4', [], (tx, results) => {
          //  var len = results.rows.item(11)['Name'];
          var len = results.rows.length;
          this.state.yesOrno.push({ value: "Select", Id: 0 });
          for (let i = 0; i < len; i++) {
            let id = results.rows.item(i)['Id'];
            let name = results.rows.item(i)['Name'];
            this.state.yesOrno.push({ value: name, Id: id });
          }
        });
      });
    }
    catch (error) {
      alert(error);
    }
  }

  getAgents = () => {
    var ref = database.ref('Realtor');
    ref.once('value').then(snapshot => {
      // snapshot.val() is the dictionary with all your keys/values from the '/store' path
      //this.setState({ agents: snapshot.val() })
      var test = this.snapshotToArray(snapshot);
      this.setState({ realtors: test });
    })
  }

  snapshotToArray(snapshot) {
    var returnArr = [];
    snapshot.forEach(function (childSnapshot) {
      var item = childSnapshot.val();
      item.key = childSnapshot.key;
      returnArr.push(item);
    });
    return returnArr;
  };

  /**
   * function to create profile for user
  */

  updateProfile = async () => {
    try {
      let userId = await AsyncStorage.getItem('userId');
      let firstName = this.state.firstName;
      let lastName = this.state.lastName;
      let gender = this.state.selectedGender;
      let maritalStatus = this.state.selectedMaritalStatus;
      // let typeOfHomeDesired = this.state.selectedTypeOfHomeDesired;
      let typeOfHomeDesired = this.state.selectedItems.join(',');

      let rangeOfHouseHoldIncome = this.state.selectedIncomeHouseHoldRange;
      let ownVehicle = this.state.ownVehicle;
      let numberOfPeopleInHome = this.state.selectedNumberOfPeople;
      let ageRangeOfPeopleFrom = this.state.selectedAgeFrom;
      let ageRangeOfPeopleTo = this.state.selectedAgeTo;
      let selectedBudgetedPrice = this.state.selectedBudgetedPrice;
      let dob = this.state.chosenDate;
      let sharingAccount = this.state.sharingAccount;
      let sharingUserFirstName = this.state.sharingUserFirstName;
      let sharingUserLastName = this.state.sharingUserLastName;
      let agent = this.state.saveAgentId == undefined ? '' : this.state.saveAgentId;
      database.ref('profile' + '/' + userId).update(
        {
          "firstName": firstName,
          "lastName": lastName,
          "gender": gender,
          "maritalStatus": maritalStatus,
          "typeOfHomeDesired": typeOfHomeDesired,
          "rangeOfHouseHoldIncome": rangeOfHouseHoldIncome,
          "ownVehicle": ownVehicle,
          "numberOfPeopleInHome": numberOfPeopleInHome,
          "ageRangeOfPeopleFrom": ageRangeOfPeopleFrom,
          "ageRangeOfPeopleTo": ageRangeOfPeopleTo,
          "BudgetedPrice": selectedBudgetedPrice,
          "dob": dob,
          "sharingAccount": sharingAccount,
          "agent": agent,
        }
      )
    }
    catch (error) {
      alert(error);
    }
  }





  getPersonalProfileDetails = async () => {

    try {

      let data = [];
      let fName = '';
      let lName = '';
      let genderId = '';
      let martialId = '';
      let TypeOfHomeDesired = '';
      let RangeOfHouseholdIncome = '';
      let DoYouOwnVehicle = '';
      let NumberOfPeopleinHome = '';
      let AgeRangeOfPeopleFrom = '';
      let AgeRangeOfPeopleTo = '';
      let BudgetedPrice = '';
      let dob = '';
      let agent = '';

      let nameArr = [];
      let userId = await AsyncStorage.getItem('userId');

      // Find  profile matching the user id.
      var ref = database.ref('profile/')
      ref.orderByChild('userId')
        .startAt(userId)
        .endAt(userId)
        .on('value', function (snapshot) {
          snapshot.forEach(function (childSnap) {

            fName = childSnap.val().firstName;
            lName = childSnap.val().lastName;
            genderId = childSnap.val().gender;
            martialId = childSnap.val().maritalStatus;
            TypeOfHomeDesired = childSnap.val().typeOfHomeDesired;
            RangeOfHouseholdIncome = childSnap.val().rangeOfHouseHoldIncome;
            DoYouOwnVehicle = childSnap.val().ownVehicle;
            NumberOfPeopleinHome = childSnap.val().numberOfPeopleInHome;
            AgeRangeOfPeopleFrom = childSnap.val().ageRangeOfPeopleFrom;
            AgeRangeOfPeopleTo = childSnap.val().ageRangeOfPeopleTo;
            BudgetedPrice = childSnap.val().BudgetedPrice;
            dob = childSnap.val().dob;
            agent = childSnap.val().agent;

            var len = data.length + 1;
            if (AgeRangeOfPeopleFrom == 0) {
              AgeRangeOfPeopleFrom = '';
            }
            if (AgeRangeOfPeopleTo == 0) {
              AgeRangeOfPeopleTo = '';
            }
            let arr = [genderId, martialId, RangeOfHouseholdIncome, DoYouOwnVehicle, BudgetedPrice];
            let arrC = [genderValuesC, maritalStautsC, RangeOfHouseholdIncomeC, QuestionYNC, BudgetedPriceC];

            for (var i = 0; i < arr.length; i++) {
              innerresults = null;
              results = arrC[i];
              var len = results.length;
              for (let j = 0; j < len; j++) {
                if (results[j]['Id'] === arr[i]) {
                  innerresults = results[j];
                  break;
                }
              }

              if (innerresults === null) {
                nameArr.push({ codevalueName: '', codeValueId: 0 });
              }
              else {
                len1 = innerresults.length;
                var name = innerresults.value;
                var Id = innerresults.Id;
                nameArr.push({ codevalueName: name, codeValueId: Id });
              }
            }
          });


          //bind to state
          if (nameArr[0].codevalueName != '') {
            this.setState({ selectedGenderName: nameArr[0].codevalueName });
            this.setState({ selectedGender: nameArr[0].codeValueId });
          }
          if (nameArr[1].codevalueName != '') {
            this.setState({ selectedMaritalName: nameArr[1].codevalueName });
            this.setState({ selectedMaritalStatus: nameArr[1].codeValueId });
          }

          if (TypeOfHomeDesired != '') {
            this.setState({ selectedItems: TypeOfHomeDesired.split(',') })
          }

          if (nameArr[2].codevalueName != '') {
            this.setState({ incomeHouseHoldRangeLabel: nameArr[2].codevalueName });
            this.setState({ selectedIncomeHouseHoldRange: nameArr[2].codeValueId });
          }

          if (nameArr[3].codevalueName != '') {
            this.setState({ ownVehicleCount: nameArr[3].codevalueName });
            this.setState({ ownVehicle: nameArr[3].codeValueId });
          }

          if (NumberOfPeopleinHome != '') {
            this.setState({ selectedNumberOfPeopleLabel: NumberOfPeopleinHome.toString() });
            this.setState({ selectedNumberOfPeople: NumberOfPeopleinHome });
          }

          if (AgeRangeOfPeopleFrom != '') {
            this.setState({ selectedAgeFromLabel: AgeRangeOfPeopleFrom.toString() });
            this.setState({ selectedAgeFrom: AgeRangeOfPeopleFrom });
          }

          if (AgeRangeOfPeopleTo != '') {
            this.setState({ selectedAgeToLabel: AgeRangeOfPeopleTo.toString() });
            this.setState({ selectedAgeTo: AgeRangeOfPeopleTo });
          }

          if (nameArr[4].codevalueName != '') {
            this.setState({ budgetedPriceChange: nameArr[4].codevalueName });
            this.setState({ selectedBudgetedPrice: nameArr[4].codeValueId });
          }

          if (dob != '') {
            this.setState({ chosenDate: dob, lastdate: dob });
            if (dob != '') {
              { this.setState({ dobColor: '#727171' }) };
            }
            if (AgeRangeOfPeopleFrom != '' && AgeRangeOfPeopleTo != '') {

              this.setState({ ageRangeColor: '#727171' })
            }
          }

          if (fName != '') {
            this.setState({ firstName: fName });
          }

          if (lName != '') {
            this.setState({ lastName: lName });
          }
          if (agent != '') {
            //get corresponding agent name
            var ref = database.ref('Realtor/')
            ref.orderByChild('agentId')
              .startAt(agent)
              .endAt(agent)
              .once('value', function (snapshot) {
                snapshot.forEach(function (childSnap) {
                  agentName = childSnap.val().lastName + ', ' + childSnap.val().firstName
                  this.setState({ agentName: agentName });
                  this.setState({ saveAgentId: childSnap.val().agentId });
                }.bind(this));
              }.bind(this));
          }

          nameArr = [];


        }.bind(this));

    }
    catch (error) {
      alert(error);
    }

  }


  showEmailSharingForwardArrow = async () => {
    try {
      var loggedIn = '';
      let userId = await AsyncStorage.getItem('userId');
      // Find  profile matching the user id.
      var ref = database.ref('User/')
      ref.orderByChild('userId')
        .startAt(userId)
        .endAt(userId)
        .on('value', function (snapshot) {
          snapshot.forEach(function (childSnap) {
            loggedIn = childSnap.val().oggedInflag;
          }.bind(this));
        });

      if (loggedIn == 2) {
        this.setState({ showEmailForwardArrow: false });
        this.setState({ showDoneButton: true })
      }
      else {
        this.setState({ showDoneButton: false })
      }
    }
    catch (error) {
      alert(error);
    }
  }


  register = async (username, password) => {
    let userId = await AsyncStorage.getItem('userId');
    firebase.auth().createUserWithEmailAndPassword(username, password)
      .then(function (user) {
        // user signed in
        //console.log("createdid", user.user.uid);
        //console.log("user creation starts")
        database.ref('User/' + user.user.uid).set({
          "accountId": userId,
          "userId": user.user.uid,
          "username": username.toLowerCase().trim(),
          "EmailId": username.toLowerCase().trim(),
          "phoneNumber": "",
          "createdDate": Date.now(),
          "updatedDate": Date.now(),
          "LoggedIn": "i",
          "loginCount": "0",
          "loggedInflag": "1",
          "status": "A"
        })
        //console.log("User creation ends")
      var agentId='';
        //get the agentId of the primary User and add same to Influencer
        var ref = database.ref('profile/')
        ref.orderByChild('userId')
          .startAt(userId)
          .endAt(userId)
          .once('value', function (snapshot2) {
            snapshot2.forEach(function (childSnap2) {
              agentId=childSnap2.val().agent;
            })
        //Profile page
        //console.log("Profile creation starts")
        database.ref('profile/' + user.user.uid).set({
          "id": user.user.uid,
          "userId": user.user.uid,
          "firstName": '',
          "lastName": '',
          "gender": '',
          "maritalStatus": '',
          "typeOfHomeDesired": '',
          "rangeOfHouseHoldIncome": '',
          "ownVehicle": '',
          "numberOfPeopleInHome": '',
          "ageRangeOfPeopleFrom": '',
          "ageRangeOfPeopleTo": '',
          "BudgetedPrice": '',
          "dob": '',
          "sharingAccount": '',
          "sharingUserFirstName": '',
          "sharingUserLastName": '',
          "agent": agentId,
        })
    })

        // console.log("Profile creation ends")

        //FilterAndSort table creation start
        // console.log("Filter and Sort table creation starts")
        database.ref('FilterAndSort/' + user.user.uid).set({
          "Id": user.user.uid,
          "UserId": user.user.uid,
          "CheckAll": '1',
          "HighToLowPrice": '0',
          "HighToLowRating": '1',
          "LowToHighPrice": '1',
          "LowToHighRating": '0',
          "NotRated": '1',
          "ParticularCriteria": '0',
          "PriceFrom": '100000',
          "PriceTo": '6000000',
          "Rated": '1',
          //"filterNsortQuery": alertMSG.en['FilterString'],
          "filterNsortQuery": "0",
          "username": ''
        })
        //console.log("filter and sort creation ends")

        // profile ends
        for (let i = 0; i < criteriaJSON.length; i++) {

          var newPostKey = database.ref().child('criteria').push().key;
          database.ref('criteria/' + newPostKey).set({
            "Id": newPostKey,
            "CriteriaName": criteriaJSON[i]['CriteriaName'],
            "CriteriaType": criteriaJSON[i]['CriteriaType'],
            "Weightage": "",
            "Percentage": "",
            "filterPercentage": "",
            "UserId": user.user.uid,
            "createdDate": "",
            "updatedDate": "",
            "SortedStatus": "",
            "Status": "Inactive",

          })

        }

        //identify the properties already added to the primary account and add the same to 
        //influencer properties bucket
        //first get all the properties belong to the user
        var ref = database.ref('UserPropertyMapping/')
        ref.orderByChild('UserId')
          .startAt(userId)
          .endAt(userId)
          .once('value', function (snapshot1) {
            snapshot1.forEach(function (childSnap1) {
              var dt = new Date();
              var newPostKey = database.ref().child('UserPropertyMapping').push().key;
              database.ref('UserPropertyMapping/' + newPostKey).set({
                "Id": newPostKey,
                "UserId": user.user.uid,
                "ListingID": childSnap1.val().ListingID,
                "PropertyName": childSnap1.val().PropertyName,
                "Address": childSnap1.val().Address,
                "Description": childSnap1.val().Description,
                "latitude": childSnap1.val().latitude,
                "longitude": childSnap1.val().longitude,
                "Amount": childSnap1.val().Amount,
                "Status": childSnap1.val().Status,
                "Beds": childSnap1.val().Beds,
                "TotalBath": childSnap1.val().TotalBath,
                "Area": childSnap1.val().Area,
                "OverallRating": 0.0,
                "SortedRating": 0.0,
                "Category": childSnap1.val().Category,
                "createdDate": dt.getTime(),
                "BuildYear": childSnap1.val().BuildYear,
                "NoOfStorey": childSnap1.val().NoOfStorey,
                "RatingStatus": 'Unrated',
                "FavouriteProperty": '',
                "ListingStatus": 'NEW LISTING',
                "Comments": '',
                "MatrixId": childSnap1.val().MatrixId,
                "NoOfPhotos": childSnap1.val().NoOfPhotos,
              })
            }.bind(this));
          }.bind(this));
        return true;


      })

      .catch(function (error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        if (errorCode == 'auth/weak-password') {
          alert('The password is too weak.');
        } else if (errorCode === 'auth/email-already-in-use') {
          //alert('Email Id '+username+' is already in use by another account.');
          this.changeUserAccount(username.toLowerCase().trim());
        } else {
          alert(errorMessage);
        }
        console.log(error);
        return false;
      }.bind(this));
  }

  //if the email Id already registered with the system and if the user is being added as influencer then
  // change the existing account userId
  // One thing to be watched here is if the primary user is adding a influencer who is already a primary user of himself then dont 
  // allow. A-->B-->A not allowed
  changeUserAccount = async (username) => {
    let userId = await AsyncStorage.getItem('userId');
    let accountId = '';
    //check the account Id of existing user
    var ref = database.ref('User/')

    ref.orderByChild('userId')
      .startAt(userId)
      .endAt(userId)
      .once('value', function (snapshot1) {
        snapshot1.forEach(function (childSnap1) {
          // check if the influencer is getting added is his primary user or not
          accountId = childSnap1.val().accountId;
          ref.orderByChild('username')
            .startAt(username)
            .endAt(username)
            .once('value', function (snapshot) {
              snapshot.forEach(function (childSnap) {
                // check if the influencer is getting added is his primary user or not
                if (childSnap.val().userId == accountId) {
                  alert("You are trying to add an account ("+username+") who is your Primary account! Please delete or update the account.")
                }
                else {
                  //change the existing influencer userid=accountid in profile
                  database.ref('User' + '/' + childSnap.val().userId).update(
                    {
                      "accountId": userId,
                    }
                  )
                  this.deletePropertyOfUser(userId, childSnap.val().userId)
                }
              }.bind(this));
            }.bind(this));
        }.bind(this));
      }.bind(this));
  }

  //converting influencer back to primary account when his email id is deleted 
  changeInfluencerAccount = async (username) => {
   
    var ref = database.ref('User/')
    ref.orderByChild('username')
      .startAt(username)
      .endAt(username)
      .once('value', function (snapshot) {
        snapshot.forEach(function (childSnap) {
        
          //change the existing influencer accountId=userId itself in profile
          database.ref('User' + '/' + childSnap.val().userId).update(
            {
              "accountId": childSnap.val().userId,
            }
          )
          this.deletePropertyOfUser(0,childSnap.val().userId)
         
        }.bind(this));
      }.bind(this));
  }

   //when influencer is deleted in profile all his associated properties should be deleted
   deletePropertyOfUser=(primaryUserId, influencerUserId)=>{
    var ref = database.ref('UserPropertyMapping/')
      ref.orderByChild('UserId')
        .startAt(influencerUserId)
        .endAt(influencerUserId)
        .once('value', function (snapshot1) {
          snapshot1.forEach(function (childSnap1) {
              ref.child(childSnap1.val().Id).remove();
          }.bind(this));
          //after delete insert the properties belong to its primary user
          if(primaryUserId!=0){
            this.addPropertyToNewInfluencer(primaryUserId,influencerUserId);
            this.addAgentIdToNewInfluencer(primaryUserId,influencerUserId);
          }
        }.bind(this));
  }

  //for a new infuencer added, its primary user properties needs to be inserted
  addPropertyToNewInfluencer = (primaryUserId, influencerUserId) => {
    //first get all the properties belong to the user
    var ref = database.ref('UserPropertyMapping/')
    ref.orderByChild('UserId')
      .startAt(primaryUserId)
      .endAt(primaryUserId)
      .once('value', function (snapshot1) {
        snapshot1.forEach(function (childSnap1) {
          var dt = new Date();
          var newPostKey = database.ref().child('UserPropertyMapping').push().key;
          database.ref('UserPropertyMapping/' + newPostKey).set({
            "Id": newPostKey,
            "UserId": influencerUserId,
            "ListingID": childSnap1.val().ListingID,
            "PropertyName": childSnap1.val().PropertyName,
            "Address": childSnap1.val().Address,
            "Description": childSnap1.val().Description,
            "latitude": childSnap1.val().latitude,
            "longitude": childSnap1.val().longitude,
            "Amount": childSnap1.val().Amount,
            "Status": childSnap1.val().Status,
            "Beds": childSnap1.val().Beds,
            "TotalBath": childSnap1.val().TotalBath,
            "Area": childSnap1.val().Area,
            "OverallRating": 0.0,
            "SortedRating": 0.0,
            "Category": childSnap1.val().Category,
            "createdDate": dt.getTime(),
            "BuildYear": childSnap1.val().BuildYear,
            "NoOfStorey": childSnap1.val().NoOfStorey,
            "RatingStatus": 'Unrated',
            "FavouriteProperty": '',
            "ListingStatus": 'NEW LISTING',
            "Comments": '',
            "MatrixId": childSnap1.val().MatrixId,
            "NoOfPhotos": childSnap1.val().NoOfPhotos,
          })
        }.bind(this));
      }.bind(this));

  }

  addAgentIdToNewInfluencer=(primaryUserId,influencerUserId)=>
  {
    var agentId='';
    //get the agentId of the primary User and add same to Influencer
    var ref = database.ref('profile/')
    ref.orderByChild('userId')
      .startAt(primaryUserId)
      .endAt(primaryUserId)
      .once('value', function (snapshot2) {
        snapshot2.forEach(function (childSnap2) {
          agentId=childSnap2.val().agent;
        })
   //update the primary user agent Id 
    database.ref('profile/' + influencerUserId).update({
      "agent": agentId,
    })
})

  }

}
