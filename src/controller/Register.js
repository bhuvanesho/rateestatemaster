
import React, { Component } from 'react';
import {Alert} from 'react-native';
import PropTypes from 'prop-types';
import RegisterModel from '../model/Register';
import alertMSG from '../assets/resources';

export default class Register extends RegisterModel {

constructor(props) {
 super(props);
 this.state = {username : "Suresh", password:"123456" ,emailId: "suresssh.s@gmail.com"};
 this.signUpValidate = this.signUpValidate.bind(this); 
}
  
  

signUpValidate()
  {
    this.validate({
      username: {minlength:3, maxlength:10, required: true},
      password: {numbers: true, required: true},
      emailId: {email: true, required: true},
    });
    
    if(this.isFormValid()){
      this.username = this.state.username;
      this.password = this.state.password;
      this.emailId = this.state.emailId;
      this.signUp();
   }
   if(!this.isFormValid()){
     Alert.alert(alertMSG.en['Invalid Input'],this.getErrorMessages());
   }
  }

}

